var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var concat = require('gulp-concat');
var php = require('gulp-connect-php');
var backend = require('./backend.js');

var reload = browserSync.reload;

// Compile SCSS files from /less into /css
gulp.task('sass', function () {
    return gulp.src('app/scss/style.scss')
            .pipe(sass())
            .pipe(gulp.dest('app/css'))
            .pipe(browserSync.stream())
});


gulp.task('minify-css', function () {
    return gulp.src(['app/css/*.css', '!app/css/*.min.css'])
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('app/css'))
            .pipe(browserSync.stream())
});

// Minify JS
gulp.task('minify-js', function () {
    return gulp.src(['app/js/*.js'])
            .pipe(uglify())
//        .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest('app/jsmin'))
            .pipe(browserSync.stream())
});

gulp.task('php', function () {
    php.server({base: './app', port: 8010, keepalive: true});
});

// Run everything
gulp.task('default', ['sass', 'minify-css', 'minify-js']);

// Configure the browserSync task
gulp.task('browserSync', function(){
    browserSync.init({
        proxy: {
            target: "http://dogsafe.dev",
            proxyRes: [
                function (proxyRes, req, res) {
                    //console.log(proxyRes.headers);
                }
            ]
        },
        watchOptions: {
            ignoreInitial: true,
            ignored: '*.txt'
        }
    });
});

//backend
gulp.task('backend', function () {
    return gulp.src(['vendor/Codando/*/*.php','app/js/*.js','app/_config/modulo.yml', 'app/_views/*.phtml', 'app/*.php'])
               .pipe(backend());
});

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'sass'], function(){

    gulp.watch('app/scss/*.scss', ['sass']);
    //gulp.watch('app/css/*.css', ['minify-css']);
    //gulp.watch('app/js/*.js', ['minify-js']);

    gulp.watch("vendor/Codando/Controller/*.php").on('change', reload);
    gulp.watch("vendor/Codando/Route/*.php").on('change', reload);
    gulp.watch("app/routes.php").on('change', reload);
    gulp.watch("app/_views/*.phtml").on('change', reload);
    gulp.watch('app/js/**/*.js').on('change', reload);
    gulp.watch('app/scss/*.scss').on('change', reload);
});
