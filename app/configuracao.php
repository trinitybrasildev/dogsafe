<?php

$root = str_replace('\\', '/', dirname(dirname(__FILE__)));

/*
 * CONFIGURACAO
 *  * 'smtp' => 'server1.trinitybrasil.com',
   'port' => '587',
   'email' => 'naoresponda@dogsafe.com.br',
   'senha' => 'h-c4Bn9&F-);',
 *         'email' => 'naorespondaosemails@gmail.com',
        'senha' => 'w3THE_h+qaW8',
 */

return array(
    'php' => array(//Configuracao Padrão PHP
        'display_errors' => 1,
        'error_reporting' => (E_ALL | E_STRICT),
        'default_charset' => 'UTF-8',
        'date_default_timezone' => 'America/Campo_Grande'
    ),
    'dev' => array(
        'nome' => 'Andrey',
        'email' => 'andrey@neexbrasil.com'
    ),
    'header' => array(//Configuracao de Cabeçario Padrão
        'X-XSS-Protection' => '1; mode=block',
        'X-Frame-Options' => 'sameorigin',
        'X-Content-Type-Options' => 'nosniff',
        'X-Powered-By' => 'www.trinitybrasil.com.br',
        'Author' => 'www.trinitybrasil.com.br',
        'Server' => '',
        'Connection' => 'Keep-Alive'
    ),
    'dominio' => array(//Configuracao Geral do Site
        'root' => 'https://dogsafe.com.br',
        'cdn' => '/',
        'notfound_image' => 'semimagem.jpg',
        'language' => 'pt-br',
        'title' => 'DOG SAFE',
        'description' => 'A DogSafe conecta pessoas que amam animais. Através da plataforma você encontra o Anfitrião perfeito para hospedar seu cãozinho na segurança de um lar! Além de buscar a hospedagem perfeita, você também pode se inscrever e ser um Anfitrião DogSafe. Com a DogSafe você pode ser um Anfitrião e hospedar um cãozinho em casa ou encontrar um lar para hospedar seu cãozinho enquanto você viaja ou trabalha!',
        'web_author' => 'Trinity Brasil - Criação de Sites e Mídias Sociais',
        'copyright' => 'Copyright (c) 2016',
        'path' => '/',
        'session_name' => 'dogsafe',
        'google-translate' => 'notranslate',
        'google-robots' => 'index,follow',
        'google-analytics' => 'UA-64673024-1',
        'og_logo' => 'https://dogsafe.com.br/img/logo.png',
        'mobile' => NULL,
        'version' => '1'
    ),
    'database' => array(//Configuracao do Banco de Dados
        'driver' => 'mysql',
        'host' => '98.142.104.74',
        'port' => '3306',
        'name' => 'dogsafec_site',
        'user' => 'dogsafec_site',
        'password' => 'K1A*nUl%S4?h',
        'cache' => false,
        'timecache' => '60',
        'nocache' => array('arquivo', 'configuracao') //Nao criar cache 
    ),
    'ftp' => array(
        'host' => '98.142.104.74',
        'port' => '21',
        'user' => 'dogsafecom',
        'password' => '8888',
        'path' => '/public_html'
    ),
    'emailsend' => array(//Configuracao para envio de E-mail
        'smtp' => 'dogsafe.com.br',
        'port' => '465',
        'email' => 'naoresponda@dogsafe.com.br',
        'senha' => NULL,
        'return' => 'nao-responda@dogsafe.com.br',
        'type' => 'ssl',
        'departament_contato' => '1',
        'departament_entrevista' => '2',
        'departament_depoimento' => '3'
    ),
    'template' => array(//Configuração do Template(View) 
        'dir' => '/_views',
        'dir_cache' => '/_views/phcache',
        'time' => '60',
        'cache' => true,
        'ext' => 'phtml',
        'minify' => true,
        'lazyload' => false
    ),
    'sgc' => array(//Configurações SGC - Sistema de Gerenciamento de Conteudo
        'dominio' => 'dogsafe.com.br',
        'system' => 'sgcsite.trinitybrasil.net.br'
    ),
    'sitemap' => array('blog', 'tornar-se-anfitriao', 'fale-conosco', 'sobre', 'encontre-um-anfitriao', 'como-funciona', 'privacidade', 'termos', 'dicas', 'blog', 'parceiros', 'depoimentos'), //GERAR SITE MAP pelo /app/sitemap.php
    'styles' => array(//Lista de CSS , que sera juntados em /css/styles.css pelo /app/styles.php
        $root . '/app/css/style.css'
    )
);