<?php

define('NOT_REDIRECT', TRUE);

require(dirname(__FILE__) . '/bootstrap.php');

error_reporting(0);

// Images must be local files, so for convenience we strip the domain if it's there
define('JSFILE', (isset($_GET['file']) === true ? preg_replace('/^(s?f|ht)tps?:\/\/[^\/]+/i', '', $_GET['file']) . '.js' : NULL));

if (JSFILE === NULL) {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: javascript não especificado';
    exit();
}

if (file_exists(COD_DIR_APP . '/js/' . JSFILE) === false) {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: javascript não encontrado';
    exit();
}

define('CACHE_DIR', COD_DIR_APP . "/js/cache/");

// For security, directories cannot contain ':', images cannot contain '..' or '<', and
// images must start with '/'
if (strpos(dirname(JSFILE), ':') || preg_match('/(\.\.|<|>)/', JSFILE)) {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: malformed javascript path. Javascript paths must begin with \'/\'';
    exit();
}

$files = array('instantclick', 'fancybox.js', 'jquery.js', 'css.js', 'require.js', 'browser_selector.js');

$modified = filemtime(COD_DIR_APP . '/js/' . JSFILE);

$offset = 60 * 60 * 24 * 7; // Cache for 1 weeks
@header('Expires: ' . gmdate("D, d M Y H:i:s", time() + $offset) . ' GMT');

if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $modified) {
    @header("Content-Type: text/javascript");
    @header('Accept-Encoding: gzip');
    @header("HTTP/1.0 304 Not Modified");
    @header('Cache-Control:');

} else {

    $closure = new \Codando\System\Closure();

    $closure->add(COD_DIR_APP . '/js/' . JSFILE)->hideDebugInfo();
    
    header('Cache-Control: max-age=' . $offset);
    header('Accept-Encoding: gzip');
    header('Vary: Accept-Encoding');
    header('Pragma:');
    header("Content-Type: text/javascript");
    $etag = md5($modified);
    header("Last-Modified: ".gmdate("D, d M Y H:i:s", $modified)." GMT"); 
    header("Etag: ".$etag); 

    ob_end_clean();
    ob_start();
    
    if (in_array(JSFILE, $files)) {
        
        
        print($closure->_readSources());
        
    } else {
   
        $closure->simpleMode()
                ->cacheDir(CACHE_DIR)
                ->write();

    }
    
     if( strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE ) {
        
         // gzip the response and prepend the gzip header
            $gz_page = ob_get_contents();
            $gz_output = "\x1f\x8b\x08\x00\x00\x00\x00\x00".gzcompress($gz_page, 9);
            $gz_content_length = strlen($gz_page);

            // purge the output buffer
            ob_clean();

            header('Content-Length: '.$gz_content_length);
            header('Content-Encoding: gzip');
            header('Vary: Accept-Encoding');

            print $gz_output;
    }
    
    ob_end_flush();
    
    unset($closure);
}

exit;
