<?php

$tituloDaPagina = "Identificação";
include '../include/topo.php';
?>
    <section class="form-generic">
        <header class="title-section-container">
            <h1 class="title-section">Identificação</h1>
        </header>

        <p class="text-sub">Preencha os campos abaixo para acessar o seu ambiente restrito.</p>

        <div class="container">
            <div class="form-container identificacao">
                <form action="">

                    <label for="login" class="none">Login</label>
                    <input type="text" id="login" name="login" class="form-control" placeholder="Login*">

                    <label for="senha" class="none">Senha</label>
                    <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">

                    <div class="wrapper">
                        <a href="#" class="text-italic link-es">Esqueci minha senha</a>

                        <span class="text-italic">É a minha primeira vez aqui.<br>
                            <a href="cadastre-se.php">Quero me cadastrar!</a>
                        </span>
                    </div>
                    <div class="wrapper">
                        <button type="submit" class="btn-icon btn-icon-flecha">Entrar</button>

                        <a href="#" class="btn-icon btn-icon-facebook">Entrar com facebook</a>
                    </div>

                    <span class="text-italic">** Não publicaremos nada em seu Facebook, serve apenas para acesso rápido.</span>
                </form>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
