<?php

$tituloDaPagina = "Parceiros";
include '../include/topo.php';
?>


    <section class="parceiros">
        <header class="title-section-container">
            <h1 class="title-section">Parceiros</h1>
        </header>

        <p class="text-sub">Cliente DogSafe tem diversos benefícios em nossa rede de parceiros credenciados.</p>

        <div class="main-content">
            <div class="parceiros">
                <?php for ($i = 1; $i <= 4; $i++) { ?>
                <div class="parceiro">
                    <a href="#" class="logo">
                        <img src="../img/parceiros/01.png" alt="">
                    </a>

                    <span class="nome">Bichos e Caprichos</span>

                    <div class="descricao">
                        5% desconto em lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </div>
                </div>

                <div class="parceiro">
                    <a href="#" class="logo">
                        <img src="../img/parceiros/02.png" alt="">
                    </a>

                    <span class="nome">eDog</span>

                    <div class="descricao">
                        5% desconto em lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="container">
            <nav class="paginacao">
                <a href="index.php" class="btn-border"><i></i>voltar</a>

                <span class="qtd"><span>Página 1</span> de 10</span>

                <div class="pag">
                    <span class="prev"></span>
                    <span class="atual">1</span>
                    <span class="next"></span>
                </div>
            </nav>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
