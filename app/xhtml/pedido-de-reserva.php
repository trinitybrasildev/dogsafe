<?php

$tituloDaPagina = "Pedido de Reserva";
include '../include/topo.php';
?>
<section class="pedido-de-reserva">
    <header class="title-section-container">
        <h1 class="title-section">Pedido de Reserva</h1>
    </header>

    <div class="main-content">
        <div class="resultado-content">
            <div class="resultado">
                <div class="box-img">
                    <a href="#" class="imagem">
                        <img src="../img/resultado-busca/01.jpg" alt="">
                    </a>
                </div>

                <div class="box-info">
                    <span class="nome">Maria</span>
                    <span class="dog">DogSitter</span>
                    <span class="local">Carandá Bosque - Campo Grande / MS</span>
                </div>

                <div class="box-price">
                    <span class="preco">R$ <span>70</span></span>
                    <span class="periodo">POR NOITE</span>
                </div>
            </div>
        </div>

        <form class="no-pet" action="">
            <div class="form-group">
                <label for="data-inicio">DATA INÍCIO</label>
                <div class="input-icon input-icon-calendar">
                    <input type="text" id="data-inicio" name="data-inicio" placeholder="DATA INÍCIO" class="input input-medium" data-datepicker>
                </div>
            </div>

            <div class="form-group">
                <label for="data-fim">DATA FIM</label>
                <div class="input-icon input-icon-calendar">
                    <input type="text" id="data-fim" name="data-fim" placeholder="DATA FIM" class="input input-medium" data-datepicker>
                </div>
            </div>

            <div class="form-group">
                <label for="idade">HORÁRIO QUE IRÁ DEIXAR O PET NO ANFITRIÃO</label>
                <div class="select-personalizado">
                    <span class="label">08:00</span>
                    <i class="seta"></i>
                    <select id="idade" name="idade" class="selected">
                        <option value="1">08:00</option>
                        <option value="2">08:05</option>
                        <option value="3">18:00</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="idade">HORÁRIO QUE IRÁ BUSCAR O PET NO ANFITRIÃO</label>
                <div class="select-personalizado">
                    <span class="label">09:00</span>
                    <i class="seta"></i>
                    <select id="idade" name="idade" class="selected">
                        <option value="1">08:00</option>
                        <option value="2">08:05</option>
                        <option value="3">18:00</option>
                    </select>
                </div>
            </div>

            <div class="form-group escolha">
                <label for="">ESCOLHA SEU SERUMANINHO</label>

                <div class="select-personalizado multiple">
                    <span class="label"></span>
                    <i class="seta"></i>
                    <select data-placeholder="..." class="chosen-select input input-medium" multiple>
                        <option value="Cindy 0">Cindy 1</option>
                        <option value="Cindy 1">Cindy 2</option>
                        <option value="Cindy 2">Cindy 3</option>
                        <option value="Cindy 2">Cindy 3</option>
                        <option value="Cindy 2">Cindy 3</option>
                    </select>
                </div>
                <a class="btn btn-add" data-fancybox-type="iframe" data-rel="fancybox-iframe" rel="fancybox-iframe" href="cadastro-do-pet.php">+</a>

            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-extra-big">ENVIAR PEDIDO DE RESERVA</button>
            </div>

            <span class="text">Somente envie o Pedido de Reserva quando estiver preparado para efetuar o pagamento. </span>
        </form>

        <a href="index.php" class="btn-border">voltar</a>
    </div>
</section>

<?php include '../include/footer.php' ?>
