<?php

$tituloDaPagina = "Meu Cadastro";
include '../include/topo.php';
?>
<section class="form-generic">
    <header class="title-section-container">
        <h1 class="title-section">MEU CADASTRO</h1>
    </header>

    <p class="text-sub">Preencha os campos abaixo para atualizar o seu cadastro na DogSafe</p>

    <div class="container">
        <div class="form-container cadastro meu-cadastro">
            <form action="">

                <div class="dados">
                    <span><span>CPF:</span> 036.371.052-53</span>
                    <span><span>NOME COMPLETO:</span> Dasayev Diogo Alves da Silva Teixeira</span>
                </div>

                <label for="email" class="none">e-mail</label>
                <input type="text" id="email" name="email" class="form-control" placeholder="e-mail*">

                <label for="celular" class="none">Celular</label>
                <input type="text" id="celular" name="celular" class="form-control" placeholder="Celular*">

                <label for="cep" class="none">CEP</label>
                <input type="text" id="cep" name="cep" class="form-control" placeholder="cep*">

                <label for="endereco" class="none">endereco</label>
                <input type="text" id="endereco" name="endereco" class="form-control" placeholder="endereco*">

                <div class="select-personalizado">
                    <span class="label">Cidade</span>
                    <i class="seta"></i>
                    <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                        <option selected="" disabled="" class="primeiro">Cidade*</option>
                        <option value="1">Cidade 1</option>
                        <option value="2">Cidade 2</option>
                        <option value="3">Cidade 3</option>
                    </select>
                </div>

                <div class="wrapper">
                    <div class="select-personalizado">
                        <span class="label">Estado</span>
                        <i class="seta"></i>
                        <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                            <option selected="" disabled="" class="primeiro">Estado*</option>
                            <option value="1">Estado 1</option>
                            <option value="2">Estado 2</option>
                            <option value="3">Estado 3</option>
                        </select>
                    </div>
                    <label for="senha" class="none">Senha</label>
                    <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">
                </div>
                <div class="box">
                    <a class="imagem" href="../img/galeria/01.jpg" data-rel="fancybox-cadastro-1" rel="fancybox-cadastro-1" >
                        <img src="../img/galeria/01.jpg" alt="">
                    </a>
                    <a class="remover">remover imagem</a>
                </div>
                <button type="submit" class="btn-icon btn-icon-flecha">ATUALIZAR CADASTRO</button>
            </form>

            <span>Ao atualizar o seu cadastro, você concorda com os <a href="termos.php">termos de uso</a> e a <a href="privacidade.php">política de privacidade</a> da DogSafe</span>
        </div>
        <div class="widgets-container">
            <a href="index.php" class="btn-border">voltar</a>
        </div>
    </div>
</section>

<?php include '../include/footer.php' ?>
