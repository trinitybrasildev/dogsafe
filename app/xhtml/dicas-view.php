<?php

$tituloDaPagina = "Trato meu cão como filho. Isto é errado?";
include '../include/topo.php';
?>
    <section class="view-generic">
        <header class="title-section-container">
            <h1 class="title-section">Dicas</h1>
        </header>
        <div class="container">
            <div class="cabecalho">
                <span class="text-date">11.06.2015 às 12:00 </span>

                <h2 class="title-view">Trato meu cão como filho. Isto é errado?</h2>

                <div class="subtitle-view">Que tal darmos o tratamento respeitoso aos nossos amigos caninos, conferindo a eles a possibilidade de expressarem seus comportamentos naturais, tal qual nós gostamos de, também, expressar? Nem mais, nem menos: exatamente cães amigos, amados, sociáveis, saudáveis e inseridos na nossa família. Pense nisto e viva feliz com seu cão!</div>

                <span class="creditos">
                <strong>Daniel Honório</strong>
                <br>Fonte: Globo.com
                </span>
            </div>

            <div class="imagens-container" data-controller="player" data-notloadimg data-center>
                <div class="prev" data-prev>
                    <i class="sprite i-seta-e"></i>
                </div>
                <div class="next" data-next>
                    <i class="sprite i-seta-d"></i>
                </div>

                <div class="imagens" data-list>
                    <a href="#" class="imagem" data-item>
                        <img src="../img/cachorros/01.jpg" alt="">
                        <p class="descricao-imagem">Legenda</p>
                    </a>
                    <a href="#" class="imagem" data-item>
                        <img src="../img/cachorros/02.jpg" alt="">
                        <p class="descricao-imagem">Legenda</p>
                    </a>

                    <div class="mask-player">
                        <img src="../img/mask-player.jpg" alt="">
                    </div>
                </div>
            </div>

            <div class="descricao">
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    <br><br> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                    <br><br> Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatu.
                    <br><br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.
                    <br><br>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    <br><br>
                    <p>Para o cão: <br> 1) Ansiedade por separação (potencial causa de abandono)<br>1) Ansiedade por separação (potencial causa de abandono)<br> 2) Transtornos compulsivos (latidos em excesso, lambeduras, estereotipias, etc)<br> 3) Insegurança<br> 4) Medos e fobias<br> 5) Supressão de comportamentos naturais à espécie<br> 6) Baixo interesse social (com a mesma espécie)<br>
                    </p>
                </p>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>

                <div class="icones-compartilhar">
                    <span>COMPARTILHAR</span>
                    <a href="#" class="sprite i-btn-whats"></a>

                    <div class="share-button share-facebook">
                        <a href="http://www.facebook.com/" title="Compartilhar via Facebook" target="_blank" rel="external">
                            <div class="sprite i-btn-facebook"> </div>
                            <span>Facebook</span>
                        </a>
                    </div>

                    <div class="share-button share-twitter">
                        <a href="http://www.facebook.com/" title="Compartilhar via Facebook" target="_blank" rel="external">
                            <div class="sprite i-btn-twitter"> </div>
                            <span>Twitter</span>
                        </a>
                    </div>

                    <div class="share-button share-googleplus">
                        <a href="http://www.facebook.com/" title="Compartilhar via Facebook" target="_blank" rel="external">
                            <div class="sprite i-btn-googleplus"> </div>
                            <span>Google+</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
