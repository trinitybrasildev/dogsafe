<?php

$tituloDaPagina = "Minha Conta";
include '../include/topo.php';
?>
    <section class="form-generic minha-conta-anfitriao">
        <header class="title-section-container">
            <h1 class="title-section">Minha Conta</h1>
        </header>

        <p class="text-sub">Aqui é seu ambiente restrito, onde você poderá atualizar o seu cadastro, perfil e gerenciar toda a sua agenda.</p>

        <div class="container">
            <div class="form-container">
                <div class="usuario">
                    <span class="nome">Oi, <span>Diogo</span></span>
                    <span>Seja bem-vindo ao Dog Safe</span>
                </div>

                <nav class="nav-generic">
                    <ul>
                        <li><a href="meu-cadastro.php">Atualizar Cadastro</a></li>
                        <li><a href="#">Meu Pets</a></li>
                        <li>
                            <a href="#">Minha Agenda / Reservas</a>
                            <span class="contador">2</span>
                        </li>
                        <li>
                            <a href="#">Chat</a>
                            <span class="contador">1</span>
                        </li>
                        <li><a href="#" class="link-sair">Sair do sistema</a></li>
                    </ul>
                </nav>
            </div>

            <p class="text-sub">Lembre-se, o dono do cãozinho aguarda a sua confirmação para seguir com a reserva e pagamento. Confirme a sua disponibilidade em Minha Agenda/Reservas.</p>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
