<?php

$tituloDaPagina = "Fale conosco";
include '../include/topo.php';
?>
    <section class="form-generic">
        <header class="title-section-container">
            <h1 class="title-section">Fale conosco</h1>
        </header>

        <p class="text-sub">Preencha o formulário abaixo e entre em contato conosco. Será um prazer atendê-lo(a).</p>

        <div class="container">

           <div class="form-container">
               
           </div>
           
            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
