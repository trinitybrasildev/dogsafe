<?php

$tituloDaPagina = "Blog";
include '../include/topo.php';
?>


    <section class="blog">
        <header class="title-section-container">
            <h1 class="title-section">Blog</h1>
        </header>

        <p class="text-sub">Acompanhe nosso blog. Você ficará por dentro de temas relacionados aos peludinhos que amamos.</p>

        <div class="container">
            <div class="postagens">
                <?php for ($i = 1; $i <= 2; $i++) { ?>
                <div class="postagem">
                    <a href="dicas-view.php" class="imagem">
                        <img src="../img/blog/01.jpg" alt="">
                    </a>

                    <span class="text-date">11.06.2015 às 12:00</span>
                    <h2 class="title-view">Cuidado com os alimentos natalinos para o seu cão</h2>

                    <div class="text-wrapper"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>

                    <a href="dicas-view.php" class="btn btn-big btn-escuro">leia mais</a>
                </div>

                <div class="postagem">
                    <a href="dicas-view.php" class="imagem">
                        <img src="../img/blog/02.jpg" alt="">
                    </a>

                    <span class="text-date">11.06.2015 às 12:00</span>
                    <h2 class="title-view">Mercado aumenta oferta de produtos de páscoa para cães</h2>

                    <div class="text-wrapper">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>

                    <a href="dicas-view.php" class="btn btn-big btn-escuro">leia mais</a>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="container">
            <nav class="paginacao">
                <a href="index.php" class="btn-border"><i></i>voltar</a>

                <span class="qtd"><span>Página 1</span> de 10</span>

                <div class="pag">
                    <span class="prev"></span>
                    <span class="atual">1</span>
                    <span class="next"></span>
                </div>
            </nav>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
