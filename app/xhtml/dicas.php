<?php

$tituloDaPagina = "Dicas";
include '../include/topo.php';
?>


    <section class="sessao-dicas">
        <header class="title-section-container">
            <h1 class="title-section">Dicas</h1>
        </header>

        <p class="text-sub">Acompanhe nossas dicas e novidades sobre o mundo pet.</p>

        <div class="main-content">
            <div class="dicas">
                <?php for ($i = 1; $i <= 3; $i++) { ?>
                <div class="dica">
                    <a href="dicas-view.php" class="imagem">
                        <img src="../img/dicas/01.jpg" alt="">
                    </a>

                    <a href="dicas-view.php" class="dica-link">Coceira em cães: seu cão se coça mais do que o normal?</a>
                </div>

                <div class="dica">
                    <a href="dicas-view.php" class="imagem">
                        <img src="../img/dicas/02.jpg" alt="">
                    </a>

                    <a href="dicas-view.php" class="dica-link">Como viajar com seu cachorro ou gato de carro, ônibus e avião? Como viajar com seu cachorro ou gato de carro, ônibus e avião?</a>
                </div>
                
                 <div class="dica">
                    <a href="dicas-view.php" class="imagem">
                        <img src="../img/dicas/01.jpg" alt="">
                    </a>

                    <a href="dicas-view.php" class="dica-link"></a>
                </div>

                <div class="dica">
                    <a href="dicas-view.php" class="imagem">
                        <img src="../img/dicas/03.jpg" alt="">
                    </a>

                    <a href="dicas-view.php" class="dica-link">Como viajar com seu cachorro</a>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="container">
            <nav class="paginacao">
                <a href="index.php" class="btn-border"><i></i>voltar</a>

                <span class="qtd"><span>Página 1</span> de 10</span>

                <div class="pag">
                    <span class="prev"></span>
                    <span class="atual">1</span>
                    <span class="next"></span>
                </div>
            </nav>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
