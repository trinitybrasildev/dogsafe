<?php

$tituloDaPagina = "Perguntas Frequentes";
include '../include/topo.php';
?>
    <section class="form-generic">
        <header class="title-section-container">
            <h1 class="title-section">PERGUNTAS FREQUENTES</h1>
        </header>

        <p class="text-sub">Aqui você poderá tirar todas as dúvidas sobre a DogSafe.</p>

        <div class="container">

            <div class="perguntas-frequentes">
                <div class="perguntas-container">
                    <h2 class="title-page">PERGUNTAS FREQUENTES</h2>

                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <div class="pergunta-wrapper">
                        <span class="pergunta">Como Recebo o pagamento?</span>
                        <div class="resposta text-sub">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="perguntas-container">
                    <h2 class="title-page">PERGUNTAS FREQUENTES HÓSPEDE</h2>

                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <div class="pergunta-wrapper">
                        <span class="pergunta">Lorem ipsum dolor sit amet, consectetur adipiscing elita?</span>
                        <div class="resposta text-sub">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>


            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
