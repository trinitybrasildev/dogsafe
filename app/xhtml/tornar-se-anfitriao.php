<?php

$tituloDaPagina = "Tornar-se um anfitrião";
include '../include/topo.php';
?>
    <section class="form-generic">
        <header class="title-section-container">
            <h1 class="title-section">Tornar-se um anfitrião</h1>
        </header>

        <p class="text-sub">Preencha os campos abaixo para se tornar um anfitrião na DogSafe.</p>

        <div class="container tornar-se-anfitriao">
            <div class="form-container">
                <div class="chamada-wrapper">
                    <span>AMA ANIMAIS?</span>
                    <span>Venha fazer parte da nossa comunidade.</span>
                </div>

                <form action="">
                    <label for="email" class="none">e-mail</label>
                    <input type="text" id="email" name="email" class="form-control" placeholder="e-mail*">

                    <label for="nome-completo" class="none">nome completo</label>
                    <input type="text" id="nome-completo" name="nome-completo" class="form-control" placeholder="nome completo*">

                    <label for="cpf" class="none">cpf</label>
                    <input type="text" id="cpf" name="cpf" class="form-control" placeholder="cpf*">

                    <label for="celular" class="none">celular</label>
                    <input type="text" id="celular" name="celular" class="form-control" placeholder="celular*">

                    <label for="cep" class="none">cep</label>
                    <input type="text" id="cep" name="cep" class="form-control" placeholder="cep*">

                    <label for="endereco" class="none">endereco</label>
                    <input type="text" id="endereco" name="endereco" class="form-control" placeholder="endereco*">

                    <div class="select-personalizado">
                        <span class="label">Estado</span>
                        <i class="seta"></i>
                        <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                            <option selected="" disabled="" class="primeiro">Estado*</option>
                            <option value="1">Estado 1</option>
                            <option value="2">Estado 2</option>
                            <option value="3">Estado 3</option>
                        </select>
                    </div>

                    <div class="select-personalizado">
                        <span class="label">Cidade</span>
                        <i class="seta"></i>
                        <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                            <option selected="" disabled="" class="primeiro">Cidade*</option>
                            <option value="1">Cidade 1</option>
                            <option value="2">Cidade 2</option>
                            <option value="3">Cidade 3</option>
                        </select>
                    </div>

                    <label for="senha" class="none">Senha</label>
                    <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">
                    <label for="senha" class="none">Senha</label>
                    <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">
                    <label for="senha" class="none">Senha</label>
                    <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">
                    <label for="senha" class="none">Senha</label>
                    <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">
                    <label for="senha" class="none">Senha</label>
                    <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">

                    <label for="data-entrevista" disabled class="hide">DATA DA ENTREVISTA COM A DOG SAFE</label>
                    <div class="input-icon input-icon-calendar">
                        <textarea type="text" id="data-entrevista" name="data-entrevista" placeholder="DATA DA ENTREVISTA COM A DOG SAFE" class="form-control" data-datepicker></textarea>
                    </div>

                    <a href="#" class="btn-icon btn-icon-camera">INSERIR FOTO DE PERFIL</a>
                    <a href="#" class="btn-icon btn-icon-camera">INSERIR ATÉ 5 FOTOS DA CASA</a>

                    <div class="text-wrapper mb-132">
                        <span>Ao se cadastrar você concorda com os <a href="termos.php">termos de uso</a> e a <a href="privacidade.php">política de privacidade</a> da DogSafe</span>

                        <span>* Seu CPF só será usado para cadastro no site e segurança.</span>
                    </div>

                    <button type="submit" class="btn-icon btn-icon-flecha">cadastra-se</button>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="topicos-container">
                <span class="title-page">POR QUE VOCÊ VAI GOSTAR DE FAZER PARTE DA DOG SAFE:</span>

                <span class="text">VOCÊ NO CONTROLE</span>

                <div class="topicos-wrapper text-sub">
                    <div class="topico">
                        <div class="i-container">
                            <i class="sprite i-pata"></i>
                        </div>
                        <div class="texto">Sinalize o serviço que você pode oferecer: Day Care, Hospedagem;</div>
                    </div>

                    <div class="topico">
                        <div class="i-container">
                            <i class="sprite i-pata"></i>
                        </div>
                        <div class="texto">Trabalhe fazendo o que ama;</div>
                    </div>

                    <div class="topico">
                        <div class="i-container">
                            <i class="sprite i-pata"></i>
                        </div>
                        <div class="texto">Escolha o tipo de Pet que você gostaria de receber: Tamanho, idade por exemplo;</div>
                    </div>

                    <div class="topico">
                        <div class="i-container">
                            <i class="sprite i-pata"></i>
                        </div>
                        <div class="texto">Reserva e pagamento online.</div>
                    </div>

                    <div class="topico">
                        <div class="i-container">
                            <i class="sprite i-pata"></i>
                        </div>
                        <div class="texto">Controle a sua agenda;</div>
                    </div>
                </div>

                <span class="text">TODA RESERVA POSSUI COBERTURA VETERINÁRIA PARA EMERGÊNCIAS DE ATÉ R$ 5 MIL REAIS.</span>
            </div>

            <div class="opcoes">
                <h2 class="title-page">COMO EU COMEÇO?</h2>

                <div class="opcao text-sub">
                    <a href="#" class="imagem">
                        <img src="../img/tornar-se-anfitriao/opcao01.png">
                        <span class="circle">1</span>
                    </a>

                    <span class="desc">CRIE SEU PERFIL</span>

                    <div class="text-content">
                        Conte nos um pouco a seu respeito. Faça um upload de uma foto sua, se tiver foto com o seu animalzinho melhor ainda! Não se esqueça de colocar também algumas fotos de sua casa, onde o cãozinho vai ficar.
                        <br><br> Nós entraremos em contato após avaliação de seu perfil para uma entrevista. Com perfil aprovado, você será avisado e fará parte da comunidade Dog Safe.
                    </div>
                </div>

                <div class="opcao text-sub">
                    <a href="#" class="imagem">
                        <img src="../img/tornar-se-anfitriao/opcao02.png">
                        <span class="circle">2</span>
                    </a>

                    <span class="desc">ADMINISTRE SEU CALENDÁRIO</span>

                    <div class="text-content">
                        Deixe sempre seus dados e calendário com datas disponíveis, atualizado. Esteja atento às suas mensagens em seu perfil.
                        <br><br> Você receberá a solicitação de reserva que precisa ser confirmada. Para isso, basta acessar a sua conta e verificar as solicitações em <a href="#">Minha Agenda/Reservas</a>.
                        <br><br> Após clicar em Aceitar a sua hospedagem estará confirmada. Agora é só esperar o seu hóspede.
                        <br><br>Lembre-se o dono do Pet poderá falar com você através do nosso site.
                    </div>
                </div>

                <div class="opcao text-sub">
                    <a href="#" class="imagem">
                        <img src="../img/tornar-se-anfitriao/opcao03.png">
                        <span class="circle">3</span>
                    </a>

                    <span class="desc">NÓS CUIDAMOS DO PAGAMENTO</span>

                    <div class="text-content">
                        Após a confirmação de sua disponibilidade e da reserva o dono do cãozinho realiza o pagamento através do site e nós repassamos a você em até 3 dias após finalização da estadia.

                        <br><br>A Dog Safe mantém uma pequena % do pagamento da estadia para o site cobrir os gastos com seguro e manutenção.
                    </div>
                </div>

                <div class="chamada-cadastrar">
                    <span class="text">COMECE JÁ, VENHA FAZER PARTE DA NOSSA COMUNIDADE</span>

                    <a href="#" class="btn-icon btn-icon-flecha-cima">cadastra-se</a>
                </div>
            </div>

            <div class="perguntas-frequentes">
                <div class="perguntas-container">
                    <h2 class="title-page">PERGUNTAS FREQUENTES</h2>

                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <div class="pergunta-wrapper">
                        <span class="pergunta">Como Recebo o pagamento?</span>
                        <div class="resposta text-sub">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.
                        </div>
                    </div>
                    <?php } ?>

                    <span class="duvidas">Quer tirar mais dúvidas? Acesso nossa página de <a href="perguntas-frequentes.php">Perguntas Frequentes.</a></span>
                </div>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
