<?php

$tituloDaPagina = "Sobre a DOG SAFE";
include '../include/topo.php';
?>
    <section class="view-generic">
        <header class="title-section-container">
            <h1 class="title-section">dog safe</h1>
        </header>

        <div class="container">
            <div class="imagens-container" data-controller="player" data-notloadimg data-center>
                <div class="prev" data-prev>
                    <i class="sprite i-seta-e"></i>
                </div>
                <div class="next" data-next>
                    <i class="sprite i-seta-d"></i>
                </div>

                <div class="imagens" data-list>
                    <a href="#" class="imagem" data-item>
                        <img src="../img/cachorros/01.jpg" alt="">
                        <p class="descricao-imagem">Amamos o que fazemos</p>
                    </a>
                    <a href="#" class="imagem" data-item>
                        <img src="../img/cachorros/02.jpg" alt="">
                        <p class="descricao-imagem">Legenda</p>
                    </a>

                    <div class="mask-player">
                        <img src="../img/mask-player.jpg" alt="">
                    </div>
                </div>
            </div>

            <div class="descricao">
                <p>Nós da DogSafe acreditamos que seu Pet, seu cãozinho é parte de sua família, mas nem sempre você consegue levá-lo para todos os lugares ou em todas as suas viagens.
                    <br><br>Acreditamos que o que um sente pelo outro é um amor incondicional e pensando nesse amor surgiu a DogSafe.
                    <br><br>Nós somos uma plataforma online que liga você, que possui um amigo inseparável à um anfitrião, pessoa disposta a receber seu cãozinho em casa, para que assim ele possa se sentir seguro em sua ausência.
                    <br><br>Conectamos pessoas que amam animais.

                    <span class="sub-title">Como Surgiu a DogSafe</span> Mel, minha amiga inseparável há 11 anos, nunca ficou em hotelzinho, gaiolas. Ela sempre me acompanhou na maioria das minhas viagens. Quando não conseguia levá-la, ela ficava hospedada na casa de uma pessoa que sempre cuidou muito bem dela, conseguia ver o quando era bem tratada e o quanto essa pessoa amava o que fazia.
                    <br><br> Foi então que comecei a pensar por que não criar uma rede, uma comunidade onde pessoas que amam animais possam conectar-se? Por que não expandir essa rede, para que mais pessoas possam encontrar anfitriões e deixar seus cãezinhos no conforto de um lar?
                    <br><br>Assim surgiu a DogSafe, pensando em proporcionar momentos de conforto, segurança para seu cãozinho enquanto você viaja ou trabalha e tranquilidade para você!

                    <span class="sub-title">No que Acreditamos</span>

                    <div class="item">
                        <i class="i-wrapper"></i>
                        <span class="text">No amor incondicional que o seu pet sente por você</span>
                    </div>
                    <div class="item">
                        <i class="i-wrapper"></i>
                        <span class="text">No respeito pelos animais</span>
                    </div>
                    <div class="item">
                        <i class="i-wrapper"></i>
                        <span class="text">Pessoas podem trabalhar com o que gostam</span>
                    </div>
                    <div class="item">
                        <i class="i-wrapper"></i>
                        <span class="text">Podemos incentivar uma economia colaborativa, com pessoas que amam o que fazem.</span>
                    </div>
                </p>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
