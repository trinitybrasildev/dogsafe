<?php

$tituloDaPagina = "Cases De Sucesso View";
include '../include/topo.php';
?>
    <section class="view-generic container">
        <header class="title-section-container">
            <h1 class="title-section"><strong>SOBRE A</strong> IEPTB-MS</h1>
        </header>

        <div class="imagens-container" data-controller="player" data-notloadimg>

            <div class="prev" data-prev>
                <i class="sprite i-seta-e"></i>
            </div>
            <div class="next" data-next>
                <i class="sprite i-seta-d"></i>
            </div>

            <div class="imagens" data-list>
                <a href="#" class="imagem" data-item>
                    <img src="" alt="">
                </a>
                <a href="#" class="imagem" data-item>
                    <img src="" alt="">
                </a>
                <a href="#" class="imagem" data-item>
                    <img src="" alt="">
                </a>
            </div>
        </div>

        <div class="descricao">
            <p class="text-regular">Lorem ipsum: dolor sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                <br><br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                <br><br> Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <div class="widgets-container">
            <a href="#" class="btn btn-small"><i></i>voltar</a>

            <div class="icones-compartilhar-container">
                <a href="#" class="icone-compartilhar"><i class="sprite i-whats"></i></a>
                <a href="#" class="icone-compartilhar"><i class="sprite i-facebook-view"></i></a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
