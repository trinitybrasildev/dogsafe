<?php

$tituloDaPagina = "Encontre um anfitrião";
include '../include/topo.php';
?>


    <section class="encontre-um-anfitriao">
        <header class="title-section-container">
            <h1 class="title-section">Encontre um anfitrião</h1>
        </header>

        <div class="main-content">
            <form>
                <label for="boarding" disabled="" class="hide">Boarding</label>
                <div class="select-personalizado">
                    <span class="label">Boarding</span>
                    <i class="seta"></i>
                    <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                    <option selected="" disabled="" class="primeiro">Boarding</option>
                    <option value="1">Formação 1</option>
                    <option value="2">Formação 2</option>
                    <option value="3">Formação 3</option>
                </select>
                </div>

                <label for="cep" disabled="" class="hide">CEP</label>
                <input type="text" id="cep" name="cep" placeholder="cep" class="input input-medium">

                <label for="data-inicio" disabled="" class="hide">DATA INÍCIO</label>
                <div class="input-icon input-icon-calendar">
                    <input type="text" id="data-inicio" name="data-inicio" placeholder="DATA INÍCIO" class="input input-medium" data-datepicker>
                </div>

                <label for="data-fim" disabled="" class="hide">DATA FIM</label>
                <div class="input-icon input-icon-calendar">
                    <input type="text" id="data-fim" name="data-fim" placeholder="DATA FIM" class="input input-medium" data-datepicker>
                </div>

                <label for="tamanho-do-pet" class="none">TAMANHO DO PET</label>
                <div class="select-personalizado">
                    <span class="label">TAMANHO DO PET</span>
                    <i class="seta"></i>
                    <select id="tamanho-do-pet" name="tamanho-do-pet" class="selected">
                        <option value="1">Tamanho 1</option>
                        <option value="2">Tamanho 2</option>
                        <option value="3">Tamanho 3</option>
                    </select>
                </div>

                <label for="tipo-ambiente" class="none">TIPO DE AMBIENTE *</label>
                <div class="select-personalizado">
                    <span class="label">TIPO DE AMBIENTE *</span>
                    <i class="seta"></i>
                    <select id="tipo-ambiente" name="tipo-ambiente" class="selected">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>

                <div class="range-wrapper">
                    <span class="text">PREÇO DA DIÁRIA:</span>

                    <span class="valor">R$ 0</span>

                    <div class="input-range noUi-target noUi-ltr noUi-horizontal noUi-background" data-min="" data-max="99500" data-star="" data-end="" data-step="1000">
                        <div class="noUi-base">
                            <div class="noUi-origin noUi-connect" style="left: 0%;">
                                <div class="noUi-handle noUi-handle-lower"></div>
                            </div>
                            <div class="noUi-origin noUi-background" style="left: 34.1709%;">
                                <div class="noUi-handle noUi-handle-upper"></div>
                            </div>
                        </div>
                    </div>

                    <span class="valor">R$ 200</span>
                </div>

                <div class="btn-wrapper">
                    <button type="submit" class="btn btn-extra-big btn-danger">Filtrar</button>
                </div>
            </form>
        </div>

        <div class="container">
            <div class="dados-resultado">
                <span class="total">ENCONTRAMOS 99 ANFITRIÕES EM <span class="cidade">CAMPO GRANDE / MS</span></span>

                <a href="#" class="limpar">Limpar Filtro</a>
            </div>
        </div>

        <div class="container">
            <div class="resultados-wrapper">
                <?php for ($i = 1; $i <= 6; $i++) { ?>
                <div class="resultado">
                    <div class="box-img">
                        <a href="#" class="imagem">
                            <img src="../img/resultado-busca/01.jpg" alt="">
                        </a>
                    </div>

                    <div class="box-info">
                        <span class="nome">Maria <?= $i?></span>
                        <span class="local">Carandá Bosque - Campo Grande / MS</span>
                        <?php if($i % 2 == 1) { ?>
                        <span class="possui-animais">Este Anfitrião possui animais</span>
                        <?php } ?>
                    </div>

                    <div class="box-price">
                        <span class="preco">R$ <span>7<?= $i?></span></span>
                        <span class="periodo">POR NOITE</span>
                    </div>

                    <div class="box-avaliacao">
                        <div class="estrelas">
                            <i class="estrela active"></i>
                            <i class="estrela active"></i>
                            <i class="estrela <?php if($i % 2 == 0) { echo " active ";}?>"></i>
                            <i class="estrela"></i>
                            <i class="estrela"></i>
                        </div>

                        <span class="avaliacoes">27 Avaliações</span>
                    </div>
                </div>
                <?php } ?>
            </div>

            <div class="mapa-wrapper">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14952.874376561536!2d-54.5950143!3d-20.4562131!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3a7c7e5c5f8b3e4b!2sTrinity+Brasil+%E2%80%93+Cria%C3%A7%C3%A3o+e+desenvolvimento+de+sites+em+Campo+Grande-MS!5e0!3m2!1spt-BR!2sbr!4v1480965762313" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>

        <div class="container">
            <nav class="paginacao">
                <a href="index.php" class="btn-border"><i></i>voltar</a>

                <span class="qtd"><span>Página 1</span> de 10</span>

                <div class="pag">
                    <span class="prev"></span>
                    <span class="atual">1</span>
                    <span class="next"></span>
                </div>
            </nav>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
