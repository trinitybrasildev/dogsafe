<?php

$tituloDaPagina = "Depoimentos";
include '../include/topo.php';
?>


    <section class="sessao-depoimentos">
        <header class="title-section-container">
            <h1 class="title-section">depoimentos</h1>
        </header>

        <p class="text-sub">Confira abaixo os depoimentos de quem confiou e aprovou os nossos serviços.</p>

        <div class="container">
            <section class="depoimentos text-sub text-sub-branco">
                <article class="depoimento depoimento-principal">
                    <div class="i-mensagem-container">
                        <i class="sprite i-mensagem"></i>
                    </div>
                    <div class="text-wrapper">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        <br><br>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</div>

                    <div class="autor-container">
                        <a href="#" class="imagem-autor">
                            <img src="../img/autor01.png">
                        </a>
                        <span class="text-sub text-sub-branco text-sub-small">LUAN FREITAS</span>
                    </div>
                </article>

                <article class="depoimento">
                    <div class="autor-container">
                        <a href="#" class="imagem-autor">
                            <img src="../img/autor02.png">
                        </a>
                        <span class="text-sub text-sub-branco text-sub-small">LUIZA OLIVEIRA</span>
                    </div>

                    <div class="i-mensagem-container">
                        <i class="sprite i-mensagem"></i>
                    </div>
                    <div class="text-wrapper">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                </article>

                <article class="depoimento ultimo">
                    <div class="autor-container">
                        <a href="#" class="imagem-autor">
                            <img src="../img/autor03.png">
                        </a>
                        <span class="text-sub text-sub-branco text-sub-small">PAULA GOMES</span>
                    </div>

                    <div class="i-mensagem-container">
                        <i class="sprite i-mensagem"></i>
                    </div>
                    <div class="text-wrapper">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
                </article>
            </section>
        </div>

        <div class="container">
            <nav class="paginacao">
                <a href="index.php" class="btn-border"><i></i>voltar</a>

                <span class="qtd"><span>Página 1</span> de 10</span>

                <div class="pag">
                    <span class="prev"></span>
                    <span class="atual">1</span>
                    <span class="next"></span>
                </div>
            </nav>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
