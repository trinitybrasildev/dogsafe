<?php

$tituloDaPagina = "Chat";
include '../include/topo.php';
?>
    <section class="sessao-chat">
        <header class="title-section-container">
            <h1 class="title-section">Chat</h1>
        </header>

        <p class="text-sub">Esse é o seu canal restrito de mensagens, onde você poderá tirar todas as dúvidas sobre os serviços e hospedagens.</p>

        <div class="container">
            <div class="chat-wrapper">
                <div class="menu-historico">
                    <span class="hamburguer">
                      <i></i>
                      <i></i>
                      <i></i>
                  </span>
                    <span class="text">Histórico de conversas</span>
                </div>
                <div class="conversas-container">
                    <div class="conversas">
                        <?php for ($i = 1; $i <= 15; $i++) { ?>
                        <div class="contato">
                            <span class="nome">Diogo Teixeira</span>
                            <span class="data">10/05/2016 - 17:06</span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="conversa">
                    <a href="#" class="text-link">apagar histórico de conversa</a>

                    <div class="mensagens">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <div class="m-01">
                            <span class="strong">Diogo  - 10/05/2016 - 17:06</span>
                            <span class="text">Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita</span>
                        </div>
                        <div class="m-02">
                            <span class="strong">Diogo  - 10/05/2016 - 17:06</span>
                            <span class="text">Oi, boaar uma visita</span>
                        </div>
                        <div class="m-01">
                            <span class="strong">Diogo  - 10/05/2016 - 17:06</span>
                            <span class="text">Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita</span>
                        </div>
                        <div class="m-02">
                            <span class="strong">Diogo  - 10/05/2016 - 17:06</span>
                            <span class="text">Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita Oi, boa tarde. Gostaria de saber se você tem disponibilidade para agendar uma visita</span>
                        </div>
                        <?php } ?>
                    </div>

                    <form action="">
                        <textarea placeholder="Mensagem"></textarea>
                        <button type="submit" class="btn-icon btn-icon-flecha">Enviar</button>
                    </form>
                </div>
            </div>

            <div class="widgets-container privacidade">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
