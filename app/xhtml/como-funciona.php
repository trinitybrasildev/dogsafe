<?php

$tituloDaPagina = "Como funciona";
include '../include/topo.php';
?>
    <section class="sessao-como-funciona">
        <header class="title-section-container">
            <h1 class="title-section">Como funciona</h1>
        </header>

        <p class="text-sub">Com DogSafe o seu melhor amigo estará em boas mãos. Saiba como funciona:</p>

        <div class="container">

            <div class="como-funciona-wrapper">
                <div class="imagens">
                    <div class="imagem">
                        <img src="../img/como-funciona/01.jpg" alt="">

                        <span class="text">POR QUE VOCÊ DEVE UTILIZAR A DAGSAFE?</span>
                    </div>
                    <div class="imagem">
                        <img src="../img/como-funciona/02.jpg" alt="">

                        <span class="text">POR QUE O SEU ANIMALZINHO VAI GOSTAR?</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="descricao">
                    <p>Com a Dog Safe você pode deixar seu amigo com um de nossos anfitriões. Todos os anfitriões da nossa comunidade são entrevistados e estão prontos para receber seu pet no conforto do lar, como se estivesse em casa.
                        <br><br> Aqui você encontra a hospedagem perfeita para seu Pet. Vai viajar e não pode levar seu cãozinho? Surgiu uma viagem de trabalho? Encontre um anfitrião e hospede seu Pet.
                        <br><br>A Dog Safe também disponibiliza a atividade de Day Care. Por que deixar seu amigo sozinho enquanto você trabalha se você pode deixá-lo com um de nossos anfitriões?</p>
                </div>

                <div class="opcoes">
                    <h2 class="title-page">ESCOLHA UMA DE NOSSAS OPÇÕES E FAÇA SUA RESERVA, É MUITO SIMPLES:</h2>

                    <div class="opcao text-sub">
                        <a href="#" class="imagem">
                            <img src="../img/como-funciona01.png">
                            <span class="circle">1</span>
                        </a>

                        <span class="desc">ENCONTRE UM ANFITRIÃO PARA O SEU ANIMALZINHO</span>

                        <div class="text-content">
                            Você e seu cãozinho são uma família, mas nem sempre você pode levá-lo com você.
                            <br><br> Com a DogSafe você pode deixar seu amigo com alguém que ama animais como você.
                            <br><br> Vai viajar? Horas trabalhando no escritório? Em qualquer situação deixe seu amigo com um de nossos anfitriões.
                            <br><br> Escolha uma de nossas opções, Hospedagem ou DayCare e faça sua reserva. É bem simples!
                            <br><br> Faça seu cadastro em nosso site, realize uma busca digitando seu CEP e encontre o anfitrião mais próximo a você.
                        </div>
                    </div>

                    <div class="opcao text-sub">
                        <a href="#" class="imagem">
                            <img src="../img/como-funciona02.png">
                            <span class="circle">2</span>
                        </a>

                        <span class="desc">ESCOLHA O ANFITRIÃO E AGENDE ONLINE</span>

                        <div class="text-content">
                            Escolha o Anfitrião que mais lhe agradar e solicite uma reserva, você pode trocar mensagens com o Anfitrião através do site.
                            <br><br> O agendamento é fácil e rápido, basta clicar no calendário do anfitrião nas datas desejadas. Você pode agendar uma visita para conhecer melhor onde seu cãozinho irá ficar.
                            <br><br> Tudo certo? Não esqueça de confirmar a sua reserva. Com a confirmação da reserva, nós guardamos o seu pagamento e repassamos para o anfitrião após a finalização da hospedagem ou DayCare.
                            <br><br> Você pode acompanhar todas as suas solicitações de reserva e confirmação de disponibilidade do Anfitrião acessando a sua área restrita no site.
                        </div>
                    </div>

                    <div class="opcao text-sub">
                        <a href="#" class="imagem">
                            <img src="../img/como-funciona03.png">
                            <span class="circle">3</span>
                        </a>

                        <span class="desc">SEU MELHOR AMIGO EM BOAS MÃOS</span>

                        <div class="text-content">
                            Agora é só aproveitar!
                            <br><br> Lembre-se que com a DogSafe você pode contar com anfitriões selecionados e garantia veterinária de até 5Mil reais para emergências durante a hospedagem.
                            <br><br> Deixe seu cãozinho em um lar como se estivesse em casa.
                            <br><br> Venha fazer parte da nossa comunidade!
                        </div>
                    </div>
                </div>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
