<?php

$tituloDaPagina = "Cadastro do pet";
include '../include/topo.php';
?>
    <section class="form-generic cadastro-pet">
        <header class="title-section-container">
            <h1 class="title-section">Cadastro do pet</h1>
        </header>

        <p class="text-sub">Preencha os campos abaixo para realizar o cadastro do seu Pet na DogSafe</p>

        <div class="container">
            <form class="form-container">
                <div class="form-wrapper">
                    <div class="input-holder">
                        <label for="nome-do-pet" class="none">NOME DO PET</label>
                        <input type="text" id="nome-do-pet" name="nome-do-pet" class="form-control input-big" placeholder="NOME DO PET">

                        <label for="idade" class="none">IDADE</label>
                        <div class="select-personalizado">
                            <span class="label">IDADE</span>
                            <i class="seta"></i>
                            <select id="idade" name="idade" class="selected">
                            <option value="1">Idade 1</option>
                            <option value="2">Idade 2</option>
                            <option value="3">Idade 3</option>
                        </select>
                        </div>
                    </div>

                    <div class="input-holder">
                        <label for="tipo-animal" class="none">TIPO</label>
                        <div class="select-personalizado s-tipo">
                            <span class="label">TIPO (EX: CACHORRO OU GATO)</span>
                            <i class="seta"></i>
                            <select id="tipo-animal" name="tipo-animal" class="selected">
                                <option value="1">Tipo 1</option>
                                <option value="2">Tipo 2</option>
                                <option value="3">Tipo 3</option>
                            </select>
                        </div>

                        <label for="idade" class="none">TAMANHO DO PET</label>
                        <div class="select-personalizado">
                            <span class="label">TAMANHO DO PET</span>
                            <i class="seta"></i>
                            <select id="tamanho-do-pet" name="tamanho-do-pet" class="selected">
                                <option value="1">Tamanho 1</option>
                                <option value="2">Tamanho 2</option>
                                <option value="3">Tamanho 3</option>
                            </select>
                        </div>

                        <label for="castrado" class="none">CASTRADO</label>
                        <div class="select-personalizado">
                            <span class="label">CASTRADO</span>
                            <i class="seta"></i>
                            <select id="castrado" name="castrado" class="selected">
                                <option value="1">SIM</option>
                                <option value="2">NÃO</option>
                            </select>
                        </div>
                    </div>

                    <div class="input-holder">
                        <label for="raca" class="none">RAÇA</label>
                        <div class="select-personalizado">
                            <span class="label">RAÇA</span>
                            <i class="seta"></i>
                            <select id="raca" name="raca" class="selected">
                                <option value="1">Raça 1</option>
                                <option value="2">Raça 2</option>
                                <option value="3">Raça 3</option>
                            </select>
                        </div>

                        <label for="sexo" class="none">SEXO</label>
                        <div class="select-personalizado">
                            <span class="label">sexo</span>
                            <i class="seta"></i>
                            <select id="sexo" name="sexo" class="selected">
                                <option value="1">M</option>
                                <option value="2">F</option>
                            </select>
                        </div>

                        <label for="plano-saude" class="none">PLANO DE SAÚDE</label>
                        <textarea type="text" id="plano-saude" name="plano-saude" class="form-control" placeholder="PLANO DE SAÚDE VETERINÁRIO?       Se sim, cadastre o telefone do plano"></textarea>
                    </div>

                    <div class="input-holder big">
                        <label for="possui-alergia" class="none">POSSUI ALERGIA? ESPECIFIQUE</label>
                        <textarea type="text" id="possui-alergia" name="possui-alergia" class="form-control" placeholder="POSSUI ALERGIA? ESPECIFIQUE"></textarea>

                        <label for="possui-alergia" class="none">POSSUI DOENÇA PRÉVIA OU CRÔNICA? ESPECIFIQUE</label>
                        <textarea type="text" id="possui-alergia" name="possui-alergia" class="form-control" placeholder="POSSUI DOENÇA PRÉVIA OU CRÔNICA? ESPECIFIQUE"></textarea>

                        <label for="possui-alergia" class="none">FAZ USO DE MEDICAÇÃO? (Caso sim, por favor descrever a medicação, dosagem e horários)</label>
                        <textarea type="text" id="possui-alergia" name="possui-alergia" class="form-control" placeholder="FAZ USO DE MEDICAÇÃO? (Caso sim, por favor descrever a medicação, dosagem e horários)"></textarea>
                    </div>

                    <div class="input-holder big">
                        <label for="cuidado-especial" class="none">REQUER CUIDADO ESPECIAL? DESCREVA</label>
                        <textarea type="text" id="cuidado-especial" name="cuidado-especial" class="form-control" placeholder="REQUER CUIDADO ESPECIAL? DESCREVA"></textarea>

                        <label for="carteira-atualizada" class="none">RAÇA</label>
                        <div class="select-personalizado">
                            <span class="label">CARTEIRA DE VACINAÇÃO ATUALIZADA COM A ANTIRRÁBICA? </span>
                            <i class="seta"></i>
                            <select id="carteira-atualizada" name="carteira-atualizada" class="selected">
                                <option value="1">SIM</option>
                                <option value="2">NÃO</option>
                            </select>
                        </div>

                        <label for="uso-recente" class="none">RAÇA</label>
                        <div class="select-personalizado">
                            <span class="label">FEZ USO RECENTE DE ANTICARRAPATICIDA E ANTIPULGAS </span>
                            <i class="seta"></i>
                            <select id="uso-recente" name="uso-recente" class="selected">
                                <option value="1">SIM</option>
                                <option value="2">NÃO</option>
                            </select>
                        </div>
                    </div>

                    <div class="input-holder big ultimo">
                        <label for="cuidado-especial" class="none">REQUER CUIDADO ESPECIAL? DESCREVAPARA REGIÕES COM LEISHMANIOSE: Possui vacina ou faz uso da coleira específica contra o mosquito?</label>
                        <textarea type="text" id="cuidado-especial" name="cuidado-especial" class="form-control" placeholder="PARA REGIÕES COM LEISHMANIOSE: Possui vacina ou faz uso da coleira específica contra o mosquito?"></textarea>

                        <label for="cuidado-especial" class="none">FALE UM POUCO A RESPEITO DE SEU CÃOZINHO: Como por exemplo se é sociável com outros cães e crianças, se gosta de brincar ou dorme bastante. Esse é um espaço para você descrevê-lo e colocar as considerações que achar importantes.</label>
                        <textarea type="text" id="cuidado-especial" name="cuidado-especial" class="form-control input-big" placeholder="FALE UM POUCO A RESPEITO DE SEU CÃOZINHO: Como por exemplo se é sociável com outros cães e crianças, se gosta de brincar ou dorme bastante. Esse é um espaço para você descrevê-lo e colocar as considerações que achar importantes."></textarea>
                    </div>

                    <div class="btn-wrapper">
                        <div class="text-container">
                            <span>Ao realizar o cadastro do seu Pet, você concorda com os <a href="termos.php">termos de uso</a> e a <a href="privacidade.php">política de privacidade</a> da DogSafe</span>
                        </div>

                        <div class="btn-content">
                            <button type="submit" class="btn-icon btn-icon-flecha">cadastrar</button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="text-sub">
                <span class="text"><i class="sprite i-pata-small"></i>Não se esqueça de imprimir a ficha e entregar ao anfitrião no momento da hospedagem ou DayCare.</span>

                <span class="text"><i class="sprite i-pata-small"></i>Lembramos que nós da DogSafe recomendamos fortemente a carteira de vacinação atualizada, uso de anticarrapaticida e antipulga além de vacina ou coleira contra Leishmaniose para regiões afetadas.</span>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
