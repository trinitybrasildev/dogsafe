<?php

$tituloDaPagina = "Cadastre-se";
include '../include/topo.php';
?>
    <section class="form-generic">
        <header class="title-section-container">
            <h1 class="title-section">Cadastre-se</h1>
        </header>

        <p class="text-sub">Preencha os campos abaixo para realizar o seu cadastro na DogSafe</p>

        <div class="container">
            <div class="form-container cadastro">
                <form action="">

                    <label for="email" class="none">e-mail</label>
                    <input type="text" id="email" name="email" class="form-control" placeholder="e-mail*">

                    <label for="nome-completo" class="none">nome completo</label>
                    <input type="text" id="nome-completo" name="nome-completo" class="form-control" placeholder="nome completo*">

                    <label for="cpf" class="none">cpf</label>
                    <input type="text" id="cpf" name="cpf" class="form-control" placeholder="cpf*">

                    <label for="celular" class="none">celular</label>
                    <input type="text" id="celular" name="celular" class="form-control" placeholder="celular*">

                    <label for="cep" class="none">cep</label>
                    <input type="text" id="cep" name="cep" class="form-control" placeholder="cep*">

                    <label for="endereco" class="none">endereco</label>
                    <input type="text" id="endereco" name="endereco" class="form-control" placeholder="endereco*">

                    <div class="select-personalizado">
                        <span class="label">Cidade</span>
                        <i class="seta"></i>
                        <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                            <option selected="" disabled="" class="primeiro">Cidade*</option>
                            <option value="1">Cidade 1</option>
                            <option value="2">Cidade 2</option>
                            <option value="3">Cidade 3</option>
                        </select>
                    </div>

                    <div class="wrapper">
                        <div class="select-personalizado">
                            <span class="label">Estado</span>
                            <i class="seta"></i>
                            <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                                <option selected="" disabled="" class="primeiro">Estado*</option>
                                <option value="1">Estado 1</option>
                                <option value="2">Estado 2</option>
                                <option value="3">Estado 3</option>
                            </select>
                        </div>

                        <label for="senha" class="none">Senha</label>
                        <input type="text" id="senha" name="senha" class="form-control" placeholder="Senha*">
                    </div>

                    <a href="#" class="btn-icon btn-icon-facebook">Entrar com facebook</a>

                    <button type="submit" class="btn-icon btn-icon-flecha">Enviar</button>
                </form>

                <div class="text-wrapper">
                    <span>Ao se cadastrar você concorda com os <a href="termos.php">termos de uso</a> e a <a href="privacidade.php">política de privacidade</a> da DogSafe</span>

                    <span>* Seu CPF só será usado para cadastro no site e segurança.</span>

                    <span>** Não publicaremos nada em seu Facebook, serve apenas para acesso rápido.</span>
                </div>
            </div>
            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
