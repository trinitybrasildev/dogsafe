<?php

$tituloDaPagina = "Meu Perfil";
include '../include/topo.php';
?>
    <section class="form-generic meu-perfil">
        <header class="title-section-container">
            <h1 class="title-section">Meu Perfil</h1>
        </header>

        <p class="text-sub">Preencha os campos abaixo para realizar o cadastro do seu perfil na DogSafe</p>

        <div class="container">
            <form class="form-container">
                <div class="form-wrapper">
                    <div class="dados-usuario">
                        <span><span>CPF:</span> 036.371.052-53</span>
                        <span><span>NOME COMPLETO:</span> Dasayev Diogo Alves da Silva Teixeira <a href="#">(Editar Cadastro)</a></span>
                    </div>

                    <div class="input-holder">
                        <label for="tipo-atividade" class="none">TIPO DE ATIVIDADE *</label>
                        <div class="select-personalizado">
                            <span class="label">TIPO DE ATIVIDADE *</span>
                            <i class="seta"></i>
                            <select id="tipo-atividade" name="tipo-atividade" class="selected">
                            <option value="1">TIPO 1</option>
                            <option value="2">TIPO 2</option>
                            <option value="3">TIPO 3</option>
                        </select>
                        </div>

                        <label for="valor-diaria-hospedagem" class="none">VALOR DA DIÁRIA HOSPEDAGEM</label>
                        <input type="text" id="valor-diaria-hospedagem" name="valor-diaria-hospedagem" class="form-control" placeholder="VALOR DA DIÁRIA HOSPEDAGEM" disabled>

                        <label for="valor-diaria-daycare" class="none">VALOR DA DIÁRIA DAYCARE</label>
                        <input type="text" id="valor-diaria-daycare" name="valor-diaria-daycare" class="form-control" placeholder="VALOR DA DIÁRIA DAYCARE" disabled>
                    </div>

                    <div class="input-holder">
                        <label for="tipo-animal" class="none">DISPONIBILIDADE PARA QUANTOS HÓSPEDES? *</label>
                        <div class="select-personalizado">
                            <span class="label big">DISPONIBILIDADE PARA QUANTOS HÓSPEDES? *</span>
                            <i class="seta"></i>
                            <select id="tipo-animal" name="tipo-animal" placeholder="idade" class="selected">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        </div>

                        <label for="horas-supervisao" class="none">HORAS DE SUPERVISÃO *</label>
                        <div class="select-personalizado">
                            <span class="label">HORAS DE SUPERVISÃO *</span>
                            <i class="seta"></i>
                            <select id="horas-supervisao" name="horas-supervisao" class="selected">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        </div>

                        <label for="tipo-cancelamento" class="none">TIPO DE CANCELAMENTO *</label>
                        <div class="select-personalizado">
                            <span class="label">TIPO DE CANCELAMENTO *</span>
                            <i class="seta"></i>
                            <select id="tipo-cancelamento" name="tipo-cancelamento" class="selected">
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                        </div>
                    </div>

                    <div class="input-holder big">
                        <div class="input-wrapper">
                            <label for="tipo-ambiente" class="none">TIPO DE AMBIENTE *</label>
                            <div class="select-personalizado">
                                <span class="label">TIPO DE AMBIENTE *</span>
                                <i class="seta"></i>
                                <select id="tipo-ambiente" name="tipo-ambiente" class="selected">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                            </div>

                            <label for="qtd-animais" class="none">POSSUI ANIMAIS? QUANTOS? *</label>
                            <div class="select-personalizado s-qtd-animais">
                                <span class="label">POSSUI ANIMAIS? QUANTOS? *</span>
                                <i class="seta"></i>
                                <select id="qtd-animais" name="qtd-animais" class="selected">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                            </div>

                            <label for="tamanho-do-pet" class="none">TAMANHO DO PET</label>
                            <div class="select-personalizado">
                                <span class="label">TAMANHO DO PET</span>
                                <i class="seta"></i>
                                <select id="tamanho-do-pet" name="tamanho-do-pet" class="selected">
                                <option value="1">Tamanho 1</option>
                                <option value="2">Tamanho 2</option>
                                <option value="3">Tamanho 3</option>
                            </select>
                            </div>

                            <label for="sexo" class="none">SEXO</label>
                            <div class="select-personalizado">
                                <span class="label">SEXO</span>
                                <i class="seta"></i>
                                <select id="sexo" name="sexo" class="selected">
                                <option value="1">MASCULINO</option>
                                <option value="2">FEMININO</option>
                            </select>
                            </div>
                        </div>

                        <div class="input-wrapper">
                            <label for="sobre-usuario" class="none">ESCREVA UM POUCO A SEU RESPEITO *</label>
                            <textarea type="text" id="sobre-usuario" name="sobre-usuario" class="form-control" placeholder="ESCREVA UM POUCO A SEU RESPEITO *"></textarea>
                        </div>
                    </div>

                    <div class="checkbox-content">
                        <span class="desc">HABILIDADES</span>

                        <div class="checkbox-container">
                            <div class="checkbox-wrapper">
                                <input type="checkbox" name="bancario[receber-valores]" id="receber-valores" value="Sim" required="">
                            </div>
                            <div class="checkbox-wrapper">
                                <label for="receber-valores">Administração de medicação oral</label>
                            </div>
                        </div>

                    </div>

                    <span class="desc">PREFERÊNCIA DE HOSPEDAGEM:</span>

                    <div class="input-holder">
                        <label for="peso-pet" class="none">PESO DO PET *</label>
                        <div class="select-personalizado">
                            <span class="label">PESO DO PET *</span>
                            <i class="seta"></i>
                            <select id="peso-pet" name="peso-pet" class="selected">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                        </div>

                        <label for="aceita-macho-femea" class="none">ACEITA FÊMEA E MACHO? *</label>
                        <div class="select-personalizado">
                            <span class="label">ACEITA FÊMEA E MACHO? *</span>
                            <i class="seta"></i>
                            <select id="aceita-macho-femea" name="aceita-macho-femea" class="selected">
                                   <option value="1">SIM</option>
                                    <option value="2">NÃO</option>
                                </select>
                        </div>

                        <label for="qual-sexo" class="none">CASO OPTE PELO NÃO, QUAL O SEXO? </label>
                        <div class="select-personalizado" data-disabled>
                            <span class="label big">CASO OPTE PELO NÃO, QUAL O SEXO? </span>
                            <i class="seta"></i>
                            <select id="qual-sexo" name="qual-sexo" class="selected" disabled>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>

                    <div class="input-holder">
                        <label for="medicacao-oral" class="none">ACEITA ANIMAIS QUE TOMAM MEDICAÇÃO ORAL? *</label>
                        <div class="select-personalizado">
                            <span class="label big">ACEITA ANIMAIS QUE TOMAM MEDICAÇÃO ORAL? *</span>
                            <i class="seta"></i>
                            <select id="medicacao-oral" name="medicacao-oral" class="selected">
                                <option value="1">SIM</option>
                                <option value="2">NÃO</option>
                            </select>
                        </div>

                        <label for="aceita-pets-idade" class="none">ACEITA PET DE TODAS AS IDADES? </label>
                        <div class="select-personalizado">
                            <span class="label big">ACEITA PET DE TODAS AS IDADES? </span>
                            <i class="seta"></i>
                            <select id="aceita-pets-idade" name="aceita-pets-idade" class="selected">
                                <option value="1">SIM</option>
                                <option value="2">NÃO</option>
                            </select>
                        </div>

                        <label for="qual-idade" class="none">CASO OPTE PELO NÃO, QUAL IDADE ACEITA?</label>
                        <div class="select-personalizado" data-disabled>
                            <span class="label big">CASO OPTE PELO NÃO, QUAL IDADE ACEITA?</span>
                            <i class="seta"></i>
                            <select id="qual-idade" name="qual-idade" class="selected" disabled>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>

                    <label for="aceita-animal-especial" class="none">ACEITA ANIMAIS QUE REQUEREM ALGUM TIPO DE CUIDADO ESPECIAL? *</label>
                    <div class="select-personalizado select-big">
                        <span class="label">ACEITA ANIMAIS QUE REQUEREM ALGUM TIPO DE CUIDADO ESPECIAL? *</span>
                        <i class="seta"></i>
                        <select id="aceita-animal-especial" name="aceita-animal-especial" class="selected">
                            <option value="1">SIM</option>
                            <option value="2">NÃO</option>
                        </select>
                    </div>

                    <span class="desc">DIFERENCIAL:</span>

                    <div class="input-holder">
                        <label for="providencia-racao" class="none">PROVIDENCIA A RAÇÃO PARA O HÓSPEDE?</label>
                        <div class="select-personalizado">
                            <span class="label big">PROVIDÊNCIA A RAÇÃO PARA O HÓSPEDE?</span>
                            <i class="seta"></i>
                            <select id="providencia-racao" name="providencia-racao" class="selected">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>

                        <label for="possui-caminha-potes" class="none">POSSUI CAMINHA, POTES PARA O HÓSPEDE?</label>
                        <div class="select-personalizado">
                            <span class="label big">POSSUI CAMINHA, POTES PARA O HÓSPEDE?</span>
                            <i class="seta"></i>
                            <select id="possui-caminha-potes" name="possui-caminha-potes" class="selected">
                                <option value="1">SIM</option>
                                <option value="2">NÃO</option>
                            </select>
                        </div>

                        <div class="small">
                            <label for="busca-e-leva" class="none">BUSCA E LEVA O HÓSPEDE</label>
                            <div class="select-personalizado">
                                <span class="label big">BUSCA E LEVA O HÓSPEDE</span>
                                <i class="seta"></i>
                                <select id="busca-e-leva" name="busca-e-leva" class="selected">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>

                            <label for="km" class="none">KM</label>
                            <input type="text" id="km" name="km" class="form-control" placeholder="KM" disabled>
                        </div>
                    </div>

                    <div class="btn-wrapper">
                        <div class="text-container">
                            <span>Ao realizar o cadastro do seu Pet, você concorda com os <a href="termos.php">termos de uso</a> e a <a href="privacidade.php">política de privacidade</a> da DogSafe</span>
                        </div>

                        <div class="btn-content">
                            <button type="submit" class="btn-icon btn-icon-flecha">cadastrar</button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="text-sub">
                <span class="text"><i class="sprite i-pata-small"></i>Lembramos que nós da DogSafe recomendamos fortemente a carteira de vacinação atualizada, uso de anticarrapaticida e antipulga além de vacina ou coleira contra Leishmaniose para regiões afetadas.</span>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
