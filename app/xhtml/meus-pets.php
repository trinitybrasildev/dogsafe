<?php

$tituloDaPagina = "Meus Pets";
include '../include/topo.php';
?>
    <section class="form-generic meus-pets">
        <header class="title-section-container">
            <h1 class="title-section">Meus Pets</h1>
        </header>

        <p class="text-sub">Faça o cadastro ou edite as informações do seu Pet na Dog Safe</p>

        <div class="container">
            <div class="form-container" data-controller="player" data-notloadimg>

                <div class="usuario">
                    <span class="nome">Oi, <span>Diogo</span></span>
                    <span>Adicione o seu melhor amigo na Dog Safe</span>
                </div>

                <nav class="nav-generic" data-list>
                    <ul data-item>
                        <?php for($i = 0; $i < 4; $i++){ ?>
                        <li>
                            <span>Cindy 1</span>
                            <div class="options">
                                <a href="#">editar</a>
                                <a href="#">imprimir ficha</a>
                                <a href="#">excluir</a>
                            </div>
                        </li>
                        <?php } ?>

                        <li class="ultimo"><a href="#" class="btn btn-big btn-escuro">cadastrar novo pet</a></li>
                    </ul>

                    <ul data-item>
                        <?php for($i = 0; $i < 4; $i++){ ?>
                        <li>
                            <span>Cindy 2</span>
                            <div class="options">
                                <a href="#">editar</a>
                                <a href="#">imprimir ficha</a>
                                <a href="#">excluir</a>
                            </div>
                        </li>
                        <?php } ?>

                        <li class="ultimo"><a href="#" class="btn btn-big btn-escuro">cadastrar novo pet</a></li>
                    </ul>
                </nav>
                <div class="paginacao-form">
                    <span class="prev" data-prev></span>
                    <span class="next" data-next></span>
                </div>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
