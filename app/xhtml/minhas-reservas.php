<?php

$tituloDaPagina = "Minhas reservas";
include '../include/topo.php';
?>
    <section class="form-generic minhas-reservas">
        <header class="title-section-container">
            <h1 class="title-section">Minhas reservas</h1>
        </header>

        <p class="text-sub">Nesta sessão será possível acompanhar todas as reservas realizadas em nosso site</p>

        <div class="container">
            <div class="lista-wrapper">
                <div class="table-header">
                    <div class="col-data">
                        <span>Data da Reserva</span>
                    </div>
                    <div class="col-pet">
                        <span>Pet</span>
                    </div>
                    <div class="col-anfitriao">
                        <span>Anfitriao</span>
                    </div>
                    <div class="col-valor">
                        <span>Valor</span>
                    </div>
                    <div class="col-status">
                        <span>Status</span>
                    </div>
                </div>

                <div class="table-body">
                    <div class="table-row">
                        <div class="table-column-name">
                            <div class="col-data">
                                <span>Data da Reserva</span>
                            </div>
                            <div class="col-pet">
                                <span>Pet</span>
                            </div>
                            <div class="col-anfitriao">
                                <span>Anfitriao</span>
                            </div>
                            <div class="col-valor">
                                <span>Valor</span>
                            </div>
                            <div class="col-status">
                                <span>Status</span>
                            </div>
                        </div>

                        <div class="col-data">
                            <span>20/10/2016</span>
                        </div>
                        <div class="col-pet">
                            <span>Cindy</span>
                        </div>
                        <div class="col-anfitriao">
                            <span>Paula Mattos</span>
                        </div>
                        <div class="col-valor">
                            <span>R$ 79,90</span>
                        </div>
                        <div class="col-status">
                            <a href="#" class="link-red">Cancelada pelo Anfitrião</a>

                            <div class="btn-wrapper">
                                <a href="#" class="btn-link">CANCELAR</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-row">
                        <div class="table-column-name">
                            <div class="col-data">
                                <span>Data da Reserva</span>
                            </div>
                            <div class="col-pet">
                                <span>Pet</span>
                            </div>
                            <div class="col-anfitriao">
                                <span>Anfitriao</span>
                            </div>
                            <div class="col-valor">
                                <span>Valor</span>
                            </div>
                            <div class="col-status">
                                <span>Status</span>
                            </div>
                        </div>

                        <div class="col-data">
                            <span>10/10/2016</span>
                        </div>
                        <div class="col-pet">
                            <span>Peu</span>
                        </div>
                        <div class="col-anfitriao">
                            <span>Henrique Ferreira</span>
                        </div>
                        <div class="col-valor">
                            <span>R$ 49,90</span>
                        </div>
                        <div class="col-status">
                            <a href="#" class="link-blue">Hospedagem Remarcada</a>

                            <div class="btn-wrapper">
                                <a href="#" class="btn-link">CANCELAR</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-row">
                        <div class="table-column-name">
                            <div class="col-data">
                                <span>Data da Reserva</span>
                            </div>
                            <div class="col-pet">
                                <span>Pet</span>
                            </div>
                            <div class="col-anfitriao">
                                <span>Anfitriao</span>
                            </div>
                            <div class="col-valor">
                                <span>Valor</span>
                            </div>
                            <div class="col-status">
                                <span>Status</span>
                            </div>
                        </div>

                        <div class="col-data">
                            <span>08/10/2016</span>
                        </div>
                        <div class="col-pet">
                            <span>Cindy</span>
                        </div>
                        <div class="col-anfitriao">
                            <span>Paula Mattos</span>
                        </div>
                        <div class="col-valor">
                            <span>R$ 79,90</span>
                        </div>
                        <div class="col-status">
                            <a href="#" class="link-blue">Reserva Confirmada</a>

                            <div class="btn-wrapper">
                                <a href="#" class="btn-link">REMARCAR</a>
                                <a href="#" class="btn-link">CANCELAR</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-row">
                        <div class="table-column-name">
                            <div class="col-data">
                                <span>Data da Reserva</span>
                            </div>
                            <div class="col-pet">
                                <span>Pet</span>
                            </div>
                            <div class="col-anfitriao">
                                <span>Anfitriao</span>
                            </div>
                            <div class="col-valor">
                                <span>Valor</span>
                            </div>
                            <div class="col-status">
                                <span>Status</span>
                            </div>
                        </div>

                        <div class="col-data">
                            <span>08/10/2016</span>
                        </div>
                        <div class="col-pet">
                            <span>Amora</span>
                        </div>
                        <div class="col-anfitriao">
                            <span>Cláudia Rodrigues</span>
                        </div>
                        <div class="col-valor">
                            <span>R$ 69,90</span>
                        </div>
                        <div class="col-status">
                            <a href="#" class="link-blue">Hospedagem Aprovada</a>

                            <div class="btn-wrapper">
                                <a href="#" class="btn-link btn-green">PAGAR</a>
                                <a href="#" class="btn-link">CANCELAR</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-row">
                        <div class="table-column-name">
                            <div class="col-data">
                                <span>Data da Reserva</span>
                            </div>
                            <div class="col-pet">
                                <span>Pet</span>
                            </div>
                            <div class="col-anfitriao">
                                <span>Anfitriao</span>
                            </div>
                            <div class="col-valor">
                                <span>Valor</span>
                            </div>
                            <div class="col-status">
                                <span>Status</span>
                            </div>
                        </div>

                        <div class="col-data">
                            <span>08/10/2016</span>
                        </div>
                        <div class="col-pet">
                            <span>Amora</span>
                        </div>
                        <div class="col-anfitriao">
                            <span>Cláudia Rodrigues</span>
                        </div>
                        <div class="col-valor">
                            <span>R$ 59,90</span>
                        </div>
                        <div class="col-status">
                            <a href="#" class="link-grey">Cancelada pelo Hóspede</a>
                        </div>
                    </div>

                    <div class="table-row">
                        <div class="table-column-name">
                            <div class="col-data">
                                <span>Data da Reserva</span>
                            </div>
                            <div class="col-pet">
                                <span>Pet</span>
                            </div>
                            <div class="col-anfitriao">
                                <span>Anfitriao</span>
                            </div>
                            <div class="col-valor">
                                <span>Valor</span>
                            </div>
                            <div class="col-status">
                                <span>Status</span>
                            </div>
                        </div>

                        <div class="col-data">
                            <span>08/10/2016</span>
                        </div>
                        <div class="col-pet">
                            <span>Cindy, Peu</span>
                        </div>
                        <div class="col-anfitriao">
                            <span>Pedro Fisher</span>
                        </div>
                        <div class="col-valor">
                            <span>R$ 109,90</span>
                        </div>
                        <div class="col-status">
                            <a href="#" class="link-grey">Hospedagem Concluída</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="widgets-container">

                <a href="#" class="btn btn-big btn-escuro">FAZER NOVA RESERVA</a>
            </div>

            <nav class="paginacao">
                <a href="index.php" class="btn-border"><i></i>voltar</a>

                <span class="qtd"><span>Página 1</span> de 10</span>

                <div class="pag">
                    <span class="prev"></span>
                    <span class="atual">1</span>
                    <span class="next"></span>
                </div>
            </nav>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
