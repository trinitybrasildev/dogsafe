<?php

$tituloDaPagina = "Minha Conta";
include '../include/topo.php';
?>
    <section class="form-generic minha-conta-hospede">
        <header class="title-section-container">
            <h1 class="title-section">Minha Conta</h1>
        </header>

        <p class="text-sub">Aqui é o seu ambiente restrito, onde você poderá atualizar o seu perfil, cadastrar seus pets e conferir reservas realizadas pelo site.</p>

        <div class="container">
            <div class="form-container">
                <div class="usuario">
                    <span class="nome">Oi, <span>Diogo</span></span>
                    <span>Seja bem-vindo ao Dog Safe</span>
                </div>

                <nav class="nav-generic">
                    <ul>
                        <li><a href="meu-cadastro.php">Atualizar Cadastro</a></li>
                        <li><a href="#">Meu Pets</a></li>
                        <li><a href="#">Minhas Reservas</a></li>
                        <li>
                            <a href="#">Chat</a>
                            <span class="contador">1</span>
                        </li>
                        <li><a href="#" class="link-sair">Sair do sistema</a></li>
                    </ul>
                </nav>
            </div>
            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
