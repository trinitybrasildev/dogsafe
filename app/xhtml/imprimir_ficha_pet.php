<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Ficha do pet</title>
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <link rel="stylesheet" type="text/css" href="../css/style.css?<?= date('YmdHis') ?>">
        <link rel="stylesheet" type="text/css" href="../css/jquery-ui.min.css">
        <meta name="msapplication-TileColor" content="#1B8692">
        <meta name="theme-color" content="#1B8692">
        <!--[if lt IE 9]>
          <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
          <script src="../js/html5.js"></script>
        <![endif]-->
    </head>
</body>
    <section id="ficha_pet">
        <div class="container"> 
            <img class="logo" src="../img/logo-dogsafe-grande.png" alt="Dog Safe">
            <span class="titulo">Detalhes do(s) pet(s)</span>
            <?php for ($i = 1; $i <= 3; $i++) { ?>
                <div class="pet">
                    <strong>Nome</strong>: Cindy<br>
                    <strong>Tipo</strong>: Cachorro<br>
                    <strong>Idade</strong>: 7 meses<br>
                    <strong>Tamanho</strong>: Até 5 KG<br>
                    <strong>Raça</strong>: Indefinida<br>
                    <strong>Castrado</strong>: Sim<br>
                    <strong>Possui Alergia?</strong>: Sim. Ela tem alergia a poeira.<br>
                    <strong>Possui Doença Grave ou Crônica?</strong>:  Não<br>
                    <strong>Faz uso de medicação</strong>: Não<br>
                    <strong>Requer cuidado especial</strong>: Não<br>
                    <strong>Carteira de vacinação atualizada com a antirrábica?</strong>: Sim<br>
                    <strong>Fez uso recente de anticarrapaticida e antipulgas?</strong>: Sim<br>
                    <strong>Possui vacina ou fez uso de coleira de Leishmaniose?</strong>: Sim<br>
                    <strong>Fale um pouco sobre o seu cãozinho?</strong>: Ela é muito inteligente, gosta de passear e brincar.<br>
                </div>
            <?php } ?>
            <a class="btn-imprimir" href=javascript:print();>Imprimir</a>
        </div>
    </section>
</body>
</html>