<?php
$tituloDaPagina = "Minhas Agenda / Reservas";
include '../include/topo.php';
?>
<section class="form-generic minha-agenda">
    <header class="title-section-container">
        <h1 class="title-section">MEUS COMPROMISSOS</h1>
    </header>

    <p class="text-sub">
        Nesta sessão você poderá acompanhar todos os seus compromissos pessoais
    </p>

    <div class="container">

        <div class="bloco-calendario">
            <div class="box-agenda">
                <span class="title-page">GERENCIE SUA AGENDA</span>

                <div class="calendario-wrapper">
                    <div class="calendario" id="datepicker"></div>
                </div>

                <div class="wrapper">
                    <span class="disponivel">Anfitrião disponível</span>
                    <span class="indisponivel">Anfitrião indisponível</span>
                </div>
            </div>
            <div class="box-form">
                <span class="title-page">CADASTRE UM  COMPROMISSO</span>

                <form action="">
                    <label for="titulo" class="none">Título</label>
                    <input type="text" id="titulo" name="titulo" placeholder="Título" class="form-control">

                    <label for="data-evento" class="none">Data do Evento</label>
                    <div class="input-icon input-icon-calendar">
                        <input type="text" id="data-evento" name="data-evento" placeholder="Data do Evento" class="form-control" data-datepicker>
                    </div>

                    <div class="horarios-wrapper">
                        <label for="" class="none">Horário</label>
                        <input type="text" id="" name="" placeholder="Horário" class="form-control">

                        <label for="" class="none">Horário</label>
                        <input type="text" id="" name="" placeholder="Horário" class="form-control flag">
                    </div>

                    <button type="submit" class="btn-icon btn-icon-flecha">cadastrar</button>
                </form>
            </div>
        </div>

        <span class="title-page">RESERVAS DO SITE</span>

        <div class="lista-wrapper">
            <div class="table-header">
                <div class="col-data">
                    <span>Data do Compromisso</span>
                </div>
                <div class="col-pet">
                    <span>Pet</span>
                </div>
                <div class="col-hospede">
                    <span>Hospede</span>
                </div>
                <div class="col-valor">
                    <span>Valor</span>
                </div>
                <div class="col-status">
                    <span>Status</span>
                </div>
                <div class="col-info">
                    <span>Info</span>
                </div>
            </div>

            <div class="table-body">
                <div class="table-row">
                    <div class="table-column-name">
                        <div class="col-data">
                            <span>Data do Compromisso</span>
                        </div>
                        <div class="col-pet">
                            <span>Pet</span>
                        </div>
                        <div class="col-hospede">
                            <span>Hospede</span>
                        </div>
                        <div class="col-valor">
                            <span>Valor</span>
                        </div>
                        <div class="col-status">
                            <span>Status</span>
                        </div>
                        <div class="col-info">
                            <span>Info</span>
                        </div>
                    </div>

                    <div class="col-data">
                        <span>20/10/2016</span>
                    </div>
                    <div class="col-pet">
                        <span>Cindy</span>
                    </div>
                    <div class="col-hospede">
                        <span>Paula Mattos</span>
                    </div>
                    <div class="col-valor">
                        <span>R$ 79,90</span>
                    </div>
                    <div class="col-status">
                        <a href="#" class="cancelada">Cancelada pelo Anfitrião</a>
                    </div>
                    <div class="col-info">
                        <span class="btn btn-big btn-add"></span>
                    </div>

                    <div class="detalhes-pet">
                        <span class="titulo">Detalhes do pet</span>

                        <?php for ($i = 1; $i <= 3; $i++) { ?>
                            <div class="info-wrapper">
                                <div class="wrapper">
                                    <span class="strong">Nome: </span>Cindy
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Tipo: </span>Cachorro
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Idade: </span>7 meses
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Tamanho: </span>Até 5 KG
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Raça: </span>Indefinida
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Castrado: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Possui Alergia?: </span>Sim. Ela tem alergia a poeira.
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Possui Doença Grave ou Crônica?:  </span>Não
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Faz uso de medicação: </span>Não
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Requer cuidado especial: </span>Não
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Carteira de vacinação atualizada com a antirrábica?: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Fez uso recente de anticarrapaticida e antipulgas?: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Possui vacina ou fez uso de coleira de Leishmaniose?: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Fale um pouco sobre o seu cãozinho?: </span>Ela é muito inteligente, gosta de passear e brincar.
                                </div>
                            </div>
                        <?php } ?>

                        <div class="horarios">
                            <div class="wrapper">
                                <span class="strong">Horário de Entrega do Pet(s) ao Anfitrião: </span>08:00
                            </div>
                            <div class="wrapper">
                                <span class="strong">Horário de Busca do Pet(s) pelo Hóspede: </span>09:00
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-row">
                    <div class="table-column-name">
                        <div class="col-data">
                            <span>Data do Compromisso</span>
                        </div>
                        <div class="col-pet">
                            <span>Pet</span>
                        </div>
                        <div class="col-hospede">
                            <span>Hospede</span>
                        </div>
                        <div class="col-valor">
                            <span>Valor</span>
                        </div>
                        <div class="col-status">
                            <span>Status</span>
                        </div>
                        <div class="col-info">
                            <span>Info</span>
                        </div>
                    </div>

                    <div class="col-data">
                        <span>20/10/2016</span>
                    </div>
                    <div class="col-pet">
                        <span>Cindy</span>
                    </div>
                    <div class="col-hospede">
                        <span>Paula Mattos</span>
                    </div>
                    <div class="col-valor">
                        <span>R$ 49,90</span>
                    </div>
                    <div class="col-status">
                        <a href="#" class="confirmar">Confirmar Reserva</a>
                    </div>
                    <div class="col-info">
                        <span class="btn btn-big btn-add"></span>
                    </div>
                </div>

                <div class="table-row">
                    <div class="table-column-name">
                        <div class="col-data">
                            <span>Data do Compromisso</span>
                        </div>
                        <div class="col-pet">
                            <span>Pet</span>
                        </div>
                        <div class="col-hospede">
                            <span>Hospede</span>
                        </div>
                        <div class="col-valor">
                            <span>Valor</span>
                        </div>
                        <div class="col-status">
                            <span>Status</span>
                        </div>
                        <div class="col-info">
                            <span>Info</span>
                        </div>
                    </div>

                    <div class="col-data">
                        <span>20/10/2016</span>
                    </div>
                    <div class="col-pet">
                        <span>Cindy</span>
                    </div>
                    <div class="col-hospede">
                        <span>Paula Mattos</span>
                    </div>
                    <div class="col-valor">
                        <span>R$ 179,90</span>
                    </div>
                    <div class="col-status">
                        <a href="#" class="aguardando">Aguardando Pagamento</a>
                    </div>
                    <div class="col-info">
                        <span class="btn btn-big btn-add"></span>
                    </div>

                    <div class="detalhes-pet">
                        <span class="titulo">Detalhes do pet</span>

                        <?php for ($i = 1; $i <= 3; $i++) { ?>
                            <div class="info-wrapper">
                                <div class="wrapper">
                                    <span class="strong">Nome: </span>Cindy
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Tipo: </span>Cachorro
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Idade: </span>7 meses
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Tamanho: </span>Até 5 KG
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Raça: </span>Indefinida
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Castrado: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Possui Alergia?: </span>Sim. Ela tem alergia a poeira.
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Possui Doença Grave ou Crônica?:  </span>Não
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Faz uso de medicação: </span>Não
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Requer cuidado especial: </span>Não
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Carteira de vacinação atualizada com a antirrábica?: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Fez uso recente de anticarrapaticida e antipulgas?: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Possui vacina ou fez uso de coleira de Leishmaniose?: </span>Sim
                                </div>
                                <div class="wrapper">
                                    <span class="strong">Fale um pouco sobre o seu cãozinho?: </span>Ela é muito inteligente, gosta de passear e brincar.
                                </div>
                            </div>
                        <?php } ?>

                        <div class="horarios">
                            <div class="wrapper">
                                <span class="strong">Horário de Entrega do Pet(s) ao Anfitrião: </span>08:00
                            </div>
                            <div class="wrapper">
                                <span class="strong">Horário de Busca do Pet(s) pelo Hóspede: </span>09:00
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-row">
                    <div class="table-column-name">
                        <div class="col-data">
                            <span>Data do Compromisso</span>
                        </div>
                        <div class="col-pet">
                            <span>Pet</span>
                        </div>
                        <div class="col-hospede">
                            <span>Hospede</span>
                        </div>
                        <div class="col-valor">
                            <span>Valor</span>
                        </div>
                        <div class="col-status">
                            <span>Status</span>
                        </div>
                        <div class="col-info">
                            <span>Info</span>
                        </div>
                    </div>

                    <div class="col-data">
                        <span>20/10/2016</span>
                    </div>
                    <div class="col-pet">
                        <span>Cindy</span>
                    </div>
                    <div class="col-hospede">
                        <span>Paula Mattos</span>
                    </div>
                    <div class="col-valor">
                        <span>R$ 179,90</span>
                    </div>
                    <div class="col-status">
                        <a href="#" class="remarcada">Hospedagem Remarcada</a>
                    </div>
                    <div class="col-info">
                        <span class="btn btn-big btn-add"></span>
                    </div>
                </div>

                <div class="table-row">
                    <div class="table-column-name">
                        <div class="col-data">
                            <span>Data do Compromisso</span>
                        </div>
                        <div class="col-pet">
                            <span>Pet</span>
                        </div>
                        <div class="col-hospede">
                            <span>Hospede</span>
                        </div>
                        <div class="col-valor">
                            <span>Valor</span>
                        </div>
                        <div class="col-status">
                            <span>Status</span>
                        </div>
                        <div class="col-info">
                            <span>Info</span>
                        </div>
                    </div>

                    <div class="col-data">
                        <span>20/10/2016</span>
                    </div>
                    <div class="col-pet">
                        <span>Cindy</span>
                    </div>
                    <div class="col-hospede">
                        <span>Paula Mattos</span>
                    </div>
                    <div class="col-valor">
                        <span>R$ 49,90</span>
                    </div>
                    <div class="col-status">
                        <a href="#" class="recusar">Recusar Reserva</a>
                    </div>
                    <div class="col-info">
                        <span class="btn btn-big btn-add"></span>
                    </div>
                </div>

                <div class="table-row">
                    <div class="table-column-name">
                        <div class="col-data">
                            <span>Data do Compromisso</span>
                        </div>
                        <div class="col-pet">
                            <span>Pet</span>
                        </div>
                        <div class="col-hospede">
                            <span>Hospede</span>
                        </div>
                        <div class="col-valor">
                            <span>Valor</span>
                        </div>
                        <div class="col-status">
                            <span>Status</span>
                        </div>
                        <div class="col-info">
                            <span>Info</span>
                        </div>
                    </div>

                    <div class="col-data">
                        <span>20/10/2016</span>
                    </div>
                    <div class="col-pet">
                        <span>Cindy</span>
                    </div>
                    <div class="col-hospede">
                        <span>Paula Mattos</span>
                    </div>
                    <div class="col-valor">
                        <span>R$ 49,90</span>
                    </div>
                    <div class="col-status">
                        <a href="#" class="concluida">Hospedagem Concluída</a>
                    </div>
                    <div class="col-info">
                        <span class="btn btn-big btn-add"></span>
                    </div>
                </div>

                <div class="table-row">
                    <div class="table-column-name">
                        <div class="col-data">
                            <span>Data do Compromisso</span>
                        </div>
                        <div class="col-pet">
                            <span>Pet</span>
                        </div>
                        <div class="col-hospede">
                            <span>Hospede</span>
                        </div>
                        <div class="col-valor">
                            <span>Valor</span>
                        </div>
                        <div class="col-status">
                            <span>Status</span>
                        </div>
                        <div class="col-info">
                            <span>Info</span>
                        </div>
                    </div>

                    <div class="col-data">
                        <span>20/10/2016</span>
                    </div>
                    <div class="col-pet">
                        <span>Cindy</span>
                    </div>
                    <div class="col-hospede">
                        <span>Paula Mattos</span>
                    </div>
                    <div class="col-valor">
                        <span>R$ 49,90</span>
                    </div>
                    <div class="col-status">
                        <a href="#" class="confirmada">Reserva Confirmada</a>
                    </div>
                    <div class="col-info">
                        <span class="btn btn-big btn-add"></span>
                    </div>
                </div>
            </div>
        </div>

        <nav class="paginacao">
            <a href="index.php" class="btn-border"><i></i>voltar</a>

            <span class="qtd"><span>Página 1</span> de 10</span>

            <div class="pag">
                <span class="prev"></span>
                <input type="text" class="atual" value="1">
                <span class="next"></span>
            </div>
        </nav>
    </div>
</section>

<?php include '../include/footer.php' ?>
