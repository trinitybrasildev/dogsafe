<?php

$tituloDaPagina = "";
include '../include/topo.php';
?>
    <section class="anfitriao-interna">
        <header class="header-wrapper">
            <div class="container">

                <a href="#" class="btn-icon btn-icon-fale">FALE COM O ANFITRIÃO</a>

                <div class="text-wrapper">
                    <h1 class="h-title">CAROL</h1>
                    <span class="tipo">Hospedagem e DayCare</span>
                </div>

                <div class="patas-wrapper">
                    <ul class="patas">
                        <li class="pata active"></li>
                        <li class="pata active"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                    </ul>
                    <span>27 Avaliações</span>
                </div>
            </div>
        </header>

        <div class="container" data-controller="carrossel">
            <div class="galeria">
                <div class="foto-maior">
                    <a href="#">
                        <img src="../img/galeria/foto-maior.jpg" alt="">
                    </a>
                </div>
                <div class="carrossel" data-carrossel>
                    <span class="i-wrapper" data-prev>
                        <img src="../img/galeria/prev.png" alt="">
                    </span>
                    <div class="wrapper">
                        <a href="#" class="imagem-wrapper">
                            <img src="../img/galeria/foto-maior.jpg" alt="">
                        </a>
                        <a href="#" class="imagem-wrapper">
                            <img src="../img/galeria/02.jpg" alt="">
                        </a>
                        <a href="#" class="imagem-wrapper">
                            <img src="../img/galeria/03.jpg" alt="">
                        </a>
                        <a href="#" class="imagem-wrapper">
                            <img src="../img/galeria/04.jpg" alt="">
                        </a>
                        <a href="#" class="imagem-wrapper">
                            <img src="../img/galeria/04.jpg" alt="">
                        </a>
                        <a href="#" class="imagem-wrapper">
                            <img src="../img/galeria/04.jpg" alt="">
                        </a>
                        <a href="#" class="imagem-wrapper">
                            <img src="../img/galeria/04.jpg" alt="">
                        </a>
                    </div>
                    <span class="i-wrapper" data-next>
                        <img src="../img/galeria/next.png" alt="">
                    </span>
                </div>
            </div>

            <form action="" class="form-wrapper none">
                <div class="input-holder">
                    <label for="data-inicio" class="none">DATA INÍCIO</label>
                    <div class="input-icon input-icon-calendar">
                        <input type="text" id="data-inicio" name="data-inicio" placeholder="DATA INÍCIO" class="input input-medium" data-datepicker>
                    </div>

                    <label for="data-fim" class="none">DATA FIM</label>
                    <div class="input-icon input-icon-calendar">
                        <input type="text" id="data-fim" name="data-fim" placeholder="DATA FIM" class="input input-medium" data-datepicker>
                    </div>
                </div>

                <div class="input-holder">
                    <div class="box-price">
                        <span class="preco">R$ <span>70</span></span>
                        <span class="periodo">POR NOITE</span>
                    </div>
                    <div class="box">
                        <button type="submit" class="btn btn-extra-big btn-danger">RESERVAR</button>

                        <a href="#" class="text" data-click>Agendar Visita</a>
                    </div>
                </div>
            </form>
            
             <form action="" class="form-wrapper agendar">
                <div class="input-holder">
                    <label for="data-inicio" class="none">DATA INÍCIO</label>
                    <div class="input-icon input-icon-calendar">
                        <input type="text" id="data-inicio" name="data-inicio" placeholder="DATA INÍCIO" class="input input-medium" data-datepicker>
                    </div>
                </div>

                <div class="input-holder">
                    <div class="box">
                        <button type="submit" class="btn btn-extra-big btn-danger">Agendar</button>

                        <a href="#" class="text" data-click>Reservar</a>
                    </div>
                </div>
            </form>

            <div class="reserva-wrapper">
                <div class="garantia">
                    Fique tranquilo! Todas as hospedagens feitas pela DogSafe estão cobertas por nossa Garantia Veterinária de até R$5.000
                </div>

                <span class="titulo">Perfil do anfitrião e ambiente</span>

                <div class="perfil-wrapper">
                    <span class="topico casa">Ambiente: Casa</span>
                    <span class="topico olho">Supervisão 24h</span>
                    <span class="topico cachorro">Atende até 3 hóspedes</span>
                    <span class="topico pilula">Administra medicamento oral: Sim</span>
                    <span class="topico cancelamento">Cancelamento: Moderado</span>
                </div>
            </div>

            <div class="text-content">
                <span class="titulo">Sobre o Anfitrião</span> Hospedo em minha casa!<br> Cães de forma livre com muito espaço e conforto. Não faço uso de gaiolas...seu cãozinho vai ficar livre e solto pela casa. <br><br> Tenho um grande gramado do qual o cãozinho pode fazer uso. Tenho experiência de hospedagem devido a minha profissão: sou médica veterinária com 28 anos de formação!
                <br><br> Hospedo apenas um cliente por vez, com dia e hora marcada! Envio fotos dos meus hospedes e do local para os interessados em deixar seu pet!
                <br><br> Ofereço total segurança e carinho! Minha cachorrinha Mel é uma excelente companheira! Meus dois filhos moram comigo, amam animais e vice-versa! Nós todos aqui, amamos animais! rs
            </div>

            <div class="info-content">
                <div class="bloco">
                    <span class="titulo">Calendário</span>

                    <div class="box-agenda">
                        <div class="calendario-wrapper">
                            <div class="calendario" id="datepicker"></div>
                        </div>

                        <div class="wrapper">
                            <span class="disponivel">Anfitrião disponível</span>
                            <span class="indisponivel">Anfitrião indisponível</span>
                        </div>
                    </div>
                </div>

                <div class="bloco">
                    <span class="titulo">Preferência de Hospedagem</span>

                    <div class="hospedagem-icons">
                        <div class="item">
                            <div class="img-wrapper">
                                <img src="../img/categorias-animais/ate5kg.png" alt="">
                            </div>
                            <span>Até 5 KG</span>
                            <i class="sprite i-sim"></i>
                        </div>

                        <div class="item">
                            <div class="img-wrapper">
                                <img src="../img/categorias-animais/5kg-10kg.png" alt="">
                            </div>
                            <span>5-10 KG</span>
                            <i class="sprite i-sim"></i>
                        </div>

                        <div class="item">
                            <div class="img-wrapper">
                                <img src="../img/categorias-animais/10kg-20kg.png" alt="">
                            </div>
                            <span>10-20 KG</span>
                            <i class="sprite i-nao"></i>
                        </div>

                        <div class="item">
                            <div class="img-wrapper">
                                <img src="../img/categorias-animais/mais30kg.png" alt="">
                            </div>
                            <span>+ de 30 KG</span>
                            <i class="sprite i-nao"></i>
                        </div>
                    </div>

                    <div class="hospedagem-text">
                        - Não aceita fêmeas;<br> - Não aceita animais que tomam medicamentos orais;<br> - Aceita animais de todas as idades;<br> - Não aceita animais que requerem algum tipo de cuidado especial;<br>
                    </div>

                    <div class="diferenciais-text">
                        <span class="titulo">Diferenciais do Anfitrião</span> - Providencia a ração para o hóspede;<br> - Não possui caminha, potes para o hóspede;<br> - Busca e leva o hóspede em um raio de até 10km.
                    </div>

                    <div class="pets-anfitriao">
                        <span class="titulo">Pets do Anfitrião</span> - 3 pet, sendo 2 Machos (com até 5kg e outro de 5-10kg) / 1 Fêmea (com até 5kg)
                    </div>
                </div>
            </div>

            <div class="regiao-anfitriao">
                <span class="titulo">Região do Anfitrião</span>

                <div class="mapa-wrapper">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14952.874376561536!2d-54.5950143!3d-20.4562131!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3a7c7e5c5f8b3e4b!2sTrinity+Brasil+%E2%80%93+Cria%C3%A7%C3%A3o+e+desenvolvimento+de+sites+em+Campo+Grande-MS!5e0!3m2!1spt-BR!2sbr!4v1480965762313" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>

            <form class="form-comentario">
                <span class="titulo">Deixe o seu comentário</span>

                <div class="input-wrapper">
                    <label for="comentario" class="none">Senha</label>
                    <textarea type="text" id="comentario" name="comentario" class="" placeholder="Escreva aqui o seu comentário sobre o anfitrião"></textarea>

                    <div class="votacao-wrapper">
                        <span>Avalie este anfitrião</span>
                        <ul class="patas">
                            <li class="pata active"></li>
                            <li class="pata active"></li>
                            <li class="pata"></li>
                            <li class="pata"></li>
                            <li class="pata"></li>
                        </ul>
                    </div>
                </div>
                <span class="aviso">* Você só poderá comentar e avaliar, caso utilize os serviços deste anfitrião.</span>
                <button type="submit" class="btn-icon btn-icon-flecha">COMENTAR</button>
            </form>

            <div class="comentarios">
                <span class="titulo">Comentários (21)</span>

                <div class="comentario">
                    <span class="nome">Rhobson Borges</span>
                    <ul class="patas">
                        <li class="pata active"></li>
                        <li class="pata active"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                    </ul>

                    <div class="comentario-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>

                <div class="comentario">
                    <span class="nome">Rhobson Borges</span>
                    <ul class="patas">
                        <li class="pata active"></li>
                        <li class="pata active"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                    </ul>

                    <div class="comentario-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>

                <div class="comentario">
                    <span class="nome">Rhobson Borges</span>
                    <ul class="patas">
                        <li class="pata active"></li>
                        <li class="pata active"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                        <li class="pata"></li>
                    </ul>

                    <div class="comentario-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>

                <div class="icones-compartilhar">
                    <span>COMPARTILHAR</span>
                    <a href="#" class="sprite i-btn-whats"></a>

                    <div class="share-button share-facebook">
                        <a href="http://www.facebook.com/" title="Compartilhar via Facebook" target="_blank" rel="external">
                            <div class="sprite i-btn-facebook"> </div>
                            <span>Facebook</span>
                        </a>
                    </div>

                    <div class="share-button share-twitter">
                        <a href="http://www.facebook.com/" title="Compartilhar via Facebook" target="_blank" rel="external">
                            <div class="sprite i-btn-twitter"> </div>
                            <span>Twitter</span>
                        </a>
                    </div>

                    <div class="share-button share-googleplus">
                        <a href="http://www.facebook.com/" title="Compartilhar via Facebook" target="_blank" rel="external">
                            <div class="sprite i-btn-googleplus"> </div>
                            <span>Google+</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
