<?php

$tituloDaPagina = "Dog Safe";
include '../include/topo.php';
?>

    <section class="home-container">
        <div class="img-fundo" style="background-image: url('../../img/home-banner/04.jpg');"></div>

        <div class="home">
            <h1 class="title title-extra-big">encontre um anfitrião <span class="text-destaque text-destaque-big text-destaque-claro"><strong>próximo</strong> de você</span></h1>

            <form class="form-banner container">
                <label for="boarding" disabled class="hide">Boarding</label>
                <div class="select-personalizado">
                    <span class="label">Boarding</span>
                    <i class="seta"></i>
                    <select id="boarging" name="boarging" placeholder="BOARDING" class="selected">
                    <option selected disabled class="primeiro">Boarding</option>
                    <option value="1">Formação 1</option>
                    <option value="2">Formação 2</option>
                    <option value="3">Formação 3</option>
                </select>
                </div>

                <label for="cep" disabled class="hide">Boarding</label>
                <input type="text" id="cep" name="cep" placeholder="cep" class="input input-medium">

                <label for="data-inicio" disabled class="hide">Boarding</label>
                <div class="input-icon input-icon-calendar">
                    <input type="text" id="data-inicio" name="data-inicio" placeholder="DATA INÍCIO" class="input input-medium" data-datepicker>
                </div>

                <label for="data-fim" disabled class="hide">Boarding</label>
                <div class="input-icon input-icon-calendar">
                    <input type="text" id="data-fim" name="data-fim" placeholder="DATA FIM" class="input input-medium" data-datepicker>
                </div>

                <button type="submit" class="btn btn-extra-big btn-danger">encontrar um anfitrião</button>
            </form>
        </div>
    </section>

    <section class="home-como-funciona">
        <h2 class="title title-big title-escuro">como funciona</h2>
        <p class="text-sub">Com DogSafe o seu melhor amigo estará em boas mãos. Saiba como funciona:</p>

        <section class="como-funciona">
            <article class="descricao">
                <a href="#" class="imagem">
                    <img src="../img/como-funciona01.png">
                    <i class="sprite passo-um"></i>
                </a>

                <h3 class="title title-small title-escuro">ENCONTRE UM ANFITRIÃO PARA O SEU ANIMALZINHO</h3>
            </article>

            <article class="descricao">
                <a href="#" class="imagem">
                    <img src="../img/como-funciona02.png">
                    <i class="sprite passo-dois"></i>
                </a>

                <h3 class="title title-small title-escuro">ESCOLHA O ANFITRIÃO E AGENDE ONLINE</h3>
            </article>

            <article class="descricao">
                <a href="#" class="imagem">
                    <img src="../img/como-funciona03.png">
                    <i class="sprite passo-tres"></i>
                </a>

                <h3 class="title title-small title-escuro">SEU MELHOR AMIGO <br>EM BOAS MÃOS</h3>
            </article>
        </section>

        <section class="cao-seguro">
            <a href="#" class="imagem">
                <img src="../img/seu-cao-seguro.png">
                <i class="i-selo"></i>
            </a>

            <div class="detalhes">
                <span class="title-bigger">Seu cão <strong>seguro</strong></span>
                <div class="detalhes-container">
                    <div class="text-container">
                        <div class="i-container">
                            <i class="sprite i-pata"></i>
                        </div>
                        <p class="text-sub">Seguro veterinário de até R$ 5 mil;</p>
                    </div>

                    <div class="text-container">
                        <div class="i-container">
                            <i class="sprite i-calendario"></i>
                        </div>
                        <p class="text-sub">Nosso site possibilita o agendamento de uma pré-visita aos anfitriões;</p>
                    </div>

                    <div class="text-container">
                        <div class="i-container">
                            <i class="sprite i-caderno"></i>
                        </div>
                        <p class="text-sub">Os anfitriões possam por um processo seletivo;</p>
                    </div>

                    <div class="text-container">
                        <div class="i-container">
                            <i class="sprite i-celular"></i>
                        </div>
                        <p class="text-sub">Atendimento exclusivo por e-mail ou telefone.</p>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <section class="home-depoimentos container">
        <h2 class="title title-big title-escuro">DEPOIMENTOS</h2>
        <p class="text-sub">Confira abaixo os depoimentos de quem confiou e aprovou os nossos serviços.</p>

        <section class="depoimentos text-sub text-sub-branco">
            <article class="depoimento depoimento-principal">
                <div class="i-mensagem-container">
                    <i class="sprite i-mensagem"></i>
                </div>
                <div class="text-wrapper">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    <br><br>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</div>

                <div class="autor-container">
                    <a href="#" class="imagem-autor">
                        <img src="../img/depoimentos-fundo.jpg">
                    </a>
                    <span class="text-sub text-sub-branco text-sub-small">LUAN FREITAS</span>
                </div>
            </article>

            <article class="depoimento">
                <div class="autor-container">
                    <a href="#" class="imagem-autor">
                        <img src="../img/autor02.png">
                    </a>
                    <span class="text-sub text-sub-branco text-sub-small">LUIZA OLIVEIRA</span>
                </div>

                <div class="i-mensagem-container">
                    <i class="sprite i-mensagem"></i>
                </div>
                <div class="text-wrapper">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
            </article>

            <article class="depoimento ultimo">
                <div class="autor-container">
                    <a href="#" class="imagem-autor">
                        <img src="../img/autor03.png">
                    </a>
                    <span class="text-sub text-sub-branco text-sub-small">PAULA GOMES</span>
                </div>

                <div class="i-mensagem-container">
                    <i class="sprite i-mensagem"></i>
                </div>
                <div class="text-wrapper">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
            </article>
        </section>
    </section>

    <?php include '../include/footer.php' ?>
