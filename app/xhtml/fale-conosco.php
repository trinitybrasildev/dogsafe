<?php

$tituloDaPagina = "Fale conosco";
include '../include/topo.php';
?>
    <section class="form-generic">
        <header class="title-section-container">
            <h1 class="title-section">Fale conosco</h1>
        </header>

        <p class="text-sub">Preencha o formulário abaixo e entre em contato conosco. Será um prazer atendê-lo(a).</p>

        <div class="container">
            <div class="form-container fale-conosco">
                <form action="">
                    <div class="input-wrapper">
                        <div class="input-holder">
                            <label for="nome" class="none">Nome</label>
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome*">

                            <label for="email" class="none">E-mail</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="E-mail*">

                            <label for="telefone" class="none">Telefone</label>
                            <input type="text" id="telefone" name="telefone" class="form-control" placeholder="Telefone*">
                        </div>

                        <div class="input-holder">
                            <label for="mensagem" class="none">Mensagem</label>
                            <textarea class="form-control" id="mensagem" name="mensagem" type="text" placeholder="Mensagem*"></textarea>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="holder">
                            <span class="aviso">* Campos Obrigatórios</span>
                            <button type="submit" class="btn-icon btn-icon-flecha">Enviar</button>
                        </div>
                        
                        <div class="holder">
                            <a href="tel:+556733333333" class="telefone">(11) 3333.33333</a>
                            <a href="#" class="email">ATENDIMENTO@DOGSAFE.COM.BR</a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="widgets-container">
                <a href="index.php" class="btn-border">voltar</a>
            </div>
        </div>
    </section>

    <?php include '../include/footer.php' ?>
