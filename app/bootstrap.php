<?php

$ip = NULL;

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

define('USER_IP', $ip);

if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|face|google|yahoo|msn/i', $_SERVER['HTTP_USER_AGENT'])) {
    
    define('GOOBOT', true);
} else {
    
    define('GOOBOT', false);
}

if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    //$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    //header('HTTP/1.1 301 Moved Permanently');
    //header('Location: ' . $redirect);
    //exit();
}

$_config_all = include 'configuracao.php';

//START
ob_start();

ini_set("display_errors", $_config_all['php']['display_errors']);
error_reporting($_config_all['php']['error_reporting']);

@session_start();

use Codando\App,
    Carbon\Carbon;

// Diz para o PHP que estamos usando strings UTF-8 até o final do script
@mb_internal_encoding('UTF-8');

// Diz para o PHP que nós vamos enviar uma saída UTF-8 para o navegador
@mb_http_output('UTF-8');
	
ini_set('default_charset', $_config_all['php']['default_charset']);

foreach ((array)$_config_all['header'] as $h => $v) @header($h. ":" . $v);

if (!defined('COD_DIR_ROOT')) {
	
    define('COD_URL', $_config_all['dominio']['root']);
    define('COD_DIR_ROOT', str_replace('\\', '/', dirname(dirname(__FILE__))));
    define('COD_DIR_APP', COD_DIR_ROOT . '/app');
    define('COD_DIR_VENDOR', COD_DIR_ROOT . '/vendor');
    define('SESSION_PATH', session_save_path());
    
    define('GPOR_SITE', 25);//Porcentagem dos ganhos do site
    define('GPOR_SITE_REEM', 11);//Porcentagem dos ganhos do site em reembolso
    
    define('ANF_ENTR', 19);//Anfitriao que realiza as entrevistas, a Patricia
    
    require_once(COD_DIR_VENDOR . '/autoload.php');
    require_once(COD_DIR_VENDOR . '/Codando/helps/functions.php');
    
    // If the IP is in the blocklist, send a 403 Forbidden header
    if(blocklist()) {
        header("HTTP/1.1 403 Forbidden");
        exit(); // We're done here
    }
    
    /* @var $app \Codando\App */
    $app = App::getInstance();
	
	//Carrega
    $app::getConfig();
	
	//Carrega
    $app::getModels();

    $_config_site = $app::getConfig('dominio');

    define('COD_APP_CDN', $_config_all['dominio']['cdn']);
    define('COD_APP_404_IMG', $_config_all['dominio']['notfound_image']);
    define('COD_SESSIONAME', $_config_all['dominio']['session_name']);
    
    define('COD_APP_MOBILE', FALSE);
	
    if(function_exists("set_time_limit") == TRUE AND @ ini_get("safe_mode") == 0){
        @set_time_limit(100);
    }

    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');//Horario oficial de brasilia/DF
    
    \Carbon\Carbon::setLocale('pt_BR');
    
}