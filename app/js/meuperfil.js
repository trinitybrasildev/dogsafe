define(function () {

    var self, tplAnimal;

    function modalidade() {

        var val = ~~self.find('select#modalidade').val();

        if (val == 1) {
            self.find('#valordiariadaycare').val('').attr('disabled', 'disabled');
            self.find('#valordiariahospedagem').removeAttr('disabled');
        }

        if (val == 2) {
            self.find('#valordiariahospedagem').val('').attr('disabled', 'disabled');
            self.find('#valordiariadaycare').removeAttr('disabled');
        }

        if (val == 3) {
            self.find('#valordiariahospedagem, #valordiariadaycare').removeAttr('disabled');
        }
    }

    function buscaleva() {

        var val = ~~self.find('#buscaleva').val();

        if (val == 1) {
            self.find('#buscakm').removeAttr('disabled');
        }

        if (val == 2) {
            self.find('#buscakm').val('').attr('disabled', 'disabled');
        }
    }

    function qtdanimais() {

        var val = ~~self.find('#qtdanimais').val();
        var ex = $("#animais").children('.input-wrapper').length;

        if (val >= 1)
            $("#animais").removeClass('hide');

        if (val >= 1 && val > ex) {

            var sval = val - ex;


            for (var iv = 0; iv < sval; iv++) {
                ex = $("#animais").children('.input-wrapper').length + 1;
                var ctplAnimal = tplAnimal.clone();
                ctplAnimal.removeClass('tmpl');
                ctplAnimal.find('.posicao').val(ex + 'º');
                $("#animais").append(ctplAnimal);
            }
        }

        if (val == 0) {
            $("#animais").addClass('hide').find('.input-wrapper').remove();
        }

    }

    return function () {

        self = $(this);

        tplAnimal = self.find('#animais .input-wrapper.tmpl').clone();
        tplAnimal.removeClass('tmpl');
        self.find('#animais .tmpl').remove();

        self.find('select#modalidade').change(modalidade).change();

        self.find('select#cancelamento').change(function () {
            var title = $(this).find('option:selected').attr('data-title');
            var info = $(this).parent().find('.info-icon');
            info.removeAttr('title', title);
            if ($.fn.tooltipster){
                info.tooltipster();
                info.tooltipster('content', title);
 
            }
        }).change();

        self.find('#buscaleva').change(buscaleva);
        self.find('#qtdanimais').change(qtdanimais);
        buscaleva();
        qtdanimais();

//        window.createCSSSelector('.chosen-container-multi .chosen-choices', ' border: none !important; background-color: #ddd;');

    };
});