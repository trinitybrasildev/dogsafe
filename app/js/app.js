define('app', ['smoothscroll', 'localStorage'], function () {

    var jQuery = $ = require('jquery'),
            el = {
                datasrc: jQuery("img[data-original]"),
                fancy: jQuery("a[data-rel^='fancybox'],a[data-rel].fancybox"),
                load: jQuery("[data-controller]")
            },
            init = {};

    init.geolocation = function () {

        var geocoder;

        $("#geolocation").one('click', function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
        });

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(showPosition);
        } else {

            //console.log("Geolocation is not supported by this browser.");
        }

        function showPosition(position) {

            if ($("input[name='latlong']")) {
                $("input[name='latlong']").val(position.coords.latitude + ',' + position.coords.longitude);
            }

            if (store('cidadegeocode') !== true) {
                require(['mapsgoogle'], function () {
                    geocoder = new google.maps.Geocoder();
                    codeLatLng(position.coords.latitude, position.coords.longitude);
                });
            }

            if (localStorage) {

                store('lat', position.coords.latitude);
                store('long', position.coords.longitude);

                store('latlong', position.coords.latitude + ',' + position.coords.longitude);
            }

        }

        function codeLatLng(lat, lng) {

            var latlng = new google.maps.LatLng(lat, lng);

            geocoder.geocode({'latLng': latlng}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    ///console.log(results)

                    if (results[1]) {

                        //console.log(results[0].formatted_address);

                        var uf, city, cep;

                        for (var i = 0; i < results[0].address_components.length; i++) {

                            for (var b = 0; b < results[0].address_components[i].types.length; b++) {

                                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {

                                    uf = results[0].address_components[i];
                                    break;
                                }

                                if (results[0].address_components[i].types[b] == "administrative_area_level_2") {

                                    city = results[0].address_components[i];
                                    break;
                                }

                                if (results[0].address_components[i].types[b] == "postal_code") {

                                    cep = results[0].address_components[i];
                                    break;
                                }
                            }
                        }

                        $.post('/cidadegeocode', {'estado': uf.short_name, 'cidade': city.short_name, 'cep': cep.short_name});
                        store('cidadegeocode', true);
                        store('uf', uf.short_name);
                        store('city', city.short_name);
                        store('cep', cep.short_name);

                        //console.log(uf.short_name);
                        //console.log(city.short_name);
                        //console.log(cep.short_name);



                    } else {
                        //alert("No results found");
                    }
                } else {

                    //alert("Geocoder failed due to: " + status);
                }
            });
        }

    };

    init.data = function () {

        if (el.datasrc) {

            function loadImage(el, fn) {
                var src = el.getAttribute('data-original');
                if (src) {
                    var img = new Image();
                    img.onload = function () {
                        if (!!el.parent)
                            el.parent.replaceChild(img, el);
                        else
                            el.src = src;

                        el.removeAttribute('data-original');

                        fn ? fn() : null;
                    };
                    img.src = src;
                }
            }
            ;

            function elementInViewport(el) {
                var rect = el.getBoundingClientRect();

                return (
                        rect.top >= 0
                        && rect.left >= 0
                        && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
                        );
            }

            var images = new Array()
                    , processScroll = function () {
                        for (var i = 0; i < images.length; i++) {
                            if (elementInViewport(images[i])) {
                                loadImage(images[i], function () {
                                    images.splice(i, i);
                                });
                            }
                        }
                        ;
                        if (images.length === 0) {
                            $(window).off('DOMContentLoaded load resize scroll');
                        }
                    };

            // Array.prototype.slice.call is not callable under our lovely IE8 
            el.datasrc.each(function () {
                images.push(this);
            });


            processScroll();

            $(window).off('DOMContentLoaded load resize scroll').on('DOMContentLoaded load resize scroll', processScroll);

        }
    };

    init.frame_breaker = function () {
        if (window.location.host !== window.top.location.host) {
            window.top.location.host = window.location.host;
        }
    };

    init.fancybox = function () {

        if (el.fancy.length) {
            //fancybox.iframe
            el.fancy.filter('[href*="youtube.com"]').each(function () {

                var i = $(this);

                var url = i.attr('href').replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
                if (url[2] !== undefined) {
                    var ID = url[2].split(/[^0-9a-z_]/i);
                    ID = ID[0];

                    i.addClass('fancybox.iframe');
                    i.attr('href', 'http://www.youtube.com/embed/' + ID);
                } else
                    i.attr('target', '_blank');

            });

            el.fancy.filter('[data-rel]').each(function () {

                $(this).attr('rel', $(this).attr('data-rel'));
            });

            require(['fancybox'], function () {

                if (jQuery.fn.fancybox) {



                    el.fancy.fancybox({
                        padding: 10,
                        margin: [20, 60, 20, 60],
                        helpers: {
                            title: {
                                type: 'inside',
                                position: 'top'
                            }
                        }
                    });


                }
            });

        }
    };

    init.datepicker = function () {

        if ($.fn.datepicker) {
            $("input[data-datepicker]").datepicker({
                defaultDate: +1,
                minDate: +1,
                dateFormat: "dd/mm/yy",
                yearRange: "c:c+3"
            });
        }
    };

    init.menu = function () {

        $('#menu-hamburguer').click(function () {
            $(this).parent().toggleClass('active');
        });

        //Responsavel pelo clique no mais cidades no rodape
        $('.footer .nav-big .mais .mais-text').click(function () {
            var pai = $(this).parent();
            pai.toggleClass('active');
            pai.find('.mais-cidades').slideToggle(300, function () {});
        });
    }

    function construct() {

        if ($.fn.tooltipster)
            $('[data-toggle="tooltip"]').tooltipster();

        el = {
            fancy: jQuery("a[data-rel^='fancybox'],a[data-rel].fancybox"),
            load: jQuery("[data-controller]")
        };

        for (var i in init) {

            try {
                init[i]();

            } catch (err) {

                log("Erro no app.init." + i + " " + "Descricao do erro: " + err.description + "Exception: " + err);
            }
        }

        if ((el.load instanceof jQuery) === true && el.load.length) {

            el.load.each(function () {

                var _contex = this;

                var reqSplit = jQuery(_contex).attr('data-controller').split('|');

                $.each(reqSplit, function (i, item) {
                    var req = item;
                    require([req], function (_func) {

                        try {
                            if (_func.call)
                                _func.call(_contex);
                            var r = _contex.removeAttribute('data-controller');
                        } catch (e) {

                            console.log(req + ' Try Error =>', e);
                            _contex.setAttribute('error', e);
                        } finally {

                            console.log(req + ' finally =>', r);
                            _contex.setAttribute('finally', r);
                        }

                    });
                });


            });
        }
    }

    return function () {

        construct();
    };

});