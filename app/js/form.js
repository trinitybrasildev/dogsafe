define('form', ['jquery', 'ajax', 'loadcidade', 'loadcep', 'chosen'], function ($, ajax, loadcidade, loadcep) {

    var feedbackLang = {
        'pt': [
            "Preencha os campos obrigatórios(*), corretamente!",
            "Enviando, aguarde...",
            "Enviado com sucesso. Obrigado pelo interesse em breve retornaremos.",
            "Desculpe, não foi possível enviar. Tente novamente..."
        ],
        'pt-br': [
            "Preencha os campos obrigatórios(*), corretamente!",
            "Enviando, aguarde...",
            "Enviado com sucesso. Obrigado pelo interesse em breve retornaremos.",
            "Desculpe, não foi possível enviar. Tente novamente..."
        ],
        'en': [
            "Complete the required (*) fields correctly!",
            "Sending wait ...",
            "Sent. Thanks for the interest will soon return.",
            "Sorry, could not send. Try again ..."
        ],
        'es': [
            "Preencha os campos obrigatórios(*), corretamente!",
            "Envío de espera ...",
            "Enviados. Gracias por el interés volverán pronto.",
            "Lo sentimos, no se pudo enviar. Inténtalo de nuevo ..."
        ]
    };

    var enviando = false;
    var lg = window.lang;

    var feedback = feedbackLang[lg];

    function valid(self) {

        var validate = true;

        $("input[required],textarea[required],input.required,textarea.required,select[required]", self)
                .filter('[type="email"]')
                .each(function () {

                    var inpSelf = $(this);
                    inpSelf.parent().removeClass('error');
                    var reg = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/);

                    if (inpSelf.val() != '' && reg.test(inpSelf.val()) == false) {

                        validate = false;
                        inpSelf.addClass('erro');
                        inpSelf.focus();
                    }

                    reg = undefined;
                })
                .end()
                .each(function () {

                    if ($.trim($(this).val()) == '' || $(this).val() == '') {

                        validate = false;
                        $(this).addClass('erro');
                    }
                });

        return validate;
    }

    function uploadanuncio(self) {

        var _self = $(self).find('#fotoanuncio-uploaded');
        var _box = _self.parent();
        var name = _self.attr('data-name');

        _box.on('click', 'i.delarq', function () {
            $(this).parent().remove('');
            $(this).remove();

            ordena();
        });

        _box.on('click', 'span.prev', function () {
            var item = $(this).parent();

            $(item.prev()).before(item);

            ordena();
        });

        _box.on('click', 'span.next', function () {
            var item = $(this).parent();

            $(item.next()).after(item);
            ordena();
        });

        function ordena() {

            var uploadfile = _box.find('.upload-file');

            uploadfile.find('i.estrela').remove();

            var len = uploadfile.length;
            var length = uploadfile.length;
            uploadfile.each(function (a, b) {

                if (length > 1) {
                    $(b).find('span.next, span.prev').show();
                } else {
                    $(b).find('span.next, span.prev').hide();
                }

                if (a == 0) {
                    $(b).append('<i class="estrela"><img  src="/img/estrela-dourada.png" width=20></i>')
                            .find('span.prev').hide();
                }

                if (len == 1) {
                    $(b).find('span.next').hide();
                }

                $(b).find('input[name*=ordem]').val(len--);
            });

        }

        function add(id, fileName) {

//            inptFileList = _self.find('input[name=' + name + '[]]');
//
//            var len = inptFileList.length;

            var file = '<div id="foto' + id + '-uploaded" class="upload-file w-inline-block file-uploader">' +
                    '<span class=prev><</span>' +
                    '<span class=next>></span>' +
                    '<span class=label>Enviando...</span>' +
                    '<input type="hidden" name="' + name + '[]" value=""/>' +
                    '<input type="hidden" name="' + name + 'principal[]" value=""/>' +
                    '<input type="hidden" name="' + name + 'ordem[]" value=""/>' +
                    '</div>';

            _box.append(file);
            enviando = true;
            ordena();
        }

        function aguarde(id, fileName, loaded, total) {

            $('#foto' + id + '-uploaded').find('.label').text('Enviando ...').end()
                    .attr('data-loader', loaded);
            enviando = true;
        }

        function completo(id, fileName, responseJSON) {

            var uploadFile = $('#foto' + id + '-uploaded');

            if (responseJSON.hasOwnProperty('filename')) {

                var name = responseJSON.filename;

                if (name.length > 13) {

                    name = name.substr(0, 5) + '..' + name.substr(-5);
                }

                uploadFile.removeClass('loading').addClass('upload-temp');
            }

            uploadFile.css({'background-image': "url('/imagens/temp/" + responseJSON.filename + "')", 'background-size': '80px'});

            uploadFile.append('<i class="delarq"><img src="../img/close-img.png" style="margin-left: 5px;" /></i>');

            uploadFile.find('.label').hide();

            uploadFile.find('input[name="fotoanuncio[]"]').val(responseJSON.filename);

            uploadFile.trigger('completeFile');
            uploadFile.removeAttr('data-loader');

            enviando = false;
        }

        if (qq != undefined && qq.hasOwnProperty('FileUploader')) {

            var uploader = new qq.FileUploader({
                element: _self.get(0),
                action: self.data().upload,
                inputName: "arquivotemp",
                uploadButtonText: '<span style="width: 100%;display: inline-block;">' + _self.attr('data-text') + '</span>',
                multiple: true,
                onSubmit: add,
                onProgress: aguarde,
                onComplete: completo,
                onCancel: function (id, fileName) {
                    enviando = false;
                },
                onError: function (id, fileName, xhr) {
                    enviando = false;
                },
                showMessage: function (message) {
                }
            });

            ordena();
        }
    }

    function upload(self) {

        var _self = $(self);
        var inptFileList = _self.find('.file-uploader[data-name]');
        var msg = self.find(".feedback");

        _self.on('click', '[data-arquivo][data-href] .file-uploader', function () {
            window.open($(this).parent().attr('data-href'));
        });

        _self.on('click', 'i.delarq', function () {
            $(this).parent().removeAttr('style');
            var name = $(this).parent().attr('data-name');
            $('input[name="' + name + '"]').val('');
            $(this).remove();
        });

        inptFileList.each(function () {

            var inptFile = $(this);
            var ismult = inptFile.attr('data-mult');
            var upappend = inptFile.attr('data-append');

            if (qq != undefined && qq.hasOwnProperty('FileUploader') && inptFile.length == 1 && inptFile.attr('data-name')) {

                var inptHidd = $('<input type="hidden" name="' + inptFile.attr('data-name') + '" value=""/>');
                var uploaded = $("#" + inptFile.attr('data-name') + "-uploaded");

                if (ismult == undefined)
                    _self.append(inptHidd);

                var uploader = new qq.FileUploader({
                    element: inptFile.get(0),
                    action: _self.data().upload,
                    inputName: "arquivotemp",
                    uploadButtonText: '<span style="width: 100%;display: inline-block;">' + inptFile.attr('data-text') + '</span>',
                    multiple: true,
                    onSubmit: function (id, fileName) {
                        inptFile.trigger('submitFile');
                        uploaded.addClass('loading').removeAttr('hidden');
                        uploaded.find('.label').text('Enviando...');
                        enviando = true;//Formulario bloqueado
                    },
                    onProgress: function (id, fileName, loaded, total) {
                        //+ (total / (loaded * 100))
                        uploaded.find('.label').html('Carregando foto');
                        uploaded.attr('data-loader', loaded);
                        enviando = true;//Formulario bloqueado
                    },
                    onComplete: function (id, fileName, responseJSON) {

                        if (responseJSON.hasOwnProperty('filename')) {

                            var name = responseJSON.filename;

                            if (name.length > 13) {

                                name = name.substr(0, 5) + '..' + name.substr(-5);
                            }

                            uploaded.removeClass('loading').addClass('upload-temp');
                        }

                        if (ismult > 1) {
                            var box = $('div.' + uploaded.attr('id'));
                            var count = box.find('p').length + 1;
                            uploaded.find('.label').html(inptFile.attr('data-text'));

                            var item = $('<p class="c-white mt-10"><input name="' + uploaded.attr('data-name') + '[]" value="' + responseJSON.filename + '" type="hidden"/>' + count + 'ª Foto Casa :' + name + ' <span class="to-right">Excluir</span></p>');

                            box.append(item);

                            item.find('span').click(function (e) {
                                $(this).parent().remove();
                            });

                        } else {

                            uploaded.find('.label').show().html(name + ' - ' + ((uploaded.attr('data-loader') / 1024) / 1024).toFixed(1) + 'MB');

                            inptHidd.val(responseJSON.filename);
                        }

                        inptFile.trigger('completeFile', [responseJSON]);
                        uploaded.removeAttr('data-loader');
                        enviando = false;//Formulario ok
                    },
                    onCancel: function (id, fileName) {
                        enviando = false;//Formulario ok
                        //ploaded.find('span.text').text('Anexar');
                    },
                    onError: function (id, fileName, xhr) {
                        inptFile.trigger('completeFile');
                        uploaded.find('.label').text('Error!');
                        enviando = false;//Formulario ok

                    },
                    showMessage: function (message) {
                        enviando = false;//Formulario ok
                        uploaded.find('.label').text(message || 'Error!');

                    }
                });

            }
        });

    }

    function send(e) {

        if (e.preventDefault)
            e.preventDefault();

        var self = $(this);

        if (enviando === false) {

            enviando = true;

            var msg = (self.attr('data-feedback') ? $(self.attr('data-feedback')) : self.find(".success,.feedback")), validate = valid(self);

            if (validate === false) {

                msg.html(feedback[0]).removeAttr('hidden');

            } else if (self.attr('data-action') != undefined) {

                var options = {
                    url: self.attr('data-action'),
                    beforeSend: function () {

                        msg.removeAttr('style').html(feedback[1]).removeAttr('hidden');

                    },
                    success: function (data) {

                        if (data.status == true) {

                            msg.removeAttr('style').html(data.msg || feedback[2]).removeAttr('hidden');


                            if (data.redir) {
                                setTimeout(function () {
                                    location.href = (data.redir);
                                    location.replace(data.redir);
                                }, 2500);

                            } else if (data.hasOwnProperty('reset')){
                               self.get(0).reset();
                            }

                        } else {

                            msg.attr('style', 'color:red;font-weight:600;').html((data.msg || feedback[3])).removeAttr('hidden');
                        }
                        self.trigger('complete', data);
                        enviando = false;

                        return false;
                    },
                    dataType: 'json',
                    data: self.serializeArray(),
                    context: self
                };

                ajax(options).error(function () {
                    enviando = false;

                    msg.html((feedback[3])).removeAttr('hidden');

                });

                options = undefined;
            }
        }

        return false;
    }

    return function () {

        var self = $(this), rtd = false;

        if (self.length > 0) {

            if ($.fn.tooltipster) {

                $('input.form-control:not([data-toggle])').each(function () {
                    
                    var _intp = $(this);
                    
                    if(_intp.find('[data-toggle]').length == 0){
                        _intp.attr('data-toggle', "tooltip");
                        _intp.attr('data-placement', "top");
                        _intp.attr('title', _intp.attr('placeholder'));
                        _intp.tooltipster();
                    }
                    
                });

                $('.select-personalizado:not([data-toggle])').each(function () {
                    
                    var selectp = $(this);
                    if(selectp.find('[data-toggle]').length == 0){
                        selectp.attr('data-toggle', "tooltip");
                        selectp.attr('data-placement', "top");
                        selectp.attr('title', selectp.find('span.label').text());
                        selectp.tooltipster();
                    }
                });
                
                $('textarea.form-control:not([data-toggle])').each(function () {
                    var _textp = $(this);
                    if(_textp.find('[data-toggle]').length == 0){
                        _textp.attr('data-toggle', "tooltip");
                        _textp.attr('data-placement', "top");
                        _textp.attr('title', _textp.attr('placeholder'));
                        _textp.tooltipster();
                    }
                });
            }
            
            if (self.data().hasOwnProperty('upload')) {

                upload(self);
            }

            if ($.fn.mask) {

                $('[data-mask]', self).each(function () {
                    $(this).mask($(this).attr('data-mask'));
                });

                var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00)00000-0000' : '(00)0000-00009';
                },
                        spOptions = {
                            onKeyPress: function (val, e, field, options) {
                                field.mask(SPMaskBehavior.apply({}, arguments), options);
                            }
                        };

                $('[data-celular]').mask(SPMaskBehavior, spOptions);

                $('[data-decimal]').mask("#.##0,00", {reverse: true});

                $('[data-km]').mask("#.##0.000", {reverse: true});
            }

            jQuery("form input,form textarea").on("invalid", function (event) {
                if (event.type == 'invalid') {
                    $(event.currentTarget).addClass('erro');
                }
            });

            self.submit(send);

            loadcidade(self);

            //CEP
            var inpCep = self.find('input.cep, input[name=cep]');
            if (inpCep) {
                inpCep.on('change blur', loadcep);

            }

            $(document).on('change', ".select-personalizado:not(.multiple) select", function () {
                var valor = $(this).find("option:selected").text();
                $(this).parent().find(".label").text(valor);
            });

            $(".select-personalizado:not(.multiple) select").each(function () {
                var selct = $(this);
                if (selct.val() != "" && selct.find("option:selected") && selct.find("option:selected").text() != selct.parent().find(".label").text()) {
                    var valor = selct.find("option:selected").text();
                    selct.parent().find(".label").text(valor);
                }
            });


            $('.chosen-select', self).chosen();


            rtd = true;
        }

        return rtd;
    };

});