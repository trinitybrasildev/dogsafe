define(function () {
    
    function loginFinal(r) {

        FB.api('/me?fields=id,name,email,permissions,public_profile', function (response) {

            $.post('/ajax/facelogin', {'code': r.authResponse.accessToken}, function (data) {

                if (data && 'redir' in data) {
                    location.href = data.redir;
                }
            });
        }, {scope: 'email'});
    }
    
    return function () {

        $("#btn-facebook").off().click(function (e) {
            e.preventDefault();

            window.FB.login(function (response) {

                if (response.status === 'connected') {

                    loginFinal(response);
                } else if (response.status === 'not_authorized') {

                    console.log('Please log into Facebook.');
                } else {

                    console.log('Please log into Facebook.');
                }
            }, {scope: 'public_profile,email', display: 'popup', redirect_uri: 'http://dogsafe.com.br/login'});
            return false;

        });
    }

});