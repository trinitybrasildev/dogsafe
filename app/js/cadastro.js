define(function () {
    
    var self, cidadeLoad = null;

    function estado() {
        
        var selCidade = self.find("#cidade");

        selCidade.empty();

        var valorEstado = self.find('#estado option:selected').val();

        if (valorEstado)
            $.getJSON('/cidades', {'estado': valorEstado}, function (cidadelist) {
                if (cidadelist) {
                    $.each(cidadelist.list, function (cidade) {
                        selCidade.append('<option value="' + this.id + '">' + this.nome + '</option>');
                    });
                    var valor = selCidade.find("option:selected").text();
                    selCidade.parent().find(".label").text(valor);
                }

            });


    }

    function loadCEP() {

        var cep = this.value;

        if (cep.length > 5 && self.find('#endereco').val() == "") {

            $.ajax({
                url: 'https://correiosapi.apphb.com/cep/' + cep,
                dataType: 'jsonp',
                crossDomain: true,
                contentType: "application/json",
                statusCode: {
                    200: function (data) {
                        if (data.hasOwnProperty('logradouro'))
                            self.find('#endereco').val(data.logradouro);

                        if (data.hasOwnProperty('estado')) {
                            self.find('#estado').find('option[data-sigla^="' + data.estado + '"]').attr('selected', 'selected');
                            estado();
                        }

                        if (data.hasOwnProperty('cidade')) {
                            cidadeLoad = data.cidade;
                            self.find('#cidade option:contains("' + encodeURI(data.cidade) + '")').attr('selected', 'selected').change();
                        }

                        if (data.hasOwnProperty('bairro'))
                            self.find('#bairro').val(data.bairro);

                        self.find("#numero").focus();

                    }
                    , 400: function (msg) {
                        console.log(msg);
                    } // Bad Request
                    , 404: function (msg) {
                        console.log("CEP não encontrado!!");
                    } // Not Found
                }
            });
        }
    }

    function facebook(r) {

        window.FB.api('/me?fields=id,name,email,permissions,public_profile', function (response) {

            $.post('/facecadastro', {'code': r.authResponse.accessToken}, function (data) {

                if (data && 'email' in data) {
                    $("#email").val(data.email);
                    $("#nome-completo").val(data.nome);
                }

            }, 'json');
        }, {scope: 'email'});
    }

    return function () {

        self = $(this);

        self.find('#cep').on('change', loadCEP);

        self.find('#estado').change(estado).change();

        $("#btn-facebook").off().click(function (e) {
            e.preventDefault();

            window.FB.login(function (response) {

                if (response.status === 'connected') {

                    facebook(response);
                } else if (response.status === 'not_authorized') {

                    console.log('Please log into Facebook.');
                } else {

                    console.log('Please log into Facebook.');
                }
            }, {scope: 'public_profile,email', display: 'popup', redirect_uri: 'http://dogsafe.com.br/identificacao'});
            return false;

        });
    };

});