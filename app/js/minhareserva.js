define(['tools'], function () {

    var self, reservas;

    function pagar() {

        $(this).off();
        var btn = $(this);

        var id = $(this).closest('[data-id]').attr('data-id');

        btn.text('Aguarde ...');

        $.getJSON('/reserva/pagar/' + id, function (r) {

            if (r && r.hasOwnProperty('status')) {

                var reserva = (r.reserva ? r.reserva : []);

                if (reserva && reserva.hasOwnProperty('paypalurl') && reserva.paypalurl.indexOf('token') != -1) {

                    btn.text('Abrindo Paypal');

                    var paypal = window.open(reserva.paypalurl, '_blank', "width=800,height=650");

                    if ((paypal == null) || (paypal.hasOwnProperty('close') == false)) {

                        alert('Não foi possível abrir o pagamento, devido ao bloqueio de pop-up.');
                    } else {

                        btn.text('Finalizar o pagamento');

                        btn.attr('href', reserva.paypalurl);
                        btn.attr('target', '_blank');

                        paypal.onbeforeunload = function () {
                            location.href = '/minhas-reservas';
                        };
                    }


                } else {

                    alert(r.msg);
                }
            }
        });
    }

    function cancelar() {

        $(this).off();
        var btn = $(this);

        var id = $(this).closest('[data-id]').attr('data-id');
        var item = $(this).closest('[data-id]');


        var r = confirm("Confirma o cancelamento?");

        if (r == true) {
            btn.text('Aguarde ...');
            $.getJSON('/reserva/cancelar/' + id, function (r) {

                if (r && r.hasOwnProperty('status')) {

                    var reserva = (r.reserva ? r.reserva : []);

                    if (reserva) {

                        item.remove();

                    } else {

                        alert(r.msg);
                    }
                }
            });
        }
    }

    return function () {

        self = $(this);

        reservas = self.find('#reservas');

        reservas.on('click', 'a.pagar', pagar);

        reservas.on('click', '.detalhes-pet a.cancelada', cancelar);
        
        var verMais = $('.col-status span.btn, a.btn-info');
        
        verMais.each(function (index) {
            
            var currentItem = $(this);
            var row = currentItem.parents('.table-row');
            var detalhes = row.find('.detalhes-pet');

            if (detalhes.length != 0) {
                currentItem.click(function (e) {
                    e.preventDefault();

                    row.toggleClass('active');
                    detalhes.slideToggle(300, function () {});
                });
            }
        });
        
        createCSSSelector('a.pagar', 'background:#89b544;');

    };

});
