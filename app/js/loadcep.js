define(function () {
    var self, xhrEnd = false;

    function loadCEP() {
        
        if(xhrEnd != false){
            xhrEnd.abort();
        }
        
        self = $(this).closest('form');
        var cep = this.value;

        if (cep.length > 5 && self.find('#endereco,[name=endereco]').val() == "") {

            xhrEnd = $.ajax({
                url: 'https://correiosapi.apphb.com/cep/' + cep,
                dataType: 'jsonp',
                crossDomain: true,
                contentType: "application/json",
                statusCode: {
                    200: function (data) {
                        
                        if (data.hasOwnProperty('logradouro'))
                            self.find('#endereco,[name=endereco]').val(data.logradouro);

                        if (data.hasOwnProperty('bairro'))
                            self.find('#bairro,[name=bairro]').val(data.bairro);

                        if (data.hasOwnProperty('estado')) {
                            self.find('#estado').find('option[data-sigla^="' + data.estado + '"]').attr('selected', 'selected');
                            self.find('#estado').change();
                        }
                        
                        self.find("#numero,[name=numero]").focus();
                        
                        if (data.hasOwnProperty('cidade')) {
                            var cidade = self.find('#cidade,[name=id_cidade]');
                            cidade.attr('data-load', data.cidade);
                            cidade.find('option:contains("' + (data.cidade) + '")').attr('selected', 'selected');
                            cidade.change();
                        }
                        
                        xhrEnd = false;
                    }
                    , 400: function (msg) {
                        console.log(msg);
                        xhrEnd = false;
                    } // Bad Request
                    , 404: function (msg) {
                        console.log("CEP não encontrado!!");
                        xhrEnd = false;
                    } // Not Found
                }
            });
        }
    }


    return loadCEP;
})