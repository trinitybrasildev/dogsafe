define(function () {
    var self;
    function excluir() {
        var id = ~~$(this).attr('data-rel');

        $.getJSON('/excluir-compromisso/' + id, function (data) {
            location.href = '/meus-compromisso';
        });
    }

    return function () {
        
        self = $(this);

        self.find('.excluir-compromisso').click(excluir);
    };

});