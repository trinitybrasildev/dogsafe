define(['moment', 'mapsgoogle'], function (moment) {

    var self, xhrcal = false, anfitriao, datainicio, datafim, modalidade;

    function calendario(datepicker, month, year) {

        $.getJSON('/celendario?anfitriao=' + anfitriao + '&mes=' + month + '&ano=' + year, function (dataDias) {

            if (dataDias && dataDias.hasOwnProperty('bloqueado'))
                _.each(dataDias.bloqueado, function (a, b) {
                    datepicker.find("a:contains('" + a + "'):first").css('background','#aaa').parent().addClass('ui-datepicker-unselectable').addClass('ui-state-disabled').attr('disable', 'disable');
                });
        });
    }

    function calcMod() {

        var mod = modalidade.val();

        var val = modalidade.find(':selected').attr('data-valor');

        $(".preco").html('R$ <span>' + val + '</span>');

        if (mod == 1) {
            datafim.parent().show();
            $(".periodo").html('POR NOITE');
        } else {
            datafim.parent().hide();
            $(".periodo").html('POR DIA');
        }
    }

    function mapa(latlong) {

        var item = $(this);
        var latlong = item.attr('data-latlong').split(',');

        var myLatlng = new google.maps.LatLng(latlong[0], latlong[1]);

        var mapOptions = {
            zoom: 14,
            center: myLatlng
        };

        var map = new google.maps.Map(item.get(0), mapOptions);

        var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#379E45',
            fillOpacity: 0.35,
            map: map,
            center: myLatlng,
            radius: 250
        });

    }

    return function () {

        self = $(this);
        var logado = self.attr('data-logado') == 'true' ? true:false;
        self.datepicker = $("#datepicker");
        anfitriao = self.attr('data-anfitriao');
        datainicio = $("#datainicio");
        datafim = $("#datafim");
        modalidade = $("#modalidade");

        if ($.fn.datepicker) {

            var data = self.datepicker.datepicker({
                changeMonth: true,
                changeYear: true,
                defaultDate: +1,
                minDate: +1,
                yearRange: "c:c+3",
                dateFormat: "dd/mm/yy",
                onChangeMonthYear: function ( year, month) {
                    calendario(self.datepicker, month, year);
                },
                onSelect: function (dateText) {
                }
            }).datepicker('getDate');

            calendario(self.datepicker, moment(data).format('MM'), moment(data).format('YYYY'));

            datainicio.datepicker({
                changeMonth: true,
                changeYear: true,
                defaultDate: +1,
                minDate: +1,
                yearRange: "c:c+3",
                dateFormat: "dd/mm/yy",
                onChangeMonthYear: function (year, month) {
                    calendario(datainicio, month, year);
                },
                onSelect: function (dateText) {

                    var dt = dateText.split('/');
                    var dia = dt[0];
                    var mes = dt[1];
                    var ano = dt[2];

                    console.log(moment([ano, mes, dia]).add(1, 'day').format());

                    //datafim.datepicker("setDate", moment([ano, mes, dia]).add(1, 'day').format("DD/MM/YYYY"));

                    //datafim.datepicker("option", "minDate", moment([ano, mes, dia]).add(1, 'day').format("DD/MM/YYYY"));
                }
            });

            datafim.datepicker({
                changeMonth: true,
                changeYear: true,
                defaultDate: +1,
                minDate: +1,
                yearRange: "c:c+3",
                dateFormat: "dd/mm/yy",
                onChangeMonthYear: function (year, month) {
                    calendario(datafim, month, year);
                },
                onSelect: function (dateText) {

                    var dt = dateText.split('/');
                    var dia = dt[0];
                    var mes = dt[1];
                    var ano = dt[2];

                    //console.log(moment([ano, mes, dia]).add(1, 'day').format("DD/MM/YYYY"));

//                    /calendario(datafim, mes, ano);
                }
            });


        }

        if (self.find('.mapa-wrapper[data-latlong]')) {
            mapa.call(self.find('.mapa-wrapper[data-latlong]'));
        }

        $('.form-wrapper [data-click]').click(function (e) {
            e.preventDefault();
            $('.form-wrapper').toggleClass('none');
        });

        self.off('change.select').on('change.select', ".select-personalizado select", function () {
            var valor = $(this).find("option:selected").text();
            $(this).parent().find(".label").text(valor);
        });

        modalidade.on('change.calc', calcMod);
    };
});