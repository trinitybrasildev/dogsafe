
var jsErro = (function(){
    var iframe = document.getElementById("jserro");
    function send(msg){
        iframe.src = iframe.src.split('?')[0] + '?jserro=' + msg;
    }
    return {'send':send};
}());

window.onerror = function (msg, file, line, col, error) {
    jsErro.send( msg +' file:' + file + '@' + line + ':' + col +' URL ' + encodeURI(location.search + location.hash) );
};