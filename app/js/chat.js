define(function () {
    var self, mensagens, logado, logadoanfitriao, timecheck, timeref = 5000;

    var block = '<div>' +
                    '<span class="strong"></span>' +
                    '<span class="text"></span>' +
                '</div>';


    function conversas() {

        clearInterval(timecheck);
        timecheck = null;

        self.find('.conversas .contato').off().click(conversas);
        self.find('.conversas .contato.selecionado').removeClass('selecionado');

        $(this).off();

        $(this).addClass('selecionado');
        var cache = (Math.floor(Math.random() * 16777215).toString(16));
        var chat = ~~$(this).attr('data-id');

        $('#idchat').val(chat);

        mensagens.empty();
        
        var _wait = $(block);
        
        _wait.find('span.strong').html('');
        _wait.find('.text').text('Carregando ...');
        
        mensagens.append(_wait).append('<div class="clearfix"></div>');

        $.getJSON('/chat/listconversa?chat=' + chat + '&' + cache, function (data) {

            mensagens.empty();

            if (data && 'list' in data) {

                _.each(data.list, function (a, b) {

                    var _block = $(block);

                    if (a.ref == logado || a.ref == logadoanfitriao) {

                        _block.addClass('m-02');
                    } else {

                        _block.addClass('m-01');
                    }

                    _block.attr('data-id', a.id_chatconversa);
                    _block.find('span.strong').text( a.nome + ' - ' + a.data);

                    _block.find('.text').multiline(a.texto);
                    mensagens.append(_block).append('<div class="clearfix"></div>');
                });

                scrollTime();
            }
        });
    }

    function check() {

        clearInterval(timecheck);
        timecheck = null;

        var _self = self.find('.conversas .selecionado');

        if (_self.length != 1) {
            return false;
        }

        var chat = ~~_self.attr('data-id');

        $.getJSON('/chat/countconversa?chat=' + chat, function (data) {

            if (data && 'count' in data) {
                var countM = mensagens.find('[data-id]').length;

                if (data.count > countM) {
                    var ref = ~~mensagens.find('[data-id]:last').attr('data-id');
                    sync(chat, ref);
                }

                scrollTime(false);
            }
        });
    }

    function sync(chat, ref) {

        clearInterval(timecheck);
        timecheck = null;

        $.getJSON('/chat/listconversa?chat=' + chat + '&ref=' + ref, function (data) {

            if (data && 'list' in data) {

                var prevR = ref;

                _.each(data.list, function (a, b) {

                    var isexist = mensagens.find('[data-id=' + a.id_chatconversa + ']');

                    if (isexist.length == 0) {

                        var _block = $(block);

                        var rblock = mensagens.find('[data-id=' + prevR + ']');

                        if (rblock.length == 0) {
                            rblock = mensagens.find('> div:last');
                        }
                        
                        if (a.ref == logado || a.ref == logadoanfitriao) {

                            _block.addClass('m-02');
                        } else {

                            _block.addClass('m-01');
                        }
                              
                        _block.attr('data-id', a.id_chatconversa);
                        _block.find('span.strong').text(a.nome + ' - ' + a.data);
                        _block.find('.text').multiline(a.texto);

                        rblock.next().after(_block);
                        prevR = a.id_chatconversa;
                        mensagens.find('[data-id=' + prevR + ']').after('<div class="clearfix"></div>');
                    }
                });

                scrollTime();
            }
        });
    }

    function deletar() {
        
        var box = $(this).parent();
        var id = box.attr('data-id');

        $.get('/chat/deletar?chat=' + id, function (data) {

            if (data && 'chat' in data) {
                box.remove();
            }
        });
    }

    function mensagem() {

        clearInterval(timecheck);
        timecheck = null;

        $(this).off();

        var chat = ~~$('#idchat').val();
        var text_mensagem = $("#mensagem").val();
        $("#mensagem").val('');
        
        $.post('/chat/adicionarconversa', {'id_chat': chat, 'texto': text_mensagem}, function (data) {
            if (data && data.hasOwnProperty('chatconversa')) {

                //Bug de mensagem rapidas
                checkJump(chat, data.chatconversa.id_chatconversa);

                var _block = $(block).addClass('m-02');
                _block.attr('data-id', data.chatconversa.id_chatconversa);
                _block.find('span.strong').text(data.chatconversa.nome + ' - ' + data.chatconversa.data);
                _block.find('.text').multiline(text_mensagem);
                mensagens.append(_block).append('<div class="clearfix"></div>');

                scrollTime();

                self.find('#enviar-chat').click(mensagem);

            }
        },'json');
    }

    function checkJump(chat, inserted) {

        var last = mensagens.find('[data-id]').last();
        var id = ~~last.attr('data-id');

        if ((id + 1) !== inserted) {
            sync(chat, id);
        }
    }

    function scrollTime(scroll) {

        if (scroll !== false) {
            var off = $('#mensagens').find('> div:last').offset();
            if (off && 'top' in off) {
                var scroll = mensagens.scrollTop() + off.top;
                mensagens.scrollTop(scroll);
            }
        }

        clearInterval(timecheck);
        timecheck = setInterval(check, timeref);
    }

    return function () {

        self = $(this);

        $('.menu-historico').click(function () {
            $(this).parent().toggleClass('active');
        });

        $.fn.multiline = function (text) {
            var _text = "" + text;
            _text = _text.replace(/&/g, '&amp;')
                    .replace(/>/g, '&gt;')
                    .replace(/</g, '&lt;')
                    .replace(/\&lt\;br\&gt\;/g, '')
                    .replace(/<br\s*[\/]?>/gi, "\n")
                    .replace(/<br>/gi, "\n");

            var el = $(this);
            var lines = _text.split('\\n');
            
            console.log(lines);
            
            $(el).empty();
            
            for (var i = 0; i < lines.length; i++) {
                if (i > 0)
                    $(el).append('<br>');
                $(el).append(document.createTextNode(lines[i]));
            }

//            var html = el.html();
//            
//            console.log((""+html).replace(/<br\s*[\/]?>/gi, "#").replace(/\&lt\;br\&gt\;/g, '#'));
                    
            return this;
        };

        logado = ~~self.attr('data-cadastro');
        logadoanfitriao = ~~self.attr('data-anfitriao');
        
        mensagens = self.find('#mensagens');
        self.find('.conversas .contato').click(conversas);
        //self.find('.conversas .contato i.excluir').click(deletar);
        self.find('#enviar-chat').off().click(mensagem);

        if (location.hash !== "") {

            var ref = ~~location.hash.split('#')[1];
            var chathash = self.find('.conversas .contato[data-id="' + ref + '"]');
            if (chathash) {
                chathash.click();
                return false;
            }
        }

        self.find('.conversas .contato').first().click();

        clearInterval(timecheck);
        timecheck = setInterval(check, timeref);
    };
});