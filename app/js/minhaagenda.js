define(['moment'], function (moment) {

    var self, anfitriao, reservas;

    function calendario(datepicker, month, year) {

        $.getJSON('/celendario?anfitriao=' + anfitriao + '&mes=' + month + '&ano=' + year, function (dataDias) {

            if (dataDias && dataDias.hasOwnProperty('bloqueado'))
                _.each(dataDias.bloqueado, function (a, b) {
                    datepicker.find("a:contains('" + a + "'):first").css('background','#aaa').parent().addClass('ui-datepicker-unselectable').addClass('ui-state-disabled').attr('disable', 'disable');
                });
        });

    }

    function datepicker() {

        var data = self.datepicker.datepicker({
            changeMonth: true,
            changeYear: true,
            defaultDate: +1,
            minDate: +1,
            yearRange: "c:c+3",
            dateFormat: "dd/mm/yy",
            onChangeMonthYear: function (year, month) {
                calendario(self.datepicker, month, year);
            },
            onSelect: function (dateText) {
            }
        }).datepicker('getDate');

        calendario(self.datepicker, moment(data).format('MM'), moment(data).format('YYYY'));
    }

    function confirmar() {

        var id = $(this).closest('[data-id]').attr('data-id');

        $.getJSON('/reserva/confirmar/' + id, function (r) {
            if (r && r.hasOwnProperty('status')) {
                  
                  alert(r.msg);
                  location.href = location.href;
            }
        });
    }

    function cancelar() {

        var id = $(this).closest('[data-id]').attr('data-id');

        $.getJSON('/reserva/cancelar/' + id, function (r) {
            if (r && r.hasOwnProperty('status')) {
                
                alert(r.msg);
                location.href = location.href; 
            }
        });
    }

    return function () {

        self = $(this);
        self.datepicker = $("#datepicker");
        anfitriao = self.attr('data-anfitriao');
        reservas = self.find('#reservas');

        if ($.fn.datepicker) {
            datepicker();
        }

        reservas.on('click', 'a.confirmar', confirmar);

        reservas.on('click', 'a.recusar', cancelar);

        var verMais = $('.col-info .btn, a.btn-info');
        
        verMais.each(function (index) {
            
            var currentItem = $(this);
            var row = currentItem.parents('.table-row');
            var detalhes = row.find('.detalhes-pet');

            if (detalhes.length != 0) {
                currentItem.click(function (e) {
                    e.preventDefault();

                    row.toggleClass('active');
                    detalhes.slideToggle(300, function () {});
                });
            }
        });


    };

});
