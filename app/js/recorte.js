define(['jquery', 'jquery-ui.min', 'chosen'], function ($) {
    // Calendario home
    $("[data-datepicker]").datepicker();
    $("#datepicker").datepicker();

    // Select personalizado
    $(".select-personalizado select").change(function () {
        var valor = $(this).find("option:selected").text();
        $(this).parent().find(".label").text(valor);
    });

    // Select personalizado
    $(".select-personalizado.multiple select").change(function () {
        var options = $(this).find('option:selected');

        var tamanho = options.size();
        var texto = tamanho + " SELECIONADOS";

        if (tamanho == 1) {
            texto = options.eq(0).text();
        }

        $(this).parent().find(".label").text(texto);
    }).change();

    // Responsavel pelo click nas perguntas
    $('.perguntas-frequentes .pergunta').click(function (e) {
        e.preventDefault();
        $(this).parent().find('.resposta').slideToggle(200, function () {});
    });

    // Reponsavel pelo clique no botao MAIS na tela de Minhas Agendas
    var verMais = $('.col-info .btn');
    verMais.each(function (index) {
        var currentItem = $(this);
        var row = currentItem.parents('.table-row');
        var detalhes = row.find('.detalhes-pet');

        if (detalhes.length != 0) {
            currentItem.click(function (e) {
                e.preventDefault();

                row.toggleClass('active');
                detalhes.slideToggle(300, function () {});
            });
        }
    });


    //Responsavel pelo clique no menu hamburguer na pagina de chat mobile
    $('.menu-historico').click(function () {
        $(this).parent().toggleClass('active');
    });

    //Responsavel pelo clique no mais cidades no rodape
    $('.footer .nav-big .mais .mais-text').click(function () {
        var pai = $(this).parent();
        pai.toggleClass('active');
        pai.find('.mais-cidades').slideToggle(300, function () {});
    });

    //
    $('.form-wrapper [data-click]').click(function (e) {
        e.preventDefault();
        $('.form-wrapper').toggleClass('none');
    });


    //
    $('.chosen-select').chosen();

});
