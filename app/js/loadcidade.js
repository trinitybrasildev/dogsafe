define(function () {
    
    var self, xhr = false, cidade;
    
    function cidades() {
        
        var selUF = $(this);
        
        self = selUF.closest('form');
        
        var estado = this.value;
        
        if(estado < 1) return false;
                
        if(xhr != false){
            xhr.abort();
        }

        cidade = self.find('select.cidade');
        
        cidade.empty();

        cidade.append('<option>Carregando ..</option>');
        cidade.trigger('change');
        xhr = $.getJSON('/cidades?estado=' + estado, function(data){

            cidade.empty();
            var selected = cidade.attr('data-selected');
            cidade.append('<option value="">selecione</option>');
            
            var htmlc = [];
            _.each(data.list, function(e){
                htmlc.push('<option value="' + e.id + '" '+ (selected == e.id ? ' selected':'') +' >' + e.nome + '</option>');
            });
            
            cidade.append(htmlc.join());
            
            if(cidade.attr('data-load') != undefined){
                cidade.find('option:contains("' + cidade.attr('data-load') + '")').attr('selected', 'selected');
                cidade.removeAttr('data-load');
            }
            
            if(cidade.attr('data-selected') != undefined){
                cidade.val(cidade.attr('data-selected'));
                cidade.removeAttr('data-selected');
            }
            
            cidade.trigger('change');
            
            xhr = false;
        });
    }

    return function(form){
        
        self = $(form);
        
        self.find('select.estado').change(cidades);
        if(self.find('select.estado').val() > 0){
            self.find('select.estado').change();
        }
    };
})