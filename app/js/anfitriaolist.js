define(['mapsgoogle'], function () {
    var self, mapa;

    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 1,
        };
    }

    function createMaps() {

        var items = self.find('[data-latlong]');
        var cepgeo = self.attr('data-cepgeo');

        if (items.length == 0) {
            return false;
        }

        var itemfirst = items.first();

        if (itemfirst.attr('data-latlong') == undefined) {
            return false;
        }

        var latlong = itemfirst.attr('data-latlong').split(',');
        var myLatlngCenter = new google.maps.LatLng(latlong[0], latlong[1]);

        var mapOptions = {
            zoom: 10,
            center: myLatlngCenter
        };
        
        var mapatop = mapa.offset().top;
        var mapaheight = mapa.height();
        
        var limit = $(".resultados-wrapper .resultado:last-child").offset().top + 198;
        
        $(window).scroll(function () {
            
            var alt = ~~$(this).scrollTop();
                
            if (mapatop >= alt) {
                mapa.css({'top':'0'});
            } else if( (alt+mapaheight) < limit ){
                mapa.css({'top': ((alt - mapatop) + 20)+'px'});
            }
            
        });

        var map = new google.maps.Map(mapa.get(0), mapOptions);
        
        var infowindowcep = false;
        
        if (cepgeo && cepgeo.indexOf(':') != -1) {
            var cepfull = cepgeo.toString().split(':')[0];
            cepgeo = cepgeo.toString().split(':')[1].split(',');

            var _myLatlngcep = new google.maps.LatLng(cepgeo[0], cepgeo[1]);

            var markercep = new google.maps.Marker({
                map: map,
                position: _myLatlngcep,
                title: 'CEP: ' + cepfull,
                icon: pinSymbol("#FFF")
            });

            infowindowcep = new google.maps.InfoWindow({
                content: 'CEP: ' + cepfull
            });

            infowindowcep.open(map, markercep);

            markercep.addListener('click', function () {
                infowindowcep.open(map, markercep);
            });
        }



        if (items.length > 0)
            _.each(items, function (a) {

                var _item = $(a);

                var latlong = _item.attr('data-latlong').split(',');

                var _myLatlng = new google.maps.LatLng(latlong[0], latlong[1]);

                var marker = new google.maps.Marker({
                    position: _myLatlng,
                    title: _item.attr('data-title'),
                    icon: pinSymbol("#41BDC8")
                });

                var infowindow = new google.maps.InfoWindow({
                    content: _item.attr('data-title')
                });

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

                marker.setMap(map);

                _item.hover(function () {
                    if(infowindowcep != false)
                    infowindowcep.close();

                    _myCenterLatlng = new google.maps.LatLng(latlong[0], latlong[1]);
                    map.setCenter(_myCenterLatlng);
                    infowindow.open(map, marker);
                }, function () {
                    infowindow.close();
                });

            });
    }

    return function () {

        self = $(this);
		
        mapa = $("#mapa");

        if (mapa && self.find('[data-latlong]').length > 0)
            createMaps();
    }
});