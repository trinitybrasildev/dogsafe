
define('tools', ['jquery'], function ($) {

    $.fn.goScroll = function () {
        var _self = this;
        if (_self.closest('div[role="dialog"].modal').length == 0)
            $('html, body').stop(true, true).animate({
                scrollTop: _self.offset().top - 20
            }, 500);
        return _self;
    }

    $.fn.toggleHidden = function () {

        return (this.attr('hidden') !== undefined ? this.revomeAttr('hidden') : this.attr('hidden', ''));

    };
    /**
     window.alert = function(a, s) {
     s = s != undefined ? s : ($("#al er t - info-erro").length == 0 ? $("div:first").prepend('<div id = "alert-info-erro"/>') : $("div:first")).find("#alert-info-erro");
     $(s).html('<div class="alert"><button type="button" class="close" data-dismis s = " alert">×</button> ' + a + ' </div>').goScroll().fadeIn();
     }
     
     window.info = function(i, s) {
     s = s != undefined ? s : ($("div:first").find("#al er t - info-erro").length == 0 ? $("div:first").prepend('<div id = "alert-info-erro"/>') : $("div:first")).find("#alert-info-erro");
     $(s).html('<div class="alert alert-info"><button type="button" class="close" data-dismis s = " alert">×</button> ' + i + ' </div>').goScroll().fadeIn();
     }
     
     window.error = function(e, s) {
     s = s != undefined ? s : ($("div:first").find("#al er t - info-erro").length == 0 ? $("div:first").prepend('<div id = "alert-info-erro"/>') : $("div:first")).find("#alert-info-erro");
     $(s).html('<div class="alert alert-error"><button type="button" class="close" data-dismis s = " alert">×</button> ' + e + ' </div>').goScroll().fadeIn();
     }
     
     
     * Função transforma elemento.id, id do elemento no padrao camelCase
     */
    $.fn.camelCase = function (type) {
        var self = $(this).get(0);
        if (typeof self !== "undefined") {
            camelCase = self.id.split(/-|_/);
            $.each(camelCase, function (i, l) {
                if (type != 'lower' || (type == 'lower' && i !== 0)) {
                    camelCase[i] = (l + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                        return $1.toUpperCase();
                    });
                }
            });
            return camelCase.join('');
        } else {
            return undefined;
        }
    };

    /**
     * Função obter elementos
     */
    var el = function () {

        var self = $(this), _elementos = {}, _ars = arguments;
        _elementos[self.camelCase('lower')] = self;
        self.find(_ars.lenght > 0 && typeof (_ars[0]) == "string" ? _ars[0 ] : '*').each(function () {
            if (this.id) {
                _elementos[$(this).camelCase('lower')] = $(this);
            }
        });
        for (var extra in  _ars) {
            if (extra > 0) {
                var _ext = $(_ars[1 ]);
                if (_ext.length == 1) {
                    _elementos[_ext.camelCase('lower')] = _ext;
                } else if (_ext.length > 1) {
                    _elementos['list' + _ext.camelCase()] = _ext;
                }
                delete _ext;
            }

        }

        return _elementos;
    };
    
    $.fn.el = el;
    
    var subir = function(sel){
        return $(this).closest(sel);
    }
    $.fn.subir = subir;

    function createCSSSelector(selector, style) {
        if (!document.styleSheets)
            return;
        if (document.getElementsByTagName('head').length == 0)
            return;

        var styleSheet, mediaType;

        if (document.styleSheets.length > 0) {
            for (var i = 0, l = document.styleSheets.length; i < l; i++) {
                if (document.styleSheets[i].disabled)
                    continue;
                var media = document.styleSheets[i].media;
                mediaType = typeof media;

                if (mediaType === 'string') {
                    if (media === '' || (media.indexOf('screen') !== -1)) {
                        styleSheet = document.styleSheets[i];
                    }
                } else if (mediaType == 'object') {
                    if (media.mediaText === '' || (media.mediaText.indexOf('screen') !== -1)) {
                        styleSheet = document.styleSheets[i];
                    }
                }

                if (typeof styleSheet !== 'undefined')
                    break;
            }
        }

        if (typeof styleSheet === 'undefined') {
            var styleSheetElement = document.createElement('style');
            styleSheetElement.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(styleSheetElement);

            for (i = 0; i < document.styleSheets.length; i++) {
                if (document.styleSheets[i].disabled) {
                    continue;
                }
                styleSheet = document.styleSheets[i];
            }

            mediaType = typeof styleSheet.media;
        }

        if (mediaType === 'string') {
            for (var i = 0, l = styleSheet.rules.length; i < l; i++) {
                if (styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
                    styleSheet.rules[i].style.cssText = style;
                    return;
                }
            }
            styleSheet.addRule(selector, style);
        } else if (mediaType === 'object') {
            var styleSheetLength = (styleSheet.cssRules) ? styleSheet.cssRules.length : 0;
            for (var i = 0; i < styleSheetLength; i++) {
                if (styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
                    styleSheet.cssRules[i].style.cssText = style;
                    return;
                }
            }
            styleSheet.insertRule(selector + '{' + style + '}', styleSheetLength);
        }
    }
    
    function makeid()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    
    window.createCSSSelector = createCSSSelector;
    $.fn.createCSSSelector = createCSSSelector;

    for (var i = 0, max = 300; i < max; i = i + 5) {
        createCSSSelector('.mt-' + i, ' margin-top:' + i + 'px; ');
        createCSSSelector('.mb-' + i, ' margin-bottom:' + i + 'px; ');
        createCSSSelector('.mr-' + i, ' margin-right:' + i + 'px; ');
        createCSSSelector('.ml-' + i, ' margin-left:' + i + 'px; ');
        createCSSSelector('.pt-' + i, ' padding-top:' + i + 'px; ');
        createCSSSelector('.pb-' + i, ' padding-bottom:' + i + 'px; ');
        createCSSSelector('.pr-' + i, ' padding-right:' + i + 'px; ');
        createCSSSelector('.pl-' + i, ' padding-left:' + i + 'px; ');
    }

//    $("[style]").each(function () {
//        var nclasse = makeid();
//        var style = $(this).attr('style');
//        createCSSSelector('.' + nclasse, style);
//        $(this).addClass(nclasse).removeAttr('style');
//    });
    
});