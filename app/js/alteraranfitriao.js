define(function () {

    function excluir() {
        var r = confirm("Deseja excluir?");

        if (r == true) {
            $(this).parent().remove();
            $('#casa-uploaded').show();
        }
    }

    return function () {

        $(this).on('click', ".excluir", excluir);
        if ($(".excluir").length >= 5)
            $('#casa-uploaded').hide();
    };
});