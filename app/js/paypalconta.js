define('paypalconta', ['tools'], function(){
    
    var self, el = [];
    
    function jatenho(){
        el.jatenho.subir('form').addClass('hide');
        $(".paypalconta").removeClass('hide');
    }
    
    return function (){
        self = $(this);
        el = self.el();
        
        el.jatenho.click(jatenho);
    };
});