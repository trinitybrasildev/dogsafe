define([], function () {

    return function () {
        var carrossel = document.querySelector('[data-carrossel]');
        if (carrossel) {
            var miniaturasList = carrossel.querySelector('.wrapper');
            var qtdItens = miniaturasList.children.length;

            if (qtdItens > 4) {
                carrossel.querySelector('[data-next]').onclick = function () {

                    var noVazio = document.createTextNode(' ');
                    var firstChild = miniaturasList.children[0];

                    miniaturasList.appendChild(noVazio);
                    miniaturasList.appendChild(firstChild);
                };

                carrossel.querySelector('[data-prev]').onclick = function () {

                    var noVazio = document.createTextNode(' ');
                    var firstChild = miniaturasList.children[0];
                    var lastChild = miniaturasList.children[qtdItens - 1];

                    miniaturasList.insertBefore(lastChild, firstChild);
                    miniaturasList.insertBefore(noVazio, firstChild);
                };
            }
        }

        // Responsavel por mover miniaturas de fotos na pagina Anfitriao Interna
        /*
         var carrossel = $('[data-carrossel]');
         var miniaturas = $(carrossel).find('.imagem-wrapper');
         var miniaturasContainer = $(carrossel).find('.wrapper');
         
         if (miniaturas.length > 4) {
         $(carrossel).find('[data-next]').click(function (e) {
         miniaturas = $(carrossel).find('.imagem-wrapper');
         
         miniaturasContainer.append(miniaturas.first());
         });
         
         $(carrossel).find('[data-prev]').click(function (e) {
         miniaturas = $(carrossel).find('.imagem-wrapper');
         
         miniaturasContainer.prepend(miniaturas.last());
         
         });
         }
         */
    };
});
