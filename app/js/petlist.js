define(function () {
    var self;

    function excluir() {
        var id = $(this).attr('data-rel');

        $.getJSON('/excluir-pet/' + id, function (data) {
            location.href = '/meus-pets';
        });
    }

    return function () {
        self = $(this);

        self.find('a.excluir-pet').click(function () {
            var r = confirm("Confirma a exclusão do pet?");
            if (r == true) {
                excluir.call(this);
            }
        });
    };

});