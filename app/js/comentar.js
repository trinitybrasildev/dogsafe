define(function () {
    var self;
    
    function pata() {
        
        $("li.pata").removeClass('active');
        
        var ava = $(this).attr('data-val');
        $("input[name=\"avaliacao\"]").val(ava);
        
        $("li.pata").each(function(){
            if($(this).attr('data-val') <= ava){
                $(this).addClass('active');
            }
        });
        
    }
    
    return function () {
        self = $(this);
        self.find('.votacao-wrapper .pata').click(pata);
    };
});