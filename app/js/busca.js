define(['nouislider', 'mapsgoogle'], function (noUiSlider) {
    var xhrEnd = false, self;

    function cep() {
        if (xhrEnd != false) {
            xhrEnd.abort();
        }

        self = $(this).closest('form');

        var cep = this.value;

        if (cep.length >= 6) {

            xhrEnd = $.ajax({
                url: 'https://correiosapi.apphb.com/cep/' + cep,
                dataType: 'jsonp',
                crossDomain: true,
                contentType: "application/json",
                statusCode: {
                    200: function (data) {

                        if (data.hasOwnProperty('logradouro'))
                            self.find('#endereco,[name=endereco]').val(data.logradouro);

                        if (data.hasOwnProperty('bairro'))
                            self.find('#bairro,[name=bairro]').val(data.bairro);

                        if (data.hasOwnProperty('estado')) {
                            self.find('#estado').find('option[data-sigla^="' + data.estado + '"]').attr('selected', 'selected');
                            self.find('#estado').change();
                        }

                        self.find("#numero,[name=numero]").focus();

                        if (data.hasOwnProperty('cidade')) {
                            var cidade = self.find('#cidade,[name=id_cidade]');
                            cidade.attr('data-load', data.cidade);
                            cidade.find('option:contains("' + (data.cidade) + '")').attr('selected', 'selected');
                            cidade.change();
                        }

                        xhrEnd = false;
                    }, 400: function (msg) {
                        console.log(msg);
                        xhrEnd = false;
                    }, 404: function (msg) {
                        console.log("CEP não encontrado!!");
                        xhrEnd = false;
                    }
                }
            });
        }
    }

    function cepLatLon(cep) {

        if (xhrEnd != false) {
            xhrEnd.abort();
        }

        var cep = this.value;
        
        if (cep.length >= 6) {
            xhrEnd = $.getJSON('/ceptolatlong/' + cep);
        }
    }

    return function () {

        self = $(this);

        self.find('#cep').change(cepLatLon);

        self.on('change', ".select-personalizado:not(.multiple) select", function () {
            var valor = $(this).find("option:selected").text();
            $(this).parent().find(".label").text(valor);
        });

//        var autocomplete = new google.maps.places.Autocomplete(self.find('#cep').get(0), {types: ['geocode']});
//
//        autocomplete.addListener('places_changed', function(){
//            var place = autocomplete.getPlace();
//
//            console.log(place);
//
//            var address = '';
//            if (place.address_components) {
//                address = [
//                    (place.address_components[0] && place.address_components[0].short_name || ''),
//                    (place.address_components[1] && place.address_components[1].short_name || ''),
//                    (place.address_components[2] && place.address_components[2].short_name || '')
//                ].join(' ');
//            }
//            
//            console.log(address);
//            
//        });
//
//        if (store('cep') != null && self.find('#cep').val() == "") {
//            var storecep = store('cep');
//            storecep = (storecep + "").replace('-', '');
//            self.find('#cep').val(storecep);
//        }

        $('.input-range', self).each(function () {
            var _input = $(this);

            var min = ~~_input.attr('data-min');
            var max = ~~_input.attr('data-max');

            var start = ~~_input.attr('data-star');
            var end = ~~_input.attr('data-end');

            var step = ~~_input.attr('data-step');

            noUiSlider.create(_input.get(0), {
                'start': [(start || min), (end || max)],
                'step': step,
                'connect': true,
                'range': {
                    'min': min,
                    'max': max
                }
            });

            var blockfiltro = _input.closest('.range-wrapper');
            var inputmax = blockfiltro.find('input[data-max]');
            var inputmin = blockfiltro.find('input[data-min]');
            var lbmax = blockfiltro.find('.valor-max');
            var lbmin = blockfiltro.find('.valor-min');

            inputmax.blur(function () {

                _input.get(0).noUiSlider.set([~~inputmin.val(), ~~inputmax.val()]);
            });

            inputmin.blur(function () {
                _input.get(0).noUiSlider.set([~~inputmin.val(), ~~inputmax.val()]);
            });

            _input.get(0).noUiSlider.on('update', function (values, handle) {
                var val = ~~values[handle];
                if (handle === 1) {
                    inputmax.val(val);
                    lbmax.text('R$ ' + val);
                } else {
                    inputmin.val(val);
                    lbmin.text('R$ ' + val);
                }
                //(handle ? limitFieldMax : limitFieldMin).innerHTML = values[handle];
            });

        });

    };
});
