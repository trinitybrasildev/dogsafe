define(function () {
    var self;

    function loginFinal(r) {
        
        FB.api('/me?fields=id,name,email,permissions,public_profile', function (response) {

            $.post('/facelogin', {'code': r.authResponse.accessToken}, function (data) {

                if(data && 'redir' in data){
                    location.href = data.redir;
                }
            },'json');
        }, {scope: 'email'});
    }


    return function () {

         var self = $(this);

        self.find('a[href="#esqueci-senha"]').click(function(e){
            
            e.preventDefault();
            
            if ($(this).hasClass('add-cadstro') == false) {
                
                self.attr('data-action', '/esquece-senha');
                
                self.find('#email').attr('name', 'recemail');
                self.find('button[type="submit"]').val('Recupera');
                self.find('a[href="#esqueci-senha"]').text('REALIZAR LOGIN');
                self.find('.titulo-login').text('Recuperar senha');
                
                self.find('#senha').css('display', 'none').removeAttr('required').attr('disabled', true);
                
                self.find('#tipo').removeClass('hide');
                        
                self.find('.off-recup').hide();
                $(this).addClass('add-cadstro');
                
            } else {
                
                location.hash = '';
                location.href = '/identificacao';
            }
        });

        if (location.hash == '#esqueci-senha') {
            self.find('a[href="#esqueci-senha"]').click();
        }
        

        $("#btn-facebook").off().click(function (e) {
            e.preventDefault();
            
            window.FB.login(function(response){

                if (response.status === 'connected') {

                    loginFinal(response);
                } else if (response.status === 'not_authorized') {
                    
                    console.log('Please log into Facebook.');
                } else {
                    
                    console.log('Please log into Facebook.');
                }
                
            }, {scope: 'public_profile,email',  display : 'popup', redirect_uri: 'https://dogsafe.com.br/identificacao'});
            
            return false;

        });

    };

});
