define(function () {
    
    return function () {
        $('.perguntas-frequentes .pergunta').click(function (e) {
            e.preventDefault();
            $(this).parent().find('.resposta').slideToggle(200, function () {});
        });
    }
});