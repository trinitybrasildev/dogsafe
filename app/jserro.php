<?php
@header("X-XSS-Protection:0; mode=block");
@header("Access-Control-Allow-Origin:*");
@header("Access-Control-Allow-Methods:GET");
@header("Access-Control-Allow-Headers:Origin, X-Requested-With, Content-Type, Accept, Authorization, x-file-name, x-mime-type");

if(isset($_GET['jserro']) && strlen($_GET['jserro']) > 3){
    
    $ip = $_SERVER['REMOTE_ADDR']? : ($_SERVER['HTTP_X_FORWARDED_FOR']? : $_SERVER['HTTP_CLIENT_IP']);
    $date  = date('Y-m-d H:i');
    $user = $_SERVER['HTTP_USER_AGENT'];
    
    @error_log("\n\t\r".$ip . '@' . $date . ' in '. $user .' => '. $_GET['jserro'], 3, "jserro.log");
       
}
exit;
