<footer class="footer">
    <div class="footer-container container">
        <div class="nav-container">
            <h2 class="title title-medium title-border">empresa</h2>

            <nav class="menu-footer">
                <ul>
                    <li class="menu-item"><a href="../xhtml/index.php">inicial</a></li>
                    <li class="menu-item"><a href="../xhtml/sobre.php">dog safe</a></li>
                    <li class="menu-item"><a href="../xhtml/como-funciona.php">como funciona</a></li>
                    <li class="menu-item"><a href="../xhtml/privacidade.php">política de privacidade</a></li>
                    <li class="menu-item"><a href="../xhtml/termos.php">termos de uso</a></li>
                </ul>
            </nav>
        </div>

        <div class="nav-container">
            <h2 class="title title-medium title-border">conteúdo</h2>

            <nav class="menu-footer">
                <ul>
                    <li class="menu-item"><a href="../xhtml/blog.php">blog</a></li>
                    <li class="menu-item"><a href="../xhtml/dicas.php">dicas</a></li>
                    <li class="menu-item"><a href="../xhtml/parceiros.php">parceiros</a></li>
                    <li class="menu-item"><a href="../xhtml/depoimentos.php">depoimentos</a></li>
                    <li class="menu-item"><a href="../xhtml/fale-conosco.php">fale conosco</a></li>
                </ul>
            </nav>
        </div>

        <div class="nav-container nav-big">
            <h2 class="title title-medium title-border">top cidades</h2>
            <div href="#" class="mais">
                <span class="mais-text">mais cidades</span>

                <div class="mais-cidades">
                    <ul>
                        <li><a href="#">SÃO PAULO/SP</a></li>
                        <li><a href="#">RIO DE JANEIRO/RJ</a></li>
                        <li><a href="#">BELO HORIZONTE/MG</a></li>
                        <li><a href="#">RIO GRANDE DO SUL/RS</a></li>
                        <li><a href="#">CAMPO GRANDE/MS</a></li>
                    </ul>
                </div>
            </div>

            <div class="nav-wrapper">
                <nav class="menu-footer">
                    <ul>
                        <li class="menu-item"><a href="#">SÃO PAULO/SP</a></li>
                        <li class="menu-item"><a href="#">RIO DE JANEIRO/RJ</a></li>
                        <li class="menu-item"><a href="#">BELO HORIZONTE/MG</a></li>
                        <li class="menu-item"><a href="#">RIO GRANDE DO SUL/RS</a></li>
                        <li class="menu-item"><a href="#">CAMPO GRANDE/MS</a></li>
                    </ul>
                </nav>

                <nav class="menu-footer">
                    <ul>
                        <li class="menu-item"><a href="#">CUIABÁ/MT</a></li>
                        <li class="menu-item"><a href="#">CURITIBA/PR</a></li>
                        <li class="menu-item"><a href="#">SALVADOR/BA</a></li>
                        <li class="menu-item"><a href="#">FLORIANÓPOLIS/SC</a></li>
                        <li class="menu-item"><a href="#">GOIÂNIA/GO</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="newsletter">
            <form class="newsletter-form">
                <input type="text" name="email" placeholder="Insira o seu e-mail e receba novidades">
                <button type="submit" class="btn btn-small btn-claro">cadastrar</button>
            </form>

            <i class="sprite i-pata-small"></i>

            <div class="localidade">
                <a href="#" class="btn btn-small btn-laranja"><i class="sprite i-localizacao-branco"></i></a>
                <span class="text-destaque text-destaque-small"><strong>VOCÊ ESTÁ EM: </strong>CAMPO GRANDE/MS</span>
            </div>
        </div>

        <div class="contatos">
            <div class="siga-container">
                <span class="text-destaque text-destaque-medium"><strong>SIGA-NOS </strong> NAS REDES SOCIAIS</span>
                <a href="#" class="i-facebook-big"><i class="sprite i-facebook-big"></i></a>
                <a href="#" class="i-instagram-big"><i class="sprite i-instagram-big"></i></a>
            </div>
            <i class="sprite i-pata-small"></i>

            <div class="pague-container">
                <span class="text-destaque text-destaque-medium"><strong>PAGUE  </strong> COM A PLATAFORMA</span>
                <a href="#" class="i-paypal"><i class="sprite i-paypal"></i></a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="base-rodape">
            <span class="copyright">Copyright &copy; 2016</span>
            <a href="#" class="logo-dog-footer"><img src="../img/logo-dogsafe-grande.png"></a>
        </div>

        <div class="base-rodape">
            <span>Site Criado por:</span>
            <a href="#" class="logo-trinity"><img src="../img/logo-trinity-brasil.png"></a>
        </div>
    </div>
</footer>


<script src="../js/require.js" data-main="../js/app"></script>
<script src="../js/browser_selector.js"></script>
<script src="../js/ie-behavior.js"></script>
<script>
    requirejs(["app", 'recorte', 'menu', 'modernizr', 'smoothscroll'], function(app, recorte, menu) {
        app();
        
    });

</script>
</body>

</html>
