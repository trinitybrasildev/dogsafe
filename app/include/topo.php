<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="pt-br"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="pt-br"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="pt-br"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="pt-br">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>
        <?= $tituloDaPagina ?>
    </title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="../css/style.css?<?= date('YmdHis') ?>">
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui.min.css">

        <link rel="apple-touch-icon" sizes="36x36" href="../img/favicon/android-icon-36x36.png">
    <link rel="apple-touch-icon" sizes="48x48" href="../img/favicon/android-icon-48x48.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../img/favicon/android-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="96x96" href="../img/favicon/android-icon-96x96.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../img/favicon/android-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="192x192" href="../img/favicon/android-icon-192x192.png">
    <link rel="apple-touch-icon" sizes="57x57" href="../img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../img/favicon/android-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="192x192" href="../img/favicon/android-icon-192x192.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">

    <meta name="msapplication-TileColor" content="#1B8692">
    <meta name="theme-color" content="#1B8692">

    <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
      <script src="../js/html5.js"></script>
    <![endif]-->
</head>

<body>
    <header class="header-principal container">
        <div class="icon-container">
            <a href="facebook.com"></a>
            <a href="instagram.com"></a>
        </div>

        <nav class="main-menu">
            <div class="main-menu-hamburger" id="menu-hamburguer">
                <i></i>
                <i></i>
                <i></i>
            </div>

            <ul class="link link-bold menu">
                <li>
                    <a href="../xhtml/encontre-um-anfitriao.php">encontre um anfitrião</a></li>
                <li><a href="../xhtml/como-funciona.php">como funciona</a></li>
                <li class="box-nao-logado">
                    <a href="../xhtml/cadastre-se.php">cadastre-se</a>
                    <a href="../xhtml/identificacao.php">entrar</a>
                </li>
                <li class="box-logado none">
                    <span class="nome">Oi, Diogo</span>
                    <div class="links-wrapper">
                        <a href="../xhtml/minha-conta-anfitriao.php" class="bt-blue">minha conta</a>
                        <a href="#" class="bt-red">sair</a>
                    </div>
                </li>
                <li>
                    <div class="link-ultimo">
                        <a href="../xhtml/tornar-se-anfitriao.php" class="btn btn-big btn-escuro">tornar-se um anfitrião</a>
                    </div>
                </li>

                <li class="ultimo">
                    <div class="icon-container">
                        <span><span class="link-bold">SIGA-NOS</span> NAS REDES SOCIAIS</span>
                        <a href="facebook.com"></a>
                        <a href="instagram.com"></a>
                    </div>
                </li>
            </ul>
        </nav>

        <a href="../xhtml/index.php" class="main-logo">
            <img src="../img/logo-dogsafe-grande.png">
        </a>
    </header>
