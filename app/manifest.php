<?php
header('Content-Type: text/cache-manifest');

$filesToCache = array(
    './css/fancybox.css',
    './css/style.css',
    './css/tooltipster.css',
    './css/jquery-ui.min.css'
);

$filejs = scandir('./js', 1);

foreach ($filejs as $js) {
    if ($js != '..' && $js != '.' && is_dir('./js/' . $js) === false)
        $filesToCache[] = './js/' . $js;
}

$fileimg = scandir('./img', 1);

foreach ($fileimg as $img) {
    if ($img != '..' && $img != '.' && is_dir('./img/' . $img) === false)
        $filesToCache[] = './img/' . $img;
}

//$filefont = scandir('./font', 1);
//
//foreach ($filefont as $font) {
//    if ($font != '..' && $font != '.' && is_dir('./font/' . $font) === false)
//        $filesToCache[] = './font/' . $font;
//}
//
//$filevideo = scandir('./video', 1);
//
//foreach ($filevideo as $video) {
//    if ($video != '..' && $video != '.' && is_dir('./video/' . $video) === false)
//        $filesToCache[] = './video/' . $video;
//}

?>
CACHE MANIFEST

CACHE:
<?php

/*
./como-funciona
./tornar-se-anfitriao
./blog
./privacidade
./termos
./css/styles.css

*/

// Print files that we need to cache and store hash data
$hashes = '';
foreach ($filesToCache as $file) {
    //echo $file . "\n";
    //$hashes .= md5_file($file);
};
//NETWORK:*
?>



# Hash Version: <?=md5(date('YmdHis'))?>