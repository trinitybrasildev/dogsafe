<?php
require(dirname(__FILE__) . '/bootstrap.php');

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'log.enabled' => true,
    'debug' => true
));

# === Routes
require_once 'routes.php';

$app->run();

db()->_disconect();
exit;