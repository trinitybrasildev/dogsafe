<?php

require(dirname(dirname(dirname(__FILE__))) . '/app/bootstrap.php');
require_once(COD_DIR_VENDOR . '/Codando/helps/simple_html_dom.php');

function getCEP($cep) {

    $cacheName = 'cep-' . $cep;

    $cache = new \Codando\System\Cache(1000, COD_DIR_APP . '/_arquivos/cache/');

    $reqcache = $cache->get($cacheName);

    if ($reqcache !== TRUE) {
        return $reqcache;
    }

    $url = 'http://www.qualocep.com/busca-cep/' . $cep;

//    $proxies = array();
//
//    $proxies[] = '189.28.166.79:80';
//
//
//    if (isset($proxies)) {
//        $proxy = $proxies[array_rand($proxies)];
//    }

    $ch = curl_init();

//    if (isset($proxy)) {
//        curl_setopt($ch, CURLOPT_PROXY, $proxy);
//    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, 'http://www.qualocep.com/');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $cache->createCache($cacheName, $result);

    $cache = NULL;

    return $result;
}

$objList = array();

//$objList[] = current(app()->listModulo('cadastro', " ( LTRIM(RTRIM(latlong)) = '' OR latlong IS NULL ) ", 1, ' RAND() '));
$objList[] = current(app()->listModulo('anfitriao', " ( LTRIM(RTRIM(latlong)) = '' OR latlong IS NULL ) ", 1, ' RAND() '));

foreach ($objList as $obj) {

    if(method_exists($obj, 'getCep')){
        
        $contents = getCEP($obj->getCep());

        $dom = new simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);

        $dom->load($contents, true, true);

        $resultado = $dom->find('#googleMapImage', 0);

        $latlong = '000';

        if ($resultado) {

            $src = $resultado->getAttribute('src');
            $colorbl = @explode('color:blue|', $src);
            $latlong = end($colorbl);
            $latlongspace = @explode(' ', $latlong);
            $latlong = implode(',', $latlongspace);
        }

        if (is_instanceof('Codando\Modulo\Anfitriao', $obj)) {

            db()->update('anfitriao', array('latlong' => $latlong), 'id_anfitriao = :id ', array('id' => $obj->getId()));
        }
    }

}

db()->_disconect();
exit;
