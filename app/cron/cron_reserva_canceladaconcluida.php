<?php

require(dirname(dirname(__FILE__)) . '/bootstrap.php');

/*
 * Concluida reserva após 3 dias
 */
$dateTimeat = new \DateTime;
$dateTimeat->modify('-3 day'); // Prazo 3 dias

$reservaController = new \Codando\Controller\Reserva();

// Listagem de 6: Cancelada 
$reservadelList = app()->listModulo('reserva', array(" ( ( DATE_FORMAT(datafim,'%Y-%m-%d') <= :date AND id_modalidade = 1 ) OR ( DATE_FORMAT(datainicio,'%Y-%m-%d') <= :date AND id_modalidade = 2 ) ) AND status = 6 AND valorcancelado > 0 ", array('date' => $dateTimeat->format('Y-m-d')))); 

/* @var $reservadel \Codando\Modulo\Reserva */
foreach ($reservadelList as $reservacon) {

    $reservaController->setModulo($reservacon);
    
    $reservaController->sendMoney();
   
}

db()->_disconect();
exit;
