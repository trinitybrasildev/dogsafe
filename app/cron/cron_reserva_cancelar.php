<?php

require(dirname(dirname(__FILE__)) . '/bootstrap.php');

/*
 * Cancelar reserva após 3 dias sem interação
 */
$date = date('Y-m-d', strtotime('-3 day'));//Menos 3 dias
$reservaController = new \Codando\Controller\Reserva();

$reservadelList = app()->listModulo('reserva', array(" DATE_FORMAT(datacadastro,'%Y-%m-%d') <= :date AND status = 3 ", array('date' => $date))); // 3: aguardando 

/* @var $reservadel \Codando\Modulo\Reserva */
foreach ($reservadelList as $reservadel) {
    
    db()->update('reserva', array('status' => 6), ' id_reserva = :id ', array('id' => $reservadel->getId()));//6:Cancelada
    
    $reservaController->setModulo($reservadel);
    
    $reservaController->_avisoCancelada('Reserva foi cancelada após 3 dias sem interação.');
    
    logsgc('cancelamento', 42, $reservadel->getId(), 'Reserva foi cancelada após 3 dias sem interação.'); //, Aguardando pagamento.
}

db()->_disconect();
exit;
