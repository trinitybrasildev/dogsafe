<?php

require(dirname(dirname(__FILE__)) . '/bootstrap.php');

$date = date('Y-m-d', strtotime('-3 day')); //Menos 3 dias

$reservaController = new \Codando\Controller\Reserva();

// 7: aguardando paypal DATE_FORMAT(datacadastro,'%Y-%m-%d') <= :date AND
$reservadelList = app()->listModulo('reserva', array(" DATE_FORMAT(datacadastro,'%Y-%m-%d') <= :date AND paypal IS NOT NULL AND status = 7 ", array('date' => $date)));

/* @var $reservack \Codando\Modulo\Reserva */
foreach ($reservadelList as $reservack) {

    $reservaController->setModulo($reservack);
    $checkout = $reservaController->getCheckout(true);
    
//    echo '<pre>';
//    var_dump($checkout);
//    echo '</pre>';
    
    if (is_array($checkout) === false || isset($checkout['ACK']) == false) {

        
       db()->update('reserva', array('status' => 6), ' id_reserva = :id ', array('id' => $reservack->getId()));
       continue;
    }
    
    switch ($checkout['ACK']){
      case 'Success':
          //db()->update('reserva', array('status' => 6), ' id_reserva = :id ', array('id' => $reservack->getId()));
          break;
      case 'Failure':
          db()->update('reserva', array('status' => 6), ' id_reserva = :id ', array('id' => $reservack->getId()));
          break;
    }
    
}

db()->_disconect();
exit;