<?php

\Slim\Route::setDefaultConditions(array(
    'url' => '[a-z0-9-]+',
    'id' => '[0-9]+',
    'ano' => '[0-9]+',
    'mes' => '[0-9]+',
    'cep' => '[0-9]+'
));

$callogado = '\Codando\Route\Auth::logado'; //@FAZER: DUPLO
$calnotlogado = '\Codando\Route\Auth::notlogado';

/* HOME */
$app->get('/', function() {
    tpl()->display('index', array('menuCurrent' => 'index'));
});

/* LOGIN */
$app->get('/sair', '\Codando\Route\Auth:logout');

$app->get('/ceptolatlong/:cep', '\Codando\Route\Cep:findTogeo');

$app->map('/identificacao', $calnotlogado, '\Codando\Route\Auth:login')->via('GET','POST');
$app->map('/facelogin', '\Codando\Route\Auth:loginfacebook')->via('GET','POST');
$app->map('/facecadastro', '\Codando\Route\Auth:cadastrofacebook')->via('GET','POST');
$app->map('/esquece-senha', $calnotlogado, '\Codando\Route\Auth:esquecesenha')->via('POST');
$app->map('/recuperar', $calnotlogado, '\Codando\Route\Auth:recuperar')->via('GET');
$app->map('/recuperar', $calnotlogado, '\Codando\Route\Auth:alterasenha')->via('POST');
$app->map('/codigo-senha', $calnotlogado, '\Codando\Route\Cadastro:codigoSenha')->via('GET','POST');
$app->map('/alterar-cadastro', $callogado, '\Codando\Route\Cadastro:update')->via('GET','POST');
$app->get('/minha-conta', $callogado, '\Codando\Route\Auth:minhaconta');
$app->get('/minha-conta-hospede', $callogado, '\Codando\Route\Auth:minhacontahospede');

// Cadastro
$app->map('/cadastre-se', $calnotlogado, '\Codando\Route\Cadastro:insert')->via('GET','POST');
$app->map('/meu-cadastro', $callogado, '\Codando\Route\Cadastro:update')->via('GET','POST');

//Anfitriao
$app->map('/tornar-se-anfitriao', '\Codando\Route\Anfitriao:insert')->via('GET','POST');
$app->map('/meu-perfil', $callogado, '\Codando\Route\Anfitriao:meuperfil')->via('GET','POST'); 
$app->map('/editar-anfitriao', $callogado,  '\Codando\Route\Anfitriao:update')->via('GET','POST'); 
$app->map('/remarcaentrevista-anfitriao', $callogado,  '\Codando\Route\Anfitriao:reagendamento')->via('GET','POST'); 
$app->get('/minha-agenda', $callogado , '\Codando\Route\Anfitriao:agenda');
$app->get('/encontre-um-anfitriao', '\Codando\Route\Anfitriao:listAll');
$app->get('/anfitriao/:url-:id', '\Codando\Route\Anfitriao:load'); 
$app->get('/anfitriaochangestatus/:id', '\Codando\Route\Anfitriao:changestatus'); // mudanca do status feita no sgc
$app->get('/entrevistas', $callogado, '\Codando\Route\Anfitriao:listEntrevista');

//Visita
$app->post('/visita', $callogado, '\Codando\Route\Visita:insert'); // @FAZER: TERMINAR

//Comentario
$app->post('/comentarioanfitriao/:id', $callogado, '\Codando\Route\Comentarioanfitriao:insert'); // @FAZER: TERMINAR

//Compromisso
$app->post('/compromisso', $callogado, '\Codando\Route\Compromisso:insert');
$app->get('/meus-compromisso', $callogado, '\Codando\Route\Compromisso:listAll'); 
$app->get('/excluir-compromisso/:id', $callogado, '\Codando\Route\Compromisso:excluir');

//Pet
$app->get('/meus-pets', $callogado, '\Codando\Route\Pet:listAll');
$app->map('/adicionar-pet', $callogado, '\Codando\Route\Pet:insert')->via('GET','POST');
$app->map('/editar-pet/:id', $callogado, '\Codando\Route\Pet:update')->via('GET','POST');
$app->get('/ficha-pet/:id', $callogado, '\Codando\Route\Pet:ficha');
$app->get('/excluir-pet/:id', $callogado, '\Codando\Route\Pet:excluir');

//Reserva
$app->get('/minhas-reservas', $callogado, '\Codando\Route\Reserva:listCadastro');
$app->get('/celendario', '\Codando\Route\Reserva:calendario');
$app->post('/reservar/:id', $callogado, '\Codando\Route\Reserva:insert');
$app->map('/reserva/:id', $callogado, '\Codando\Route\Reserva:index')->via('GET','POST');
$app->get('/reserva/confirmar/:id', $callogado, '\Codando\Route\Reserva:confirmar');
$app->get('/reserva/cancelar/:id', $callogado, '\Codando\Route\Reserva:cancelar');
$app->get('/reserva/pagar/:id', $callogado, '\Codando\Route\Reserva:pagar');

$app->get('/reservar/executepayment/:id', '\Codando\Route\Reserva:payment');
$app->get('/reservar/getpayment/:id', '\Codando\Route\Reserva:getPayment');
$app->get('/reservar/masspay/:id', '\Codando\Route\Reserva:massPay');

$app->get('/email/pay', '\Codando\Route\Reserva:email');

//FAQ
$app->get('/perguntas-frequentes', '\Codando\Route\Faq:listAll');

//Chat
$app->get('/chat', $callogado, '\Codando\Route\Chat:index');
$app->get('/chat/:id', $callogado, '\Codando\Route\Chat:insert');
$app->get('/chat/listconversa', $callogado, '\Codando\Route\Chat:listConversa'); 
$app->get('/chat/countconversa', $callogado, '\Codando\Route\Chat:listConversa');
$app->post('/chat/adicionarconversa', $callogado, '\Codando\Route\Chat:insertConversa');

//Blog
$app->get('/blog', '\Codando\Route\Noticia:listAll');
$app->get('/blog/:url-:id', '\Codando\Route\Noticia:load');

//Dicas
$app->get('/dicas', '\Codando\Route\Dica:listAll');
$app->get('/dicas/:url-:id', '\Codando\Route\Dica:load');

//Fotos
$app->get('/fotos', '\Codando\Route\Foto:listAll');
$app->get('/fotos/:url-:id', '\Codando\Route\Foto:load');

//Video
$app->get('/videos', '\Codando\Route\Video:listAll');
$app->get('/videos/:url-:id', '\Codando\Route\Video:load');

//Parceiros
$app->get('/parceiros', '\Codando\Route\Parceiro:listAll');

//Depoimento
$app->get('/depoimentos', '\Codando\Route\Depoimento:listAll');
$app->post('/depoimento/novo', $callogado, '\Codando\Route\Depoimento:insert');

//Fale Conosco
$app->map('/fale-conosco', '\Codando\Route\Contato:index')->via('GET','POST');
$app->map('/trabalhe-conosco', '\Codando\Route\Trabalhe:index')->via('GET','POST');
$app->post('/newsletter', '\Codando\Route\Newsletter:insert');

$app->get('/ver-meus-dados', '\Codando\Route\Find:index');
$app->post('/consulta-dados', '\Codando\Route\Find:load');
$app->post('/deletar-fale-conosco', '\Codando\Route\Find:deleteContato');
$app->post('/deletar-cadastro', '\Codando\Route\Find:deleteCadastro');
$app->post('/deletar-anfitriao', '\Codando\Route\Find:deleteAnfitriao');

$app->get('/como-funciona', '\Codando\Route\Comofunciona:index');
$app->get('/sobre', '\Codando\Route\Pagina:loadSobre');
$app->get('/termo', '\Codando\Route\Pagina:loadTermo');
$app->get('/termos', '\Codando\Route\Pagina:loadTermo');
$app->get('/privacidade', '\Codando\Route\Pagina:loadPrivacidade');

$app->get('/erro404', function(){
    tpl()->display('erro', array('menuCurrent' => 'erro'));
});

$app->get('/cidades', '\Codando\Route\Cidade:listAll');
$app->post('/cidadegeocode', '\Codando\Route\Cidade:cidadeGeocode');