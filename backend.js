'use strict';

var fs = require('fs'),
        events = require('events'),
        path = require('path'),
        gutil = require('gulp-util'),
        through = require('through2'),
        YAML = require('yamljs');

var PluginError = gutil.PluginError,
        event = new events.EventEmitter();

//Util
var log = console.log.bind(console);

String.prototype.matchAll = function (regexp) {
    var matches = [];
    this.replace(regexp, function () {
        var arr = ([]).slice.call(arguments, 0);
        var extras = arr.splice(-2);
        arr.index = extras[0];
        arr.input = extras[1];
        matches.push(arr);
    });
    return matches.length ? matches : null;
};

function ucwords(str) {
    return (str + '')
            .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase();
            });
}

function read(file, callback) {
    try {

        if (callback === undefined)
            return fs.readFileSync(file, 'utf8');

        fs.readFile(file, 'utf8', function (err, data) {
            if (err) {
                return log(err);
            }
            callback(file, data);
        });

    } catch (e) {

        log('Error:', e.stack);
    }
}
;

function write(file, data) {
    fs.writeFile(file, data, 'utf8', function (err) {
        if (err)
            return log(err);
    });
}
;

//Variavel config

//Diretorios padrão
var pathVendor = 'vendor';
var pathApp = 'app';
var routeList = [];
var moduloList = [];

//http://www.regexpal.com/
// namespace Codando\Modulo; // Sempre check namespace
// /is_instanceof(?:[a-z]*)\(\'([a-z\\]+)\'\,(.*)\)/ig USANDO MODULO
// /\$(.*)\s\=\sapp\(\)\-\>(load|list)Modulo\(\'([a-z]+)\'\,?(.*)\)\;/ig USANDO MODULO
// /\>(map|get|post)\(\'(\/[a-z-\/]+)\'\,\s?(?:[a-z\$\_\,]*)\s\'([A-z\:]+)(.*)\'\)\;\s?([\/\/]*)/ig PEGAR ROTAS
// /\-\>display\(\'([a-z\/]+)\'\,\s?(.*)\)\;/ig USANDO Templete
// /data\-controller\=\"([a-z0-9A-Z]+)\"/ig Pegando controller JS
// /action\=\"([a-z0-9A-Z-\/]+)\"/ig Pegando URL action
// /href\=\"([a-z0-9A-Z-\/\?\.\=\&]+)\"/ig Pegando URL


//Expressões
var regEx = {
    'route': '/\>(map|get|post)\(\'(\/[a-z-\/]+)\'\,\s?(?:[a-z\$\_\,]*)\s\'([A-z\:]+)(.*)\'\)\;\s?([\/\/]*)/ig',
    'usetmpl': '/\-\>display\(\'([a-z\/]+)\'\,\s?(.*)\)\;/ig'
};

/*
 * Carregar as rotas de /app/routes.php
 */
function routes(file, base) {

    routeList = [];
    var data = read(file);
    var res = data.matchAll(regEx.route);
    var count = res && res.length ? res.length : 0;

    for (var i = 0; i < count; i++) {
        var text = res[i][0];
        var method = res[i][1];
        var url = res[i][2];
        var callmetodo = res[i][3];
        var pathClass = pathProject + pathVendor + callmetodo.split(':')[0];
        var splClas = pathClass.split('\\');
        var nameClass = splClas[splClas.length - 1];
        var classExist = fs.existsSync(pathClass + '.php');

        routeList.push({
            'find': text,
            'url': url,
            'method': method,
            'callmetodo': callmetodo,
            'nameClass': nameClass,
            'pathClass': pathClass,
            'classExist': classExist
        });

        log(nameClass, classExist);
    }

}

/*
 * Carregar os mudulo de /app/config/modulo.yml
 */
function laodModuloYml(file) {
    //paths['moduloyml'] = file;
    try {

        moduloList = YAML.load(file);

//            _.each(self.moduloList, function (a, b) {
//                //log(a, b);
//            });

        //var moduloyaml = YAML.stringify(self.moduloList, 4);

    } catch (e) {

        moduloList = [];
    }
}


//Fn de Tipos files
function php(file, base) {

    console.log(arguments, 'php');

    if (base.indexOf('route') !== -1) {
        routes(file);
        return;
    }
}

function phtml(file, base) {
    console.log(arguments, 'phtml');
}

function html(file, base) {
    console.log(arguments, 'html');
}

function js(file, base) {
    console.log(arguments, 'js');
}

function yml(file, base) {
    console.log(arguments, 'yml');

    if (base === 'modulo') {
        laodModuloYml(file);
        return;
    }
}

function classif(file) {

    var ext = path.extname(file);
    var base = path.basename(file, ext);

    switch (ext) {
        case '.phtml'://templates
            phtml(file, base);
            break
        case '.html'://html recorte
            html(file, base);
            break;
        case '.js'://javascript
            js(file, base);
            break;
        case '.php'://Classes ou routes ou config
            php(file, base);
            break;
        case '.yml'://Config de modulos
            yml(file, base);
            break;
    }
}

//Exports e Init
module.exports = function () {

    return through.obj(function (file, enc, callback) {

        if (file.isNull() || file.isDirectory()) {
            this.push(file);
            return callback();
        }

        if (['.php', '.phtml', '.yml', '.html', '.js'].indexOf(path.extname(file.path)) === -1) {

            this.emit('error', new PluginError({
                plugin: 'backend',
                message: 'Not Supported formats .'
            }));

            return callback();
        }

        if (file.isStream()) {

//            this.emit('error', new PluginError({
//                plugin: 'backend',
//                message: 'Streams are not supported.'
//            }));

            return callback();
        }

        if (!file.isBuffer()) {

//            this.emit('error', new PluginError({
//                plugin: 'backend',
//                message: 'Not Buffer are supported.'
//            }));

            return callback();
        }

        // Windows...
        var pathname = file.relative.replace(/\\/g, '/');

        if (!pathname) {
            callback();
            return;
        }

        //Classificar
        classif(pathname);

        //console.log('path', pathname, 'cwd', file.cwd, 'base', file.base);

        callback();
    });
};