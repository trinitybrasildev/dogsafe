<?php

namespace Codando\Controller;

class Comofunciona extends Controller {

    public function _load() {

        $pag_id = (int) input()->_toGet('id_comofunciona', '%d', true);

        if ($pag_id !== NULL) {

            $this->modulo = app()->loadModulo('comofunciona', array('id_comofunciona = :id_comofunciona', array('id_comofunciona' => $pag_id)));

            if (is_instanceof('Codando\Modulo\Comofunciona', $this->modulo) === FALSE) {

                $this->error = true;
                $this->messageList = "Comofunciona não encontrado";
            }
        } else {

            $this->error = true;
            $this->messageList = "Comofunciona não encontrado";
        }
    }

    public function _list($order = 'ordem ASC', $where = array()) {

        $parsWhere = array();

        $this->findWhere("comofunciona", $where, $parsWhere, $order);
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct($autoInitialize = false) {

    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
