<?php

namespace Codando\Controller;

class Chat extends Controller {

    public $feedback;

    public static function _insert(\Codando\Modulo\Reserva $reserva) {

        $cadastro = $reserva->getCadastro();
        
        $anfitriao = $reserva->getAnfitriao();
        
        $this->modulo = app()->loadModulo('chat', array('id_anfitriao = :id_anfitriao AND id_cadastro = :cadastro ', array('id_anfitriao' => $anfitriao->getId(), 'cadastro' => $cadastro->getId() )));

        if (is_instanceof('Codando\Modulo\Chat', $this->modulo) === true) {
            return true;
        }

        $chat['id_cadastro'] = $cadastro->getId();
        $chat['id_anfitriao'] = $anfitriao->getId();
        $chat['data'] = date('Y-m-d H:i:s');

        $id_chat = db()->insert('chat', $chat);

        $this->_load($id_chat);

        if (is_instanceof('Codando\Modulo\Chat', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Não foi possivel.';
            return false;
        }

        return true;
    }

    protected function insertBy(\Codando\Modulo\Cadastro $cadastro,  \Codando\Modulo\Anfitriao $anfitriao) {

        $this->modulo = app()->loadModulo('chat', array('id_anfitriao = :id_anfitriao AND id_cadastro = :cadastro ', array('id_anfitriao' => $anfitriao->getId(), 'cadastro' => $cadastro->getId() )));

        if (is_instanceof('Codando\Modulo\Chat', $this->modulo) === true) {
            return true;
        }

        $chat['id_cadastro'] = $cadastro->getId();
        $chat['id_anfitriao'] = $anfitriao->getId();
        $chat['data'] = date('Y-m-d H:i:s');

        $id_chat = db()->insert('chat', $chat);

        $this->_load($id_chat);

        if (is_instanceof('Codando\Modulo\Chat', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Não foi possivel.';
            return false;
        }

        return true;
    }
    
    
    public function _load($con_id) {

        $cadastro = (int) input()->getSession('logadocadastro');

        $this->modulo = app()->loadModulo('chat', array('id_chat = :id_chat AND id_cadastro = :cadastro ', array('id_chat' => $con_id, 'cadastro' => $cadastro)));

        if (is_instanceof('Codando\Modulo\Chat', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Chat não encontrado';
            return false;
        }
    }

    protected function _listConversa($order = 'id_chatconversa ASC', $where = array()) {

        $parsWhere = array();
        $sqlWhere = NULL;

        $ref = (int) input()->_toGet('ref', '%d', false);

        $chat = (int) input()->_toGet('chat', '%d', false);

        if (is()->id($chat) === true) {

            $parsWhere['chat'] = $chat;

            $where[] = '( id_chat = :chat )';
        } else {

            $where[] = '( id_chat = 0 )';
        }

        if (is()->id($ref) === true) {

            $parsWhere['ref'] = $ref;

            $where[] = '( id_chatconversa > :ref )';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo('chatconversa', $sqlWhere);

        if ($count < 1)
            return $this->moduloList = array();

        $this->setPaginar($count);

        $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

        $this->moduloList = app()->listModulo('chatconversa', $sqlWhere, $limit, $order);
    }

    public function _countConversa() {

        $where = array();
        $parsWhere = array();
        $sqlWhere = NULL;

        $chat = (int) input()->_toGet('chat', '%d', false);

        if (is()->id($chat) === true) {

            $parsWhere['chat'] = $chat;

            $where[] = '( id_chat = :chat )';
        } else {

            $where[] = '( id_chat = 0 )';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo('chatconversa', $sqlWhere);

        global $retornoList;

        $retornoList['count'] = $count;
    }

    public function _insertConversa() {

        $_chat = array(
            'id_chat' => '%d',
            'texto' => 'xss%s'
        );

        $chat = input()->_toPost($_chat, '%s', true);

        if (is_array($chat) === false) {

            $this->error = true;
            $this->messageList[] = 'Digite a mensagem';
            return false;
        }

        $cadastro = (int) input()->getSession('logadocadastro');
        $anfitriao = (int) input()->getSession('logadoanfitriao');
        
        $this->modulo = app()->loadModulo('chat', array(' id_chat = :chat AND (id_cadastro = :cad OR id_anfitriao = :anfitriao) ', array('chat' => $chat['id_chat'], 'cad' => $cadastro, 'anfitriao' => $anfitriao)));

        if (is_instanceof('Codando\Modulo\Chat', $this->modulo) === false) {

            $this->error = true;
            return false;
        }
        
        $chat['nome'] = input()->getSession('nome');
         
        if($cadastro == $this->modulo->getCadastro()->getId()){
            $chat['tipo'] = 1;
            $chat['ref'] = $cadastro;
        }
        
        if($anfitriao == $this->modulo->getAnfitriao()->getId()){
            $chat['tipo'] = 2;
            $chat['ref'] = $anfitriao;
        }
        
        $chat['data'] = date('Y-m-d H:i:s');
        
        //Insert conversa
        $id_chatconversa = db()->insert('chatconversa', $chat);
        
        if($id_chatconversa < 1){
            return false;
        }
        
        $chatconversa = app()->loadModulo('chatconversa', array(' id_chatconversa = :chatconversa ', array('chatconversa' => $id_chatconversa)));
        
        if (is_instanceof('Codando\Modulo\Chatconversa', $chatconversa) === false) {
             return false;
        }
        
        $count = app()->countModulo('chatconversa', array(' id_chat = :chat', array('chat' => $chat['id_chat'])));

        if ($count == 1)
            $this->_send();
        
        
        return $chatconversa;
    }

    public function _list($order = 'id_chat DESC', $where = array()) {

        $parsWhere = array();
        $sqlWhere = NULL;

        //$busca = input()->_toGet('qc', 'xss%s', false);

        $cadastro = (int) input()->getSession('logadocadastro');
        $anfitriao = (int) input()->getSession('logadoanfitriao');
        
        if ($cadastro > 0 && $anfitriao < 1) {
            $parsWhere['cadastro'] = $cadastro;
            $where[] = '( id_cadastro = :cadastro )';
        }

        
        if ($anfitriao > 0 && $cadastro < 1) {
            $parsWhere['anfitriao'] = $anfitriao;
            $where[] = '( id_anfitriao = :anfitriao )';
        }
        
        if ($anfitriao > 0 && $cadastro > 0) {
            $parsWhere['cadastro'] = $cadastro;
            $parsWhere['anfitriao'] = $anfitriao;
            $where[] = '( id_anfitriao = :anfitriao OR id_cadastro = :cadastro  )';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo('chat', $sqlWhere);

        if ($count < 1)
            return $this->moduloList = array();

        $this->setPaginar($count);

        $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

        $this->moduloList = app()->listModulo('chat', $sqlWhere, $limit, $order);
    }

    public function _excluir() {

        $chat = input()->_toGet('chat');

        if (is()->id($chat) == false) {
            return false;
        }

        $cadastro = (int) input()->getSession('logadocadastro');

        $this->modulo = app()->loadModulo('chat', array('( id_chat IN ( SELECT ch.id_chat FROM chat ch WHERE ch.id_chat = :chat AND ( ch.id_cadastro = :cad OR ch.id_anfitriao IN ( SELECT an.id_anfitriao FROM anfitriao an WHERE an.id_cadastro = :cada ) ) ) )', array('chat' => $chat, 'cad' => $cadastro, 'cada' => $cadastro)));

        if (is_instanceof('Codando\Modulo\Chat', $this->modulo) === false) {
            $this->error = true;
            $this->messageList[] = 'Chat não encontrado';
            return false;
        }

        db()->delete('chatconversa', ' id_chat = :id ', array('id' => $this->modulo->getId()));
        db()->delete('chat', ' id_chat = :id ', array('id' => $this->modulo->getId()));
    }

    public function _send() {
        
             
        /* @var $chat \Codando\Modulo\Chat */
        $chat = $this->modulo;

        /* @var $anfitriaoTemp \Codando\Modulo\Anfitriao */
        $anfitriaoTemp = $chat->getAnfitriao();

        /* @var $cadastroTemp \Codando\Modulo\Cadastro */
        $cadastroTemp = $chat->getCadastro();
        $cadastro = (int) input()->getSession('logado');

        /* @var $cadastroLogado \Codando\Modulo\Cadastro */
        $cadastroLogado = app()->loadModulo('cadastro', array('id_cadastro = :id ', array('id' => $cadastro)));

        $emailArray = explode(',', $anfitriaoTemp->getEmail());

        $emailFields = array(
            'Nome' => $cadastroLogado->getNome(),
            'Email' => $cadastroLogado->getEmail(),
            'Veja o chat:' => '<a href="http://dogsafe.com.br/chat#' . $chat->getId() . '" target="_blank"> Abrir o Chat</a>'
        );

        $html_email = tpl()->display('contato_send', array('titulo' => 'Oba! Tem mensagem nova pra você com: ' . $cadastroTemp->getNome(), 'emailFields' => $emailFields), false);
        
        $mailer = $this->swiftMailer();
        
        $message = \Swift_Message::newInstance('Chat')
                ->setFrom(array($cadastroLogado->getEmail() => $cadastroLogado->getNome()))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        //$message->setBcc(array("luizz@luizz.com.br" => "Luizz"));

        if ($mailer->send($message)) {

            return true;
        }

        return false;
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function _initialize() {

        $this->_doRoute();
    }

    public function __construct($autoInitialize = false) {

        if ($autoInitialize === true) {
            $this->_initialize();
        }

        $this->feedback = array();
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}