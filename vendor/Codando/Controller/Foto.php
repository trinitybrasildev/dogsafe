<?php

namespace Codando\Controller;

class Foto extends Controller {

    public function _list($order = 'date DESC, id_foto DESC', $where = array()) {

        $parsWhere = array();
        $sqlWhere = NULL;

        $busca = input()->_toGet('q', 'xss%s', false);

        if (is()->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) OR LOWER(date) LIKE LOWER(:busca) )';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo('foto', $sqlWhere);

        $this->setPaginar($count);

        $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

        $this->moduloList = app()->listModulo('foto', $sqlWhere, $limit, $order);
    }

    public function _load($con_id = NULL) {

        $con_id = $con_id !== NULL ? $con_id : input()->_toRequest('id_foto', '%d', true);

        if ($con_id !== NULL) {

            $this->modulo = app()->loadModulo('foto', array('id_foto = :id_foto', array('id_foto' => $con_id)));

            if (is_instanceof('Codando\Modulo\Foto', $this->modulo) === false) {

                $this->error = true;
                $this->messageList[] = "Foto não encontrado";
            }
        } else {

            $this->error = true;
            $this->messageList = input()->getMsg();
        }
    }

    public function updadeAcesso($foto) {

        if (is_instanceof('Codando\Modulo\Foto', $foto) === true) {

            db()->update('foto', array('acesso' => ($foto->getAcesso() + 1)), ' id_foto = :id_foto ', array('id_foto' => $foto->getId()));
        }
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
