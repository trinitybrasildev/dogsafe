<?php

namespace Codando\Controller;

/**
 * 	E-mail: luizz@luizz.com.br
 * 	Copyright 2016 - Todos os direitos reservados
 */
class Depoimento extends Controller {

    protected function _valid() {

        $this->clearMessages();

        $dado_required = array(
            'texto' => 'xss%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Digite seu depoimento.';
            return false;
        }

        $dado['id_cadastro'] = input()->getSession('logadocadastro');
        $dado['id_anfitriao'] = input()->getSession('logadoanfitriao');

        if ($dado['id_cadastro'] < 1 || $dado['id_anfitriao'] < 1) {
            $this->error = TRUE;
            $this->messageList[] = " Você tem que esta logado.";
            return false;
        }

        $dado['nome'] = input()->getSession('nome');

        $dado['arquivo'] = '/_arquivos/cadastro/' . input()->getSession('imagemcadastro');

        if (input()->getSession('imagemcadastro') == null || strpos($dado['arquivo'], 'semimagem/')) {

            $dado['arquivo'] = '/_arquivos/anfitriao/' . input()->getSession('imagemanfitriao');
        }

        if (input()->getSession('imagemanfitriao') == null || strpos($dado['arquivo'], 'semimagem/')) {

            $dado['arquivo'] = false;
        }

        return $dado;
    }

    protected function _list($order = 'data DESC, id_depoimento', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', false);

        if (is()->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) OR LOWER(texto) LIKE LOWER(:busca) )';
        }
        $where[] = 'status <> 2';

        $this->findWhere("depoimento", $where, $parsWhere, $order);
    }

    protected function _insert() {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        $arquivo = $dado['arquivo'];

        unset($dado['arquivo']);
        $dado['status'] = 2;
        $dado['data'] = date('Y-m-d');
        //inserir depoimento
        $id_cadastro = db()->insert('depoimento', $dado);

        $this->_load($id_cadastro);

        if (is_instanceof('Codando\Modulo\Depoimento', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Depoimento não inserido.';
            return false;
        }

        if ($arquivo !== false) {

            $filenameRe = pathinfo($arquivo);

            $new_arquivo = array(
                'url' => $filenameRe['filename'],
                'id_est' => $this->modulo->getId(),
                'id_modulo' => 29, //Depoimento
                'ordem' => 1
            );
    
            if (copy(COD_DIR_APP . $arquivo, COD_DIR_APP . '/_arquivos/depoimento/' . $filenameRe['filename'])) {

                $id_arquivo = db()->insert('arquivo', $new_arquivo);

                return $id_arquivo;
            }
        }

        //Mensagem de retorno
        $this->messageList[] = "Depoimento cadastrado com sucesso.";
        $this->error = false;
        
        $this->_avisoNovo();
        
        return true;
    }

    protected function _load($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toRequest('id_depoimento', '%d', true);

        if (is()->id($not_id) == false) {

            $this->error = true;
            $this->messageList[] = "Dica invalida";
            return false;
        }

        $this->modulo = app()->loadModulo('depoimento', array('id_depoimento = :id_depoimento', array('id_depoimento' => $not_id)));

        if (is_instanceof('Codando\Modulo\Depoimento', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Depoimento não encontrado";
        }
    }

    protected function _avisoNovo() {

        $_config = \Codando\App::getConfig('emailsend');

        $departamento = app()->loadModulo('departamento', ' id_departamento = ' . $_config['departament_depoimento']);

        if (is_instanceof('Codando\Modulo\Departamento', $departamento) === false) {
            return false;
        }

        /* @var $depoimento \Codando\Modulo\Depoimento */
        $depoimento = $this->modulo;

        $emailArray = (array) explode(',', $departamento->getEmail());

        $emailFields = array(
            'Nome' => $depoimento->getNome(),
            'Data' => $depoimento->getData(),
            'Gerenciar' => 'http://dogsafe.com.br/sgc#/carregar/depoimento/' . $depoimento->getId(),
        );

        $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields, 'titulo' => 'Novo depoimento'), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('DogSafe - Depoimento')
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
