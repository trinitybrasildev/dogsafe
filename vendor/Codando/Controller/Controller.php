<?php

namespace Codando\Controller;

use Codando\System\Paginar;

/**
 * 	E-mail: luizz@luizz.com.br
 * 	Copyright 2016 - Todos os direitos reservados
 */
abstract class Controller {

    /**
     * Lista de mensagens
     * @var array<string>
     */
    protected $messageList;

    /**
     * Informa se ocorreu algum erro no processamento da requisição
     * @var bool
     */
    protected $error;

    /**
     * Objeto que representa um Modulo
     * @var stdClass
     */
    protected $modulo;

    /**
     * Objeto que representa um Modulo
     * @var <array>stdClass
     */
    protected $moduloList;
    protected $paginar;
    protected $isPaginar;
    private $setLoadAllDependencies;
    protected $quantidadePorPagina = 20;
    protected $quantidadePaginacaoPorPagina = 10;
    protected $quantidadeRegistro;

    protected function findWhere($modulo, $where = array(), $parsWhere = array(), $order = NULL, $cond = " AND ") {

        $sqlWhere = array();

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode($cond, $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode($cond, $where);
        }

        $count = app()->countModulo($modulo, $sqlWhere);

        if ($count > 0) {

            $this->setPaginar($count);

            $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

            $this->moduloList = app()->listModulo($modulo, $sqlWhere, $limit, $order);
        } else {

            $this->moduloList = array();
        }
    }

    public function getModulo($returnNewObject = false) {

        if ($this->modulo == NULL && $returnNewObject !== false) {

            $this->modulo = new $returnNewObject;
        }

        return $this->modulo;
    }
    
    public function setModulo($moduloObject) {

        $this->modulo = $moduloObject;

    }

    public function getModuloList() {

        return (is_array($this->moduloList) ? $this->moduloList : array());
    }

    public function isPaginar($isPaginar = NULL) {
        return $this->isPaginar = $isPaginar !== NULL ? $isPaginar : $this->isPaginar;
    }

    public function setPaginar($quantidade) {

        $page = (int) input()->_toRequest('p', '%d');

        $this->quantidadeRegistro = $quantidade;

        $this->paginar = new \Codando\System\Paginar($quantidade, $this->getRegistrosPorPagina(), $page, $this->quantidadePaginacaoPorPagina);
    }

    public function getOffSetPaginar() {
        return $this->paginar->getOffSet();
    }

    public function getLimitPaginar() {
        return $this->paginar->getLimit();
    }

    public function getStartPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getPagStart();
        }
        return NULL;
    }

    public function getEndPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getPagEnd();
        }
        return NULL;
    }

    public function getTotalPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getNumPages();
        }
        return NULL;
    }

    public function getCurrentPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getPagNum();
        }

        return NULL;
    }

    public function getQuantidadeRegistro() {
        return $this->quantidadeRegistro;
    }

    public function setRegistrosPorPagina($quantidadePorPagina) {
        $this->quantidadePorPagina = $quantidadePorPagina;
    }

    protected function getRegistrosPorPagina() {
        return $this->quantidadePorPagina;
    }

    protected function swiftMailer() {

        $_config = \Codando\App::getConfig('emailsend');

        if (is_null($_config['senha']) === true) {

            $transport = \Swift_MailTransport::newInstance();
        } else {

            $transport = \Swift_SmtpTransport::newInstance($_config['smtp'], $_config['port'], $_config['type'])
                    ->setUsername($_config['email'])
                    ->setPassword($_config['senha']);
        }


        return \Swift_Mailer::newInstance($transport);
    }

    public function setQuantidadePaginacaoPorPagina($quantidadePaginacaoPorPagina) {
        $this->quantidadePaginacaoPorPagina = $quantidadePaginacaoPorPagina;
    }

    public function setUrl($urlBase) {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {

            $this->paginar->setBaseUrl($urlBase);
        }
    }

    public function getPaginacao($urlBase = NULL) {

        if ($urlBase !== NULL && is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE)
            $this->paginar->setBaseUrl($urlBase);

        return (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE ? $this->paginar->getPaginar() : NULL);
    }

    public function getMessageList() {

        return is_array($this->messageList) ? $this->messageList : array();
    }

    public function setLoadAllDependencies($loadAllDependencies) {
        $this->setLoadAllDependencies = $loadAllDependencies;
    }

    public function isError() {

        return $this->error;
    }

    public function clearMessages() {
        $this->setLoadAllDependencies = TRUE;
        $this->messageList = array();
        $this->error = false;
    }

    abstract public function __construct();
}
