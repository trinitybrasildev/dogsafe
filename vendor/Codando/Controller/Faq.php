<?php

namespace Codando\Controller;

/**
 * 	E-mail: luizz@luizz.com.br
 * 	Copyright 2016 - Todos os direitos reservados
 */
class Faq extends Controller {


    protected function _list($order = 'id_faq DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', FALSE);

        if (is()->nul($busca) === FALSE) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) OR LOWER(texto) LIKE LOWER(:busca) )';
        }


        $this->findWhere("faq", $where, $parsWhere, $order);
    }

    protected function _load($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toRequest('id_faq', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Faq invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('faq', array('id_faq = :id_faq', array('id_faq' => $not_id)));

        if (is_instanceof('Codando\Modulo\Faq', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Faq não encontrado";
        }
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
