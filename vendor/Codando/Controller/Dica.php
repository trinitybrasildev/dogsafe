<?php

namespace Codando\Controller;

/**
 * 	E-mail: luizz@luizz.com.br
 * 	Copyright 2016 - Todos os direitos reservados
 */
class Dica extends Controller {


    protected function _list($order = 'data DESC, id_dica DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', FALSE);

        if (is()->nul($busca) === FALSE) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) OR LOWER(texto) LIKE LOWER(:busca) )';
        }

        $parsWhere['data'] = date('Y-m-d H:i');
        $where[] = '( data <= :data AND status = 1 )';


        $this->findWhere("dica", $where, $parsWhere, $order);
    }

    protected function _load($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toRequest('id_dica', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Dica invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('dica', array('id_dica = :id_dica AND status = 1 ', array('id_dica' => $not_id)));

        if (is_instanceof('Codando\Modulo\Dica', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Dica não encontrado";
        }
    }

    public function updadeAcesso($dica) {

        if (is_instanceof('Codando\Modulo\Dica', $dica) === true) {

            db()->update('dica', array('acesso' => ($dica->getAcesso() + 1)), ' id_dica = :id_dica ', array('id_dica' => $dica->getId()));
        }
    }

    protected function _loadAtivo($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toGet('id_dica', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Dica invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('dica', array(' id_dica = :id_dica AND status = 1 AND data <= :data ', array('data' => date('Y-m-d H:i'), 'id_dica' => $not_id)));

        if (is_instanceof('Codando\Modulo\Dica', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Dica não encontrado";
        }
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
