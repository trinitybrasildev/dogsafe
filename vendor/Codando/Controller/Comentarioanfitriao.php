<?php

namespace Codando\Controller;

/**
 *  E-mail: luizz@luizz.com.br
 * Copyright 2016 - Todos os direitos comentarioanfitriaodos
 */
class Comentarioanfitriao extends Controller {

    public function _valid() {

        $this->clearMessages();

        $dado_required = array(
            'mensagem' => '%s',
            'avaliacao' => '%d'
        );

        $dado = input()->_toPost($dado_required, '%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*)';
            return false;
        }

        $dado['id_cadastro'] = input()->getSession('logadocadastro');

        if ($dado['id_cadastro'] < 1) {
            $this->error = true;
            $this->messageList[] = "Você tem que está logado como hospede.";
            return false;
        }

        return $dado;
    }

    public function _insert(\Codando\Modulo\Anfitriao $anfitriao) {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        $countReserva = app()->countModulo('reserva', array(' status = 5 AND id_anfitriao = :anf AND id_cadastro = :cad '), array('anf' => $anfitriao->getId(), 'cad' => $dado['id_cadastro']));

        if ($countReserva >= 1) {
            
            $this->error = true;
            $this->messageList[] = "Você não utilizou o serviço do anfitrião.";
            return false;
        }

        //Inserir comentarioanfitriao
        $id_cadastro = db()->insert('comentarioanfitriao', $dado);

        $this->_load($id_cadastro);

        if (is_instanceof('Codando\Modulo\Comentarioanfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Comentario não inserido.';
            return false;
        }

        //Mensagem de retorno
        $this->messageList[] = "Comentario cadastro com sucesso.";
        $this->error = false;

        //$this->_email();

        return true;
    }

    protected function _list($order = 'data DESC, id_comentarioanfitriao DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', FALSE);

        if (is()->nul($busca) === FALSE) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) )';
        }

        $parsWhere['id_anfitriao'] = input()->getSession('logadoanfitriao');
        $where[] = '( id_anfitriao = :id_anfitriao )';

        $this->findWhere("comentarioanfitriao", $where, $parsWhere, $order);
    }

    protected function _load($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toRequest('id_comentarioanfitriao', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Comentario invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('comentarioanfitriao', array('id_comentarioanfitriao = :id_comentarioanfitriao', array('id_comentarioanfitriao' => $not_id)));

        if (is_instanceof('Codando\Modulo\Comentarioanfitriao', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Comentario não encontrado";
        }
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
