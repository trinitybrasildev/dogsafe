<?php

namespace Codando\Controller;

class Pagina extends Controller {

    public function _load($id = null) {

        $pag_id = (int) ($id > 0 ? $id : input()->_toGet('id_pagina', '%d', true));

        if ($pag_id === NULL) {
            $this->error = true;
            $this->messageList = "Pagina não encontrado";
            return false;
        }

        $this->modulo = app()->loadModulo('pagina', array('id_pagina = :id_pagina', array('id_pagina' => $pag_id)));

        if (is_instanceof('Codando\Modulo\Pagina', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList = "Pagina não encontrado";
            return false;
        }
    }

    public function _list($order = 'id_pagina DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', FALSE);
        $busca = ltrim(rtrim($busca));
        $busca = htmlentities($busca, ENT_QUOTES, 'UTF-8');

        if (is()->nul($busca) === FALSE) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) )';
        }

        $this->findWhere("pagina", $where, $parsWhere, $order);
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct($autoInitialize = false) {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
