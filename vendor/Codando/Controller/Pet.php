<?php

namespace Codando\Controller;

/**
 *  E-mail: luizz@luizz.com.br
 * Copyright 2016 - Todos os direitos petdos
 */
class Pet extends Controller {

    public function _valid() {

        $this->clearMessages();

        $dado_required = array(
            'nome' => 'xss%s',
            'idade' => '%s',
            'id_tamanhopet' => '%d',
            'tipo' => '%d'
        );

        $dado_opt = array(
            'castrado' => '%d',
            'raca' => '%s',
            'sexo' => '%d',
            'planodesaude' => '%s',
            'alergia' => '%s',
            'doenca' => '%s',
            'medicacao' => '%s',
            'reqcuidado' => '%s',
            'carteira' => '%d',
            'rapaticidapulga' => '%s',
            'sobre' => '%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*)';
            return false;
        }

        $dado = array_merge($dado, input()->_toPost($dado_opt, '%s', false));

        $dado['id_cadastro'] = input()->getSession('logadocadastro');

        if ($dado['id_cadastro'] < 1) {
            $this->error = TRUE;
            $this->messageList[] = " Você tem que esta logado como hospede.";
            return false;
        }

        return $dado;
    }

    public function _insert() {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        //Inserir pet
        $id_pet = db()->insert('pet', $dado);

        $this->_load($id_pet);

        if (is_instanceof('Codando\Modulo\Pet', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Pet não inserido.';
            return false;
        }

        //Mensagem de retorno
        $this->messageList[] = "Pet cadastrado com sucesso.";
        $this->error = false;

        return true;
    }
    
    public function _update() {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        //Inserir pet
        $update = db()->update('pet', $dado, ' id_pet = :pet ', array('pet' => $this->modulo->getId()));


        //Mensagem de retorno
        $this->messageList[] = "Pet atualizado com sucesso.";
        $this->error = false;


        return true;
    }


    protected function _list($order = 'id_pet DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', false);

        $id_cadastro = input()->getSession('logadocadastro');
        
        if (is()->id($id_cadastro) == false) {

            $this->error = true;
            $this->messageList[] = "Pet inválido";
            return false;
        }
        
        if (is()->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(nome) LIKE LOWER(:busca))';
        }

        $parsWhere['cadastro'] = $id_cadastro;
        $where[] = '( id_cadastro = :cadastro AND excluido <> 1 )';

        $this->findWhere("pet", $where, $parsWhere, $order);
    }

    protected function _load($not_id) {

        $id_cadastro = input()->getSession('logadocadastro');

        if (is()->id($not_id) == false || is()->id($id_cadastro) == false) {

            $this->error = true;
            $this->messageList[] = "Pet inválido";
            return false;
        }

        $this->modulo = app()->loadModulo('pet', array('id_pet = :id_pet AND id_cadastro = :cadastro ', array('cadastro' => $id_cadastro, 'id_pet' => $not_id)));

        if (is_instanceof('Codando\Modulo\Pet', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Pet não encontrada";
        }
    }

    protected function _loadAtivo($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toGet('id_pet', '%d', true);

        if (is()->id($not_id) == false) {

            $this->error = true;
            $this->messageList[] = "Pet inválido";
            return false;
        }

        $this->modulo = app()->loadModulo('pet', array(' id_pet = :id_pet AND status = 1 AND data <= :data ', array('data' => date('Y-m-d H:i'), 'id_pet' => $not_id)));

        if (is_instanceof('Codando\Modulo\Pet', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Pet não encontrado";
        }
    }

    protected function _excluir() {

        db()->update('pet', array('excluido' => 1), ' id_pet = :id ', array('id' => $this->modulo->getId()));
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
