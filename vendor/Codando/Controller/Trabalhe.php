<?php

 namespace Codando\Controller;

 class Trabalhe extends Controller {

     public $feedback;
     public $dir = 'trabalhe';

     public function _insert() {

         $_trabalhe = array(
             'nome' => 'xss%s',
             'email' => 'EMAIL',
             'estado' => 'xss%s',
             'id_cidade' => '%d',
             'telefone' => 'xss%s',
             'cargo' => 'xss%s',
             'mensagem' => 'xss%s',
             'curriculo' => 'FILETEMP'
         );

         $trabalhe = input()->_toPost($_trabalhe, true);

         if (is_array($trabalhe) === FALSE) {
             $this->error = TRUE;
             $this->messageList[] = $this->feedback['pt'][0];
             return FALSE;
         }

         $arquivotemp = $trabalhe['curriculo'];
         $trabalhe['curriculo'] = NULL;
         unset($trabalhe['curriculo']);

         $id_trabalhe = db()->insert('trabalhe', $trabalhe);

         $this->_load($id_trabalhe);

         if (is_instanceof('Codando\Modulo\Trabalhe', $this->modulo) === FALSE) {

             $this->error = true;
             $this->messageList[] = $this->feedback['pt'][1];

             return FALSE;
         }

         $this->_send();

         $this->_upload($arquivotemp);

         return TRUE;
     }

     public function _load($con_id) {

         $this->modulo = app()->loadModulo('trabalhe', array('id_trabalhe = :id_trabalhe', array('id_trabalhe' => $con_id)));

         if (is_instanceof('Codando\Modulo\Trabalhe', $this->modulo) === False) {

             $this->error = true;
             $this->messageList[] = $this->feedback['pt'][2];
         }
     }

     /*
      * Verifica se mensagem ja foi envia, para nao repetir
      */
     public function _isSend($email, $mensagem) {

         $this->modulo = app()->loadModulo('trabalhe', array('MD5(mensagem) = :mensagem AND email = :email ', array('mensagem' => md5($mensagem), 'email' => $email)));

         if (is_instanceof('Codando\Modulo\Trabalhe', $this->modulo) === TRUE) {

             $this->messageList[] = $this->feedback['pt'][3];
             return TRUE;
         }

         return FALSE;
     }

     public function _upload($fileTemp) {

         //Exstensão permitida
         $ext = array('gif', 'jpeg', 'jpg', 'png', 'doc', 'docx', 'txt', 'pdf', 'rar', 'zip', 'dot');

         if (is_instanceof('Codando\Modulo\Trabalhe', $this->modulo) === FALSE) {

             $this->error = true;
             $this->messageList[] = "Trabalhe não encontrado";
             return FALSE;
         }

         if ($fileTemp === NULL || file_exists(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp) === FALSE) {

             $this->error = true;
             $this->messageList[] = "Arquivo temporario não encontrado.";
             return FALSE;
         }

         $pathinfor = pathinfo(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp);

         if (is_array($pathinfor) === FALSE) {
             $this->error = true;
             $this->messageList[] = "Arquivo temporario corrompido.";
             return FALSE;
         }

         $extRe = $pathinfor['extension'];
         $filenameRe = $pathinfor['filename'];

         if (in_array(strtolower($extRe), $ext) === FALSE) {

             $this->error = true;
             $this->messageList[] = "Arquivo não permitido";
             return FALSE;
         }

         if (strrpos($filenameRe, $this->dir) === FALSE) {

             $filenameRe = $this->dir . "-" . $filenameRe;
         }

         while (file_exists(COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe) === TRUE) {
             $filenameRe .= rand(10, 99);
         }

         $new_arquivo = array(
             'url' => $filenameRe . '.' . $extRe,
             'id_est' => $this->modulo->getId(),
             'id_modulo' => 31, // Modulo Trabalhe
             'ordem' => 1
         );

         if (rename(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp, COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe)) {

             $id_arquivo = db()->insert('arquivo', $new_arquivo);

             if ($id_arquivo > 0)
                 if (is_instanceof('Codando\Modulo\Arquivo', $this->modulo->getArquivo()) === TRUE && file_exists(COD_DIR_APP . "/_arquivos/" . $this->dir . "/" . $this->modulo->getArquivo()->getUrl()) === TRUE) {

                     $del = db()->delete('arquivo', ' id_arquivo = :arquivo ', array('arquivo' => $this->modulo->getArquivo()->getId()));

                     if ($del === TRUE) {
                         @unlink(COD_DIR_APP . "/_arquivos/" . $this->dir . "/" . $this->modulo->getArquivo()->getUrl());
                     }
                 }

             return $id_arquivo;
         }
     }

     public function _send() {

         $_config = \Codando\App::getConfig('emailsend');
         
         $estados = array(0 => " ", 1 => "Acre", 2 => "Alagoas", 3 => "Amazonas", 4 => "Amapá", 5 => "Bahia", 6 => "Ceará", 7 => "Distrito Federal", 8 => "Espírito Santo", 9 => "Goiás", 10 => "Maranhão", 11 => "Minas Gerais", 12 => "Mato Grosso do Sul", 13 => "Mato Grosso", 14 => "Pará", 15 => "Paraíba", 16 => "Pernambuco", 17 => "Piauí", 18 => "Paraná", 19 => "Rio de Janeiro", 20 => "Rio Grande do Norte", 21 => "Rondônia", 22 => "Roraima", 23 => "Rio Grande do Sul", 24 => "Santa Catarina", 25 => "Sergipe", 26 => "São Paulo", 27 => "Tocantins");
         
         $deparmento = app()->loadModulo('departamento', array('id_departamento = :id_departamento', array('id_departamento' => $_config['departament_trabalhe'])));

         if (is_instanceof('Codando\Modulo\Departamento', $deparmento) === TRUE) {

             // Create the Transport
             $transport = \Swift_MailTransport::newInstance();



             /* @var $trabalhe \Codando\Modulo\Trabalhe */
             $trabalhe = $this->modulo;

             $emailArray = explode(',', $deparmento->getEmail());

             $emailFields = array(
                 'Nome' => $trabalhe->getNome(),
                 'Telefone' => $trabalhe->getTelefone(),
                 'Email' => $trabalhe->getEmail(),
                 'Cidade' => $trabalhe->getCidade(),
                 'Estado' => $estados[$trabalhe->getEstado()],
                 'Cargo' => $trabalhe->getCargo()
             );

             $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields), false);

             $mailer = \Swift_Mailer::newInstance($transport);

             $message = \Swift_Message::newInstance('Novo Curriculo')
                     ->setFrom(array('trabalhe@site.com.br' => 'Novo Curriculo'))
                     ->setTo($emailArray)
                     ->addPart($html_email, 'text/html');

             if ($mailer->send($message)) {

                 // Create the Transport
                 $transportCliente = \Swift_MailTransport::newInstance();
                 $mailerCliente = \Swift_Mailer::newInstance($transportCliente);
                 $messageCliente = \Swift_Message::newInstance('Envio Curriculo')
                         ->setFrom(array('contato@site.com.br' => 'Envio Curriculo'))
                         ->setTo(array($trabalhe->getEmail()))
                         ->addPart('<h1>Obrigado por enviar seu curriculo.</h1>', 'text/html');

                 $mailerCliente->send($messageCliente);

                 return TRUE;
             } else {

                 return FALSE;
             }
         } else {

             return FALSE;
         }
     }

     public function cleanStateController() {

         $this->clearMessages();
         $this->modulo = NULL;
         $this->moduloList = array();
     }


     public function __construct() {

         $this->feedback = array(
             'pt' => array('Preencha os campos obrigatórios(*), corretamente!', 'Curriculo não inserido', 'Curriculo não encontrado', 'Curriculo já foi enviado anteriormente.'),
         );
     }

     public function __destruct() {

         unset($this->modulo, $this->moduloList);
     }

 }
 