<?php

namespace Codando\Controller;

class Cadastro extends Controller {

    private $feedback;
    private $dir = 'cadastro';

    public function _valid($isup = false) {

        $this->clearMessages();

        $dado_required = array(
            'cep' => '%s',
            'endereco' => '%s',
            'celular' => 'xss%s',
            'id_cidade' => '%d',
            'email' => 'EMAIL'
        );
        
        $dado_opt = array('perfil' => 'FILETEMP', 'termos' => '%d', 'latlong' => '%s');
        
        if ($isup === false){
            $dado_required['senha'] = '%s';
            $dado_required['nome'] = '%s';
            $dado_required['cpf'] = '%s';
        }else{
            $dado_opt['senha'] = '%s';
        }

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*) e o arquivo(s)';
            return false;
        }

        $dado = array_merge(input()->_toPost($dado_opt, 'xss%s', false), $dado);
        
        if ($isup === false)
        $dado['cpf'] = preg_replace("/[^0-9]/", '', $dado['cpf']);
        $dado['cep'] = preg_replace("/[^0-9]/", '', $dado['cep']);

        $telefoneClean = preg_replace("/[^0-9]/", '', $dado['celular']);

        $ddd = substr($telefoneClean, 1, 2);
        $telefone = substr($telefoneClean, 2);

        if (strlen($ddd) < 2) {
            $this->error = true;
            $this->messageList[] = " DDD do Telefone do cadastro esta invalido.";
            return false;
        }

        if (strlen($telefone) < 8) {
            $this->error = true;
            $this->messageList[] = " Telefone do cadastro esta invalido.";
            return false;
        }

        //Senha
        if (strlen($dado['senha']) < 4 && $isup == false) {

            $this->error = true;
            $this->messageList[] = 'Senha deve conter mais de 4 caracteres.';
            return false;
        }


        if ($isup != false) {

            //Email
            if ($this->_isExist(array('email' => $dado['email'], 'emaildiff' => $this->modulo->getEmail()), 'email = :email AND email <> :emaildiff') == true) {

                $this->error = true;
                return FALSE;
            }
            
//            if ($this->_isExist(array('cpf' => $dado['cpf'], 'cpfdiff' => $this->modulo->getCpf()), 'cpf = :cpf AND cpf <> :cpfdiff', 'CPF já cadastrado') == true) {
//
//                $this->error = true;
//                return FALSE;
//            }
            
        } else {
            
            if ($this->_isExist(array('cpf' => $dado['cpf']), 'cpf = :cpf', 'CPF já cadastrado') == true) {

                $this->error = true;
                return false;
            }
            
            if ($this->_isExist(array('email' => $dado['email'])) == true) {

                $this->error = true;
                return false;
            }
        }

        unset($dado['termos'], $dado['confirmarsenha'], $dado['fotoperfil']); //Campo não salvos

        return $dado;
    }

    public function _insert() {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        $dado['password'] = password_hash($dado['senha'], PASSWORD_BCRYPT);
        $dado['datacadastro'] = date('Y-m-d H:i:s');
        unset($dado['perfil'], $dado['senha']);
        
        //Inserir cadastro
        $id_cadastro = db()->insert('cadastro', $dado);

        $this->_load($id_cadastro);

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Cadastro não inserido, tente mais tarde.';
            return false;
        }

        //lastlogin
        $this->lastLogin();

        //Mensagem de retorno
        $this->messageList[] = "Cadastro realizado com sucesso, bem vindo a DogSafe.";
        $this->error = false;

        
        //$this->messageList[] = 'Cadastro salvo com sucesso';
        //$this->error = false;
        //$this->_updateImagem('fotoperfil');
        $this->_emailBemvindo();

        return true;
    }

    private function lastLogin() {

        $ip = $_SERVER['REMOTE_ADDR'] ?: ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']);
        $date = date('Y-m-d H:i');
        $user = $_SERVER['HTTP_USER_AGENT'];

        $lastlogin = $ip . '@' . $date . ' in ' . $user;

        db()->update('cadastro', array('lastlogin' => $lastlogin), ' id_cadastro = :cads ', array('cads' => $this->modulo->getId()));

        input()->setSession('logado', $this->modulo->getId());
        input()->setSession('logadocadastro', $this->modulo->getId());
        input()->setSession('imagemcadastro', is_instanceofOrNew('Codando\Modulo\Arquivo', $this->modulo->getArquivo())->getUrl());
        input()->setSession('nome', current(@explode(' ', $this->modulo->getNome())));
        input()->setSession('lastlogincadastro', $this->modulo->getLastlogin());
        
    }

    public function _update() {

        $this->_loadLogado();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Usuario não logado';
            return false;
        }

        $dado = $this->_valid(true);

        if (is_array($dado) === false) {
            return false;
        }

        $fotoperfil = isset($dado['perfil']) ? $dado['perfil'] : null;

        //Senha
        if (is()->nul($dado['senha']) === true) {

            $dado['senha'] = NULL;
            unset($dado['senha']);
        } else {

            $dado['senha'] = password_hash($dado['senha'], PASSWORD_BCRYPT);
            $dado['password'] = $dado['senha'];
            unset($dado['senha']);
        }

        unset($dado['perfil']);

        $update = db()->update('cadastro', $dado, 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));

//        if ($update === false) {
//
//            $this->error = true;
//            $this->messageList[] = 'Cadastro não alterado';
//        }

        $this->messageList[] = 'Cadastro alterado com sucesso';
        $this->error = false;

        if ($fotoperfil !== NULL)
            $this->_updateFotoPerfil($fotoperfil);

        return true;
    }

    public function _updateSenha() {

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false)
            $this->_loadLogado();

        $dado_required = array(
            'senha' => 'xss%s',
            'confsenha' => 'xss%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->messageList[] = "Informe os dados da senha.";
            $this->error = false;
            return false;
        }

        //Senha
        if (md5($dado['senha']) != md5($dado['confsenha'])) {

            $this->error = true;
            $this->messageList[] = 'Confirmação de senha está diferente da senha.';
            return false;
        }

        $dado['confsenha'] = NULL;
        unset($dado['confsenha']);

        $dado['senha'] = md5($dado['senha']);

        $update = db()->update('cadastro', $dado, 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));

        if ($update === false) {

            $this->error = true;
            $this->messageList[] = "Senha não atualizada";
            return false;
        }

        $this->messageList[] = "Senha alterado com sucesso";
        $this->error = false;
    }

    public function _login() {

        $this->clearMessages();

        global $retornoList;

        $recuperar = input()->_toPost('recemail');

        if ($recuperar != NULL) {
            return $this->_recupera($recuperar);
        }

        $_dado = array(
            'email' => 'EMAIL',
            'senha' => '%s'
        );

        $dado = input()->_toPost($_dado);

        if (is_array($dado) === false) {
            $this->error = true;
            $this->messageList[] = 'Informe os dados de login.';
            return false;
        }

        /* @var $this->modulo Codando\Modulo\Cadastro */
        $this->modulo = app()->loadModulo('cadastro', array(' email = :email', array('email' => $dado['email'])));

        //Logar Geral
        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === true && $dado['senha'] === 'd0gs4f3') {
            //lastlogin
            $this->lastLogin();
            $this->error = false;
            $this->messageList[] = 'Aguarde ...';
            $retornoList['redir'] = '/minha-conta';
            return true;
        }

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false || password_verify($dado['senha'], $this->modulo->getSenha()) === false) {

            $this->error = true;
            $this->messageList[] = 'Dados invalidos, porfavor verifique seu dados.';
            $this->modulo = NULL;
            return false;
        }

        //lastlogin
        $this->lastLogin();
        $this->error = false;
        $this->messageList[] = 'Aguarde ...';
        $retornoList['redir'] = '/minha-conta';
        return true;
    }

    public function _cadastroFacebook($name, $email) {

        $this->clearMessages();

        if (is_null($name) === true || is_null($email) === true) {
            $this->error = true;
            $this->messageList[] = 'Não foi possivel realizar o login com facebook.';
            return false;
        }

        //Email
        if ($this->_isExist(array('email' => $email)) == true) {

            $this->error = true;
            return false;
        }

        $senha = geraSenha(2, true, false, false) . geraSenha(3, false, false, true);
        $_config = \Codando\App::getConfig('cadastro');

        $dado['nome'] = $name;
        $dado['email'] = $email;
        $dado['id_plano'] = $_config['planogratis'];
        $dado['senha'] = password_hash($senha, PASSWORD_BCRYPT);
        $dado['datacadastro'] = date('Y-m-d H:i:s');
        $dado['tipo'] = 1; //Particular
        $dado['id_cidade'] = 1; //Particular
        //Inserir cadastro
        $id_cadastro = db()->insert('cadastro', $dado);

        $this->_load($id_cadastro);

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Cadastro não inserido.';
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->error = false;
        $this->messageList[] = "aguarde ...";

        global $retornoList;

        $retornoList['redir'] = '/';

        $this->_emailBemvindo();

        return true;
    }

    public function _loginFacebook($email) {

        $this->clearMessages();

        if (is_null($email) === true) {
            $this->error = true;
            $this->messageList[] = 'Não foi possivel realizar o login com facebook.';
            return false;
        }

        /* @var $this->modulo \Codando\Modulo\Cadastro */
        $this->modulo = app()->loadModulo('cadastro', array(' email = :email ', array('email' => $email)));

//            if (password_verify($dado['senha'], $this->modulo->getSenha())) {
//                //Senha correta
//            } else {
//                $this->error = true;
//                $this->messageList[] = "E-mail ou senha está incorreta.";
//                return false;
//            }

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado.";
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->error = false;
        $this->messageList[] = "aguarde ...";

        return true;
    }

    public function _recupera($email) {

        $this->clearMessages();

        $this->modulo = app()->loadModulo('cadastro', array(' email = :email ', array('email' => $email)));

        /* @var $this->modulo \Codando\Modulo\Cadastro */
        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Cadastro não encontrado.';
            sleep(2);
            return false;
        }

        $datetime = date('Y-m-d H:i');

        do {
            $senhahash = cryp()->enc($this->modulo->getId() . '@:@' . $datetime);
        } while (strrpos($senhahash, '+') !== false);

        $emailFields = array(
            'Acesse o link para alterar sua senha' => 'https://dogsafe.com.br/recuperar?senha=' . $senhahash . '&tipo=2',
            'Caso não solicitou ignore esta mensagem.' => NULL
        );

        $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields, 'titulo' => 'Pedido de recuperação de senha'), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('Recuperar a senha na DOGSAFE')
                ->setFrom(array('contato@dogsafe.com.br' => 'Recuperar a senha'))
                ->setTo(array($email))
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            $this->error = false;
            $this->messageList[] = 'Um e-mail foi enviado a você com instruções.';
            return true;
        }

        $this->error = true;
        $this->messageList[] = 'Não foi possível, tente mais tarde.';
        return false;
    }

    public function _loadRecupera($senhaHash) {

        $recuperar = cryp()->dec($senhaHash);

        $dados = explode('@:@', $recuperar);

        if (count($dados) != 2) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        $strtotime1 = strtotime('+1 day', strtotime($dados[1]));
        $strtotime2 = strtotime(date('Y-m-d H:i'));

        if ($strtotime1 < $strtotime2) {
            $this->error = true;
            $this->messageList[] = "Link expirado";
            return false;
        }

        $this->modulo = app()->loadModulo('cadastro', array('id_cadastro = :id_cadastro', array('id_cadastro' => $dados[0])));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        $this->error = false;
        $this->messageList[] = "Logado com sucesso";

        input()->setSession('logado-senha', $this->modulo->getId());
    }

    public function _updateRecuperarSenha() {


        $this->modulo = app()->loadModulo('cadastro', array('id_cadastro = :id_cadastro ', array('id_cadastro' => input()->getSession('logado-senha'))));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {
            $this->error = true;
            $this->messageList[] = 'Cadastro não encontrado';
            return false;
        }

        $dado_required = array(
            'senha' => 'xss%s',
            'confsenha' => 'xss%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {
            $this->error = true;
            $this->messageList[] = 'Informe a nova senha';
            return false;
        }

        //Senha
        if ($dado['senha'] !== $dado['confsenha']) {

            $this->error = true;
            $this->messageList[] = 'Confirmação de senha está diferente da senha';
            return false;
        }

        $dado['confsenha'] = NULL;
        unset($dado['confsenha']);

        $dado['senha'] = password_hash($dado['senha'], PASSWORD_BCRYPT);
        $dado['password'] = $dado['senha'];
        unset($dado['senha']);
        
        $update = db()->update('cadastro', $dado, 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));

        if ($update === false) {

            $this->error = true;
            $this->messageList[] = 'Senha não foi atualizada';
            return false;
        }

        $this->messageList[] = 'Senha alterado com sucesso';
        $this->error = false;
        return true;
    }

    public function _list($order = 'id_cadastro DESC', $where = array()) {

        $parsWhere = array();
        $sqlWhere = NULL;

        $busca = input()->_toGet('q', 'xss%s', false);

        if (is()->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(nome) LIKE LOWER(:busca) OR LOWER(email) LIKE LOWER(:busca) )';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo('cadastro', $sqlWhere);

        if ($count < 1)
            return $this->moduloList = array();

        $this->setPaginar($count);

        $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

        $this->moduloList = app()->listModulo('cadastro', $sqlWhere, $limit, $order);
    }

    public function isLogado() {
        return is()->id(input()->getSession('logadocadastro')) ? input()->getSession('logadocadastro') : false;
    }

    public function _loadLogado() {

        if ($this->isLogado() !== false) {
            $this->_load($this->isLogado());
        }
    }

    public function _load($con_id = NULL) {

        $con_id = $con_id !== NULL ? $con_id : input()->_toGet('id_cadastro', '%d', true);

        if ($con_id !== NULL) {

            $this->modulo = app()->loadModulo('cadastro', array('id_cadastro = :id_cadastro', array('id_cadastro' => $con_id)));

            if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

                $this->error = true;
                $this->messageList[] = "Cadastro não encontrado";
                return false;
            }
            return false;
        }

        $this->error = true;
        $this->messageList = "Cadastro não encontrado";
    }

    public function _loadByUrl() {

        $url = input()->_toGet('url_nome', 'xss%s', true);

        if ($url !== NULL) {

            $this->modulo = app()->loadModulo('cadastro', array('url_nome = :url', array('url' => $url)));

            if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

                $this->error = true;
                $this->messageList[] = 'Não foi possivel encontra cadastro';
                return false;
            }
        }

        return true;
    }

    public function _isExist($dados, $where = 'email = :email', $msg = NULL) {

        $msg = $msg === NULL ? 'E-mail já esta sendo utilizado.' : $msg;

        $_where = array($where, $dados);

        $modulo = app()->loadModulo('cadastro', $_where);

        if (is_instanceof('Codando\Modulo\Cadastro', $modulo) === true) {

            $this->error = true;
            $this->messageList[] = $msg;
        }

        return is_instanceof('Codando\Modulo\Cadastro', $modulo);
    }

    private function _emailBemvindo() {

        $_config = \Codando\App::getConfig('emailsend');


        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $this->modulo;

        $emailArray = array($cadastro->getEmail());

        $html_email = tpl()->display('email/default', array('texto' => 'bemvindo'), false, false);

        $mailer = $this->swiftMailer();
        
        $nome = current(@explode(' ', $cadastro->getNome()));
        
        $message = \Swift_Message::newInstance('Bem-vindo à DogSafe, '. $nome)
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        }

        return false;
    }

    public function _loadConfirma() {

        $emailHash = input()->_toGet('email');

        $email = cryp()->dec($emailHash);

        $this->modulo = app()->loadModulo('cadastro', array('email = :email AND status = 2', array('email' => $email)));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        $update = db()->update('cadastro', array('status' => '1'), 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));
        input()->setSession('logado-confirmacao', $this->modulo->getId());
    }

    public function _sendNewConf() {

        $email = input()->_toPost('email', 'EMAIL');

        if ($email === NULL) {

            $this->error = true;
            $this->messageList[] = "E-mail não encontrado";
            return false;
        }

        $this->modulo = app()->loadModulo('cadastro', array('email = :email AND status = 2', array('email' => $email)));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        if ($this->_sendConfirmacao($this->modulo) === true) {

            $this->messageList[] = "Enviamos um email para '" . strip_tags($this->modulo->getEmail()) . "' com a confirmação do seu cadastro.";
            $this->error = false;
            return false;
        }
    }

    /**
     * Enviar confirma de cadastro
     * @param \Codando\Modulo\Cadastro $cadastro Cadastro inativo
     * @return Boolean true Enviado, FALSO não enviado
     */
    public function _sendConfirmacao($cadastro) {

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === true) {

            $_config = \Codando\App::getConfig('emailsend');

            $emailArray = explode(',', $cadastro->getEmail());

            do {
                $pro_hash = cryp()->enc($cadastro->getEmail());
            } while (strrpos($pro_hash, '+') !== false);

            $emailFields = array(
                'Link para confirma' => COD_URL . 'confirmacao-email/' . $pro_hash
            );

            $html_email = tpl()->display('contato_send', array('titulo' => 'Está é uma confirmação do seu cadastro realizado no site:' . COD_URL, 'emailFields' => $emailFields), false);

            $mailer = $this->swiftMailer();

            $message = \Swift_Message::newInstance('Confirmação de cadastro')
                    ->setFrom(array($_config['return'] => 'Confirmação de cadastro'))
                    ->setTo($emailArray)
                    ->addPart($html_email, 'text/html');

            if ($mailer->send($message)) {

                return true;
            }

            return false;
        }
    }

    public function _updateFotoPerfil($fotoperfil) {

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false)
            $this->_loadLogado();

        $this->_upload($fotoperfil, 'perfil', 1);
    }

    public function _updateImagem($nome = 'fotoperfil', $ordem = 1) {

        $this->clearMessages();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false)
            return false;

        $dado_required = array();
        $dado_required[$nome] = 'FILETEMP';

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            //$this->messageList[] = "Selecione uma imagem.";
            //$this->error = true;
            return false;
        }

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->messageList[] = "Você não esta logado.";
            $this->error = true;
            return false;
        }

        $image = getimagesize(COD_DIR_APP . "/_arquivos/temp/" . $dado[$nome]);

        if ($image[0] > 3000 || $image[1] > 3000) {

            $this->error = true;
            $this->messageList[] = "Sua imagem de perfil deve ter a resolução menor que 3000 pixel";
            return false;
        }

        /* @var $arquivoTemp \Codando\Modulo\Arquivo */
        foreach ($this->modulo->getArquivoList() as $arquivoTemp) {

            if (strpos($arquivoTemp->getUrl(), $nome) != false) {

                $del = db()->delete('arquivo', ' id_modulo = 43 AND id_est = :cadastro AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'cadastro' => $this->modulo->getId()));

                if ($del === true) {
                    @unlink(COD_DIR_APP . "/_arquivos/" . $this->dir . "/" . $arquivoTemp->getUrl());
                }

                break;
            }
        }

        $upload = $this->_upload($dado[$nome], $nome, $ordem);

        if ($upload === false)
            return $upload;
    }

    public function _upload($fileTemp, $prefix, $ordem = 1) {

        $ext = array('jpeg', 'jpg', 'png');

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        if ($fileTemp === NULL || file_exists(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp) === false) {

            $this->error = true;
            $this->messageList[] = "Arquivo temporario não encontrado.";
            return false;
        }

        $pathinfor = pathinfo(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp);

        if (is_array($pathinfor) === false) {
            $this->error = true;
            $this->messageList[] = "Arquivo temporario corrompido.";
            return false;
        }

        $extRe = $pathinfor['extension'];
        $filenameRe = $prefix . md5(uniqid()) . date('YmdHis');

        if (in_array(strtolower($extRe), $ext) === false) {

            $this->error = true;
            $this->messageList[] = "Arquivo não permitido";
            return false;
        }

        if (strrpos($filenameRe, $this->dir) === false) {

            $filenameRe = $this->dir . "-" . $filenameRe;
        }

        while (file_exists(COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe) === true) {
            $filenameRe .= rand(10, 999);
        }

        $new_arquivo = array(
            'url' => $filenameRe . '.' . $extRe,
            'id_est' => $this->modulo->getId(),
            'id_modulo' => 30, //Cadastro
            'ordem' => $ordem
        );

        if (rename(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp, COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe)) {

            $id_arquivo = db()->insert('arquivo', $new_arquivo);

            return $id_arquivo;
        }

        return false;
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {

        $this->feedback = array();
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
