<?php

namespace Codando\Controller;

/**
 * 	E-mail: luizz@luizz.com.br
 * 	Copyright 2016 - Todos os direitos reservados
 */
class Noticia extends Controller {

    public function _doRoute() {

        $this->cleanStateController();

        $acao = $this->_getRoute();

        switch ($acao) {

            case "listar":

                $this->_list();

                break;
        }
    }

    protected function _list($order = 'data DESC, id_noticia DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', FALSE);

        if (is()->nul($busca) === FALSE) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) OR LOWER(texto) LIKE LOWER(:busca) )';
        }

        $parsWhere['date'] = date('Y-m-d H:i');
        $where[] = '( data <= :date AND status = 1 )';

        $this->findWhere("noticia", $where, $parsWhere, $order);
    }

    protected function _load($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toRequest('id_noticia', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Noticia invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('noticia', array('id_noticia = :id_noticia AND status = 1 ', array('id_noticia' => $not_id)));

        if (is_instanceof('Codando\Modulo\Noticia', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Noticia nÃ£o encontrado";
        }
    }

    public function updadeAcesso($noticia) {

        if (is_instanceof('Codando\Modulo\Noticia', $noticia) === true) {

            db()->update('noticia', array('acesso' => ($noticia->getAcesso() + 1)), ' id_noticia = :id_noticia ', array('id_noticia' => $noticia->getId()));
        }
    }

    protected function _loadAtivo($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toGet('id_noticia', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Noticia invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('noticia', array(' id_noticia = :id_noticia AND status = 1 AND data <= :data ', array('data' => date('Y-m-d H:i'), 'id_noticia' => $not_id)));

        if (is_instanceof('Codando\Modulo\Noticia', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Noticia nÃ£o encontrado";
        }
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
