<?php

namespace Codando\Controller;

/**
 *  E-mail: luizz@luizz.com.br
 * Copyright 2016 - Todos os direitos reservados
 */
class Reserva extends Controller {

    private $sandbox;

    public function _livre(\Codando\Modulo\Anfitriao $anfitriao, $dado) {

        $datainicio = $dado['datainicio'];
        $datafim = $dado['datafim'];

        if (is_null($datafim) == true) {
            $datafim = $dado['datainicio'];
        }

        //Check data nos compromisso
        $compromissoBuscar = app()->countModulo('compromisso', array(" id_anfitriao = :anf AND "
            . " DATE_FORMAT(data,'%Y-%m-%d') >= :dateent AND "
            . " DATE_FORMAT(data,'%Y-%m-%d') <= :dateentf AND "
            . " CAST(horainicio as time) >= :horaini AND "
            . " CAST(horafim as time) < :horafim ",
            array('horafim' => $dado['horabuscar'],
                'horaini' => $dado['horabuscar'],
                'dateent' => $datainicio,
                'dateentf' => $datafim,
                'anf' => $anfitriao->getId())));

        $compromissoDeixar = app()->countModulo('compromisso', array(" id_anfitriao = :anf AND "
            . " DATE_FORMAT(data,'%Y-%m-%d') >= :dateent AND "
            . " DATE_FORMAT(data,'%Y-%m-%d') <= :dateentf AND "
            . " CAST(horainicio as time) >= :horaini AND "
            . " CAST(horafim as time) < :horafim ",
            array('horafim' => $dado['horadeixar'],
                'horaini' => $dado['horadeixar'],
                'dateent' => $datainicio,
                'dateentf' => $datafim,
                'anf' => $anfitriao->getId())));

        $compromisso = app()->countModulo('compromisso', array(" id_anfitriao = :anf AND "
            . " DATE_FORMAT(data,'%Y-%m-%d') >= :dateent AND "
            . " DATE_FORMAT(data,'%Y-%m-%d') <= :dateentf ",
            array('dateent' => $datainicio,
                'dateentf' => $datafim,
                'anf' => $anfitriao->getId())));

        $reserva = 0;
//                
//                = app()->countModulo('reserva', array(" id_anfitriao = :anf AND "
//            . " DATE_FORMAT(datainicio,'%Y-%m-%d') >= :dateent AND "
//            . " DATE_FORMAT(datafim,'%Y-%m-%d') <= :dateentf AND "
//            . " status IN ( 4, 7) ", //paga ou aguardando
//            array('dateent' => $datainicio,
//                'dateentf' => $datafim,
//                'anf' => $anfitriao->getId())));

        if ($compromissoDeixar >= 1) {

            $this->error = true;
            $this->messageList[] = 'O anfitrião tem um compromisso nesta hora de deixar.';
            return false;
        }

        if ($compromissoBuscar >= 1) {
            $this->error = true;
            $this->messageList[] = 'O anfitrião tem um compromisso nesta hora de buscar.';
            return false;
        }

        if ($compromissoDeixar < 1 && $compromissoBuscar < 1 && $compromisso > 0) {

            $this->error = true;
            $this->messageList[] = 'O anfitrião tem um compromisso nesta(s) data, entre contato com anfitrião pelo chat.';
            return false;
        }

        if ($reserva >= 1) {
            $this->error = true;
            $this->messageList[] = 'O anfitrião já tem uma reserva nessa data.';
            return false;
        }

        return true;
    }

    public function _valid() {

        $this->clearMessages();

        $dado_required = array(
            'datainicio' => 'DATE',
            'horadeixar' => '%s',
            'horabuscar' => '%s',
            'id_modalidade' => '%d'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*)';
            return false;
        }

        if (input()->getSession('logadocadastro') < 0) {

            $this->error = true;
            $this->messageList[] = 'Realize o login como hospede.';
            return false;
        }
        
        $dado = array_merge($dado, input()->_toPost(array('datafim' => 'DATE'), 'DATE', false));
        
        $dado['id_cadastro'] = input()->getSession('logadocadastro');

        if ($dado['id_cadastro'] < 1) {
            $this->error = true;
            $this->messageList[] = "Você tem que esta logado como hospede.";
            return false;
        }
        
        $cupomcod = input()->_toPost('cupom', 'xss%s');
        
        if(is()->nul($cupomcod) == false){
            
            /* @var $cupom \Codando\Modulo\Cupom */
            $cupom = app()->loadModulo('cupom', array(" LOWER(codigo) = LOWER(:cod) AND datavalido >= '".date('Y-m-d')."' AND quantidade > 0 ", array('cod' => $cupomcod)));
            
            if(is_instanceof('Codando\Modulo\Cupom', $cupom) == true){
                
                db()->update('cupom', array('quantidade' => ($cupom->getQuantidade()-1) ), " id_cupom = :id AND datavalido >= '".date('Y-m-d')."' ", array('id' => $cupom->getId()));
                
                $dado['id_cupom'] = $cupom->getId();
                $dado['desconto'] = $cupom->getPorcentagem();
            }
            
        }
        
        $dado['pets'] = isset($_POST['pets']) ? $_POST['pets'] : array();

        if (count($dado['pets']) < 1) {

            $this->error = true;
            $this->messageList[] = "Selecione um pet para hospedar.";
            return false;
        }

        return $dado;
    }

    public function _confirmar() {

        $this->clearMessages();

        //3:Confirma e aguarda o pagamento
        $update = db()->update('reserva', array('status' => 3, 'dataconfirmacao' => date('Y-m-d H:i:s')), ' id_reserva = :res ', array('res' => $this->modulo->getId()));

        if ($update !== false) {

            $this->error = true;
            $this->messageList[] = 'Reserva confirmada com sucesso.';

            $this->_avisoConfirmada();

            return true;
        }

        $this->error = true;
        $this->messageList[] = 'Não foi possível confirma a reserva.';
    }

    /*
     * Aviso Cancelada pelo cadastro
     */

    public function _cancelar() {

        $this->clearMessages();

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        //6: Mudar status cancelada
        $update = db()->update('reserva', array('status' => 6, 'datacancelada' => date('Y-m-d H:i:s'), 'cancelado' => 2), ' id_reserva = :res ', array('res' => $this->modulo->getId()));

        if ($update != false && $reserva->getStatus() == 4) {//foi paga entao realizar reembolso
            $this->reembolso();
        }

        if ($update !== false) {

            $this->error = true;
            $this->messageList[] = 'Reserva cancelada com sucesso.';

            $this->_avisoCancelada();

            //return true;
        }

//        $this->error = true;
//        $this->messageList[] = 'Não foi possível cancelar a reserva.';
    }

    public function _cancelarAnfitriao() {

        $this->clearMessages();

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        //6: Mudar status cancelada
        $update = db()->update('reserva', array('status' => 6, 'datacancelada' => date('Y-m-d H:i:s'), 'cancelado' => 1), ' id_reserva = :res ', array('res' => $this->modulo->getId()));

        if ($update !== false) {

            $this->error = true;
            $this->messageList[] = 'Reserva cancelada com sucesso.';

            $this->_avisoCancelada();

            //return true;
        }

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $this->modulo->getAnfitriao();

        $countReserva = app()->countModulo('reserva', array(' status = 6 AND id_anfitriao = :anfi ', array('anfi' => $anfitriao->getId())));

        if ($countReserva == 2) {//Duas reserva cancelada avisar dogsafe
            $this->_avisoDogCancelado();
        }

        if ($reserva->getStatus() == 4) {//foi paga entao realizar reembolso
            $this->reembolso();
        }

//        $this->error = true;
//        $this->messageList[] = 'Não foi possível cancelar a reserva.';
    }

    public function _pagar() {

        $this->clearMessages();

        $url = $this->expressCheckout();

        if ($url === false) {

            $this->error = true;
            $this->messageList[] = 'Não foi possível integra com Paypal, tente mais tarde.';
            return true;
        }

        //7:Aguardando Paypal
        $update = db()->update('reserva', array('status' => 7, 'paypalurl' => $url, 'datapagamento' => date('Y-m-d H:i:s')), ' id_reserva = :res ', array('res' => $this->modulo->getId()));

        if ($update !== false) {

            $this->error = true;
            $this->messageList[] = 'Paga com sucesso.';
            $this->_load($this->modulo->getId()); //Recarregar

            return true;
        }

        $this->error = true;
        $this->messageList[] = 'Não foi possível pagar a reserva.';
    }

    public function _insert($anfitriao) {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        if ($this->_livre($anfitriao, $dado) === false) {
            return false;
        }

        $anfitriaomodalidade = app()->listModulo('anfitriaomodalidade', array(' id_anfitriao = :anfitriao ', array('anfitriao' => $anfitriao->getId())));

        /* @var $anfitriaomodalidade <Array>\Codando\Modulo\Anfitriaomodalidade */
        $anfitriaomodalidade;

        $countanfitriao = count($anfitriaomodalidade);

        if ($countanfitriao == 1) {

            $antmodalidade = current($anfitriaomodalidade);
            $dado['id_modalidade'] = $antmodalidade->getModalidade()->getId();
        } else {

            $key_modalidade = array();

            foreach ($anfitriaomodalidade as $anfitriaomodalidadeTemp) {
                $key_modalidade[] = $anfitriaomodalidadeTemp->getModalidade()->getId();
            }

            if (in_array($dado['id_modalidade'], $key_modalidade) === false) {

                $this->error = true;
                $this->messageList[] = 'Modalidade invalida.';
                return false;
            }
        }

        $dia = array();

        /* @var $reserva \Codando\Modulo\Reserva */

        $datainicio = new \DateTime($dado['datainicio']);
        $dia[] = $datainicio->format('d');

        if (is()->nul($dado['datafim']) == false) {

            $datafim = new \DateTime($dado['datafim']);
            $loopDia = true;

            while ($loopDia) {
                $datainicio->modify('+1 day');
                $addDia = $datainicio->format('d');
                $dia[] = $addDia;

                if ($datainicio == $datafim || $addDia >= 31) {
                    $loopDia = false;
                }
            }
        }

        //Valor

        $valor = ($dado['id_modalidade'] == 1) ? $anfitriao->getValordiariahospedagem() : $anfitriao->getValordiariadaycare();
        $dado['valormodalidade'] = ($dado['id_modalidade'] == 1) ? $anfitriao->getValordiariahospedagem(false) : $anfitriao->getValordiariadaycare(false);
        $dado['valor'] = count($dia) * $valor;

        if(isset($dado['desconto']) == true && $dado['desconto'] > 0){
            $dado['valordesconto'] = ( ($dado['valor'] * $dado['desconto']) / 100 );
            $dado['valor'] = $dado['valor'] - $dado['valordesconto'];
        }
        
        $dado['id_anfitriao'] = $anfitriao->getId();
        $dado['datacadastro'] = date('Y-m-d H:i:s');

        $pets = $dado['pets'];

        $dado['status'] = 1; //Pendente

        unset($dado['pets']);

        $countReserva = 0; //app()->countModulo('reserva', array(" ( DATE_FORMAT(datainicio,'%d-%m-%Y') >= :dtinicio AND DATE_FORMAT(datafim,'%d-%m-%Y') <= :dtfim ) AND status <> 6 ", array('dtinicio' => $dado['datainicio'], 'dtfim' => $dado['datafim'])));

        if ($countReserva > 0) {
            $this->error = true;
            $this->messageList[] = 'Data não está disponível.';
            return false;
        }

        //Inserir reserva
        $id_reserva = db()->insert('reserva', $dado);

        $this->_load($id_reserva);

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Reserva não inserida.';
            return false;
        }

        foreach ($pets as $pet) {
            db()->insert('reservapet', array('id_reserva' => $id_reserva, 'id_pet' => $pet));
        }

        $this->modulo->setPets(app()->listModulo('reservapet', array(' id_reserva = :id ', array('id' => $id_reserva))));

        //Mensagem de retorno
        $this->messageList[] = "Reserva realizada com sucesso, aguarde ...";
        $this->error = false;

        //$this->messageList[] = 'Reserva salvo com sucesso';
        //$this->error = false;
        $this->_avisoNova($anfitriao);

        return true;
    }

    public function _listCalendario($order = 'datainicio DESC, id_reserva DESC', $where = array()) {

        $parsWhere = array();

        $datainicio = input()->_toGet('datainicio', '%s', false);
        $datafim = input()->_toGet('datafim', '%s', false);

        $mes = input()->_toGet('mes', '%d', false);
        $ano = input()->_toGet('ano', '%d', false);

        //$exdata = (array) explode('-', $data);
        $id_anfitriao = input()->_toGet('anfitriao', '%d', false);

        if (is()->id($id_anfitriao) === false) {
            return;
        }

        if (($mes >= 1 && $mes <= 12) && $ano >= date('Y')) {

            $mes = sprintf('%02d', $mes);

            $parsWhere['data'] = $mes . '-' . $ano;
            $parsWhere['dataf'] = $mes . '-' . $ano;

            $where[] = " ( DATE_FORMAT(datainicio,'%m-%Y') >= :data AND DATE_FORMAT(datafim,'%m-%Y') <= :dataf )";

            $parsWhere['datac'] = $mes . '-' . $ano;
            $parsWhere['datafc'] = $mes . '-' . $ano;

            $where[] = "( id_anfitriao NOT IN ( SELECT c.id_anfitriao FROM compromisso c WHERE DATE_FORMAT(c.data,'%m-%Y') >= :datac AND DATE_FORMAT(c.data,'%m-%Y') <= :datafc ) )";
        }

        if (is()->date($datainicio) == true && is()->date($datafim) == true) {
            $parsWhere['datainicio'] = $datainicio;
            $parsWhere['datafim'] = $datafim;

            $where[] = " ( DATE_FORMAT(datainicio,'%m-%Y') >= :datainicio AND DATE_FORMAT(datafim,'%m-%Y') <= :datafim )";
        }

        if (is()->date($datainicio) == true && is()->date($datafim) == true) {

            $parsWhere['datainicio'] = $datainicio;
            $parsWhere['datafim'] = $datafim;

            $where[] = " id_anfitriao NOT IN ( SELECT c.id_anfitriao FROM compromisso c WHERE DATE_FORMAT(c.data,'%d-%m-%Y') >= :datainicio AND DATE_FORMAT(c.data,'%d-%m-%Y') <= :datafim )";
        }

        $parsWhere['id_anfitriao'] = (int) $id_anfitriao;
        $where[] = '( id_anfitriao = :id_anfitriao )';

        $where[] = '( status <> 6 )'; //Nao cancelada    
        //$where[] = '( status = 4 )'; //Ativa

        $this->findWhere("reserva", $where, $parsWhere, $order);
    }

    public function _list($order = 'datainicio DESC, id_reserva DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', false);

        $id_anfitriao = input()->getSession('logadoanfitriao');

        if (is()->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) OR LOWER(texto) LIKE LOWER(:busca) )';
        }

        $parsWhere['id_anfitriao'] = $id_anfitriao;
        $where[] = '( id_anfitriao = :id_anfitriao )';


        $this->findWhere("reserva", $where, $parsWhere, $order);
    }

    public function _listByCadastro($order = 'datainicio DESC, id_reserva DESC', $where = array()) {

        $parsWhere = array();


        $id_cadastro = input()->getSession('logadocadastro');

        if (is()->id($id_cadastro) == false) {

            $this->error = true;
            $this->messageList[] = "Reserva invalida";
            return false;
        }


        $parsWhere['cadastro'] = $id_cadastro;
        $where[] = '( id_cadastro = :cadastro )';


        $this->findWhere("reserva", $where, $parsWhere, $order);
    }

    public function _load($not_id = NULL) {

        if (is()->id($not_id) == false) {

            $this->error = true;
            $this->messageList[] = "Reserva invalida";
            return false;
        }

        $this->modulo = app()->loadModulo('reserva', array('id_reserva = :id_reserva ', array('id_reserva' => $not_id)));

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Reserva não encontrada";
        }
    }

    protected function _loadByCadastro($res_id) {

        if (is()->id($res_id) == false) {

            $this->error = true;
            $this->messageList[] = "Reserva inválido";
            return false;
        }

        $id_cadastro = input()->getSession('logadocadastro');

        if (is()->id($id_cadastro) == false) {

            $this->error = true;
            $this->messageList[] = "Cadastro é inválido";
            return false;
        }

        $this->modulo = app()->loadModulo('reserva', array('id_reserva = :id_reserva AND id_cadastro = :cad AND status <> 6 ', array('cad' => $id_cadastro, 'id_reserva' => $res_id)));

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Reserva não encontrada";
            return false;
        }
    }

    protected function _loadByAnfitriao($res_id) {

        if (is()->id($res_id) == false) {

            $this->error = true;
            $this->messageList[] = "Reserva invalida";
            return false;
        }

        $id_anfitriao = input()->getSession('logadoanfitriao');

        if (is()->id($id_anfitriao) == false) {

            $this->error = true;
            $this->messageList[] = "Anfitrião inválido";
            return false;
        }

        $this->modulo = app()->loadModulo('reserva', array('id_reserva = :id_reserva AND id_anfitriao = :ant AND status <> 6 ', array('ant' => $id_anfitriao, 'id_reserva' => $res_id)));

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Reserva não encontrada";
            return false;
        }
    }

    protected function _loadAtivo($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toGet('id_reserva', '%d', true);

        if (is()->id($not_id) == false) {

            $this->error = true;
            $this->messageList[] = "Reserva invalida";
            return false;
        }

        $this->modulo = app()->loadModulo('reserva', array(' id_reserva = :id_reserva AND status = 1 AND data <= :data ', array('data' => date('Y-m-d H:i'), 'id_reserva' => $not_id)));

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Reserva não encontrado";
        }
    }

    /*
     * Aviso Nova pelo cadastro
     */

    protected function _avisoNova($anfitriao) {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $reserva->getCadastro();

        $emailArray = array($anfitriao->getEmail());

        $emailFields = array(
            'Inícia' => $reserva->getDatainicio(),
            'Até' => $reserva->getDatafim(),
            'Deixa às' => $reserva->getHoradeixar(),
            'Vai buscar' => $reserva->getHorabuscar(),
            'Hospede' => $cadastro->getNome(),
            'Valor' => $reserva->getValor(),
            'Qtd. Pet' => count($reserva->getPets()),
            'Gerenciar' => 'https://dogsafe.com.br/minha-conta',
        );

        $html_email = tpl()->display('contato_send', array('titulo' => ' Você acaba de receber uma solicitação de reserva, por favor confirme sua disponibilidade em <a href="https://dogsafe.com.br/minhas-reservas">Minhas Reservas</a>', 'emailFields' => $emailFields), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('DogSafe - Nova Reserva')
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    /*
     * Aviso Confirmada pelo anfitriao
     */

    protected function _avisoConfirmada() {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $reserva->getCadastro();

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        $emailArray = array($cadastro->getEmail());

        $nome = current(@explode(' ', $anfitriao->getNome()));

        $html_email = tpl()->display('contato_send', array('titulo' => $nome . ', confirmou a reserva de hospedagem com inicio em ' . $reserva->getDatainicio() . ', acesse o site e realize o pagamento da reserva em <a href="https://dogsafe.com.br/minhas-reservas">suas reservas</a>.'), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('DogSafe - Reserva confirmada')
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    /*
     * Aviso Cancelada pelo cadastro
     */

    public function _avisoCancelada($titulo = NULL) {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $reserva->getCadastro();

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        $emailArray = array($cadastro->getEmail());

        $nome = current(@explode(' ', $anfitriao->getNome()));

        $titulo = $titulo == NULL ? $nome . ', cancelou a reserva de hospedagem com inicio em ' . $reserva->getDatainicio() . ', acesse o site e tente novamente outra data.' : $titulo;

        $html_email = tpl()->display('contato_send', array('titulo' => $titulo), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('DogSafe - Reserva cancelada')
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    /*
     * Aviso Dog cancelado mais de dois
     */

    public function _avisoDogCancelado() {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $deparmento Departamento */
        $deparmento = app()->loadModulo('departamento', array('id_departamento = :id_departamento', array('id_departamento' => $_config['departament_cancelamento'])));

        if (is_instanceof('Codando\Modulo\Departamento', $deparmento) === false) {
            return false;
        }

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $this->modulo;

        $emailArray = explode(',', $deparmento->getEmail());

        $html_email = tpl()->display('contato_send', array('titulo' => 'O anfitrião ' . $anfitriao->getNome() . ' ( ' . $anfitriao->getCep() . ' - ' . $anfitriao->getEmail() . ' ) realizou dois cancelamento de reserva. '), false);

        $mailer = $this->swiftTransport();

        $message = \Swift_Message::newInstance($deparmento->getNome())
                ->setFrom(array($_config['return'] => $deparmento->getNome()))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    public function _avisoPaga() {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $reserva->getCadastro();

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        $emailArray = array($anfitriao->getEmail());

        $nome = current(@explode(' ', $cadastro->getNome()));

        $html_email = tpl()->display('contato_send', array('titulo' => $nome . ', pagou a reserva de hospedagem com inicio em ' . $reserva->getDatainicio() . ', acesse o site e utilize o chat.'), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('DogSafe - Reserva paga')
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    //Criar cobranca
    protected function expressCheckout() {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $reserva->getCadastro();

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        //Existe
        if (is_null($reserva->getPaypalurl()) == false && strpos($reserva->getPaypalurl(), 'token') !== false) {
            return $reserva->getPaypalurl();
        }

        $baseUrl = 'https://dogsafe.com.br/';
        $paypalURL = 'https://www.' . ($this->sandbox === true ? 'sandbox.' : '') . 'paypal.com/cgi-bin/webscr';

        $requestNvp = array(
            'METHOD' => 'SetExpressCheckout',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'SALE', //Sale || Order
            'PAYMENTREQUEST_0_AMT' => $reserva->getValor(false),
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'BRL',
            'PAYMENTREQUEST_0_ITEMAMT' => $reserva->getValor(false),
            'PAYMENTREQUEST_0_INVNUM' => $reserva->getId(),
            'L_PAYMENTREQUEST_0_NAME0' => 'DogSafe reserva',
            'L_PAYMENTREQUEST_0_DESC0' => 'DogSafe reserva: ' . $anfitriao->getNome() . ' ' . $reserva->getDatainicio(),
            'L_PAYMENTREQUEST_0_AMT0' => $reserva->getValor(false),
            'L_PAYMENTREQUEST_0_ITEMAMT' => $reserva->getValor(false),
            'RETURNURL' => $baseUrl . 'reservar/executepayment/' . $reserva->getId() . '?success=true',
            'CANCELURL' => $baseUrl . 'reservar/executepayment/' . $reserva->getId() . '?success=false',
            'BUTTONSOURCE' => 'BR_EC_EMPRESA',
            'SUBJECT' => 'dog@dogsafe.com.br',
            'LOCALECODE' => 'pt_BR'
        );

        //Envia a requisição e obtém a resposta da PayPal
        $responseNvp = $this->paypal($requestNvp);

        if (is_array($responseNvp) === false) {
            return false;
        }

        if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {

            $query = array(
                'cmd' => '_express-checkout',
                'token' => $responseNvp['TOKEN']
            );

            //Token
            db()->update('reserva', array('paypal' => $responseNvp['TOKEN']), ' id_reserva = :res ', array('res' => $this->modulo->getId()));

            $redirectURL = sprintf('%s?%s', $paypalURL, http_build_query($query));

            return $redirectURL; // . '&useraction=commit';
        }

        $this->error = true;
        $this->messageList[] = 'Não foi possível gerar a cobrança, tente mais tarde.';
        
        $msgpay = (isset($responseNvp['L_LONGMESSAGE0']) ? $responseNvp['L_LONGMESSAGE0'] : NULL);
 
        logsgc('pagamento', 42, $reserva->getId(), ( strlen($msgpay) > 4 ? $msgpay: 'Não foi possível gerar a cobrança, resposta do paypal com erro.'));

        return false;
    }

    //Confirmar pedido
    public function paypalPayment() {

        // CHECKOUTSTATUS
        $response = $this->getCheckout();

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        if ($response === false || isset($response['CHECKOUTSTATUS']) === false) {
            return false;
        }

        if (isset($response['ACK']) === false) { // || $response['Success'] != 'Success'
            logsgc('paypalpayment', 42, $reserva->getId(), 'Paypal ACK Invalido');

            return false;
        }

        if (isset($response['PAYERID']) === false) {

            logsgc('paypalpayment', 42, $reserva->getId(), 'Paypal PAYERID Invalido');
            return false;
        }

        if (isset($response['TOKEN']) === false) {

            logsgc('paypalpayment', 42, $reserva->getId(), 'Paypal TOKEN Invalido');
            return false;
        }

        $up = db()->update('reserva', array('paypal' => $response['TOKEN'], 'payerid' => $response['PAYERID']), ' id_reserva = :res ', array('res' => $reserva->getId()));

        logsgc('paypalpayment', 42, $reserva->getId(), 'Check Status do paypal ' . $response['CHECKOUTSTATUS']);

        switch ($response['CHECKOUTSTATUS']) {
            case 'PaymentCompleted'://Paga

                $up = db()->update('reserva', array('status' => 4, 'datapagamento' => date('Y-m-d H:i')), ' id_reserva = :res ', array('res' => $reserva->getId()));

                if ($up) { //Criar um chat entre os dois!
                    \Codando\Controller\Chat::_insert($reserva);
                    $this->_avisoPaga();
                }

                break;
            case 'PaymentActionCompleted'://Paga

                $up = db()->update('reserva', array('status' => 4, 'datapagamento' => date('Y-m-d H:i')), ' id_reserva = :res ', array('res' => $reserva->getId()));

                if ($up) { //Criar um chat entre os dois!
                    \Codando\Controller\Chat::_insert($reserva);
                    $this->_avisoPaga();
                }

                break;
            case 'PaymentActionNotInitiated'://Nao pagou

                db()->update('reserva', array('status' => 7), ' id_reserva = :res ', array('res' => $this->modulo->getId()));
                $this->doCheckout($response);

                break;

            case 'PaymentActionFailed'://Não foi possivel

                db()->update('reserva', array('status' => 7), ' id_reserva = :res ', array('res' => $this->modulo->getId()));
                $this->doCheckout($response);

                break;
        }
    }

    public function getCheckout($return = false) {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        if ($reserva->getTransactionid() != NULL) {
            return false;
        }

        $param = array(
            'METHOD' => 'GetExpressCheckoutDetails',
            'TOKEN' => $reserva->getPaypal()
        );

        $responseNvp = $this->paypal($param);

        if (is_array($responseNvp) === false) {
            return $responseNvp;
        }

        if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {

            logsgc('paypalcheckout', 42, $reserva->getId(), 'Check Status do paypal ' . $responseNvp['CHECKOUTSTATUS']);

//            switch ($responseNvp['CHECKOUTSTATUS']) {
//                case 'PaymentCompleted'://Paga
//
//                    break;
//                case 'PaymentActionNotInitiated'://Nao pagou
//
//                    db()->update('reserva', array('status' => 7), ' id_reserva = :res ', array('res' => $this->modulo->getId()));
//                    break;
//            }

            return $responseNvp;
        }

        return $return == false ? false : $responseNvp;
    }

    protected function doCheckout($resp) {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        if ($reserva->getTransactionid() != NULL) {
            return false;
        }

        //Campos da requisição da operação DoExpressCheckoutPayment
        $param = array(
            'METHOD' => 'DoExpressCheckoutPayment',
//            'SUBJECT' => 'dog@dogsafe.com.br',
            'TOKEN' => $resp['TOKEN'],
            'PAYERID' => $resp['PAYERID'],
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'SALE', // SALE || Order
            'PAYMENTREQUEST_0_AMT' => $reserva->getValor(false),
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'BRL'
        );

        //Envia a requisição e obtém a resposta da PayPal
        $response = $this->paypal($param);

        if (isset($response['L_ERRORCODE0']) === true && $response['L_ERRORCODE0'] != '10486') {//Return ao paypal
            redirecionar($reserva->getPaypalurl());
        }

        if (isset($response['ACK']) === false || $response['ACK'] == 'Failure') {
            logsgc('paypaldocheckout', 42, $reserva->getId(), 'Paypal ACK Invalido');
            return false;
        }

        if (isset($response['PAYMENTINFO_0_TRANSACTIONID']) && $response['PAYMENTINFO_0_PAYMENTSTATUS'] == 'Completed') {

            db()->update('reserva', array('status' => 4, 'datapagamento' => date('Y-m-d H:i')), ' id_reserva = :res ', array('res' => $this->modulo->getId()));
        } else {

            logsgc('paypaldocheckout', 42, $reserva->getId(), 'Paypal PayStatus Invalido' . $response['PAYMENTINFO_0_PAYMENTSTATUS']);
            return false;
        }

        if (isset($response['PAYMENTINFO_0_TRANSACTIONID']) === false) { // || $response['Success'] != 'Success'
            logsgc('paypaldocheckout', 42, $reserva->getId(), 'Paypal TRANSACTIONID Invalido');
            return false;
        }

        //echo 'O ID da transação é: ' . $responseNvp['PAYMENTINFO_0_TRANSACTIONID'];
        db()->update('reserva', array('transactionid' => $response['PAYMENTINFO_0_TRANSACTIONID']), ' id_reserva = :res ', array('res' => $reserva->getId()));
        return true;
    }

    public function getEmail($email) {

        $param = array(
            'METHOD' => 'GetVerifiedStatus',
            'emailAddress' => $email
        );

        $responseNvp = $this->paypal($param);

        if (is_array($responseNvp) === false) {
            return $responseNvp;
        }

        if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {
            return $responseNvp;
        }

        return $responseNvp;
    }

    public function reembolsoDayCare() {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        $dateTimeat = new \DateTime;
        $datainicio = $reserva->getDatainicio('Y-m-d');
        $horadeixar = $reserva->getHoradeixar();
        $dtreserva = new \DateTime($datainicio . ' ' . $horadeixar);

        //Valor total
        $valor_reembolso = $reserva->getValor(false);

        $dtrestituicao = clone $dateTimeat;
        $dtrestituicao->modify('-12 hour'); // Prazo 12hs
        //dias 
        $dtreservaformt = $dtreserva->format('Y-m-d H:i');

        //DayCare cancelamento ate 12hs
        if ($dtreservaformt > $dtrestituicao->format('Y-m-d H:i')) {

            $this->messageList[] = 'Não foi possivel o reembolso, prazo até 12hs.';
            return false;
        }

        //Remover % da operação de cancelamento cobrada pela DOGSAFE   
        $valor_reembolso = $valor_reembolso - ( ($valor_reembolso * GPOR_SITE_REEM ) / 100 ); //Remover % dos ganhos da DOGSAFE

        $obs = 'Cancelamento ' .
                '<br>- Modalidade Daycare ' .
                '<br>- Data atual ' . $dateTimeat->format('Y-m-d H:i') .
                '<br>- Data Inicio ' . $dtreserva->format('Y-m-d H:i') .
                '<br>- Valor Total ' . $reserva->getValor(false) .
                '<br>- ' . ($dateTimeat->format('Y-m-d H:i') > $dtreserva->format('Y-m-d H:i') ? 'Reserva já iniciada ' : 'nao iniciada') .
                '<br>- Reembolso R$: ' . $valor_reembolso;

//        echo $obs;
//
//        return; //Dev
        //Reemboso para o hospede
        $requestNvp = array(
            'METHOD' => 'RefundTransaction', //Metodo reembolso
            'REFUNDTYPE' => 'Partial',
            'TRANSACTIONID' => $reserva->getTransactionid(),
            'CURRENCYCODE' => 'BRL', //Moeda
            'AMT' => str_replace(",", ".", str_replace(".", "", $valor_reembolso))
        );

        //Envia a requisição e obtém a resposta da PayPal
        $responseNvp = $this->paypal($requestNvp);

        if (is_array($responseNvp) === false) {

            //Registrar Erro
            db()->insert('reembolso', array(
                'id_reserva' => $reserva->getId(),
                'data' => date('Y-m-d'),
                'email' => $anfitriao->getEmail(),
                'status' => 'ERROR PAYPAL',
                'valor' => $valor_reembolso,
                'observacao' => "ERRO NA API DO PAYPAL <br>" . $obs
            ));

            return false;
        }

        //Verifica se a operação foi bem sucedida
        if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {

            //Registrar Erro
            db()->insert('reembolso', array(
                'id_reserva' => $reserva->getId(),
                'data' => date('Y-m-d'),
                'email' => $anfitriao->getEmail(),
                'status' => 'SUCESS',
                'observacao' => $obs,
                'valor' => $valor_reembolso,
                'transaction' => (isset($responseNvp['REFUNDTRANSACTIONID']) ? $responseNvp['REFUNDTRANSACTIONID'] : NULL)
            ));

            $this->messageList[] = 'Reembolso realizado com sucesso.';

            //Cancelar e concluir reserva
            db()->update('reserva', array('status' => 8), ' id_reserva = :res ', array('res' => $reserva->getId()));

            return true;
        }

        //Registrar Erro
        db()->insert('reembolso', array(
            'id_reserva' => $reserva->getId(),
            'data' => date('Y-m-d'),
            'email' => $anfitriao->getEmail(),
            'status' => 'ERROR',
            'valor' => $valor_reembolso,
            'observacao' => ( isset($responseNvp['L_LONGMESSAGE0']) ? $responseNvp['L_LONGMESSAGE0'] : "ERRO NA API DO PAYPAL" ) . '<br>' . $obs
        ));

        return false;
    }

    public function reembolsoHospedagem() {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        /* @var $cancelamento \Codando\Modulo\Cancelamento */
        $cancelamento = $anfitriao->getCancelamento();

        $dateTimeat = new \DateTime;
        $datainicio = $reserva->getDatainicio('Y-m-d');
        $horadeixar = $reserva->getHoradeixar();
        $dtreserva = new \DateTime($datainicio . ' ' . $horadeixar);
        $dataReserva = new \DateTime($datainicio . ' 00:00:00');
        $dataFimReserva = new \DateTime($reserva->getDatafim('Y-m-d') . ' 24:00:00');

        //Reserva concluida
        $dtreservafim = new \DateTime($reserva->getDatafim('Y-m-d') . ' ' . $reserva->getHorabuscar());

        if ($dateTimeat->format('Y-m-d') >= $dtreservafim->format('Y-m-d')) {

            $this->error = true;
            $this->messageList[] = 'Reembolso não pode ser feito, reserva foi concluida em ' . $dtreservafim->format('Y-m-d H:i') . '.';
            logsgc('reembolso', 42, $reserva->getId(), 'Reembolso não pode ser feito, reserva foi concluida em ' . $dtreservafim->format('Y-m-d H:i') . '.');
            return false;
        }

        //Valor total
        $valor_reembolso = $reserva->getValor(false);
        $valormodalidade = $reserva->getValormodalidade(false);

        $porc_dcada = (100 - GPOR_SITE_REEM) / 2;
        $dtrestituicao = null;

        //Classifica o tipo de cancelamento
        switch ($cancelamento->getId()) {

            case '1': //Flexivel 24hs

                $dtrestituicao = clone $dateTimeat;
                $dtrestituicao->modify('-24 hour'); // Prazo 24hs

                break;

            case '2': //Moderada 3 dias

                $dtrestituicao = clone $dateTimeat;
                $dtrestituicao->modify('-3 day'); // Prazo 3 dias

                break;

            case '3': //Rigida 7 dias

                $dtrestituicao = clone $dateTimeat;
                $dtrestituicao->modify('-7 day'); // Prazo 7 dias

                break;
        }

        if ($dtrestituicao === NULL) {

            $this->error = true;
            $this->messageList[] = 'Não foi possível cancelar, tente mais tarde.';

            logsgc('reembolso', 42, $reserva->getId(), 'erro cancelamento não classificado.');
            return false;
        }

        //Dias 
        $diasRestante = $valdias = $diasTotais = $diasrealizados = 0;
        $dtreservaformt = $dtreserva->format('Y-m-d H:i');
        $valorReservaCancelado = NULL;

        //Remover % da operação de cancelamento cobrada pela DOGSAFE   
        $valor_reembolso = $valor_reembolso - ( ($valor_reembolso * GPOR_SITE_REEM ) / 100 ); //Remover % dos ganhos da DOGSAFE
        //Passou o prazo e nao inicio a reserva
        if ($dtreservaformt < $dtrestituicao->format('Y-m-d H:i') && !($dtreservaformt <= $dateTimeat->format('Y-m-d H:i'))) {

            $valor_reembolso = $valor_reembolso - ( ($valor_reembolso * $porc_dcada) / 100 ); //Remover % do anfitriao 50% / hospede 50%
            $valorReservaCancelado = $valor_reembolso;
            logsgc('reembolso', 42, $reserva->getId(), 'Remover % do anfitriao 50% / hospede 50%.');
        } else if ($dtreservaformt <= $dateTimeat->format('Y-m-d H:i')) { //Iniciada entao qualquer ganhos por dia
            $diasTotais = $dataReserva->diff($dataFimReserva)->format('%d');
            $diasRestante = $dateTimeat->diff($dataFimReserva)->format('%d');

            //cancelou no dia, subtrair o dia atual
            if ($dtreserva->format('Y-m-d') == $dateTimeat->format('Y-m-d')) {

                //$diasRestante = $diasRestante - 1;
            }

            //Recalcular com base nos dias
            $valor_reembolso = $valormodalidade * $diasRestante;

            //Valor que deve ser reembolso
            $valor_reembolso = (($valor_reembolso * $porc_dcada) / 100 ); //Calcular o reemboso com base nos dias
            //Dias realizado    
            $diasrealizados = $diasTotais - $diasRestante;

            if ($diasrealizados > 0) {

                //Calcular novo valor da hospedagem
                $valorReservaCancelado = $valormodalidade * $diasrealizados;
                logsgc('reembolso', 42, $reserva->getId(), 'Novo calculo no valor da hospedagem.');
            }
        }


        $obs = 'Cancelamento ' . $cancelamento->getTitulo() .
                '<br>- Modalidade Hospedagem ' .
                '<br>- Data atual ' . $dateTimeat->format('Y-m-d H:i') .
                '<br>- Data Inicio ' . $dtreserva->format('Y-m-d H:i') .
                '<br>- Data Fim ' . $dtreservafim->format('Y-m-d H:i') .
                '<br>- Valor Total ' . $reserva->getValor(false) .
                '<br>- ' . ($dateTimeat->format('Y-m-d H:i') > $dtreserva->format('Y-m-d H:i') ? 'Reserva já iniciada ' : 'nao iniciada') .
                '<br>- % de cada ' . $porc_dcada .
                '<br>- Dias totais ' . $diasTotais .
                '<br>- Dias Realizado ' . $diasrealizados .
                '<br>- Dias Cancelados ' . $diasRestante .
                '<br>- Valor por dias R$: ' . $valormodalidade .
                '<br>- Valor Reembolso em dias  R$: ' . $valor_reembolso .
                '<br>- Reembolso R$: ' . $valor_reembolso;

//        echo $obs;
//        return; //Dev
        //Reemboso para o hospede
        $requestNvp = array(
            'METHOD' => 'RefundTransaction', //Metodo reembolso
            'REFUNDTYPE' => 'Partial',
            'TRANSACTIONID' => $reserva->getTransactionid(),
            'CURRENCYCODE' => 'BRL', //Moeda
            'AMT' => str_replace(",", ".", str_replace(".", "", $valor_reembolso))
        );

        //Envia a requisição e obtém a resposta da PayPal
        $responseNvp = $this->paypal($requestNvp);

        if (is_array($responseNvp) === false) {

            //Registrar Erro
            db()->insert('reembolso', array(
                'id_reserva' => $reserva->getId(),
                'data' => date('Y-m-d'),
                'email' => $anfitriao->getEmail(),
                'status' => 'ERROR PAYPAL',
                'cancelamento' => $cancelamento->getTitulo(),
                'valor' => $valor_reembolso,
                'observacao' => "ERRO NA API DO PAYPAL <br>" . $obs
            ));
            logsgc('reembolso', 42, $reserva->getId(), "ERRO NA API DO PAYPAL <br>" . $obs);
            return false;
        }

        //Verifica se a operação foi bem sucedida
        if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {

            //Registrar Erro
            db()->insert('reembolso', array(
                'id_reserva' => $reserva->getId(),
                'data' => date('Y-m-d'),
                'email' => $anfitriao->getEmail(),
                'status' => 'SUCESS',
                'cancelamento' => $cancelamento->getTitulo(),
                'observacao' => $obs,
                'valor' => $valor_reembolso,
                'transaction' => (isset($responseNvp['REFUNDTRANSACTIONID']) ? $responseNvp['REFUNDTRANSACTIONID'] : NULL)
            ));

            //6:Cancelar e atualizar o valorcancelado
            db()->update('reserva', array('status' => 6, 'valorreembolso' => $valor_reembolso, 'valorcancelado' => $valorReservaCancelado), ' id_reserva = :res ', array('res' => $reserva->getId()));

            $this->messageList[] = 'Reembolso realizado com sucesso.';

            return true;
        }

        //Registrar Erro
        db()->insert('reembolso', array(
            'id_reserva' => $reserva->getId(),
            'data' => date('Y-m-d'),
            'email' => $anfitriao->getEmail(),
            'status' => 'ERROR',
            'cancelamento' => $cancelamento->getTitulo(),
            'valor' => $valor_reembolso,
            'observacao' => ( isset($responseNvp['L_LONGMESSAGE0']) ? $responseNvp['L_LONGMESSAGE0'] : "ERRO NA API DO PAYPAL" ) . '<br>' . $obs
        ));

        return false;
    }

    public function reembolsototal() {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        if (input()->getSession('logadoanfitriao') != $anfitriao->getId()) {

            $this->messageList[] = 'Reembolso so o anfitriao pode solicitar.';
            $this->error = false;
            logsgc('reembolsototal', 42, $reserva->getId(), 'erro anfitriao');
            return false;
        }

        $datainicio = $reserva->getDatainicio('Y-m-d');
        $horadeixar = $reserva->getHoradeixar();
        $dtreserva = new \DateTime($datainicio . ' ' . $horadeixar);

        //Valor total
        $valor_reembolso = $reserva->getValor(false);

        //Remover % da operação de cancelamento cobrada pela DOGSAFE   
        //$valor_reembolso = $valor_reembolso - ( ($valor_reembolso * GPOR_SITE_REEM ) / 100 ); //Remover % dos ganhos da DOGSAFE

        $obs = 'Cancelamento Total' .
                '<br>- Data Inicio ' . $dtreserva->format('Y-m-d H:i') .
                '<br>- Valor Total ' . $reserva->getValor(false) .
                '<br>- Reembolso R$: ' . $valor_reembolso;

        //Reemboso para o hospede
        $requestNvp = array(
            'METHOD' => 'RefundTransaction', //Metodo reembolso
            'REFUNDTYPE' => 'Full',
            'TRANSACTIONID' => $reserva->getTransactionid(),
            'CURRENCYCODE' => 'BRL'
        );

        //Envia a requisição e obtém a resposta da PayPal
        $responseNvp = $this->paypal($requestNvp);

        if (is_array($responseNvp) === false) {

            //Registrar Erro
            db()->insert('reembolso', array(
                'id_reserva' => $reserva->getId(),
                'data' => date('Y-m-d'),
                'email' => $anfitriao->getEmail(),
                'status' => 'ERROR PAYPAL',
                'valor' => $valor_reembolso,
                'observacao' => "ERRO NA API DO PAYPAL <br>" . $obs
            ));

            return false;
        }

        //Verifica se a operação foi bem sucedida
        if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {

            //Registrar Erro
            db()->insert('reembolso', array(
                'id_reserva' => $reserva->getId(),
                'data' => date('Y-m-d'),
                'email' => $anfitriao->getEmail(),
                'status' => 'SUCESS',
                'observacao' => $obs,
                'valor' => $valor_reembolso,
                'transaction' => (isset($responseNvp['REFUNDTRANSACTIONID']) ? $responseNvp['REFUNDTRANSACTIONID'] : NULL)
            ));

            $this->messageList[] = 'Reembolso realizado com sucesso.';

            //Cancelar e concluir reserva
            db()->update('reserva', array('status' => 8), ' id_reserva = :res ', array('res' => $reserva->getId()));

            return true;
        }

        //Registrar Erro
        db()->insert('reembolso', array(
            'id_reserva' => $reserva->getId(),
            'data' => date('Y-m-d'),
            'email' => $anfitriao->getEmail(),
            'status' => 'ERROR',
            'valor' => $valor_reembolso,
            'observacao' => ( isset($responseNvp['L_LONGMESSAGE0']) ? $responseNvp['L_LONGMESSAGE0'] : "ERRO NA API DO PAYPAL" ) . '<br>' . $obs
        ));

        return false;
    }

    //Reembolso parcial 
    public function reembolso() {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        /* @var $modalidade \Codando\Modulo\Modalidade */
        $modalidade = $reserva->getModalidade();

        /* @var $cancelamento \Codando\Modulo\Cancelamento */
        $cancelamento = $anfitriao->getCancelamento();

        if (is_null($reserva->getTransactionid()) == true) {

            $this->error = true;
            $this->messageList[] = 'Não foi possível cancelar, tente mais tarde.';

            logsgc('reembolso', 42, $reserva->getId(), 'erro reserva não tem transacao no paypal.');
            return false;
        }

        if (is_instanceof('Codando\Modulo\Modalidade', $modalidade) === false) {

            $this->error = true;
            $this->messageList[] = 'Não foi possível cancelar, tente mais tarde.';

            logsgc('reembolso', 42, $reserva->getId(), 'erro anfitriao não tem o tipo de modalidade.');
            return false;
        }

        if (is_instanceof('Codando\Modulo\Cancelamento', $cancelamento) === false) {

            $this->error = true;
            $this->messageList[] = 'Não foi possível cancelar, tente mais tarde.';

            logsgc('reembolso', 42, $reserva->getId(), 'erro anfitriao não tem o tipo de cancelamento.');
            return false;
        }

        if (input()->getSession('logadoanfitriao') != $anfitriao->getId()) {

            db()->update('reserva', array('cancelado' => 2), ' id_reserva = ' . $reserva->getId());

            switch ($modalidade->getId()) {
                case '1': //Hospedagem
                    return $this->reembolsoHospedagem();
                    break;

                case '2': //Daycare
                    return $this->reembolsoDayCare();
                    break;
            }
        } else {

            db()->update('reserva', array('cancelado' => 1), ' id_reserva = ' . $reserva->getId());
            return $this->reembolsototal();
        }

        $this->error = true;
        $this->messageList[] = 'Não foi possível classificar a modalidade.';

        logsgc('reembolso', 42, $reserva->getId(), 'erro modalidade, não tem o tipo de modalidade.');

        return false;
    }

    //Repasse para o anfitriao
    public function sendMoney() {

        /* @var $reserva \Codando\Modulo\Reserva */
        $reserva = $this->modulo;

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $reserva->getAnfitriao();

        //Existe
        if (is_null($reserva->getPaypalurl()) == true || strpos($reserva->getPaypalurl(), 'token') == false) {
            return false;
        }

        //Retira % do site, o resto repassar pro anfitriao
        if(intval($reserva->getDesconto()) > 0){
            $porcentagem = 100 - ( GPOR_SITE - intval($reserva->getDesconto()) );
        }else{
            $porcentagem = 100 - GPOR_SITE;
        }

        $valorReserva = $reserva->getValorcancelado() > 0 ? $reserva->getValorcancelado(false) : $reserva->getValor(false);

        $valor_trans = ( ($valorReserva * $porcentagem ) / 100 ); //Valor a ser transferido 75 % para o anfitriao

        if ($reserva->getValorreembolso() > 0) {
            $valor_trans = $valor_trans + $reserva->getValorreembolso(false);
        }

        $valor_trans = number_format((float) $valor_trans, 2, ".", "");

        $requestNvp = array(
            'METHOD' => 'MassPay', //Metodo
            'EMAILSUBJECT' => 'Dog Safe Reserva #' . $reserva->getId(),
            'RECEIVERTYPE' => 'EmailAddress', //O tipo do repasse
            'CURRENCYCODE' => 'BRL', //Moeda
            'L_EMAIL0' => $anfitriao->getEmail(), //Email conta paypal
            'L_Amt0' => $valor_trans, //Valor a ser transferido 75 % para o anfitriao
            'L_NOTE0' => 'Trans. Reserva #' . $reserva->getId(), //Descricao
            'BUTTONSOURCE' => 'BR_EC_EMPRESA',
            'LOCALECODE' => 'pt_BR'
        );

        //'RETURNURL' => $this->baseUrl . 'reservar/masspay/' . $reserva->getId() . '?success=true',
        //'CANCELURL' => $this->baseUrl . 'reservar/masspay/' . $reserva->getId() . '?success=false',
        //Envia a requisição e obtém a resposta da PayPal
        $responseNvp = $this->paypal($requestNvp);

        if (is_array($responseNvp) === false) {
            return false;
        }

        if (isset($responseNvp['ACK']) && "SUCCESS" == strtoupper($responseNvp["ACK"])) {

            //echo('MassPay Completed Successfully: ');
            //print_r($responseNvp, true);

            $this->error = false;
            $this->messageList[] = 'Transferência realizada.';

            db()->insert('transferencia', array(
                'id_reserva' => $reserva->getId(),
                'data' => date('Y-m-d'),
                'email' => $anfitriao->getEmail(),
                'valor' => $valor_trans,
                'status' => strtoupper($responseNvp["ACK"]),
                'observacao' => 'Transferência realizada com sucesso'
            ));

            logsgc('transferencia', 42, $reserva->getId(), 'Transferência realizada com sucesso');


            //5:Concluír reserva
            db()->update('reserva', array('status' => 5), ' id_reserva = :res ', array('res' => $reserva->getId()));

            return true;
        }

        $this->error = true;
        $this->messageList[] = 'Não foi possível transferir, tente mais tarde.';

        var_dump($requestNvp, $responseNvp);

        db()->insert('transferencia', array(
            'id_reserva' => $reserva->getId(),
            'data' => date('Y-m-d'),
            'email' => $anfitriao->getEmail(),
            'valor' => $valor_trans,
            'status' => 'ERROR',
            'observacao' => ( isset($responseNvp['L_LONGMESSAGE0']) ? $responseNvp['L_LONGMESSAGE0'] : "ERRO NA API DO PAYPAL" )
        ));

        logsgc('transferencia', 42, $reserva->getId(), (isset($responseNvp['L_LONGMESSAGE0']) ? $responseNvp['L_LONGMESSAGE0'] : 'Não foi possível transferir, resposta do paypal com erro.'));

        return false;
    }

    protected function paypal($requestNvp) {

        //Vai usar o Sandbox, ou produção?
        $sandbox = false;
        $this->sandbox = $sandbox;
        $paypalURL = 'https://www.' . ($this->sandbox === true ? 'sandbox.' : '') . 'paypal.com/cgi-bin/webscr';

        if ($this->sandbox) {

            $user = 'dog_api1.dogsafe.com.br';
            $pswd = '32H26CLC2P2EJ2N9';
            $signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AnECK3.XYozB2rhTtm0eMQDQGFK.';
        } else {

            $user = 'dog_api1.dogsafe.com.br';
            $pswd = '32H26CLC2P2EJ2N9';
            $signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AnECK3.XYozB2rhTtm0eMQDQGFK.';
        }

        $requestNvp = array_merge(array(
            'USER' => $user,
            'PWD' => $pswd,
            'SIGNATURE' => $signature,
            'VERSION' => '108.0', //108.0
                ), (array) $requestNvp);


        $responseNvp = $this->sendNvpRequest($requestNvp, $sandbox);

        if (is_array($responseNvp) === false) {
            return false;
        }

        return $responseNvp;
    }

    /**
     * Envia uma requisição NVP para uma API PayPal.
     *
     * @param array $requestNvp Define os campos da requisição.
     * @param boolean $sandbox Define se a requisição será feita no sandbox ou no
     *                         ambiente de produção.
     *
     * @return array Campos retornados pela operação da API. O array de retorno poderá
     *               ser vazio, caso a operação não seja bem sucedida. Nesse caso, os
     *               logs de erro deverão ser verificados.
     */
    private function sendNvpRequest(array $requestNvp, $sandbox = false) {

        //Endpoint da API
        $apiEndpoint = 'https://api-3t.' . ($this->sandbox ? 'sandbox.' : null);
        $apiEndpoint .= 'paypal.com/nvp';

        //Executando a operação
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $apiEndpoint);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestNvp));

        $response = urldecode(curl_exec($curl));

        curl_close($curl);

        //Tratando a resposta
        $responseNvp = array();

        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $responseNvp[$name] = $matches['value'][$offset];
            }
        }

        //Verificando se deu tudo certo e, caso algum erro tenha ocorrido,
        //gravamos um log para depuração.
        if (isset($responseNvp['ACK']) && $responseNvp['ACK'] != 'Success') {
            for ($i = 0; isset($responseNvp['L_ERRORCODE' . $i]); ++$i) {
                $message = sprintf("PayPal NVP %s[%d]: %s\n", $responseNvp['L_SEVERITYCODE' . $i], $responseNvp['L_ERRORCODE' . $i], $responseNvp['L_LONGMESSAGE' . $i]);

                error_log($message);
            }
        }

        return $responseNvp;
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function setSandbox($sandbox) {
        $this->sandbox = $sandbox;
    }

    public function __construct() {

        $this->sandbox = false;
        $this->baseUrl = 'https://dogsafe.com.br/';
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
