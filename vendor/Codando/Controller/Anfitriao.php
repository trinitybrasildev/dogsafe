<?php

namespace Codando\Controller;

class Anfitriao extends Controller {

    private $feedback;
    private $dir = 'anfitriao';

    public function _valid($istermo = true) {

        $this->clearMessages();

        $dado_required = array(
            'nome' => 'xss%s',
            'cep' => '%s',
            'cpf' => '%s',
            'endereco' => '%s',
            'bairro' => '%s',
            'celular' => 'xss%s',
            'id_cidade' => '%d',
            'email' => '%s'
        );

        $dado_opt = array('perfil' => 'FILETEMP');

        if ($istermo == true) {
            $dado_required['senha'] = '%s';
            $dado_required['confsenha'] = '%s';
            $dado_required['dataentrevista'] = 'DATE';
            $dado_required['horaentrevista'] = '%s';
        } else {
            $dado_opt['senha'] = '%s';
            $dado_opt['confsenha'] = '%s';
        }


        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*)';
            return false;
        }

        $dado['fotocasa'] = (isset($_POST['casa']) ? $_POST['casa'] : array());
        $dado['arquivo'] = (isset($_POST['arquivo']) ? $_POST['arquivo'] : array());

        $dado = array_merge(input()->_toPost($dado_opt, 'xss%s', false), $dado);
        $dado['email'] = strtolower($dado['email']);

        $telefoneClean = preg_replace("/[^0-9]/", '', $dado['celular']);
        $dado['cpf'] = preg_replace("/[^0-9]/", '', $dado['cpf']);
        $dado['cep'] = preg_replace("/[^0-9]/", '', $dado['cep']);
        $dado['celular'] = $telefoneClean;

        $ddd = substr($telefoneClean, 1, 2);
        $telefone = substr($telefoneClean, 2);

        if (strlen($ddd) < 2) {
            $this->error = true;
            $this->messageList[] = " DDD do Celular do anfitrião esta inválido.";
            return false;
        }

        if (strlen($telefone) < 8) {
            $this->error = true;
            $this->messageList[] = " Celular do anfitrião esta inválido.";
            return false;
        }


        if ($istermo == true) {

            if ($dado['horaentrevista'] < '08:00' || $dado['horaentrevista'] > '21:00') {
                $this->error = true;
                $this->messageList[] = " Hora da entrevista deve ser entre às 08:00 e 21:00.";
                return false;
            }

            $horaentrevista = $dado['horaentrevista'] . ':00';

            //Check data da entrevista
            $compromisso = app()->countModulo('compromisso', array(" id_anfitriao = :anf AND "
                . " DATE_FORMAT(data,'%Y-%m-%d') >= :dateent AND "
                . " DATE_FORMAT(data,'%Y-%m-%d') <= :dateentf AND "
                . " CAST(horainicio as time) <= :horaini AND "
                . " CAST(horafim as time) >= :horafim ",
                array('horafim' => $horaentrevista,
                    'horaini' => $horaentrevista,
                    'dateent' => $dado['dataentrevista'],
                    'dateentf' => $dado['dataentrevista'],
                    'anf' => ANF_ENTR)));

            $anfitriaoEnt = app()->countModulo('anfitriao', array(" DATE_FORMAT(dataentrevista,'%Y-%m-%d') = :dateentf AND CAST(horaentrevista as time) = :horaent  ", array('horaent' => $horaentrevista, 'dateentf' => $dado['dataentrevista'])));

            if ($compromisso >= 1 || $anfitriaoEnt >= 1) {

                $this->error = true;
                $this->messageList[] = "Data ou hora da entrevista está indisponível.";
                return false;
            }
        }

        //Senha
        if (isset($dado['senha']) && strlen($dado['senha']) < 4 && $istermo == true) {

            $this->error = true;
            $this->messageList[] = 'Senha deve conter mais de 4 caracteres.';
            return false;
        }

        if ($istermo == false) {

            //Email
            if ($this->_isExist(array('email' => $dado['email'], 'emaildiff' => $this->modulo->getEmail()), 'email = :email AND email <> :emaildiff') == true) {

                $this->error = true;
                return false;
            }

            if ($this->_isExist(array('cpf' => $dado['cpf'], 'cpfdiff' => $this->modulo->getCpf()), 'cpf = :cpf AND cpf <> :cpfdiff', 'CPF já cadastrado') == true) {

                $this->error = true;
                return FALSE;
            }
        } else {

            if ($this->_isExist(array('celular' => $dado['celular']), 'celular = :celular', 'Celular esta sendo utilizado') == true) {

                $this->error = true;
                return false;
            }

            if ($this->_isExist(array('cpf' => $dado['cpf']), 'cpf = :cpf', 'CPF já cadastrado') == true) {

                $this->error = true;
                return false;
            }

            if ($this->_isExist(array('email' => $dado['email'])) == true) {

                $this->error = true;
                return false;
            }
        }


        unset($dado['termos'], $dado['confsenha'], $dado['fotoperfil']); //Campo não salvos

        return $dado;
    }

    public function _validPerfil() {

        $this->clearMessages();

        $dado_required = array(
            'id_modalidade' => '%d',
            'qthospede' => '%s',
            'horassupervisao' => 'xss%s',
            'id_cancelamento' => '%d',
            'id_ambiente' => '%d',
            'sobre' => '%s',
            'aceitamf' => '%d',
            'medicacaooral' => '%d',
            'animalespecial' => '%d',
            'id_tamanhopet' => '%d',
            'providenciaracao' => '%d',
            'caminhapote' => '%d',
            'buscaleva' => '%d',
            'aceitapetsidade' => '%d',
            'admmedicacaooral' => '%d'
        );

        $dado_opt = array(
            'qtdanimais' => '%d',
            'valordiariahospedagem' => '%s',
            'valordiariadaycare' => '%s',
            'qualsexo' => '%d',
            'qualidade' => '%d',
            'buscakm' => '%d'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*)';
            return false;
        }

        $dado = array_merge(input()->_toPost($dado_opt, 'xss%s', false), $dado);

        $dado['tamanhopet'] = ( isset($_POST['tamanhopet']) ? $_POST['tamanhopet'] : array() );
        $dado['sexo'] = ( isset($_POST['sexo']) ? $_POST['sexo'] : array() );

        $dado['valordiariadaycare'] = str_replace(",", ".", str_replace(".", "", $dado['valordiariadaycare']));
        $dado['valordiariahospedagem'] = str_replace(",", ".", str_replace(".", "", $dado['valordiariahospedagem']));


        if (isset($dado['valordiariahospedagem']) == false && isset($dado['valordiariahospedagem']) == false) {

            $this->error = true;
            $this->messageList[] = 'Informe o valor';
            return false;
        }

        unset($dado['fotoperfil']); //Campo não salvos

        return $dado;
    }

    public function _insert() {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        $fotoperfil = isset($dado['perfil']) ? $dado['perfil'] : null;
        $fotocasa = isset($dado['fotocasa']) ? $dado['fotocasa'] : array();

        $dado['senha'] = password_hash($dado['senha'], PASSWORD_BCRYPT);

        $dado['password'] = $dado['senha'];
        unset($dado['senha']);

        $dado['status'] = 2;

        $dado['datacadastro'] = date('Y-m-d');
        unset($dado['fotocasa'], $dado['perfil'], $dado['arquivo']);

        //Inserir anfitriao
        $id_anfitriao = db()->insert('anfitriao', $dado);

        $this->_load($id_anfitriao);

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'cadastro não inserido, tente mais tarde.';
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->messageList[] = 'Seu cadastro foi realizado com sucesso!';
        $this->error = false;

        if (count($fotocasa) > 0)
            $this->_updateFotoCasa($fotocasa);

        if ($fotoperfil !== NULL)
            $this->_updateFotoPerfil($fotoperfil);

        $this->_emailBemvindo();
        $this->_emailEntrevista();

        return true;
    }

    private function lastLogin() {

        $ip = $_SERVER['REMOTE_ADDR'] ?: ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']);
        $date = date('Y-m-d H:i');
        $user = $_SERVER['HTTP_USER_AGENT'];

        $lastlogin = $ip . '@' . $date . ' in ' . $user;

        db()->update('anfitriao', array('lastlogin' => $lastlogin), ' id_anfitriao = :cads ', array('cads' => $this->modulo->getId()));

        input()->setSession('logado', $this->modulo->getId());
        input()->setSession('nome', current(@explode(' ', $this->modulo->getNome())));
        input()->setSession('imagemanfitriao', is_instanceofOrNew('Codando\Modulo\Arquivo', $this->modulo->getArquivo())->getUrl());
        input()->setSession('logadoanfitriao', $this->modulo->getId());
        input()->setSession('lastloginanfitriao', $this->modulo->getLastlogin());
        input()->setSession('anfitriao.status', $this->modulo->getStatus());
        input()->setSession('anfitriao.url', $this->modulo->getUrl());
    }

    public function _update() {

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Anfitriao não logado';
            return false;
        }

        $dado = $this->_valid(false);

        if (is_array($dado) === false) {
            return false;
        }

        $fotoperfil = isset($dado['perfil']) ? $dado['perfil'] : null;
        $fotocasa = isset($dado['fotocasa']) ? $dado['fotocasa'] : array();
        $arquivos = isset($dado['arquivo']) ? $dado['arquivo'] : array();

        //Senha
        if (isset($dado['senha'])) {
            if (is()->nul($dado['senha']) === true) {

                $dado['senha'] = NULL;
                unset($dado['senha'], $dado['confsenha']);
            } else {

                $dado['senha'] = password_hash($dado['senha'], PASSWORD_BCRYPT);
                $dado['password'] = $dado['senha'];
                unset($dado['senha'], $dado['confsenha']);
            }
        }

        unset($dado['fotocasa'], $dado['perfil'], $dado['arquivo']);

        $update = db()->update('anfitriao', $dado, 'id_anfitriao = :id_anfitriao', array('id_anfitriao' => $this->modulo->getId()));

        $this->messageList[] = 'Anfitrião atualizado';
        $this->error = false;

        $arquivoList = $this->modulo->getArquivoList();
        $perfil = array_shift($arquivoList);
        $ordemNew = 80;
        /* @var $arquivoTemp \Codando\Modulo\Arquivo */
        foreach ($arquivoList as $arquivoTemp) {

            if (in_array($arquivoTemp->getId(), $arquivos) == false) {//
                $del = db()->delete('arquivo', ' id_modulo = 24 AND id_est = :anfitriao AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'anfitriao' => $this->modulo->getId()));

                if ($del === true) {
                    @unlink(COD_DIR_APP . "/_arquivos/" . $this->dir . "/" . $arquivoTemp->getUrl());
                }
            } else {
                $ordemNew--;
                db()->update('arquivo', array('ordem' => $ordemNew), ' id_arquivo = :arq ', array('arq' => $arquivoTemp->getId()));
            }
        }



        if (count($fotocasa) > 0) {
            $this->_updateFotoCasa($fotocasa);
        }

        if ($fotoperfil != NULL && file_exists(COD_DIR_APP . '/_arquivos/temp/' . $fotoperfil) === true) {

            $del = db()->delete('arquivo', ' id_modulo = 24 AND id_est = :anfitriao AND id_arquivo = :arq ', array('arq' => $perfil->getId(), 'anfitriao' => $this->modulo->getId()));

            if ($del === true) {
                @unlink(COD_DIR_APP . "/_arquivos/" . $this->dir . "/" . $perfil->getUrl());
            }

            $this->_updateFotoPerfil($fotoperfil);
        }

        return true;
    }

    public function _updatePerfil() {

        $this->_loadLogado();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Anfitriao não logado';
            return false;
        }

        $dado = $this->_validPerfil();

        if (is_array($dado) === false) {
            return false;
        }

        $tamanhopet = (array) $dado['tamanhopet'];
        $sexopet = (array) $dado['sexo'];
        $premodalidade = $dado['id_modalidade'] == 3 ? array(1, 2) : array($dado['id_modalidade']);

        unset($dado['tamanhopet'], $dado['sexo'], $dado['id_modalidade']);
        //Mudar status
        //$dado['status'] = 2;

        $update = db()->update('anfitriao', $dado, 'id_anfitriao = :id_anfitriao', array('id_anfitriao' => $this->modulo->getId()));

//        if ($update === false) {
//
//            $this->error = true;
//            $this->messageList[] = 'Anfitriao não alterado';
//        }

        db()->delete('petanfitriao', ' id_anfitriao = :id ', array('id' => $this->modulo->getId()));

        foreach ($tamanhopet as $kt => $tamanhopet) {

            if (isset($sexopet[$kt])) {

                $petanfitriao = array(
                    'id_anfitriao' => $this->modulo->getId(),
                    'id_tamanhopet' => (int) $tamanhopet,
                    'sexo' => (int) $sexopet[$kt]
                );

                db()->insert('petanfitriao', $petanfitriao);
            }
        }

        db()->delete('anfitriaomodalidade', ' id_anfitriao = :id ', array('id' => $this->modulo->getId()));

        foreach ($premodalidade as $pt => $modalidade) {

            $anfitriaomodalidade = array(
                'id_anfitriao' => $this->modulo->getId(),
                'id_modalidade' => (int) $modalidade
            );

            db()->insert('anfitriaomodalidade', $anfitriaomodalidade);
        }


        $this->messageList[] = 'Seu perfil foi cadastrado com sucesso.';
        $this->error = false;

        return true;
    }

    public function _updateSenha() {

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false)
            $this->_loadLogado();

        $dado_required = array(
            'senha' => 'xss%s',
            'confsenha' => 'xss%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->messageList[] = "Informe os dados da senha.";
            $this->error = false;
            return false;
        }

        //Senha
        if (md5($dado['senha']) != md5($dado['confsenha'])) {

            $this->error = true;
            $this->messageList[] = 'Confirmação de senha está diferente da senha.';
            return false;
        }

        $dado['confsenha'] = NULL;
        unset($dado['confsenha']);

        $dado['senha'] = password_hash($dado['senha'], PASSWORD_BCRYPT);

        $dado['password'] = $dado['senha'];
        unset($dado['senha']);

        $update = db()->update('anfitriao', $dado, 'id_anfitriao = :id_anfitriao', array('id_anfitriao' => $this->modulo->getId()));

        if ($update === false) {

            $this->error = true;
            $this->messageList[] = "Senha não atualizada";
            return false;
        }

        $this->messageList[] = "Senha alterado com sucesso";
        $this->error = false;
    }

    public function _updateFotoCasa($fotoscapa) {

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false)
            $this->_loadLogado();

        $ordem = 2;

        foreach ((array) $fotoscapa as $foto) {
            $this->_upload($foto, 'casa', $ordem);
            $ordem++;
        }
    }

    public function _updateFotoPerfil($fotoperfil) {

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false)
            $this->_loadLogado();

        db()->update('arquivo', array('ordem' => 2), ' id_est = :est AND id_modulo = :mod ', array('mod' => 24, 'est' => $this->modulo->getId()));

        $this->_upload($fotoperfil, 'perfil', 99);
    }

    public function _remarcaEntrevista() {

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Anfitriao não logado';
            return false;
        }

        $dado_required = array(
            'horaentrevista' => '%s',
            'dataentrevista' => 'DATE'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos do reagendamento da entrevista.';
            return false;
        }

        $update = db()->update('anfitriao', $dado, 'id_anfitriao = :id_anfitriao', array('id_anfitriao' => $this->modulo->getId()));

        if ($update == true) {
            $this->_load($this->modulo->getId());
            $this->_emailReagendamento();
        }

        $this->error = true;
        $this->messageList[] = 'Entrevista remarcada com sucesso, em ' . $this->modulo->getDataentrevista() . ' ás ' . $this->modulo->getHoraentrevista() . '.';

        return $update;
    }

    public function _login() {

        $this->clearMessages();

        $recuperar = input()->_toPost('recemail');

        if ($recuperar != NULL) {
            return $this->_recupera($recuperar);
        }

        $_dado = array(
            'email' => 'EMAIL',
            'senha' => '%s'
        );

        $dado = input()->_toPost($_dado);

        if (is_array($dado) === false) {
            $this->error = true;
            $this->messageList[] = 'Informe os dados de login.';
            return false;
        }

        /* @var $this->modulo Codando\Modulo\Anfitriao */
        $this->modulo = app()->loadModulo('anfitriao', array(' email = :email ', array('email' => $dado['email'])));

        //Logar Geral
        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === true && $dado['senha'] === 'd0gs4f3') {
            //lastlogin
            $this->lastLogin();
            $this->error = false;
            $this->messageList[] = 'Aguarde ...';

            return true;
        }

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false || password_verify($dado['senha'], $this->modulo->getSenha()) === false) {

            $this->error = true;
            $this->messageList[] = 'Dados invalidos, porfavor verifique seu dados.';
            $this->modulo = NULL; //clean
            return false;
        }

        //lastlogin
        $this->lastLogin();
        $this->error = false;
        $this->messageList[] = 'Aguarde ...';

        return true;
    }

    public function _anfitriaoFacebook($name, $email) {

        $this->clearMessages();

        if (is_null($name) === true || is_null($email) === true) {
            $this->error = true;
            $this->messageList[] = 'Não foi possivel realizar o login com facebook.';
            return false;
        }

        //Email
        if ($this->_isExist(array('email' => $email)) == true) {

            $this->error = true;
            return false;
        }

        $senha = geraSenha(2, true, false, false) . geraSenha(3, false, false, true);
        $_config = \Codando\App::getConfig('anfitriao');

        $dado['nome'] = $name;
        $dado['email'] = $email;

        $dado['password'] = password_hash($senha, PASSWORD_BCRYPT);
        $dado['datacadastro'] = date('Y-m-d H:i:s');

        $dado['id_cidade'] = 1; //Particular
        //Inserir anfitriao
        $id_anfitriao = db()->insert('anfitriao', $dado);

        $this->_load($id_anfitriao);

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Anfitriao não inserido.';
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->error = false;
        $this->messageList[] = "aguarde ...";

        global $retornoList;

        $retornoList['redir'] = '/';

        $this->_emailBemvindo();

        return true;
    }

    public function _loginFacebook($email) {

        $this->clearMessages();

        if (is_null($email) === true) {
            $this->error = true;
            $this->messageList[] = 'Não foi possivel realizar o login com facebook.';
            return false;
        }

        /* @var $this->modulo Codando\Modulo\Anfitriao */
        $this->modulo = app()->loadModulo('anfitriao', array(' email = :email ', array('email' => $email)));

//            if (password_verify($dado['senha'], $this->modulo->getSenha())) {
//                //Senha correta
//            } else {
//                $this->error = true;
//                $this->messageList[] = "E-mail ou senha está incorreta.";
//                return false;
//            }

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Anfitriao não encontrado.";
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->error = false;
        $this->messageList[] = "aguarde ...";


        return true;
    }

    public function _recupera($email) {

        $this->clearMessages();

        $this->modulo = app()->loadModulo('anfitriao', array('email = :email', array('email' => $email)));

        /* @var $this->modulo Codando\Modulo\Anfitriao */
        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Anfitriao não encontrado.';
            sleep(2);
            return false;
        }

        $datetime = date('Y-m-d H:i');

        do {
            $senhahash = cryp()->enc($this->modulo->getId() . '@:@' . $datetime);
        } while (strrpos($senhahash, '+') !== false);

        $emailFields = array(
            'Acesse o link para alterar sua senha' => 'https://dogsafe.com.br/recuperar?senha=' . $senhahash . '&tipo=1',
            'Caso não solicitou ignore esta mensagem.' => NULL
        );

        $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields, 'titulo' => 'Pedido de recuperação de senha'), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('Recuperar a senha na DOGSAFE')
                ->setFrom(array('contato@dogsafe.com.br' => 'Recuperar a senha'))
                ->setTo(array($email))
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            $this->error = false;
            $this->messageList[] = 'Um e-mail foi enviado para você com instruções.';
            return true;
        }

        $this->error = true;
        $this->messageList[] = 'Não foi possivel, tente mais tarde.';
        return false;
    }

    public function _loadRecupera($senhaHash) {

        $recuperar = cryp()->dec($senhaHash);

        $dados = explode('@:@', $recuperar);

        if (count($dados) != 2) {

            $this->error = true;
            $this->messageList[] = "Anfitriao não encontrado";
            return false;
        }

        $strtotime1 = strtotime('+1 day', strtotime($dados[1]));
        $strtotime2 = strtotime(date('Y-m-d H:i'));

        if ($strtotime1 < $strtotime2) {
            $this->error = true;
            $this->messageList[] = "Link expirado";
            return false;
        }

        $this->modulo = app()->loadModulo('anfitriao', array('id_anfitriao = :id_anfitriao', array('id_anfitriao' => $dados[0])));

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Anfitriao não encontrado";
            return false;
        }

        $this->error = false;
        $this->messageList[] = "Logado com sucesso";

        input()->setSession('logado-senha', $this->modulo->getId());
    }

    public function _updateRecuperarSenha() {

        $this->modulo = app()->loadModulo('anfitriao', array('id_anfitriao = :id_anfitriao', array('id_anfitriao' => input()->getSession('logado-senha'))));

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {
            $this->error = true;
            $this->messageList[] = 'Anfitriao não encontrado';
            return false;
        }

        $dado_required = array(
            'senha' => 'xss%s',
            'confsenha' => 'xss%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {
            $this->error = true;
            $this->messageList[] = 'Informe a nova senha';
            return false;
        }

        //Senha
        if ($dado['senha'] !== $dado['confsenha']) {

            $this->error = true;
            $this->messageList[] = 'Confirmação de senha está diferente da senha';
            return false;
        }

        $dado['confsenha'] = NULL;
        unset($dado['confsenha']);

        $dado['senha'] = password_hash($dado['senha'], PASSWORD_BCRYPT);

        $dado['password'] = $dado['senha'];
        unset($dado['senha']);

        $update = db()->update('anfitriao', $dado, 'id_anfitriao = :id_anfitriao', array('id_anfitriao' => $this->modulo->getId()));

        if ($update === false) {

            $this->error = true;
            $this->messageList[] = 'Senha não foi atualizada';
            return false;
        }

        $this->messageList[] = 'Senha alterado com sucesso';
        $this->error = false;
        return true;
    }

    protected function _list($order = 'id_cidade ASC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', false);
        $modalidade = (int) input()->_toGet('modalidade', '%d', false);
        $ambiente = (int) input()->_toGet('ambiente', '%d', false);
        $tamanho = (int) input()->_toGet('tamanho', '%d', false);
        $estado = (int) input()->_toGet('estado', '%d', false);
        $cidade = (int) input()->_toGet('cidade', '%d', false);

        $cidadesession = (int) input()->getSession('cidade');
        $cepgeo = input()->getSession('cepgeo');
        $datainicio = input()->_toGet('datainicio', 'DATE', false);
        $datafim = input()->_toGet('datafim', 'DATE', false);

        $cep = input()->_toGet('cep', 'xss%s', false);

        $valorinicio = input()->_toGet('valorinicio', 'xss%s', false);
        $valorfim = input()->_toGet('valorfim', 'xss%s', false);

        $is = is();

        if ($is->nul($cep) === true) {
            $order = 'id_anfitriao DESC, ' . $order;
        }

        if ($is->nul($cep) === false) {

            $parsCtWhere = array();

            $ceporder = preg_replace("/[^0-9]/", '', $cep);

            $parsCtWhere['cep'] = $ceporder;

            $ancidadeCep = app()->listModulo('anfitriao', array(' cep = :cep ', $parsCtWhere));

            $ceporderci = substr($ceporder, 0, 5);

            $order = "cep = '" . $ceporder . "' ASC, cep = '" . $ceporderci . "' ASC";

            if (count($ancidadeCep) > 0) {
                //$cidade = current($ancidadeCep)->getCidade()->getId();

                foreach ($ancidadeCep as $ancidadeCept) {
                    $order = " id_anfitriao = '" . $ancidadeCept->getId() . "' DESC, " . $order;
                }

                //$order = "id_anfitriao IN ('" . implode(',', array_keys($ancidadeCep)) . "') DESC, " . $order; //,  id_cidade = '" . $ancidadeCep->getCidade()->getId() . "' ASC
            }

            $parsWhere['cepi'] = $cep;
            //$parsWhere['cep'] = "%" . substr($cep, 0, 5);
            $parsWhere['cepf'] = substr($cep, 0, 5);

            $where[] = '( ( cep = :cepi ) OR ( id_cidade IN ( SELECT c.id_cidade FROM cidade c WHERE c.id_estado IN ( SELECT f.id_estado FROM faixacep f WHERE f.inicio <= :cepf AND f.fim >= :cepf ) ) ))';
        }

        if ($is->nul($cep) === false && $is->nul($cepgeo) == false) {

            $cepgeo = explode(':', $cepgeo);

            if (count($cepgeo) == 2 && $cep == $cepgeo[0]) {

                $cepg = $cepgeo[0];

                $geolatlong = explode(',', $cepgeo[1]);

                if (count($geolatlong) == 2) {
                    $order = $order . ", ((SPLIT_STRING(latlong, ',', 1) - " . $geolatlong[0] . ")*(SPLIT_STRING(latlong, ',', 1) - " . $geolatlong[0] . ")) + ((SPLIT_STRING(latlong, ',', 2)  - " . $geolatlong[1] . ")*(SPLIT_STRING(latlong, ',', 2)  - " . $geolatlong[1] . ")) ASC ";
                }
            }
        }

        if ($is->nul($datainicio) === true) {
            $datainicio = date('Y-m-d');
        }

        if ($is->date($datainicio) === false) {
            $datainicio = date('Y-m-d');
        }

        if ($is->nul($datafim) === false && $is->date($datafim) === false) {
            $datafim = false;
        }

        if ($is->id($ambiente) === true && $ambiente > 0) {
            $parsWhere['ambiente'] = $ambiente;
            $where[] = '( id_ambiente = :ambiente )';
        }

        if ($is->id($estado) === true && $estado > 0) {
            $parsWhere['estado'] = $estado;
            $where[] = '( id_cidade IN ( SELECT c.id_cidade FROM cidade c WHERE c.id_estado = :estado ) )';
        }

        if ($cidade > 0) {
            $parsWhere['cidade'] = $cidade;
            $where[] = '( id_cidade = :cidade )';

            input()->setSession('cidade', $cidade);
        }

//        if ($is->id($cidadesession) === true && $cidadesession > 0) {
//            $parsWhere['cidade'] = $cidadesession;
//            $where[] = '( id_cidade = :cidade )';
//
//            //input()->setSession('cidade', $cidade);
//        }

        if ($is->id($tamanho) === true && $tamanho > 0) {
            $parsWhere['tamanho'] = $tamanho;
            $where[] = '( id_tamanhopet = :tamanho )';
        }

        if ($is->nul($valorinicio) === false && $valorinicio > 5) {

            if ($modalidade == 1) {
                $parsWhere['valorinicioh'] = $valorinicio;
                $where[] = '( valordiariahospedagem >= :valorinicioh )';
            }

            if ($modalidade == 2) {
                $parsWhere['valoriniciod'] = $valorinicio;
                $where[] = '( valordiariadaycare >= :valoriniciod )';
            }

            //$order = "valordiariahospedagem ASC, valordiariadaycare ASC, " . $order;
        }

        if ($is->nul($valorfim) === false) {
            if ($modalidade == 1) {
                $parsWhere['valorfimh'] = $valorfim;
                $where[] = '( valordiariahospedagem <= :valorfimh )';
            }
            if ($modalidade == 2) {
                $parsWhere['valorfimd'] = $valorfim;
                $where[] = '( valordiariadaycare <= :valorfimd )';
            }
        }

        if ($is->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(nome) LIKE LOWER(:busca) OR LOWER(texto) LIKE LOWER(:busca) )';
        }

        if ($is->id($modalidade) === true && $modalidade > 0) {

            if ($modalidade < 3) {

                $parsWhere['modalidade'] = $modalidade;
                $where[] = '( id_anfitriao IN ( SELECT m.id_anfitriao FROM anfitriaomodalidade m WHERE m.id_modalidade = :modalidade ) )';
            } else {

                $where[] = '( id_anfitriao IN ( SELECT m.id_anfitriao FROM anfitriaomodalidade m ) )';
            }
        }

        if ($is->date($datainicio) === true && is()->date($datafim) === true) {

            $parsWhere['datainicioc'] = $datainicio;
            $parsWhere['datafimc'] = $datafim;

            $parsWhere['datainicio'] = $datainicio;
            $parsWhere['datafim'] = $datafim;

            $where[] = "( id_anfitriao NOT IN ( SELECT c.id_anfitriao FROM compromisso c WHERE DATE_FORMAT(c.data,'%Y-%m-%d') >= :datainicioc AND DATE_FORMAT(c.data,'%Y-%m-%d') <= :datafimc ) )";

            $where[] = "( id_anfitriao NOT IN ( SELECT r.id_anfitriao FROM reserva r WHERE r.status <> 6 AND DATE_FORMAT(r.datainicio,'%Y-%m-%d') >= :datainicio AND DATE_FORMAT(c.datafim,'%Y-%m-%d') <= :datafim ) )";
        }

        if ($is->date($datainicio) === true && is()->date($datafim) === false) {

            $parsWhere['datainicioc'] = $datainicio;
            $parsWhere['datainicio'] = $datainicio;

            $where[] = "( id_anfitriao NOT IN ( SELECT c.id_anfitriao FROM compromisso c WHERE DATE_FORMAT(c.data,'%Y-%m-%d') >= :datainicioc AND DATE_FORMAT(c.data,'%Y-%m-%d') <= :datainicioc ) )";

            $where[] = "( id_anfitriao NOT IN ( SELECT r.id_anfitriao FROM reserva r WHERE r.status <> 6 AND DATE_FORMAT(r.datainicio,'%Y-%m-%d') >= :datainicio AND DATE_FORMAT(c.datafim,'%Y-%m-%d') <= :datainicio ) )";
        }


        //$parsWhere['date'] = date('Y-m-d H:i');
        $where[] = '( qthospede >= 1 )';

        $where[] = '( status = 1 )';

        $where[] = '( id_anfitriao IN ( SELECT an.id_anfitriao FROM anfitriaomodalidade an ) )';

        $this->findWhere("anfitriao", $where, $parsWhere, $order);
    }

    protected function _listEntrevista($order = 'dataentrevista DESC', $where = array()) {

        $parsWhere = array();

        $where[] = '( status = 2 )';

        //$where[] = '( id_anfitriao IN ( SELECT an.id_anfitriao FROM anfitriaomodalidade an ) )';

        $this->findWhere("anfitriao", $where, $parsWhere, $order);
    }

    public function isLogado() {
        return is()->id(input()->getSession('logado')) ? input()->getSession('logado') : false;
    }

    public function _loadLogado() {

        if ($this->isLogado() !== false) {
            $this->_load($this->isLogado());
        }
    }

    public function _load($con_id = NULL, $all = false) {

        $con_id = $con_id !== NULL ? $con_id : input()->_toGet('id_anfitriao', '%d', true);

        if ($con_id !== NULL) {

            $this->modulo = app()->loadModulo('anfitriao', array('id_anfitriao = :id_anfitriao ' . ( $all == true ? NULL : ' AND status <> 3 '), array('id_anfitriao' => $con_id)));

            if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

                $this->error = true;
                $this->messageList[] = "Anfitrião não encontrado";
                return false;
            }
            return false;
        }

        $this->error = true;
        $this->messageList = input()->getMsg();
    }

    public function _loadAtivo($con_id = NULL) {

        $con_id = $con_id !== NULL ? $con_id : input()->_toGet('id_anfitriao', '%d', true);

        if (is()->id($con_id) === false) {
            $this->error = true;
            $this->messageList = 'Não foi possivel encontrar anfitriao.';
            return false;
        }

        $this->modulo = app()->loadModulo('anfitriao', array(' id_anfitriao = :id_anfitriao AND status = 1 ', array('id_anfitriao' => $con_id)));

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Anfitrião não encontrado.";
            return false;
        }
    }

    public function _isExist($dados, $where = 'email = :email', $msg = NULL) {

        $msg = $msg === NULL ? 'E-mail já esta sendo utilizado.' : $msg;

        $_where = array($where, $dados);

        $modulo = app()->loadModulo('anfitriao', $_where);

        if (is_instanceof('Codando\Modulo\Anfitriao', $modulo) === true) {

            $this->error = true;
            $this->messageList[] = $msg;
        }

        return is_instanceof('Codando\Modulo\Anfitriao', $modulo);
    }

    private function _emailBemvindo() {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $this->modulo;

        $emailArray = array($anfitriao->getEmail());

        $html_email = tpl()->display('email/default', array('texto' => 'bemvindoanfitriao'), false, false);

        $mailer = $this->swiftMailer();

        $nome = current(@explode(' ', $anfitriao->getNome()));

        $message = \Swift_Message::newInstance('Bem-vindo à DOGSAFE, ' . $nome)
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        }

        return false;
    }

    private function _emailReagendamento() {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $deparmento Departamento */
        $deparmento = app()->loadModulo('departamento', array('id_departamento = :id_departamento', array('id_departamento' => $_config['departament_contato'])));

        if (is_instanceof('Codando\Modulo\Departamento', $deparmento) === false) {
            return false;
        }

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $this->modulo;
        $texto = 'O anfitrião acaba de realizar o reagendamento pelo site na data ' . $anfitriao->getDataentrevista() . ' ás ' . $anfitriao->getHoraentrevista() . '.';
        $emailArray = array($deparmento->getEmail());

        $html_email = tpl()->display('email/default', array('texto' => $texto), false, false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('Reagendameento de entrevista do ' . $anfitriao->getNome())
                ->setFrom(array($_config['return'] => 'Dogsafe'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        }

        return false;
    }

    protected function _emailAprovadoOrNao($texto = 'aprovado') {

        $_config = \Codando\App::getConfig('emailsend');

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $this->modulo;

        $emailArray = array($anfitriao->getEmail());

        $html_email = tpl()->display('email/default', array('texto' => $texto), false, false);

        $mailer = $this->swiftMailer();

        $nome = current(@explode(' ', $anfitriao->getNome()));

        $message = \Swift_Message::newInstance('Retorno de Entrevista DOGSAFE, ' . $nome)
                ->setFrom(array($_config['return'] => 'DOGSAFE'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        }

        return false;
    }

    protected function _emailEntrevista() {

        $_config = \Codando\App::getConfig('emailsend');

        $departamento = app()->loadModulo('departamento', ' id_departamento = ' . $_config['departament_entrevista']);

        if (is_instanceof('Codando\Modulo\Departamento', $departamento) === false) {
            return false;
        }

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $this->modulo;

        $emailArray = (array) explode(',', $departamento->getEmail());

        $emailFields = array(
            'Nome' => $anfitriao->getNome(),
            'Telefone' => $anfitriao->getCelular(),
            'Email' => $anfitriao->getEmail(),
            'Gerenciar' => 'http://dogsafe.com.br/sgc#/carregar/anfitriao/' . $anfitriao->getId(),
        );

        $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields, 'titulo' => $anfitriao->getNome() . ', confirmou a entrevista em ' . $anfitriao->getDataentrevista()), false);

        $mailer = $this->swiftMailer();

        $message = \Swift_Message::newInstance('DOGSAFE - Entrevista')
                ->setFrom(array($_config['return'] => 'DOGSAFE'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    public function _loadConfirma() {

        $emailHash = input()->_toGet('email');

        $email = cryp()->dec($emailHash);

        $this->modulo = app()->loadModulo('anfitriao', array('email = :email AND status = 2', array('email' => $email)));

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Anfitriao não encontrado";
            return false;
        }

        $update = db()->update('anfitriao', array('status' => '1'), 'id_anfitriao = :id_anfitriao', array('id_anfitriao' => $this->modulo->getId()));
        input()->setSession('logado-confirmacao', $this->modulo->getId());
    }

    public function _sendNewConf() {

        $email = input()->_toPost('email', 'EMAIL');

        if ($email === NULL) {

            $this->error = true;
            $this->messageList[] = "E-mail não encontrado";
            return false;
        }

        $this->modulo = app()->loadModulo('anfitriao', array('email = :email AND status = 2', array('email' => $email)));

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Anfitriao não encontrado";
            return false;
        }

        if ($this->_sendConfirmacao($this->modulo) === true) {

            $this->messageList[] = "Enviamos um email para '" . strip_tags($this->modulo->getEmail()) . "' com a confirmação do seu cadastro de anfitrião.";
            $this->error = false;
            return false;
        }
    }

    /**
     * Enviar confirma de anfitriao
     * @param \Codando\Modulo\Anfitriao $anfitriao Anfitriao inativo
     * @return Boolean true Enviado, FALSO não enviado
     */
    public function _sendConfirmacao($anfitriao) {

        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === true) {

            $_config = \Codando\App::getConfig('emailsend');

            $emailArray = explode(',', $anfitriao->getEmail());

            do {
                $pro_hash = cryp()->enc($anfitriao->getEmail());
            } while (strrpos($pro_hash, '+') !== false);

            $emailFields = array(
                'Link para confirma' => COD_URL . 'confirmacao-email/' . $pro_hash
            );

            $html_email = tpl()->display('contato_send', array('titulo' => 'Está é uma confirmação do seu anfitrião realizado no site:' . COD_URL, 'emailFields' => $emailFields), false);

            $mailer = $this->swiftMailer();

            $message = \Swift_Message::newInstance('Confirmação de anfitrião')
                    ->setFrom(array($_config['return'] => 'Confirmação de anfitrião'))
                    ->setTo($emailArray)
                    ->addPart($html_email, 'text/html');

            if ($mailer->send($message)) {

                return true;
            }

            return false;
        }
    }

    public function _updateImagem($nome = 'fotoperfil', $ordem = 1) {

        $this->clearMessages();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false)
            return false;

        $dado_required = array();
        $dado_required[$nome] = 'FILETEMP';

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            //$this->messageList[] = "Selecione uma imagem.";
            //$this->error = true;
            return false;
        }

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->messageList[] = "Você não esta logado.";
            $this->error = true;
            return false;
        }

        $image = getimagesize(COD_DIR_APP . "/_arquivos/temp/" . $dado[$nome]);

        if ($image[0] > 5000 || $image[1] > 5000) {

            $this->error = true;
            $this->messageList[] = "Sua imagem deve ter a resolução menor que 5000 pixel";
            return false;
        }

        /* @var $arquivoTemp \Codando\Modulo\Arquivo */
        foreach ($this->modulo->getArquivoList() as $arquivoTemp) {

            if (strpos($arquivoTemp->getUrl(), $nome) != false) {

                $del = db()->delete('arquivo', ' id_modulo = 24 AND id_est = :anfitriao AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'anfitriao' => $this->modulo->getId()));

                if ($del === true) {
                    @unlink(COD_DIR_APP . "/_arquivos/" . $this->dir . "/" . $arquivoTemp->getUrl());
                }

                break;
            }
        }

        $upload = $this->_upload($dado[$nome], $nome, $ordem);

        if ($upload === false)
            return $upload;
    }

    public function _upload($fileTemp, $prefix, $ordem = 1) {

        $ext = array('jpeg', 'jpg', 'png');

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Anfitriao não encontrado";
            return false;
        }

        if ($fileTemp === NULL || file_exists(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp) === false) {

            $this->error = true;
            //$this->messageList[] = "Arquivo temporario não encontrado.";
            return false;
        }

        $pathinfor = pathinfo(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp);

        if (is_array($pathinfor) === false || isset($pathinfor['extension']) === false) {
            $this->error = true;
            //$this->messageList[] = "Arquivo temporario corrompido.";
            return false;
        }

        $extRe = $pathinfor['extension'];
        $filenameRe = $prefix . md5(uniqid()) . date('YmdHis');

        if (in_array(strtolower($extRe), $ext) === false) {

            $this->error = true;
            $this->messageList[] = "Arquivo '" . htmlentities($ext) . "' não permitido";
            return false;
        }

        if (strrpos($filenameRe, $this->dir) === false) {

            $filenameRe = $this->dir . "-" . $filenameRe;
        }

        while (file_exists(COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe) === true) {
            $filenameRe .= rand(10, 999);
        }

        $new_arquivo = array(
            'url' => $filenameRe . '.' . $extRe,
            'id_est' => $this->modulo->getId(),
            'id_modulo' => 24, //Anfitriao
            'ordem' => $ordem
        );

        if (rename(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp, COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe)) {

            $id_arquivo = db()->insert('arquivo', $new_arquivo);

            return $id_arquivo;
        }

        return false;
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {

        $this->feedback = array();
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
