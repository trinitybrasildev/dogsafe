<?php

namespace Codando\Controller;

/**
 *  E-mail: luizz@luizz.com.br
 * Copyright 2016 - Todos os direitos compromissodos
 */
class Compromisso extends Controller {

    public function _valid() {

        $this->clearMessages();

        $dado_required = array(
            'titulo' => 'xss%s',
            'data' => 'DATE',
            'horainicio' => '%s',
            'horafim' => '%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*)';
            return false;
        }

        $dado['id_anfitriao'] = input()->getSession('logadoanfitriao');

        if ($dado['id_anfitriao'] < 1) {
            $this->error = TRUE;
            $this->messageList[] = "Você tem que está logado como anfitrião.";
            return false;
        }

        return $dado;
    }

    public function _insert() {

        $dado = $this->_valid();

        if (is_array($dado) === false) {
            return false;
        }

        //Inserir compromisso
        $id_cadastro = db()->insert('compromisso', $dado);

        $this->_load($id_cadastro);

        if (is_instanceof('Codando\Modulo\Compromisso', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Compromisso não inserido.';
            return false;
        }

        //Mensagem de retorno
        $this->messageList[] = "Compromisso cadastrado com sucesso";
        $this->error = false;

        return true;
    }

    public function _excluir() {

        $cadastro = (int) input()->getSession('logadoanfitriao');

        if (is_instanceof('Codando\Modulo\Compromisso', $this->modulo) === false) {
            $this->error = true;
            $this->messageList[] = 'Compromisso não encontrado';
            return false;
        }

        db()->delete('compromisso', ' id_compromisso = :id AND id_anfitriao = :anf ', array('anf' => $cadastro,'id' => $this->modulo->getId()));
        
    }
    
    public function _listCalendario($order = 'data DESC, id_compromisso DESC', $where = array()) {

        $parsWhere = array();

        $mes = input()->_toGet('mes', '%d', false);
        $ano = input()->_toGet('ano', '%d', false);

        //$exdata = (array) explode('-', $data);
        $id_anfitriao = input()->_toGet('anfitriao', '%d', false);

        if (is()->id($id_anfitriao) === false) {
            return;
        }

        if (($mes >= 1 && $mes <= 12) && $ano >= date('Y')) {

            $mes = sprintf('%02d', $mes);

            $parsWhere['data'] = $mes . '-' . $ano;
            $where[] = " ( DATE_FORMAT(data,'%m-%Y') = :data ) ";

        }

        $parsWhere['id_anfitriao'] = (int) $id_anfitriao;
        $where[] = '( id_anfitriao = :id_anfitriao )';

        //$where[] = '( status <> 6 )';


        $this->findWhere("compromisso", $where, $parsWhere, $order);
    }
    
    protected function _list($order = 'id_compromisso DESC', $where = array()) {

        $parsWhere = array();

        $busca = input()->_toGet('q', 'xss%s', FALSE);

        if (is()->nul($busca) === FALSE) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) )';
        }

        $parsWhere['id_anfitriao'] = input()->getSession('logadoanfitriao');
        $where[] = '( id_anfitriao = :id_anfitriao )';

        $this->findWhere("compromisso", $where, $parsWhere, $order);
    }

    protected function _load($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toRequest('id_compromisso', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Compromisso invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('compromisso', array('id_compromisso = :id_compromisso', array('id_compromisso' => $not_id)));

        if (is_instanceof('Codando\Modulo\Compromisso', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Compromisso não encontrada";
        }
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
