<?php

namespace Codando\Controller;

/**
 * 	E-mail: luizz@luizz.com.br
 * 	Copyright 2016 - Todos os direitos reservados
 */
class Find extends Controller {

    protected function _deleteContato() {
        $contatos = isset($_POST['contato']) ? (array) $_POST['contato'] : array();

        foreach ($contatos as $k => $contato) {
            db()->delete('contato', 'id_contato = :id', ['id' => $k]);
        }

        $this->error = false;

        $this->messageList[] = 'Contato(s) removido(s) com sucesso.';

        return TRUE;
    }

    protected function _deleteCadastro() {
        $cadastros = isset($_POST['cadastro']) ? (array) $_POST['cadastro'] : array();

        $id = array_keys($cadastros)[0];
        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = app()->loadModulo('cadastro', ['id_cadastro = :id AND cpf = :cpf', ['id' => $id, 'cpf' => $cadastros[$id]]]);

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === FALSE) {
            $this->error = true;
            $this->messageList[] = 'Não foi possível deletar o cadastro.';
            return FALSE;
        }
        /* @var $arquivoTemp \Codando\Modulo\Arquivo */
        foreach ($cadastro->getArquivoList() as $arquivoTemp) {
            db()->delete('arquivo', ' id_modulo = 30 AND id_est = :idEst AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'idEst' => $cadastro->getId()));

            @unlink(COD_DIR_APP . "/_arquivos/cadastro/" . $arquivoTemp->getUrl());
        }

        $where = 'id_cadastro = :id';

        $parsWhere = ['id' => $cadastro->getId()];

        $depoimentos = app()->listModulo('depoimento', [$where, $parsWhere]);

        /* @var $pet \Codando\Modulo\Pet */
        foreach ($depoimentos as $depoimento) {
            /* @var $arquivoTemp \Codando\Modulo\Arquivo */
            foreach ($depoimento->getArquivoList() as $arquivoTemp) {
                db()->delete('arquivo', ' id_modulo = 29 AND id_est = :idEst AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'idEst' => $depoimento->getId()));

                @unlink(COD_DIR_APP . "/_arquivos/depoimento/" . $arquivoTemp->getUrl());
            }
        }

        $pets = app()->listModulo('pet', [$where, $parsWhere]);
        /* @var $pet \Codando\Modulo\Pet */
        foreach ($pets as $pet) {
            /* @var $arquivoTemp \Codando\Modulo\Arquivo */
            foreach ($pets->getArquivoList() as $arquivoTemp) {
                db()->delete('arquivo', ' id_modulo = 31 AND id_est = :idEst AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'idEst' => $pet->getId()));

                @unlink(COD_DIR_APP . "/_arquivos/pet/" . $arquivoTemp->getUrl());
            }
        }
        //Delete Chat
        db()->delete('chat', $where, $parsWhere);
        //Delete Comentarioanfitriao
        db()->delete('comentarioanfitriao', $where, $parsWhere);
        //Delete Reserva
        db()->delete('reserva', $where, $parsWhere);
        //Visita
        db()->delete('visita', $where, $parsWhere);
        //Delete Depoimento
        db()->delete('depoimento', $where, $parsWhere);
        //Delete Pet
        db()->delete('pet', $where, $parsWhere);
        //Delete Cadastro
        db()->delete('cadastro', $where, $parsWhere);

        $this->error = false;

        $this->messageList[] = 'Cadastro removido com sucesso.';

        return TRUE;
    }

    protected function _deleteAnfitriao() {
        $cadastros = isset($_POST['anfitriao']) ? (array) $_POST['anfitriao'] : array();

        $id = array_keys($cadastros)[0];
        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = app()->loadModulo('anfitriao', ['id_anfitriao = :id AND cpf = :cpf', ['id' => $id, 'cpf' => $cadastros[$id]]]);
        
        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === FALSE) {
            $this->error = true;
            $this->messageList[] = 'Não foi possível deletar o cadastro.';
            return FALSE;
        }
        /* @var $arquivoTemp \Codando\Modulo\Arquivo */
        foreach ($anfitriao->getArquivoList() as $arquivoTemp) {
            db()->delete('arquivo', ' id_modulo = 24 AND id_est = :idEst AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'idEst' => $anfitriao->getId()));

            @unlink(COD_DIR_APP . "/_arquivos/anfitriao/" . $arquivoTemp->getUrl());
        }

        $where = 'id_anfitriao = :id';

        $parsWhere = ['id' => $anfitriao->getId()];

        $depoimentos = app()->listModulo('depoimento', [$where, $parsWhere]);

        /* @var $pet \Codando\Modulo\Pet */
        foreach ($depoimentos as $depoimento) {
            /* @var $arquivoTemp \Codando\Modulo\Arquivo */
            foreach ($depoimento->getArquivoList() as $arquivoTemp) {
                db()->delete('arquivo', ' id_modulo = 29 AND id_est = :idEst AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'idEst' => $depoimento->getId()));

                @unlink(COD_DIR_APP . "/_arquivos/depoimento/" . $arquivoTemp->getUrl());
            }
        }

        //Delete anfitriaomodalidade
        db()->delete('anfitriaomodalidade', $where, $parsWhere);
        //Delete anfitriaotamanhopet
        db()->delete('anfitriaotamanhopet', $where, $parsWhere);
        //Delete chat
        db()->delete('chat', $where, $parsWhere);
        //Delete comentarioanfitriao
        db()->delete('comentarioanfitriao', $where, $parsWhere);
        //Delete compromisso
        db()->delete('compromisso', $where, $parsWhere);
        //Delete depoimento
        db()->delete('depoimento', $where, $parsWhere);
        //Delete petanfitriao
        db()->delete('petanfitriao', $where, $parsWhere);
        //Delete reserva
        db()->delete('reserva', $where, $parsWhere);
        //Delete visita
        db()->delete('visita', $where, $parsWhere);
        //Delete anfitriao
        db()->delete('anfitriao', $where, $parsWhere);

        $this->error = false;

        $this->messageList[] = 'Contato(s) removido(s) com sucesso.';

        return TRUE;
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
