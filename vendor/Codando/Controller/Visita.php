<?php

namespace Codando\Controller;

/**
 *  E-mail: luizz@luizz.com.br
 * Copyright 2016 - Todos os direitos visitados
 */
class Visita extends Controller {

    public function _valid() {

        $this->clearMessages();

        $dado_required = array(
            'data' => 'DATE'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*)';
            return false;
        }

        if (input()->getSession('logadocadastro') < 0) {
            $this->error = true;
            $this->messageList[] = 'Realize o login como hospede.';
            return false;
        }

        $dado['id_cadastro'] = input()->getSession('logadocadastro');

        if ($dado['id_cadastro'] < 1) {
            $this->error = TRUE;
            $this->messageList[] = "Você tem que esta logado como hospede.";
            return false;
        }

        return $dado;
    }

    public function _insert($anfitriao) {

        $dado = $this->_valid();
        $dado['id_anfitriao'] = $anfitriao->getId();

        if (is_array($dado) === false) {
            return false;
        }

        //Inserir visita
        $id_visita = db()->insert('visita', $dado);

        $this->_load($id_visita);

        if (is_instanceof('Codando\Modulo\Visita', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Visita não agendada.';
            return false;
        }

        //Mensagem de retorno
        $this->messageList[] = "Visita agendada com sucesso, aguarde ...";
        $this->error = false;


        //$this->messageList[] = 'Cadastro salvo com sucesso';
        //$this->error = false;
        //$this->_email();

        $this->_send($anfitriao);

        return true;
    }

    protected function _load($not_id = NULL) {

        $not_id = $not_id !== NULL ? $not_id : input()->_toRequest('id_visita', '%d', true);

        if (is()->id($not_id) == FALSE) {

            $this->error = true;
            $this->messageList[] = "Visita invalida";
            return FALSE;
        }

        $this->modulo = app()->loadModulo('visita', array('id_visita = :id_visita AND status = 2 ', array('id_visita' => $not_id)));

        if (is_instanceof('Codando\Modulo\Visita', $this->modulo) === FALSE) {

            $this->error = true;
            $this->messageList[] = "Visita não encontrada";
        }
    }

    private function _send($anfitriao) {

        $_config = \Codando\App::getConfig('emailsend');

        // Create the Transport
        $transport = \Swift_SmtpTransport::newInstance($_config['smtp'], $_config['port'], 'tls')
                ->setUsername($_config['email'])
                ->setPassword($_config['senha']);

        /* @var $visita \Codando\Modulo\Visita */
        $visita = $this->modulo;

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $emailArray = array($anfitriao->getEmail());

        $emailFields = array(
            'Nome' => $visita->getCadastro()->getNome(),
            'Email' => $visita->getCadastro()->getEmail(),
        );

        $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields), false, false);

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance('Dog Safe - Visita solicitada')
                ->setFrom(array($_config['return'] => 'SITE'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        }

        return false;
    }

    protected function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
