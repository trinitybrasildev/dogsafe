<?php

namespace Codando\System;

/**
 * Description of Benchmark
 * @author luizzcombr
 */
class Benchmark {

    private static $marks = array();

    public static function add($name) {

        self::$marks[$name] = array(
            'memory' => memory_get_usage(),
            'seconds' => microtime(true)
        );
    }

    public static function stop($name) {
        self::$marks[$name] = array(
            'memory' => (memory_get_usage() - self::$marks[$name]['memory']) / (1024 * 1024),
            'seconds' => microtime(true) - self::$marks[$name]['seconds']
        );
    }

    public static function show($name) {
        print_r(self::$marks[$name]);
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
