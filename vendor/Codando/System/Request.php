<?php

namespace Codando\System;

/**
 * Description of request
 * @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br> 
 * @copyright 2016 Luizz 
 * @license http://www.luiz.com.br 
 * @version 2.1 
 */
class Request {

    private static $instance = NULL;
    private $msg = array();
    private $erro = array();
    private $nameInstance = 'temp';
    public $break;

    public function __construct($name = 'temp') {

        $this->nameInstance = $name;

        self::$instance = $this;
    }

    public function _toRequest($array, $opt = 'xss%s', $valid = true, $feedback = array(), $prefix = NULL) {

        $post = $this->_toPost($array, $opt, $valid, $feedback, $prefix);

        if ($post === NULL) {
            return $this->_toGet($array, $opt, $valid, $feedback, $prefix);
        }

        return $post;
    }

    public function _toPost($array, $opt = 'xss%s', $valid = true, $feedback = array(), $prefix = NULL) {

        $r = NULL;

        if (is_array($array)) {

            foreach ($array as $k => $v) {

                $name = $prefix !== NULL ? str_replace($prefix, '', $k) : $k;

                $r[$name] = isset($_POST[$k]) ? $this->sprint_f($v, $_POST[$k]) : NULL;

                if ($valid === true) {

                    if (( empty($_POST[$k]) || is_null($r[$name]))) {

                        $this->erro = true;

                        if (empty($_POST[$k])) {

                            $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' campo não informado!';
                        } else {

                            $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' invalido!';
                        }

                        return NULL;
                    }
                }
            }
        } else {

            $name = $prefix !== NULL ? str_replace($prefix, '', $array) : $array;

            $r = isset($_POST[$array]) ? $this->sprint_f($opt, $_POST[$array]) : NULL;

            if (( empty($_POST[$array]) || is_null($r) ) && $valid === true) {

                $this->erro = true;

                if (empty($_POST[$array])) {

                    $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' campo não informado!';
                } else {

                    $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' invalido!';
                }

                return NULL;
            }
        }

        return $r;
    }

    public function _toGet($array, $opt = 'xss%s', $valid = true, $feedback = array(), $prefix = NULL) {

        $r = NULL;

        if (is_array($array)) {

            foreach ($array as $k => $v) {

                $name = $prefix !== NULL ? str_replace($prefix, '', $k) : $k;

                $r[$name] = isset($_GET[$k]) ? $this->sprint_f($v, $_GET[$k]) : NULL;

                if ($valid === true) {
                    if (( empty($_GET[$k]) || is_null($r[$name]))) {
                        $this->erro = true;
                        if (empty($_GET[$k])) {
                            $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' campo não informado!';
                        } else {
                            $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' invalido!';
                        }
                        return NULL;
                    }
                }
            }
        } else {
            $name = $prefix !== NULL ? str_replace($prefix, '', $array) : $array;

            $r = isset($_GET[$array]) ? $this->sprint_f($opt, $_GET[$array]) : NULL;

            if (( empty($_GET[$array]) || is_null($r) ) && $valid === true) {
                $this->erro = true;
                if (empty($_GET[$array])) {
                    $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' campo não informado!';
                } else {
                    $this->msg = array_key_exists($name, $feedback) ? $feedback[$name] : ucfirst($name) . ' invalido!';
                }
                return NULL;
            }
        }
        return $r;
    }

    public function getSession($key, $sprint = '%s') {

        $value = NULL;

        if (isset($_SESSION[COD_SESSIONAME][$key]) === TRUE) {
            $value = $_SESSION[COD_SESSIONAME][$key];
        }

        return $value;
    }

    public function setSession($key, $value) {

        $_SESSION[COD_SESSIONAME][$key] = $value;
    }

    public function unsetSession($key) {

        if ($_SESSION[COD_SESSIONAME][$key]) {
            $_SESSION[COD_SESSIONAME][$key] = NULL;
        }
    }

    private function sprint_f($format, $arg) {

        $xss = explode("%", $format);
        if (count($xss) == 2 && current($xss) == 'xss') {
            $arg = xss()->clean($arg);
            $format = "%" . end($xss);
        }

        if (is_array($arg)) {
            while (list($key) = each($arg)) {
                $arg[$key] = $this->sprint_f($format, $arg[$key]);
            }
            return $arg;
        }

        $return = NULL;

        switch ($format) {
            case 'EMAIL':
                return (filter_var($arg, FILTER_VALIDATE_EMAIL) === FALSE ) ? NULL : $arg;
                break;
            case 'URL':
                return (filter_var($arg, FILTER_VALIDATE_URL) === FALSE ) ? NULL : $arg;
                break;
            case 'IP':
                return (filter_var($arg, FILTER_VALIDATE_IP) === FALSE ) ? NULL : $arg;
                break;
            case 'DATE':

                $time = explode(" ", $arg);
                if (count($time) == 2)
                    $arg = $time[0];

                $arg_r = implode('-', array_reverse(explode('/', $arg)));

                return ( is()->date($arg) === true ? $arg_r . ((count($time) == 2) ? " " . $time[1] : NULL) : NULL);

                break;
            case 'DATETIME':
                $ex_data = explode(" ", $arg);
                $time = date('H:i');
                $data = current($ex_data);
                if (count($ex_data) == 2)
                    $time = end($ex_data);
                unset($ex_data);

                $date = implode('-', array_reverse(explode('/', $data)));

                return ( is()->date($data) === true ? $date . " " . $time : NULL);

                break;
            case 'HASH':
                return is()->nul($arg) ? NULL : hash(HASH_SENHA, trim($arg));
                break;
            case 'senha':
                return is()->nul($arg) ? NULL : md5(trim($arg));
                break;
            case 'cpf':
                return (is()->nul($arg) === false && is()->cpf($arg) === true) ? $arg : NULL;
                break;
            case 'nome':
                $arg = sprintf('%s', $arg);
                return (strlen(trim($arg)) < 3 ? NULL : $arg);
                break;
            case 'youtube':
                return $this->urlYoutube($arg) ? $arg : NULL;
                break;
            case 'FILETEMP':
                return file_exists(COD_DIR_APP . "/_arquivos/temp/" . $arg) === FALSE ? NULL : $arg;
                break;
            default:
                return sprintf($format, $arg);
        }
        return $return;
    }

    private function urlYoutube($url) {

        if (!preg_match('/http:\/\/(?:www\.)?youtube.*watch\?v=([a-zA-Z0-9\-_]+)/', $url)) {
            return false;
        } else
            return true;
    }

    public function getMsg() {
        return $this->msg;
    }

    public function isErro() {
        return $this->erro;
    }

    public static function get_request() {

        static $instance = null;

        return $instance ?: $instance = new static;
    }

}
