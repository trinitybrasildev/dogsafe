<?php

class Describe {

    private static $instance;
    private $describe = array();
    private $reg = '/\{(?:[^{}]|(?R))*\}/';

    public function getLabel($describe) {
        $label = $describe->comment != NULL ? $describe->comment : str_replace('_', ' ', ucfirst(strtolower($describe->field)));

        $label = preg_replace($this->reg, '', $label);

        return utf8_encode($label);
    }

    public function isJson($describe) {

        if ($describe->comment !== NULL) {
            $matches = array();
            preg_match($this->reg, $describe->comment, $matches);
            return count($matches) > 0 ? TRUE : FALSE;
        }
        return FALSE;
    }

    public function getJson($describe) {

        $matches = array();

        preg_match($this->reg, $describe->comment, $matches);

        return (array) json_decode(current($matches), true);
    }

    public function getJsonOptions($describe, $value = NULL) {

        $json = $this->getJson($describe);

        $option = array();

        foreach ($json as $key => $val) {

            $option[] = '<option value="' . $json[$key] . '" ' . ($value == $json[$key] ? 'selected' : NULL) . '>' . ($key) . '</option>';
        }

        return implode('', $option);
    }

    public function createClass($modulo) {

        $clssName = $modulo->getClasseNome();

        if (file_exists(COD_DIR_VENDOR . "/vendor/Codando/Modulo/" . $clssName . ".php") === FALSE) {

            $sourceClassTemp = "<?php";
            $sourceClassTemp .= "\n/**";
            $sourceClassTemp .= "\n* Classe que representa objeto " . $clssName . " ";
            $sourceClassTemp .= "\n* /";
            $sourceClassTemp .= "\n* @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>";
            $sourceClassTemp .= "\n* @copyright  Luizz";
            $sourceClassTemp .= "\n* @license www.luiz.com.br";
            $sourceClassTemp .= "\n* @package Codando";
            $sourceClassTemp .= "\n*/";
            $sourceClassTemp .= "\n";
            $sourceClassTemp .= "\tnamespace Codando\Modulo;\n";
            $sourceClassTemp .= "\n";
            $sourceClassTemp .= "\tclass " . $clssName . "{\n";
            $atributosClass = array();
            $getClass = array();
            $setClass = array();
            $describeListTemp = (object) $this->tableDescribeToObjeto($modulo->getTabela(true));
            $describe = array();
            $toString = 'return NULL;';

            foreach ($describeListTemp as $describeTemp) {

                $ex = explode("_", $describeTemp->field);

                $nomeMetodo = $this->toGetMethod($describeTemp->field, '');

                $explodeType = explode('(', $describeTemp->type);
                $describeTemp->type = reset($explodeType);
                $explodeType = NULL;
                unset($explodeType);

                if ($describeTemp->is_key == "PRI") {

                    if ($describeTemp->type == 'int') {

                        $atributosClass[] = "\t\n\tprivate $" . $describeTemp->field . ";\n";
                        $getClass[] = "\t\n\tpublic function getId(){\n\t\treturn (int)\$this->" . $describeTemp->field . ";\n\t}";
                        $setClass[] = "\t\n\tpublic function setId(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = (int)\$" . $describeTemp->field . ";\n\t}";
                        $toString = "\n\t\treturn (string)\$this->" . $describeTemp->field . ";\n\t";
                    }
                } else if ($describeTemp->is_key == "MUL") {
                    $mul_modulo = end($ex);
                    $atributosClass[] = "\t\n\tprivate $" . $describeTemp->field . ";\n";
                    $nomeMetodo = ucwords($mul_modulo);
                    $mul_modulo = NULL;
                    unset($mul_modulo);

                    if ($describeTemp->type == 'varchar') {

                        $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(){\n\t\treturn \$this->" . $describeTemp->field . ";\n\t}";
                        $setClass[] = "\t\n\tpublic function set" . $nomeMetodo . "(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = \$" . $describeTemp->field . ";\n\t}";
                    } else {

                        $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(){\n\t\treturn (int)\$this->" . $describeTemp->field . ";\n\t}";
                        $setClass[] = "\t\n\tpublic function set" . $nomeMetodo . "(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = (int)\$" . $describeTemp->field . ";\n\t}";
                    }
                } else {

                    switch ($describeTemp->type) {
                        case 'date':
                            $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(\$format='d/m/Y'){\n\t\treturn \$this->" . $describeTemp->field . " !== NULL ? date(\$format,strtotime(\$this->" . $describeTemp->field . ")):NULL;\n\t}";
                            break;
                        case 'datetime':
                            $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(\$format='d/m/Y H:i'){\n\t\treturn \$this->" . $describeTemp->field . " !== NULL ? date(\$format,strtotime(\$this->" . $describeTemp->field . ")):NULL;\n\t}";
                            break;
                        default:
                            $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(){\n\t\treturn \$this->" . $describeTemp->field . ";\n\t}";
                            break;
                    }

                    $atributosClass[] = "\t\n\tprivate $" . $describeTemp->field . ";\n";
                    $setClass[] = "\t\n\tpublic function set" . $nomeMetodo . "(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = \$" . $describeTemp->field . ";\n\t}";
                }


                $describe[] = serialize(array(
                    'field' => $describeTemp->field,
                    'is_key' => $describeTemp->is_key,
                    'is_null' => $describeTemp->is_null,
                    'type' => $describeTemp->type,
                    'comment' => $describeTemp->comment,
                    'max_length' => $describeTemp->max_length,
                    'default' => $describeTemp->defaults));
            }

            $describeListTemp = NULL;

            unset($describeListTemp);

            if ($modulo->getArquivo() == 1) {

                if ($modulo->getArquivoQuantidade() > 1) {

                    $atributosClass[] = "\t\n\tprivate \$arquivoList = array();\n";
                    $getClass[] = "\t\n\tpublic function getArquivoList(){\n\t\treturn \$this->arquivoList;\n\t}";
                    $getClass[] = "\t\n\tpublic function getArquivo(){\n\t\treturn current(\$this->arquivoList);\n\t}";
                    $setClass[] = "\t\n\tpublic function setArquivoList(\$arquivo){\n\t\t\$this->arquivoList = \$arquivo;\n\t}";
                    $setClass[] = "\t\n\tpublic function setArquivoAdd(Arquivo \$arquivo){\n\t\t\$this->arquivoList[] = \$arquivo;\n\t}";
                } else {

                    $atributosClass[] = "\t\n\tprivate \$arquivo;\n";
                    $getClass[] = "\t\n\tpublic function getArquivo(){\n\t\treturn \$this->arquivo;\n\t}";
                    $setClass[] = "\t\n\tpublic function setArquivo(Arquivo \$arquivo){\n\t\t\$this->arquivo = \$arquivo;\n\t}";
                }
            }

            //$atributosClass[] = "\t\n\tprivate \$decribe = array(\n\t'".implode("',\n\t'",$describe)."'\n\t);\n";
            //$getClass[] = "\t\n\tpublic function getDescribe(){\n\t\treturn \$this->decribe;\n\t}";

            $setClass[] = "\t\n\tpublic function isEquals(\$isEqual) {return (\$isEqual instanceof " . $clssName . " && \$this->getId() == \$isEqual->getId());}";
            $setClass[] = "\t\n\tpublic function getObjectVars(){ return get_object_vars(\$this); }";
            $setClass[] = "\t\n\tpublic function __toString(){" . $toString . "}";
            $setClass[] = "\t\n\tpublic function __construct(){}";
            $setClass[] = "\t\n\tpublic function __destruct(){unset(\$this);}";

            $sourceClassTemp .= implode("", $atributosClass) . implode("", $getClass) . implode("", $setClass) . "\n};\n?>";

            try {

                if (file_exists(COD_DIR_VENDOR . "/vendor/Codando/Modulo/" . $clssName . ".php") === FALSE) {

                    $file = fopen(COD_DIR_VENDOR . "/vendor/Codando/Modulo/" . $clssName . ".php", "w+");

                    if (!fwrite($file, ($sourceClassTemp))) {

                        return "++ ERRO: Ocorreu uma falha ao gravar o arquivo";
                    }

                    fclose($file);

                    unset($file);
                }
            } catch (Exception $e) {

                return $e->getMessage();
            }
        } 

        return TRUE;
    }

    public function toGetMethod($extract, $type = 'get', $sep = "") {
        return $type . ucwords(str_replace(array("id_", "_"), array("", $sep), $extract));
    }

    public function isFieldsHidden($describe) {

        return ($describe->is_key == 'PRI' || $describe->is_key == 'MUL' || substr($describe->field, 0, 3) == 'url' || substr($describe->field, 0, 2) == 'id');
    }

    public function isFieldsForeign($describe) {

        return ($describe->is_key != 'PRI' && $describe->is_key == 'MUL' && substr($describe->field, 0, 2) == 'id');
    }

    public function getIdAndUnset($array, $mod = NULL) {

        $newArray = array();
        if ($mod === NULL)
            $mod = $this->getModulo();

        $pri = $this->describeByFieldPrimariKey($mod->getTabela(true));

        $newArray[$pri] = $array[$pri];

        unset($array[$pri]);

        $is = is();

        foreach ($array as $k => $value) {

            if ($is->nul($value) === true) {
                $array[$k] = NULL;
                unset($array[$k]);
            }
        }
        $is = NULL;
        unset($is);

        return (object) array('fields' => $array, 'where' => $newArray);
    }

    public function tableDescribeToObjeto($table_name, $foreign = FALSE) {

        $this->clearAll();

        $table = $foreign !== FALSE ? $table_name . "_foreign" : $table_name;

        if (array_key_exists($table, $this->describe) === TRUE) {

            return $this->describe[$table];
        } else {

            $foreign_sql = "SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = '" . $table_name . "';";

            $describe_sql = "SELECT column_name AS field, is_nullable AS is_null, data_type AS type, character_maximum_length AS max_length,  column_key AS is_key,  column_default AS defaults, column_comment AS comment FROM  INFORMATION_SCHEMA.Columns WHERE TABLE_SCHEMA = '" . $this->getDatabase() . "' AND table_name = '" . $table_name . "'";

            $this->queryType = true;

            $queryF = NULL;

            $this->sql = $describe_sql;
            $queryD = $this->exeQuery();

            if ($foreign !== FALSE) {
                $this->sql = $foreign_sql;
                $queryF = $this->exeQuery();
            }

            $describeTemp = NULL;

            if ($this->erro === FALSE) {

                try {

                    $describeTemp = $queryD->fetchAll(PDO::FETCH_OBJ);

                    if ($foreign !== FALSE) {
                        $describeTemp->foreign = $queryF->fetchAll(PDO::FETCH_OBJ);
                    }
                } catch (PDOException $e) {
                    erro($e);
                    $this->erro = true;
                    $this->msg['DESCRIBE'] = $e->getMessage();
                    return NULL;
                }
                //Cache form_admin
                $this->describe[$table] = $describeTemp;
            }

            unset($queryD, $queryF);
        }

        return $this->describe[$table];
    }

    protected function describeDepend($table, $referencia = NULL) {

        $return = array();

        $this->clearAll();

        try {

            $this->queryType = true;
            $this->sql = "SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = '" . $table . "';";
            $query = $this->exeQuery();
            $describe = $query->fetchAll(PDO::FETCH_OBJ);

            unset($query);

        } catch (PDOException $e) {

            $this->erro = true;
            $this->msg['DESCRIBE'] = $e->getMessage();
            return NULL;
        }

        foreach ($describe as $foreign) {


            $modulo_temp = new Modulo();
            $modulo_temp->setClasseNome('Modulo');
            $modulo_temp->setTabela('modulo');

            $moduloTemp = $this->_load($modulo_temp, array('tabela' => $foreign->table_name));

            $modulo_temp = NULL;
            unset($modulo_temp);

            if ($moduloTemp instanceof Modulo == TRUE && $moduloTemp->getVisibilidade() >= 3) {

                $moduloList = array();

                if ($referencia !== NULL) {
                    $moduloList = (array) $this->listModulo($moduloTemp, array($foreign->column_name => $referencia->getId()));
                }

                $return[$foreign->table_name] = array(
                    $moduloTemp,
                    $moduloList
                );
            }
        }

        return $return;
    }

    protected function describeByFieldByDefault($table, $type = 1) {

        $fields = array();

        $describe = $this->tableDescribeToObjeto($table);

        if ($describe != NULL && is_array($describe)) {
            foreach ($describe as $describeTemp) {
                $fields[$describeTemp->field] = $describeTemp->defaults;
            }
        }

        if ($type == 1) {
            return ((object) $fields);
        } else {
            return $fields;
        }
    }

    protected function describeByFieldByComments($table, $type = 1) {
        $fields = array();

        $describe = $this->tableDescribeToObjeto($table);

        if ($describe != NULL && is_array($describe)) {
            foreach ($describe as $describeTemp) {
                $fields[$describeTemp->field] = utf8_encode($describeTemp->comment);
            }
        }
        if ($type == 1)
            return ((object) $fields);
        else
            return $fields;
    }

    protected function describeByFieldPrimariKey($table) {
        $fields = NULL;

        $describe = $this->tableDescribeToObjeto($table);

        if ($describe != NULL && is_array($describe)) {
            foreach ($describe as $describeTemp) {
                if ($describeTemp->is_key == 'PRI')
                    $fields = $describeTemp->field;
            }
        }
        return $fields;
    }

    public static function getInstace() {

        if (self::$instance instanceof Describe === FALSE) {

            self::$instance = new Describe();
        }

        return self::$instance;
    }

}
