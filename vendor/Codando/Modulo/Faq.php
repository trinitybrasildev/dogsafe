<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Faq 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Faq {

    private $id_faq;
    private $tipo;
    private $pergunta;
    private $resposta;

    public function getId() {
        return (int) $this->id_faq;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getPergunta() {
        return $this->pergunta;
    }

    public function getResposta() {
        return $this->resposta;
    }

    public function setId($id_faq) {
        $this->id_faq = (int) $id_faq;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function setPergunta($pergunta) {
        $this->pergunta = $pergunta;
    }

    public function setResposta($resposta) {
        $this->resposta = $resposta;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Faq && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_faq;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
