<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Pet 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Pet {

    private $id_pet;
    private $nome;
    private $datanasc;
    private $idade;
    private $raca;
    private $id_tamanhopet;
    private $castrado;
    private $sexo;
    private $planodesaude;
    private $alergia;
    private $doenca;
    private $medicacao;
    private $reqcuidado;
    private $carteira;
    private $rapaticidapulga;
    private $regleishmaniose;
    private $sobre;
    private $id_cadastro;
    private $arquivoList = array();
    private $tipo;

    public function getId() {
        return (int) $this->id_pet;
    }

    public function getNome() {
        return $this->nome;
    }
    
    public function getIdade() {
        return $this->idade;
    }
    
    public function getTipo() {
        return $this->tipo;
    }

    public function getDatanasc($format = 'd/m/Y') {
        return $this->datanasc !== NULL ? date($format, strtotime($this->datanasc)) : NULL;
    }

    public function getRaca() {
        return $this->raca;
    }

    public function getTamanhopet() {
        return $this->id_tamanhopet;
    }

    public function getCastrado() {
        return $this->castrado;
    }

    public function getSexo() {
        return $this->sexo;
    }

    public function getPlanodesaude() {
        return $this->planodesaude;
    }

    public function getAlergia() {
        return $this->alergia;
    }

    public function getDoenca() {
        return $this->doenca;
    }

    public function getMedicacao() {
        return $this->medicacao;
    }

    public function getReqcuidado() {
        return $this->reqcuidado;
    }

    public function getCarteira() {
        return $this->carteira;
    }

    public function getRapaticidapulga() {
        return $this->rapaticidapulga;
    }

    public function getRegleishmaniose() {
        return $this->regleishmaniose;
    }

    public function getSobre() {
        return $this->sobre;
    }

    public function getCadastro() {
        return $this->id_cadastro;
    }

    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function getArquivo() {
        return current($this->arquivoList);
    }

    public function setId($id_pet) {
        $this->id_pet = (int) $id_pet;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setDatanasc($datanasc) {
        $this->datanasc = $datanasc;
    }

    public function setRaca($raca) {
        $this->raca = $raca;
    }

    public function setTamanhopet($id_tamanhopet) {
        $this->id_tamanhopet = $id_tamanhopet;
    }

    public function setCastrado($castrado) {
        $this->castrado = $castrado;
    }

    public function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    public function setPlanodesaude($planodesaude) {
        $this->planodesaude = $planodesaude;
    }

    public function setAlergia($alergia) {
        $this->alergia = $alergia;
    }

    public function setDoenca($doenca) {
        $this->doenca = $doenca;
    }

    public function setMedicacao($medicacao) {
        $this->medicacao = $medicacao;
    }

    public function setReqcuidado($reqcuidado) {
        $this->reqcuidado = $reqcuidado;
    }

    public function setCarteira($carteira) {
        $this->carteira = $carteira;
    }

    public function setRapaticidapulga($rapaticidapulga) {
        $this->rapaticidapulga = $rapaticidapulga;
    }

    public function setRegleishmaniose($regleishmaniose) {
        $this->regleishmaniose = $regleishmaniose;
    }

    public function setSobre($sobre) {
        $this->sobre = $sobre;
    }

    public function setCadastro($id_cadastro) {
        $this->id_cadastro = $id_cadastro;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Pet && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->nome;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
