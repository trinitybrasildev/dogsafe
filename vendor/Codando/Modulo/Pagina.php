<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Pagina 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Pagina {

    private $id_pagina;
    private $titulo;
    private $subtitulo;
    private $texto;
    private $arquivoList = array();

    public function getId() {
        return (int) $this->id_pagina;
    }

    public function getTitulo() {
        return $this->titulo;
    }
    
    public function getSubtitulo() {
        return $this->subtitulo;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function getArquivo() {
        return current($this->arquivoList);
    }

    public function setId($id_pagina) {
        $this->id_pagina = (int) $id_pagina;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Pagina && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_pagina;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}
