<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Video 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Video {

    private $id_video;
    private $titulo;
    private $url_titulo;
    private $texto;
    private $link;
    private $data;

    public function getId() {
        return (int) $this->id_video;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }
    
    public function getUrl() {
        return '/videos/'.$this->url_titulo.'-'.$this->id_video;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getLink() {
        return $this->link;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function setId($id_video) {
        $this->id_video = (int) $id_video;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Video && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_video;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}