<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Parceiro 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Parceiro {

    private $id_parceiro;
    private $titulo;
    private $url_titulo;
    private $ordem;
    private $link;
    private $texto;
    private $arquivo;

    public function getId() {
        return (int) $this->id_parceiro;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }

    public function getOrdem() {
        return $this->ordem;
    }

    public function getLink() {
        return $this->link;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getArquivo() {
        return $this->arquivo;
    }

    public function setId($id_parceiro) {
        $this->id_parceiro = (int) $id_parceiro;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setArquivo(Arquivo $arquivo) {
        $this->arquivo = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Parceiro && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_parceiro;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

;
?>