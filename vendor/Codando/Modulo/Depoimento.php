<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Depoimento 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Depoimento {

    private $id_depoimento;
    private $nome;
    private $data;
    private $texto;
    private $arquivo;

    public function getId() {
        return (int) $this->id_depoimento;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getArquivo() {
        return $this->arquivo;
    }

    public function setId($id_depoimento) {
        $this->id_depoimento = (int) $id_depoimento;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setArquivo(Arquivo $arquivo) {
        $this->arquivo = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Depoimento && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_depoimento;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

;
?>