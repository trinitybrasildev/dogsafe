<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Dica 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Dica {

    private $id_dica;
    private $titulo;
    private $url_titulo;
    private $subtitulo;
    private $data;
    private $status;
    private $autor;
    private $fonte;
    private $resumo;
    private $texto;
    private $arquivoList = array();

    public function getId() {
        return (int) $this->id_dica;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }

    public function getSubtitulo() {
        return $this->subtitulo;
    }

    public function getUrl() {
        return '/dicas/' . $this->url_titulo . '-' . $this->id_dica;
    }

    public function getData($format = 'd/m/Y H:i') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getAutor() {
        return $this->autor;
    }

    public function getFonte() {
        return $this->fonte;
    }

    public function getResumo() {
        return $this->resumo;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function getArquivo() {
        return current($this->arquivoList);
    }

    public function setId($id_dica) {
        $this->id_dica = (int) $id_dica;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function setSubtitulo($subtitulo) {
        $this->subtitulo = $subtitulo;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setAutor($autor) {
        $this->autor = $autor;
    }

    public function setFonte($fonte) {
        $this->fonte = $fonte;
    }

    public function setResumo($resumo) {
        $this->resumo = $resumo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Dica && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_dica;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

;
?>
