<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Visita 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Visita {

    private $id_visita;
    private $id_cadastro;
    private $id_anfitriao;
    private $data;
    private $status;

    public function getId() {
        return $this->id_visita;
    }

    public function getCadastro() {
        return $this->id_cadastro;
    }

    public function getAnfitriao() {
        return $this->id_anfitriao;
    }

    public function getData($format = 'd/m/Y H:i') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setId($id_visita) {
        $this->id_visita = $id_visita;
    }

    public function setCadastro($id_cadastro) {
        $this->id_cadastro = $id_cadastro;
    }

    public function setAnfitriao($id_anfitriao) {
        $this->id_anfitriao = $id_anfitriao;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Visita && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_visita;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

;
?>