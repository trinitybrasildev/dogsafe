<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Transferencia 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Transferencia {

    private $id_transferencia;
    private $valor;
    private $data;
    private $email;
    private $id_reserva;
    private $status;

    public function getId() {
        return (int) $this->id_transferencia;
    }

    public function getValor() {
        return $this->valor;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getReserva() {
        return $this->id_reserva;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setId($id_transferencia) {
        $this->id_transferencia = (int) $id_transferencia;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setReserva($id_reserva) {
        $this->id_reserva = $id_reserva;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Transferencia && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_transferencia;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}