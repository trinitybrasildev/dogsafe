<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Cidade 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Cidade {

    private $id_cidade;
    private $nome;
    private $id_estado;

    public function getId() {
        return $this->id_cidade;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEstado() {
        return $this->id_estado;
    }

    public function setId($id_cidade) {
        $this->id_cidade = $id_cidade;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEstado($id_estado) {
        $this->id_estado = $id_estado;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Cidade && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_estado->getSigla();
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

;
?>