<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Foto 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Foto {

    private $id_foto;
    private $titulo;
    private $url_titulo;
    private $date;
    private $descricao;
    private $arquivoList = array();

    public function getId() {
        return (int) $this->id_foto;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }
    
    public function getUrl() {
        return '/fotos/'.$this->url_titulo.'-'.$this->id_foto;
    }

    public function getDate($format = 'd/m/Y') {
        return $this->date !== NULL ? date($format, strtotime($this->date)) : NULL;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function getArquivo() {
        return current($this->arquivoList);
    }

    public function setId($id_foto) {
        $this->id_foto = (int) $id_foto;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Foto && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_foto;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}
