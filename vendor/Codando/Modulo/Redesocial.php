<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Redesocial 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Redesocial {

    private $id_redesocial;
    private $nome;
    private $link;

    public function getId() {
        return (int) $this->id_redesocial;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getLink() {
        return $this->link;
    }

    public function setId($id_redesocial) {
        $this->id_redesocial = (int) $id_redesocial;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Redesocial && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_redesocial;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

