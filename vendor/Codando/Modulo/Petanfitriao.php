<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Petanfitriao 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Petanfitriao {

    private $id_petanfitriao;
    private $id_tamanhopet;
    private $sexo;
    private $id_anfitriao;

    public function getId() {
        return (int) $this->id_petanfitriao;
    }

    public function getTamanhopet() {
        return $this->id_tamanhopet;
    }

    public function getSexo() {
        return $this->sexo;
    }

    public function getAnfitriao() {
        return $this->id_anfitriao;
    }

    public function setId($id_petanfitriao) {
        $this->id_petanfitriao = $id_petanfitriao;
    }

    public function setTamanhopet($id_tamanhopet) {
        $this->id_tamanhopet = $id_tamanhopet;
    }

    public function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    public function setAnfitriao($id_anfitriao) {
        $this->id_anfitriao =  $id_anfitriao;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Petanfitriao && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_petanfitriao;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
