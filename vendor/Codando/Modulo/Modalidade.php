<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Modalidade 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Modalidade {

    private $id_modalidade;
    private $titulo;
    private $url_titulo;

    public function getId() {
        return (int) $this->id_modalidade;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }

    public function setId($id_modalidade) {
        $this->id_modalidade = (int) $id_modalidade;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Modalidade && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->titulo;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
    
}