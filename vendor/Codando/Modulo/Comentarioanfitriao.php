<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Comentarioanfitriao 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Comentarioanfitriao {

    private $id_comentarioanfitriao;
    private $id_cadastro;
    private $id_anfitriao;
    private $texto;
    private $avaliacao;
    private $data;

    public function getId() {
        return (int) $this->id_comentarioanfitriao;
    }

    public function getCadastro() {
        return $this->id_cadastro;
    }

    public function getAnfitriao() {
        return $this->id_anfitriao;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getAvaliacao() {
        return $this->avaliacao;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function setId($id_comentarioanfitriao) {
        $this->id_comentarioanfitriao = (int) $id_comentarioanfitriao;
    }

    public function setCadastro($id_cadastro) {
        $this->id_cadastro = $id_cadastro;
    }

    public function setAnfitriao($id_anfitriao) {
        $this->id_anfitriao = $id_anfitriao;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setAvaliacao($avaliacao) {
        $this->avaliacao = $avaliacao;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Comentarioanfitriao && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_comentarioanfitriao;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}