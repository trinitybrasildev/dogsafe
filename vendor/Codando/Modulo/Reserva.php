<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Reserva 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Reserva {

    private $id_reserva;
    private $datainicio;
    private $datafim;
    private $horadeixar;
    private $horabuscar;
    private $id_anfitriao;
    private $id_cadastro;
    private $datacadastro;
    private $status;
    private $valor;
    private $pets;
    private $paypalurl;
    private $paypal;
    private $datapagamento;
    private $dataconfirmacao;
    private $datacancelada;
    private $payerid;
    private $transactionid;
    private $id_cancelamento;
    private $id_modalidade;
    private $valormodalidade;
    private $valorcancelado;
    private $valorreembolso;
    private $desconto;

    public function getId() {
        return (int) $this->id_reserva;
    }

    public function getCancelamento() {
        return $this->id_cancelamento;
    }

    public function getModalidade() {
        return $this->id_modalidade;
    }

    public function getDesconto() {
        return $this->desconto;
    }

    public function getDatainicio($format = 'd/m/Y') {
        return $this->datainicio !== NULL ? date($format, strtotime($this->datainicio)) : NULL;
    }

    public function getDatafim($format = 'd/m/Y') {
        return $this->datafim !== NULL ? date($format, strtotime($this->datafim)) : NULL;
    }

    public function getDatapagamento($format = 'd/m/Y') {
        return $this->datapagamento !== NULL ? date($format, strtotime($this->datapagamento)) : NULL;
    }

    public function getDatacancelada($format = 'd/m/Y') {
        return $this->datacancelada !== NULL ? date($format, strtotime($this->datacancelada)) : NULL;
    }

    public function getDataconfirmacao($format = 'd/m/Y') {
        return $this->dataconfirmacao !== NULL ? date($format, strtotime($this->dataconfirmacao)) : NULL;
    }

    public function getHoradeixar() {
        return $this->horadeixar;
    }

    public function getPayerid() {
        return $this->payerid;
    }

    public function getTransactionid() {
        return $this->transactionid;
    }

    public function getHorabuscar() {
        return $this->horabuscar;
    }

    public function getAnfitriao() {
        return $this->id_anfitriao;
    }

    public function getPaypalurl() {
        return $this->paypalurl;
    }

    public function getPaypal() {
        return $this->paypal;
    }

    public function getPets() {
        return $this->pets;
    }

    public function getCadastro() {
        return $this->id_cadastro;
    }

    public function getDatacadastro($format = 'd/m/Y') {
        return $this->datacadastro !== NULL ? date($format, strtotime($this->datacadastro)) : NULL;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getValor($format = true) {
        return $format === true ? number_format((float) $this->valor, 2, ",", ".") : $this->valor;
    }

    public function getValorreembolso($format = true) {
        return $format === true ? number_format((float) $this->valorreembolso, 2, ",", ".") : $this->valorreembolso;
    }

    public function getValorcancelado($format = true) {
        return $format === true ? number_format((float) $this->valorcancelado, 2, ",", ".") : $this->valorcancelado;
    }

    public function getValormodalidade($format = true) {
        return $format === true ? number_format((float) $this->valormodalidade, 2, ",", ".") : $this->valormodalidade;
    }

    public function setId($id_reserva) {
        $this->id_reserva = (int) $id_reserva;
    }

    public function setDesconto($desconto) {
        $this->desconto = $desconto;
    }

    public function setModalidade($modalidade) {
        $this->id_modalidade = $modalidade;
    }

    public function setDatainicio($datainicio) {
        $this->datainicio = $datainicio;
    }

    public function setDatafim($datafim) {
        $this->datafim = $datafim;
    }

    public function setHoradeixar($horadeixar) {
        $this->horadeixar = $horadeixar;
    }

    public function setHorabuscar($horabuscar) {
        $this->horabuscar = $horabuscar;
    }

    public function setAnfitriao($id_anfitriao) {
        $this->id_anfitriao = $id_anfitriao;
    }

    public function setPets($pets) {
        $this->pets = $pets;
    }

    public function setCadastro($id_cadastro) {
        $this->id_cadastro = $id_cadastro;
    }

    public function setDatacadastro($datacadastro) {
        $this->datacadastro = $datacadastro;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Reserva && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_reserva;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
