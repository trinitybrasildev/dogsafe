<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Tamanhopet 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Tamanhopet {

    private $id_tamanhopet;
    private $titulo;

    public function getId() {
        return (int) $this->id_tamanhopet;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setId($id_tamanhopet) {
        $this->id_tamanhopet = (int) $id_tamanhopet;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Tamanhopet && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_tamanhopet;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}