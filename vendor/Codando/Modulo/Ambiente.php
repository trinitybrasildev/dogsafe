<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Ambiente 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Ambiente {

    private $id_ambiente;
    private $titulo;

    public function getId() {
        return (int) $this->id_ambiente;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setId($id_ambiente) {
        $this->id_ambiente = (int) $id_ambiente;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Ambiente && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->titulo;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
