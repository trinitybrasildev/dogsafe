<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Chat 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Chat {

    private $id_chat;
    private $id_cadastro;
    private $id_anfitriao;
    private $data;
    private $status;
    private $lastint;

    public function getId() {
        return (int) $this->id_chat;
    }

    public function getCadastro() {
        return $this->id_cadastro;
    }

    public function getAnfitriao() {
        return $this->id_anfitriao;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getLastint($format = 'd/m/Y H:i') {
        return $this->lastint !== NULL ? date($format, strtotime($this->lastint)) : NULL;
    }

    public function setId($id_chat) {
        $this->id_chat = (int) $id_chat;
    }

    public function setCadastro($id_cadastro) {
        $this->id_cadastro = $id_cadastro;
    }

    public function setAnfitriao($id_anfitriao) {
        $this->id_anfitriao = $id_anfitriao;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Chat && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        
        $this->cadastro = $this->id_cadastro->getObjectVars();
        $this->anfitriao = $this->id_anfitriao->getObjectVars();
        
        unset($this->id_cadastro, $this->id_anfitriao);
        
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_chat;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}