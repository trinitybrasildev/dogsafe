<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Anfitriaomodalidade 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Anfitriaomodalidade {

    private $id_anfitriaomodalidade;
    private $id_anfitriao;
    private $id_modalidade;

    public function getId() {
        return (int) $this->id_anfitriaomodalidade;
    }

    public function getAnfitriao() {
        return  $this->id_anfitriao;
    }

    public function getModalidade() {
        return $this->id_modalidade;
    }

    public function setId($id_anfitriaomodalidade) {
        $this->id_anfitriaomodalidade = (int) $id_anfitriaomodalidade;
    }

    public function setAnfitriao($id_anfitriao) {
        $this->id_anfitriao =  $id_anfitriao;
    }

    public function setModalidade($id_modalidade) {
        $this->id_modalidade =  $id_modalidade;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Anfitriaomodalidade && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_anfitriaomodalidade;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
