<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Departamento 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Departamento {

    private $id_departamento;
    private $nome;
    private $email;

    public function getId() {
        return (int) $this->id_departamento;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setId($id_departamento) {
        $this->id_departamento = (int) $id_departamento;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Departamento && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_departamento;
    }

    public function __construct() { }

    public function __destruct() {
        unset($this);
    }
}
