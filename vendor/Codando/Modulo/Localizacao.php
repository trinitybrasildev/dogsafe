<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Localizacao 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Localizacao {

    private $id_localizacao;
    private $titulo;
    private $endereco;
    private $telefone;
    private $email;
    private $cep;
    private $cidadeuf;
    private $resumo;
    private $latlong;

    public function getId() {
        return (int) $this->id_localizacao;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getEndereco() {
        return $this->endereco;
    }

    public function getTelefone() {
        return $this->telefone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getCidadeuf() {
        return $this->cidadeuf;
    }

    public function getResumo() {
        return $this->resumo;
    }

    public function getLatlong() {
        return $this->latlong;
    }

    public function setId($id_localizacao) {
        $this->id_localizacao = (int) $id_localizacao;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    public function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function setCidadeuf($cidadeuf) {
        $this->cidadeuf = $cidadeuf;
    }

    public function setResumo($resumo) {
        $this->resumo = $resumo;
    }

    public function setLatlong($latlong) {
        $this->latlong = $latlong;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Localizacao && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_localizacao;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}
