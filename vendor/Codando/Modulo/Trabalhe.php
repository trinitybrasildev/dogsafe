<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Trabalhe 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Trabalhe {

    private $id_trabalhe;
    private $nome;
    private $email;
    private $estado;
    private $cidade;
    private $telefone;
    private $cargo;
    private $mensagem;
    private $arquivo;

    public function getId() {
        return (int) $this->id_trabalhe;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function getTelefone() {
        return $this->telefone;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function getMensagem() {
        return $this->mensagem;
    }

    public function getArquivo() {
        return $this->arquivo;
    }

    public function setId($id_trabalhe) {
        $this->id_trabalhe = (int) $id_trabalhe;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    public function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    public function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    public function setArquivo(Arquivo $arquivo) {
        $this->arquivo = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Trabalhe && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_trabalhe;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

;
?>