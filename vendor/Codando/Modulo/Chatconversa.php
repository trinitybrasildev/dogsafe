<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Chatconversa 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Chatconversa {

    private $id_chatconversa;
    private $id_chat;
    private $ref;
    private $tipo;
    private $nome;
    private $texto;
    private $data;

    public function getId() {
        return (int) $this->id_chatconversa;
    }

    public function getChat() {
        return $this->id_chat;
    }

    public function getRef() {
        return $this->ref;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getData($format = 'd/m/Y H:i') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function setId($id_chatconversa) {
        $this->id_chatconversa = (int) $id_chatconversa;
    }

    public function setChat($id_chat) {
        $this->id_chat = $id_chat;
    }

    public function setRef($ref) {
        $this->ref = $ref;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Chatconversa && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {

        //$this->cadastro = $this->id_cadastro->getObjectVars();
        //$this->chat = $this->id_chat->getObjectVars();
        $this->data = $this->getData();

        unset($this->id_cadastro, $this->id_chat);

        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_chatconversa;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
