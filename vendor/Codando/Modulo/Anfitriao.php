<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Anfitriao 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Anfitriao {

    private $id_anfitriao;
    private $nome;
    private $email;
    private $password;
    private $status;
    private $data;
    private $dataentrevista;
    private $horaentrevista;
    private $cpf;
    private $cep;
    private $endereco;
    private $id_cidade;
    private $lastlogin;
    private $latlong;
    private $id_ambiente;
    private $id_tamanhopet;
    private $id_cancelamento;
    private $sobre;
    private $avaliacao;
    private $qtdavaliacao;
    private $celular;
    private $datacadastro;
    private $qthospede;
    private $valordiariahospedagem;
    private $valordiariadaycare;
    private $horassupervisao;
    private $qtdanimais;
    private $aceitamf;
    private $qualsexo;
    private $medicacaooral;
    private $admmedicacaooral;
    private $aceitapetsidade;
    private $qualidade;
    private $animalespecial;
    private $providenciaracao;
    private $caminhapote;
    private $buscaleva;
    private $buscakm;
    private $bairro;
    private $arquivoList = array();

    public function getId() {
        return (int) $this->id_anfitriao;
    }

    public function getNome() {
        return $this->nome;
    }
    
    public function getBairro() {
        return $this->bairro;
    }
    
    public function shortNome() {
        $short = '';
        $exNome = explode(' ', rtrim(mb_strtolower($this->nome)));
        $short = ucwords(current($exNome)).' '. ucwords(end($exNome));
       
        return $short;
    }
    
    public function getUrl() {
        return '/anfitriao/' . str()->slugify($this->nome) . '-' .$this->id_anfitriao;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getSenha() {
        return $this->password;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getDataentrevista($format = 'd/m/Y') {
        return $this->dataentrevista !== NULL ? date($format, strtotime($this->dataentrevista)) : NULL;
    }
    
    public function getHoraentrevista() {
        return $this->horaentrevista;
    }

    public function getCpf() {
        return $this->cpf;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getEndereco() {
        return $this->endereco;
    }

    public function getCidade() {
        return $this->id_cidade;
    }

    public function getLastlogin() {
        return $this->lastlogin;
    }

    public function getLatlong() {
        return $this->latlong;
    }

    public function getAmbiente() {
        return $this->id_ambiente;
    }

    public function getTamanhopet() {
        return $this->id_tamanhopet;
    }

    public function getCancelamento() {
        return $this->id_cancelamento;
    }

    public function getSobre() {
        return $this->sobre;
    }

    public function getAvaliacao() {
        return $this->avaliacao;
    }

    public function getQtdavaliacao() {
        return $this->qtdavaliacao;
    }

    public function getCelular() {
        return $this->celular;
    }

    public function getDatacadastro($format = 'd/m/Y') {
        return $this->datacadastro !== NULL ? date($format, strtotime($this->datacadastro)) : NULL;
    }

    public function getQthospede() {
        return $this->qthospede;
    }

    public function getValordiariahospedagem($format = true) {
        return $format === true ? number_format((float)$this->valordiariahospedagem, 2, ",", ".") : $this->valordiariahospedagem;
    }

    public function getValordiariadaycare($format = true) {
        return $format === true ? number_format((float)$this->valordiariadaycare, 2, ",", ".") : $this->valordiariadaycare;
    }

    public function getHorassupervisao() {
        return $this->horassupervisao;
    }

    public function getQtdanimais() {
        return $this->qtdanimais;
    }

    public function getAceitamf() {
        return $this->aceitamf;
    }

    public function getQualsexo() {
        return $this->qualsexo;
    }

    public function getMedicacaooral() {
        return $this->medicacaooral;
    }

    public function getAdmmedicacaooral() {
        return $this->admmedicacaooral;
    }

    public function getAceitapetsidade() {
        return $this->aceitapetsidade;
    }

    public function getQualidade() {
        return $this->qualidade;
    }

    public function getAnimalespecial() {
        return $this->animalespecial;
    }

    public function getProvidenciaracao() {
        return $this->providenciaracao;
    }

    public function getCaminhapote() {
        return $this->caminhapote;
    }

    public function getBuscaleva() {
        return $this->buscaleva;
    }

    public function getBuscakm() {
        return $this->buscakm;
    }

    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function getArquivo() {
        return current($this->arquivoList);
    }

    public function setId($id_anfitriao) {
        $this->id_anfitriao = (int) $id_anfitriao;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setSenha($password) {
        $this->password = $password;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setDataentrevista($dataentrevista) {
        $this->dataentrevista = $dataentrevista;
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    public function setCidade($id_cidade) {
        $this->id_cidade = $id_cidade;
    }

    public function setLastlogin($lastlogin) {
        $this->lastlogin = $lastlogin;
    }

    public function setLatlong($latlong) {
        $this->latlong = $latlong;
    }

    public function setAmbiente($id_ambiente) {
        $this->id_ambiente = $id_ambiente;
    }

    public function setTamanhopet($id_tamanhopet) {
        $this->id_tamanhopet = $id_tamanhopet;
    }

    public function setCancelamento($id_cancelamento) {
        $this->id_cancelamento = $id_cancelamento;
    }

    public function setSobre($sobre) {
        $this->sobre = $sobre;
    }

    public function setAvaliacao($avaliacao) {
        $this->avaliacao = $avaliacao;
    }

    public function setQtdavaliacao($qtdavaliacao) {
        $this->qtdavaliacao = $qtdavaliacao;
    }

    public function setCelular($celular) {
        $this->celular = $celular;
    }

    public function setDatacadastro($datacadastro) {
        $this->datacadastro = $datacadastro;
    }

    public function setQthospede($qthospede) {
        $this->qthospede = $qthospede;
    }

    public function setValordiariahospedagem($valordiariahospedagem) {
        $this->valordiariahospedagem = $valordiariahospedagem;
    }

    public function setValordiariadaycare($valordiariadaycare) {
        $this->valordiariadaycare = $valordiariadaycare;
    }

    public function setHorassupervisao($horassupervisao) {
        $this->horassupervisao = $horassupervisao;
    }

    public function setQtdanimais($qtdanimais) {
        $this->qtdanimais = $qtdanimais;
    }

    public function setAceitamf($aceitamf) {
        $this->aceitamf = $aceitamf;
    }

    public function setQualsexo($qualsexo) {
        $this->qualsexo = $qualsexo;
    }

    public function setMedicacaooral($medicacaooral) {
        $this->medicacaooral = $medicacaooral;
    }

    public function setAdmmedicacaooral($admmedicacaooral) {
        $this->admmedicacaooral = $admmedicacaooral;
    }

    public function setAceitapetsidade($aceitapetsidade) {
        $this->aceitapetsidade = $aceitapetsidade;
    }

    public function setQualidade($qualidade) {
        $this->qualidade = $qualidade;
    }

    public function setAnimalespecial($animalespecial) {
        $this->animalespecial = $animalespecial;
    }

    public function setProvidenciaracao($providenciaracao) {
        $this->providenciaracao = $providenciaracao;
    }

    public function setCaminhapote($caminhapote) {
        $this->caminhapote = $caminhapote;
    }

    public function setBuscaleva($buscaleva) {
        $this->buscaleva = $buscaleva;
    }

    public function setBuscakm($buscakm) {
        $this->buscakm = $buscakm;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Anfitriao && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_anfitriao;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}