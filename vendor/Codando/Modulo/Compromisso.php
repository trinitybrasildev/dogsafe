<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Compromisso 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Compromisso {

    private $id_compromisso;
    private $titulo;
    private $data;
    private $horainicio;
    private $horafim;
    private $id_anfitriao;

    public function getId() {
        return (int) $this->id_compromisso;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getHorainicio() {
        return $this->horainicio;
    }

    public function getHorafim() {
        return $this->horafim;
    }

    public function getAnfitriao() {
        return (int) $this->id_anfitriao;
    }

    public function setId($id_compromisso) {
        $this->id_compromisso = (int) $id_compromisso;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setHorainicio($horainicio) {
        $this->horainicio = $horainicio;
    }

    public function setHorafim($horafim) {
        $this->horafim = $horafim;
    }

    public function setAnfitriao($id_anfitriao) {
        $this->id_anfitriao = (int) $id_anfitriao;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Compromisso && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_compromisso;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}
