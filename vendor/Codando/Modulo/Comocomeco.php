<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Comocomeco 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Comocomeco {

    private $id_comocomeco;
    private $titulo;
    private $texto;
    private $ordem;
    private $arquivo;

    public function getId() {
        return (int) $this->id_comocomeco;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getOrdem() {
        return $this->ordem;
    }

    public function getArquivo() {
        return $this->arquivo;
    }

    public function setId($id_comocomeco) {
        $this->id_comocomeco = (int) $id_comocomeco;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    public function setArquivo(Arquivo $arquivo) {
        $this->arquivo = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Comocomeco && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_comocomeco;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}