<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Comofunciona 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Comofunciona {

    private $id_comofunciona;
    private $titulo;
    private $url_titulo;
    private $texto;
    private $ordem;
    private $tipo;
    private $arquivo;

    public function getId() {
        return (int) $this->id_comofunciona;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getOrdem() {
        return $this->ordem;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getArquivo() {
        return $this->arquivo;
    }

    public function setId($id_comofunciona) {
        $this->id_comofunciona = (int) $id_comofunciona;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function setArquivo(Arquivo $arquivo) {
        $this->arquivo = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Comofunciona && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_comofunciona;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}