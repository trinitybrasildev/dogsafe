<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Reserva 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Reservapet {
    
    private $id_reservapet;
    private $id_reserva;
    private $id_pet;
    

    public function getId() {
        return (int) $this->id_reservapet;
    }
    
    public function getReserva() {
        return $this->id_reserva;
    }
    
    public function getPet() {
        return $this->id_pet;
    }

    public function setId($id_reservapet) {
        $this->id_reservapet = (int) $id_reservapet;
    }

    public function setPet($id_pet) {
        $this->id_pet = $id_pet;
    }
    
    public function setReserva($id_reserva) {
        $this->id_reserva = $id_reserva;
    }
   
    public function isEquals($isEqual) {
        return ($isEqual instanceof Reserva && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_reserva;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }
}