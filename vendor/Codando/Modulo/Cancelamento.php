<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Cancelamento 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Cancelamento {

    private $id_cancelamento;
    private $titulo;
    private $descricao;

    public function getId() {
        return (int) $this->id_cancelamento;
    }

    public function getTitulo() {
        return $this->titulo;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }

    public function setId($id_cancelamento) {
        $this->id_cancelamento = (int) $id_cancelamento;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }
    
    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Cancelamento && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->titulo;
    }

    public function __construct() {}

    public function __destruct() {
        unset($this);
    }
}