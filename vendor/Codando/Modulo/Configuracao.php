<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Configuracao 
 *
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Configuracao {

    private $id_configuracao;
    private $titulo;
    private $valor;

    public function getId() {
        return (int) $this->id_configuracao;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getValor() {
        return $this->valor;
    }

    public function setId($id_configuracao) {
        $this->id_configuracao = (int) $id_configuracao;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Configuracao && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_configuracao;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}