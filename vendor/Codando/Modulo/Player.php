<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Player 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Player {

    private $id_player;
    private $titulo;
    private $arquivoList = array();

    public function getId() {
        return $this->id_player;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function getArquivo() {
        return current($this->arquivoList);
    }

    public function setId($id_player) {
        $this->id_player = $id_player;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Player && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_player;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}