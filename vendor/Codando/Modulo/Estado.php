<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Estado 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Estado {

    private $id_estado;
    private $nome;
    private $sigla;

    public function getId() {
        return (int) $this->id_estado;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getSigla() {
        return $this->sigla;
    }

    public function setId($id_estado) {
        $this->id_estado = (int) $id_estado;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setSigla($sigla) {
        $this->sigla = $sigla;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Estado && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->sigla;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}