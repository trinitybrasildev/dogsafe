<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Cupom 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Cupom {

    private $id_cupom;
    private $datavalido;
    private $quantidade;
    private $codigo;
    private $porcentagem;

    public function getId() {
        return (int) $this->id_cupom;
    }

    public function getDatavalido($format = 'd/m/Y') {
        return $this->datavalido !== NULL ? date($format, strtotime($this->datavalido)) : NULL;
    }

    public function getQuantidade() {
        return $this->quantidade;
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getPorcentagem() {
        return $this->porcentagem;
    }

    public function setId($id_cupom) {
        $this->id_cupom = (int) $id_cupom;
    }

    public function setDatavalido($datavalido) {
        $this->datavalido = $datavalido;
    }

    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setPorcentagem($porcentagem) {
        $this->porcentagem = $porcentagem;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Cupom && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->codigo;
    }

    public function __construct() {}

    public function __destruct() {}

}