<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Cadastro 
 * /
 * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Cadastro {

    private $id_cadastro;
    private $nome;
    private $email;
    private $password;
    private $cep;
    private $celular;
    private $endereco;
    private $id_cidade;
    private $facebook;
    private $cpf;
    private $datacadastro;
    private $arquivoList = array();
    private $lastlogin;

    public function getId() {
        return (int) $this->id_cadastro;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getSenha() {
        return $this->password;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getCelular() {
        return $this->celular;
    }

    public function getEndereco() {
        return $this->endereco;
    }

    public function getCidade() {
        return $this->id_cidade;
    }

    public function getFacebook() {
        return $this->facebook;
    }

    public function getLastlogin() {
        return $this->lastlogin;
    }

    public function getCpf() {
        return $this->cpf;
    }

    public function getDatacadastro($format = 'd/m/Y H:i') {
        return $this->datacadastro !== NULL ? date($format, strtotime($this->datacadastro)) : NULL;
    }

    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function getArquivo() {
        return current($this->arquivoList);
    }

    public function setId($id_cadastro) {
        $this->id_cadastro = (int) $id_cadastro;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function setCelular($celular) {
        $this->celular = $celular;
    }

    public function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    public function setCidade($id_cidade) {
        $this->id_cidade = $id_cidade;
    }

    public function setFacebook($facebook) {
        $this->facebook = $facebook;
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    public function setDatacadastro($datacadastro) {
        $this->datacadastro = $datacadastro;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Cadastro && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_cadastro;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}
