<?php

 namespace Codando\Modulo;

 /**
  * Classe cria modulo para persitencia dos registos com banco
  * /
  * @author  Luiz A. J. S. Thomas <luizz@luizz.com.br>
  * @copyright  Luizz
  * @license www.luiz.com.br
  * @package Codando
  */
 class Model {

     protected $modulo;

     public function insert() {

         $data = $this->getObjectVars();

         unset($data[$this->modulo->getPrimarykey()]); //Remove ID

         $id_save = db()->insert($this->modulo->getTabela(true), $data);

         if ($id_save > 0) {
             $this->setId($id_save);
         }
     }

     public function delete() {
         
     }

     public function update() {
         
     }

     public function findId($id) {

         $this->setId($id);

         $findId = app()->loadModulo($this->modulo->getTabela(true), array($this->modulo->getPrimarykey() . ' = :findId ', array('findId' => (int) $id)));

         if ($this->isEquals($findId) === TRUE) {
             $this = $findId;
         }
     }

     protected function init() {

         $className = strtolower(str_replace("Codando\\Modulo\\", "", get_class($this)));
         $this->modulo = app()->getModulo($className);
         if (is_instanceof('Codando\Modulo\Modulo', $this->modulo) === FALSE)
             return FALSE;
     }

     public function __toString() {
         
     }

     public function __construct() {
         
     }

     public function __destruct() {
         unset($this);
     }

 }
 