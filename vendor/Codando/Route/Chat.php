<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Chat extends Controller\Chat {

    private $app;

    public function index() {

        $this->_list();
        meta()->addMeta('title', 'Chat');
        tpl()->display('auth/chat', array('menuCurrent' => 'chat', 'chatList' => $this->moduloList));
        return;
    }

    public function listConversa() {

        $responde = array('status' => false);

        $this->_listConversa();

        /* @var $chatconversa \Codando\Modulo\Chatconversa */
        foreach ($this->moduloList as $chatconversa) {
            $responde['list'][] = $chatconversa->getObjectVars();
        }

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function insert($id_anfitriao) {

        $anfitriao = app()->loadModulo('anfitriao', array(' id_anfitriao = :id  AND status = 1 ', array('id' => $id_anfitriao)));

        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        $logado = input()->getSession('logadocadastro');

        $cadastro = app()->loadModulo('cadastro', array(' id_cadastro = :id ', array('id' => $logado)));

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        $this->insertBy($cadastro, $anfitriao);

        $this->index();
    }

    public function insertConversa() {

        $responde = array('status' => false);

        /* @var $chatconversa \Codando\Modulo\Chatconversa */
        $chatconversa = $this->_insertConversa();

        if (is_instanceof('Codando\Modulo\Chatconversa', $chatconversa) === true) {
            $responde = array('status' => true, 'chatconversa' => $chatconversa->getObjectVars());
        }

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {
        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
