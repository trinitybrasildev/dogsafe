<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Visita extends Controller\Visita {

    private $anfitriaoController;
    private $cadastroController;
    private $app;

    public function insert($id) {

        $this->anfitriaoController->_loadAtivo($id);
        $anfitriao = $this->anfitriaoController->getModulo();

        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        $this->cadastroController->_loadLogado();
        $cadastro = $this->cadastroController->getModulo();

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        if ($this->app->request->isGet()) {
            return;
        }

        $responde = array();

        $this->_insert($anfitriao);

        if (is_instanceof('Codando\Modulo\Visita', $this->modulo) === true) {
            $responde = array('redir' => '/minha-conta', 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {
        $this->cadastroController = new \Codando\Controller\Cadastro();
        $this->anfitriaoController = new \Codando\Controller\Anfitriao();
        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
