<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Cep {

    private $app;

    public function findTogeo($cep) {
      
        $cepen = preg_replace("/[^0-9]/", '', $cep);

        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$cepen."&sensor=false";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);

        $jsondata = json_decode($output, true);

        // If the json data is invalid, return empty array
        if (is_array($jsondata) == false || isset($jsondata["results"]) == false || count($jsondata["results"]) == 0)
            return array();

        $LatLng = array(
            'lat' => str_replace(',', '.', $jsondata["results"][0]["geometry"]["location"]["lat"]),
            'lng' => str_replace(',', '.', $jsondata["results"][0]["geometry"]["location"]["lng"]),
        );
       
        input()->setSession('cepgeo', $cepen . ':' . $LatLng['lat'].','.$LatLng['lng']);
        
        echo $LatLng['lat'].','.$LatLng['lng'];
        
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
