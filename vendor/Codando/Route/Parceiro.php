<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Parceiro extends Controller\Parceiro {

    private $app;

    public function listAll() {

        $this->_list();

        $this->setRegistrosPorPagina(8);
        meta()->addMeta('title', 'Parceiros');
        
        tpl()->display('parceiro_list', array('menuCurrent' => 'parceiro',
            'paginacao' => $this->getPaginacao('/parceiros'),
            'parceiroList' => $this->moduloList)
        );
    }

    public function load($url, $id) {

        $this->_loadAtivo($id);

        if (is_instanceof('Codando\Modulo\Parceiro', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        meta()->addMeta('title', 'Parceiro')
                ->addMeta('title', $this->modulo->getTitulo())
                ->addMeta('description', $this->modulo->getResumo());


        /* @var $imagemTemp \Codando\Modulo\Arquivo  */
        $imagemTemp = is_instanceof('Codando\Modulo\Arquivo', $this->modulo->getArquivo()) ? $this->modulo->getArquivo() : NULL;

        meta()->setMeta('og:url', $this->modulo->getUrl());

        If ($imagemTemp != NULL) {
            meta()->setMeta('og:image', $imagemTemp->getImage(600, 600));
        }

        tpl()->display('parceiro_view', array('menuCurrent' => 'parceiro', 'parceiro' => $this->modulo));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
