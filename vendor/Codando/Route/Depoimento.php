<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Depoimento extends Controller\Depoimento {

    private $app;

    public function listAll() {

        $this->setRegistrosPorPagina(3);

        $this->_list();
        meta()->addMeta('title', 'Depoimentos');
        tpl()->display('depoimento_list', array('menuCurrent' => 'depoimento',
            'paginacao' => $this->getPaginacao('/depoimentos'),
            'depoimentoList' => $this->moduloList)
        );
    }

    public function insert() {

        $responde = array('status' => false);

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Depoimento', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/minha-conta', 'depoimento' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
