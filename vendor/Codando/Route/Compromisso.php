<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Compromisso extends Controller\Compromisso {

    private $app;

    public function listAll() {

        $this->_list();

        tpl()->display('auth/compromisso_list', array('menuCurrent' => 'compromissos',
            'paginacao' => $this->getPaginacao('/meus-compromisso'),
            'compromissoList' => $this->moduloList)
        );
    }

    public function insert() {

        $responde = array('status' => false);

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Compromisso', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/minha-agenda', 'compromisso' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function excluir($id) {

        $this->_load($id);

        if (is_instanceof('Codando\Modulo\Compromisso', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        $this->_excluir();

        $responde = array('status' => true, 'redir' => '/meus-compromisso', 'compromisso' => $this->modulo->getObjectVars());

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
