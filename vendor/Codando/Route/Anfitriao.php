<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Anfitriao extends Controller\Anfitriao {

    private $app;

    //@fazer: ajax
    public function agenda() {

        $this->_loadLogado();

        $reservaController = new \Codando\Controller\Reserva();
        $reservaController->_list();
        $reservaList = $reservaController->getModuloList();

        $paginacao = $reservaController->getPaginacao();

        tpl()->display('auth/minha_agenda', array('menuCurrent' => 'minha-agenda', 'reservaList' => $reservaList, 'paginacao' => $paginacao));
        return;
    }

    public function changestatus($id) {

        $this->_load($id);

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        /* @var $anfitriao \Codando\Modulo\Anfitriao */
        $anfitriao = $this->modulo;

        if ($anfitriao->getStatus() == 1) {//Aprovado
            $this->_emailAprovadoOrNao();
        }

        if ($anfitriao->getStatus() == 3) {//Reprovado
            $this->_emailAprovadoOrNao('naofoiaprovado');
        }
    }

    public function load($url, $id) {

        $this->_load($id);

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        $urlAnf = '/anfitriao/' . $url . '-' . $id;

        if ($this->modulo->getUrl() !== $urlAnf) {
            $this->app->redirect($urlAnf);
            $this->app->stop();
            return false;
        }

        $tamanhopetList = app()->listModulo('tamanhopet');
        $anfitriaomodalidade = app()->listModulo('anfitriaomodalidade', array(' id_anfitriao = :anfitriao ', array('anfitriao' => $this->modulo->getId())));
        $petanfitriao = app()->listModulo('petanfitriao', array(' id_anfitriao = :anfitriao ', array('anfitriao' => $this->modulo->getId())));
        $comentarioList = app()->listModulo('comentarioanfitriao', array(' id_anfitriao = :anfitriao ', array('anfitriao' => $this->modulo->getId())));

        tpl()->display('anfitriao_view', array('menuCurrent' => 'anfitriao_view', 'comentarioList' => $comentarioList, 'petanfitriao' => $petanfitriao, 'anfitriaomodalidade' => $anfitriaomodalidade, 'tamanhopetList' => $tamanhopetList, 'anfitriao' => $this->modulo));

        return;
    }

    public function login() {

        if ($this->app->request->isGet()) {
            meta()->addMeta('title', 'Login');
            tpl()->display('login', array('menuCurrent' => 'login'));
            return;
        }

        $responde = array('status' => false);

        $this->_login();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo)) {

            $return = input()->getSession('return');
            input()->setSession('return', NULL);

            $responde = array('status' => true, 'redir' => ($return !== NULL ? $return : '/'), 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function insert() {

        if ($this->app->request->isGet()) {
            meta()->addMeta('title', 'Tornar-se anfitrião');
            tpl()->display('anfitriao_cadastro', array('menuCurrent' => 'anfitriao-se'));
            return;
        }

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/meu-perfil', 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function update() {

        $this->_loadLogado();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        if ($this->app->request->isGet()) {

            tpl()->display('auth/alterar_anfitriao', array('anfitriao' => $this->modulo, 'menuCurrent' => 'altera-anfitriao'));
            return;
        }

        $responde = array('status' => false);

        $up = $this->_update();
        

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === true) {
            $responde = array('status' => true, 'anfitriao' => $this->modulo->getObjectVars());
        }
        
        if($up){
            $responde['redir'] = '/editar-anfitriao';
        }
        
        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }
    
    public function reagendamento() {

        $this->_loadLogado();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        if ($this->app->request->isGet()) {

            tpl()->display('auth/remarcaentrevista_anfitriao', array('anfitriao' => $this->modulo, 'menuCurrent' => 'altera-anfitriao'));
            return;
        }

        $responde = array('status' => false);

        $up = $this->_remarcaEntrevista();
        

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === true) {
            $responde = array('status' => true, 'anfitriao' => $this->modulo->getObjectVars());
        }
        
        if($up){
            $responde['redir'] = '/minha-conta';
        }
        
        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    //@fazer: termina
    public function meuperfil() {

        $this->_loadLogado();

        if ($this->app->request->isGet()) {

            $ambienteList = app()->listModulo('ambiente');
            $cancelamentoList = app()->listModulo('cancelamento');
            $modalidadeList = app()->listModulo('modalidade');
            $tamanhopetList = app()->listModulo('tamanhopet');
            $anfitriaomodalidade = app()->listModulo('anfitriaomodalidade', array(' id_anfitriao = :anfitriao ', array('anfitriao' => $this->modulo->getId())));
            $petanfitriao = app()->listModulo('petanfitriao', array(' id_anfitriao = :anfitriao ', array('anfitriao' => $this->modulo->getId())));

            tpl()->display('auth/meu_perfil', array('anfitriao' => $this->modulo, 'petanfitriao' => $petanfitriao, 'anfitriaomodalidade' => $anfitriaomodalidade, 'tamanhopetList' => $tamanhopetList, 'menuCurrent' => 'meu-perfil', 'modalidadeList' => $modalidadeList, 'ambienteList' => $ambienteList, 'cancelamentoList' => $cancelamentoList));
            return;
        }

        $responde = array('status' => false);

        $this->_updatePerfil();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo) === true) {
            $responde = array('status' => true, 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function esqueceSenha() {

        if ($this->app->request->isGet()) {
            tpl()->display('esquece-senha', array('menuCurrent' => 'esquece-senha'));
            return;
        }

        $responde = array('status' => false);

        $this->_recuperaApp();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo)) {
            $responde = array('status' => true, 'redir' => '/codigo-senha', 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function codigoSenha() {

        if ($this->app->request->isGet()) {
            tpl()->display('codigo-senha', array('menuCurrent' => 'codigo-senha'));
            return;
        }

        $responde = array('status' => false);

        $this->_updateRecuperarCodigo();

        if (is_instanceof('Codando\Modulo\Anfitriao', $this->modulo)) {
            $responde = array('status' => true, 'redir' => '/codigo-senha', 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function listAll() {

        $this->_list();

        $modalidadeList = app()->listModulo('modalidade');
        $tamanhopetList = app()->listModulo('tamanhopet');
        $ambienteList = app()->listModulo('ambiente');

        meta()->addMeta('title', 'Encontre um anfitrião');

        tpl()->display('anfitriao_list', array('menuCurrent' => 'anfitriao',
            'modalidadeList' => $modalidadeList,
            'tamanhopetList' => $tamanhopetList,
            'ambienteList' => $ambienteList,
            'paginacao' => $this->getPaginacao('/encontre-um-anfitriao'), 'anfitriaoList' => $this->moduloList));
    }

    public function listEntrevista() {

        if (ANF_ENTR != input()->getSession('logadoanfitriao')) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return;
        }

        $this->_listEntrevista();

        meta()->addMeta('title', 'Entrevistas');

        tpl()->display('auth/entrevista_list', array('menuCurrent' => 'anfitriao', 'paginacao' => $this->getPaginacao('/entrevistas'), 'anfitriaoList' => $this->moduloList));
    }

    public function __construct() {
        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
