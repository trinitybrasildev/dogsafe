<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Foto extends Controller\Foto {

    private $app;

    public function listAll() {

        $this->_list();

        tpl()->display('foto_list', array('menuCurrent' => 'foto',
            'paginacao' => $this->getPaginacao('/fotos'),
            'fotoList' => $this->moduloList)
        );
    }

    public function load($url, $id) {

        $this->_load($id);

        if (is_instanceof('Codando\Modulo\Foto', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
		
		meta()->addMeta('title', 'Fotos')
          ->addMeta('title', $this->modulo->getTitulo());
    

		/* @var $imagemTemp \Codando\Modulo\Arquivo  */
		$imagemTemp = is_instanceof('Codando\Modulo\Arquivo', $this->modulo->getArquivo()) ? $this->modulo->getArquivo() :NULL;

		meta()->setMeta('og:url', $config['root'] . $this->modulo->getUrl());

		If($imagemTemp != NULL){
			meta()->setMeta('og:image', $config['root'] . $imagemTemp->getImage(600, 600));    
		}

        tpl()->display('foto_view', array('menuCurrent' => 'foto', 'foto' => $this->modulo));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
