<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Video extends Controller\Video {

    private $app;

    public function listAll() {

        $this->_list();

        tpl()->display('video_list', array('menuCurrent' => 'video',
            'paginacao' => $this->getPaginacao('/videos'),
            'videoList' => $this->moduloList)
        );
    }

    public function load($url, $id) {

        $this->_load($id);
        
        if (is_instanceof('Codando\Modulo\Video', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
		
		meta()->addMeta('title', 'Vídeos')
          ->addMeta('title', $this->modulo->getTitulo());
    
		meta()->setMeta('og:url', $config['root'] . $this->modulo->getUrl());

        tpl()->display('video_view', array('menuCurrent' => 'video', 'video' => $this->modulo));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
