<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Pagina extends Controller\Pagina {

    private $app;

    public function listAll() {

        $this->_list();

        tpl()->display('pagina_list', array('menuCurrent' => 'pagina',
            'paginacao' => $this->getPaginacao('/paginas'),
            'paginaList' => $this->moduloList)
        );
    }
    
    public function loadTermo() {

        $this->load('termo', 3);
    }
    
    public function loadSobre() {

        $this->load('sobre', 1);
    }
    
    public function loadPrivacidade() {

        $this->load('privacidade', 4);
    }
    
    public function load($url, $id) {

        $this->_load($id);
        
        if (is_instanceof('Codando\Modulo\Pagina', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        tpl()->display('pagina_view', array('menuCurrent' => 'pagina', 'pagina' => $this->modulo));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
