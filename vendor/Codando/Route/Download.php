<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Download extends Controller\Download {

    private $app;

    public function listAll() {

        $this->_list();
        $this->downloadList = $this->getModuloList();

        $paginacao = $this->getPaginacao('/arquivos');

        tpl()->display('arquivo_list', array('menuCurrent' => 'arquivos', 'paginacao' => $paginacao, 'tipodownload' => $this->tipodownload, 'downloadList' => $this->downloadList));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
