<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Auth {

    private $app;
    private $anfitriaoController;
    private $cadastroController;

    public static function notlogado() {

        $app = Slim::getInstance();

        if (is()->id(input()->getSession('logado')) === true) {
            $app->redirect('/');
            $app->stop();
            return false;
        }

        return true;
    }

    public static function logado() {

        $app = Slim::getInstance();

        if (is()->id(input()->getSession('logado')) === false) {
            $actual_link = $_SERVER['REQUEST_URI'];

            input()->setSession('return', $actual_link);
            
            //LOG ERRO
            logsgc('auth', 1, 1, ' not logado ['. urlencode($actual_link) .']');
            
            $app->redirect('/identificacao?return=' . urlencode($actual_link));
            $app->stop();
        }

        return true;
    }

    public function minhaconta() {

        $countReserva = app()->countModulo('reserva', array(' id_anfitriao = :ant AND status = 5 ', array( 'ant' => input()->getSession('logadoanfitria'))));

        $countDepoimento = app()->countModulo('depoimento', array(' id_anfitriao = :ant ', array( 'ant' => input()->getSession('logadoanfitria'))));

        tpl()->display('auth/minhaconta', array('menuCurrent' => 'minhaconta', 'countReserva' => $countReserva, 'countDepoimento' => $countDepoimento));
        return;
    }
    
    public function minhacontahospede() {

        $countReserva = app()->countModulo('reserva', array(' id_cadastro = :cad AND status = 5 ', array('cad' => input()->getSession('logadocadastro'))));

        $countDepoimento = app()->countModulo('depoimento', array(' id_cadastro = :cad ', array('cad' => input()->getSession('logadocadastro'))));

        tpl()->display('hospede/minhaconta', array('menuCurrent' => 'minhaconta', 'countReserva' => $countReserva, 'countDepoimento' => $countDepoimento));
        return;
    }

    public function logout() {

        unset($_SESSION);
        session_destroy();

        $this->app->redirect('/identificacao');
    }

    private function graphFacebook($query) {

        $ch = curl_init($query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $r = curl_exec($ch);
        curl_close($ch);
        return $r;
    }

    public function cadastrofacebook() {

        $responde = array();

        if (isset($_POST['code'])) {

            $appId = '279480292466896';
            $appSecret = '32356105f726aaf20031dc7188f8ab99';

            $code = array_key_exists('code', $_POST) === true ? $_POST['code'] : '0';

            $graph_url = "https://graph.facebook.com/me?fields=id,name,email,permissions&access_token=" . $code;
            $graph_dado = $this->graphFacebook($graph_url);
            $user = json_decode($graph_dado);

            $responde["dado"] = $user;

            if (isset($user->email) && $user->email) {

                $responde["status"] = true;

                $responde["email"] = $user->email;
                $responde['nome'] = $user->name;

                input()->setSession('fb_email', $user->email);
                input()->setSession('fb_nome', $user->name);
                input()->setSession('fb_uid_facebook', $user->id);
            } else {

                $responde["msg"] = "Não foi possível obter dados do facebook.";
            }
        } else if (isset($_GET['error'])) {

            $responde["msg"] = 'Permissão não concedida';
        }

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function loginfacebook() {

        $responde = array();

        if (isset($_POST['code'])) {

            $cadastroController = new \Codando\Controller\Cadastro();
            $anfitriaoController = new \Codando\Controller\Anfitriao();

            $appId = '279480292466896';
            $appSecret = '32356105f726aaf20031dc7188f8ab99';

            $code = array_key_exists('code', $_POST) === true ? $_POST['code'] : '0';

            $graph_url = "https://graph.facebook.com/me?fields=id,name,email,permissions&access_token=" . $code;
            $graph_dado = $this->graphFacebook($graph_url);
            $user = json_decode($graph_dado);

            $responde["dado"] = $user;

            if (isset($user->email) && $user->email) {

                $responde["status"] = true;
                $logincadastro = $cadastroController->_loginFacebook($user->email);
                $loginanfitriao = $anfitriaoController->_loginFacebook($user->email);

                if ($logincadastro === false && $loginanfitriao === false) {
                    $responde["status"] = false;
                }
                
                //LOG
                if ($logincadastro !== false)
                    logsgc('auth', 30, input()->getSession('logado'), 'logadoface');
                
                if ($loginanfitriao !== false)
                    logsgc('auth', 24, input()->getSession('logado'), 'logadoface');
                
                $return = input()->getSession('return');
                input()->setSession('return', NULL);
                
                if ($responde["status"] === false) {

                    $responde["msg"] = "Seu email '" . $user->email . "' não foi encontrado.";
                } else {

                    $responde["msg"] = "Logado '" . $user->name . "'.";
                    $responde['redir'] = ($return !== NULL ? $return : '/minha-conta');
                }

                input()->setSession('fb_email', $user->email);
                input()->setSession('fb_nome', $user->name);
                input()->setSession('fb_uid_facebook', $user->id);
            } else {

                $responde["msg"] = "Não foi possível obter dados do facebook.";
            }
        } else if (isset($_GET['error'])) {

            $responde["msg"] = 'Permissão não concedida';
        }

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function login() {

        if ($this->app->request->isGet()) {
            meta()->addMeta('title', 'Login');
            tpl()->display('auth/login', array('menuCurrent' => 'login'));
            return;
        }

        $responde = array('status' => false);
        
        $cadastro = $anfitriao = NULL;
        
        $tipo = input()->_toPost('tipo', '%d');
        
        if($tipo == 2){
            $this->cadastroController->_login();
            $cadastro = $this->cadastroController->getModulo();
        }
        
        if($tipo == 1){
            $this->anfitriaoController->_login();
            $anfitriao = $this->anfitriaoController->getModulo();
        }
        
        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === true) {

            $return = input()->getSession('return');
            input()->setSession('return', NULL);
            
            //LOG
            logsgc('auth', 30, $cadastro->getId(), 'logado');
            
            $responde = array('status' => true, 'redir' => ($return !== NULL ? $return : '/minha-conta-hospede'), 'cadastro' => $cadastro->getObjectVars());
        }

        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === true) {

            $return = input()->getSession('return');
            input()->setSession('return', NULL);
            
            //LOG
            logsgc('auth', 24, $anfitriao->getId(), 'logado');
            
            $responde = array('status' => true, 'redir' => ($return !== NULL ? $return : '/minha-conta'), 'anfitriao' => $anfitriao->getObjectVars());
        }

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === true || is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === true) {

            $responde["msg"] = 'entrando, aguardo...';
        } else {
            
            //LOG ERRO
            logsgc('auth', 1, 1, 'login invalido');
            
            $responde["msg"] = 'Verifique seu login.';
        }

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function esquecesenha() {
        
        header('Content-type: application/json');
        
        $recuperar = input()->_toPost('recemail');
        $tipo = input()->_toPost('tipo');

        $responde = array('status' => false);

        if ($tipo == 2) {//2:cadastro

            $this->cadastroController->_recupera($recuperar);
            //$cadastro = $this->cadastroController->getModulo();
            $responde["msg"] = (implode("<br/>", $this->cadastroController->getMessageList()));
        } else {


            $this->anfitriaoController->_recupera($recuperar);
            //$anfitriao = $this->anfitriaoController->getModulo();
            $responde["msg"] = (implode("<br/>", $this->anfitriaoController->getMessageList()));
        }

        echo json_encode($responde);
    }

    public function recuperar() {
        
        $senha = input()->_toGet('senha');
        $tipo = input()->_toGet('tipo');
        
        $cadastro = $anfitriao = NULL;
        
        input()->setSession('tipo', $tipo);

        if ($tipo == 1) {// 1:Anfitriao

            $this->anfitriaoController->_loadRecupera($senha);
            $anfitriao = $this->anfitriaoController->getModulo();
        } else {

            $this->cadastroController->_loadRecupera($senha);
            $cadastro = $this->cadastroController->getModulo();
        }

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === true || is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === true) {

            tpl()->display('recuperar-senha', array(''));
        } else {

            $this->app->redirect('/identificacao');
            $this->app->stop();
        }
        
        
    }

    public function alterasenha() {
        
        header('Content-type: application/json');
        $responde = array('status' => false);
        
        $tipo = input()->getSession('tipo');

        if ($tipo == 1) {//Anfitriao

            $up = $this->anfitriaoController->_updateRecuperarSenha();
            $responde["msg"] = (implode("<br/>", $this->anfitriaoController->getMessageList()));
            if($up == true){
                $responde['status'] = true;
                $responde['redir'] = '/identificacao';
            }
        } else {

            $up = $this->cadastroController->_updateRecuperarSenha();
            $responde["msg"] = (implode("<br/>", $this->cadastroController->getMessageList()));
            if($up == true){
                $responde['status'] = true;
                $responde['redir'] = '/identificacao';
            }
        }
    
        
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
        $this->cadastroController = new \Codando\Controller\Cadastro();
        $this->anfitriaoController = new \Codando\Controller\Anfitriao();
    }

    public function __destruct() {
        
    }

}
