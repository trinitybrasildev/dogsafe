<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Pet extends Controller\Pet {

    private $app;

    public function listAll() {
        
        $this->setPaginar(100);
        $this->_list();
        
        tpl()->display('hospede/pet_list', array('menuCurrent' => 'pets',
            'paginacao' => $this->getPaginacao('/meus-pet'),
            'petList' => $this->moduloList)
        );
    }

    public function insert() {

        if ($this->app->request->isGet()) {

            $tamanhopetList = app()->listModulo('tamanhopet');

            tpl()->display('hospede/adicionar_pet', array('tamanhopetList' => $tamanhopetList, 'menuCurrent' => 'adicionar-pet'));
            return;
        }

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Pet', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/meus-pets', 'pet' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function ficha($id){
         
        $this->_load($id);
        
        if (is_instanceof('Codando\Modulo\Pet', $this->modulo) === false ) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
        
        tpl()->display('hospede/ficha_pet', array('pet' => $this->modulo, 'menuCurrent' => 'ficha-pet'));
        
        return;
    }
    
    public function update($id) {
        
        $this->_load($id);

        if ($this->app->request->isGet()) {
            
            $tamanhopetList = app()->listModulo('tamanhopet');
            
            tpl()->display('hospede/alterar_pet', array('pet' => $this->modulo, 'menuCurrent' => 'altera-pet', 'tamanhopetList' => $tamanhopetList));
            return;
        }

        $responde = array('status' => false);

        $this->_update();

        if (is_instanceof('Codando\Modulo\Pet', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/editar-pet/' . $this->modulo->getId(), 'pet' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }
    
    public function excluir($id){
        
        $this->_load($id);
        
        if (is_instanceof('Codando\Modulo\Pet', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
        
        $this->_excluir();
        
        $responde = array('status' => true, 'redir' => '/meus-pets', 'pet' => $this->modulo->getObjectVars());
        
        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
