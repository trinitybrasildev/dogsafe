<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Cadastro extends Controller\Cadastro {

    private $app;

    public function login() {

        if ($this->app->request->isGet()) {
            meta()->addMeta('title', 'Login');
            tpl()->display('login', array('menuCurrent' => 'login'));
            return;
        }

        $responde = array('status' => false);

        $this->_login();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo)) {

            $return = input()->getSession('return');
            input()->setSession('return', NULL);

            $responde = array('status' => true, 'redir' => ($return !== NULL ? $return : '/'), 'cadastro' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function insert() {

        if ($this->app->request->isGet()) {
            meta()->addMeta('title', 'Cadastra se');
            tpl()->display('cadastro_view', array('menuCurrent' => 'cadastro-se'));
            return;
        }

        $responde = array('status' => false);

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/minha-conta', 'cadastro' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function update(){

        $this->_loadLogado();
        
        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
        
        if ($this->app->request->isGet()) {
            
            tpl()->display('hospede/alterar_cadastro', array('cadastro' => $this->modulo, 'menuCurrent' => 'altera-cadastro'));
            return;
        }

        $responde = array('status' => false);

        $this->_update();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === true) {
            $responde = array('status' => true, 'cadastro' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function esqueceSenha() {

        if ($this->app->request->isGet()) {
            tpl()->display('esquece-senha', array('menuCurrent' => 'esquece-senha'));
            return;
        }

        $responde = array('status' => false);

        $this->_recuperaApp();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo)) {
            $responde = array('status' => true, 'redir' => '/codigo-senha', 'cadastro' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function codigoSenha() {

        if ($this->app->request->isGet()) {
            tpl()->display('codigo-senha', array('menuCurrent' => 'codigo-senha'));
            return;
        }

        $responde = array('status' => false);

        $this->_updateRecuperarCodigo();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo)) {
            $responde = array('status' => true, 'redir' => '/codigo-senha', 'cadastro' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {
        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
