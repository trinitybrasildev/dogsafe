<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Comofunciona extends Controller\Comofunciona {

    private $app;

    public function index() {

        $this->_list();
        $pagina = app()->loadModulo('pagina', ' id_pagina = 2 ');
        meta()->addMeta('title', 'Como Funciona');
        tpl()->display('como_funciona', array('menuCurrent' => 'comofunciona','pagina' => $pagina, 'comofuncionaList' => $this->moduloList) );
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
