<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Trabalhe extends Controller\Trabalhe {
    
    private $app;
    
    public function index() {

        if ($this->app->request->isGet()) {

            tpl()->display('trabalhe_conosco', array('menuCurrent' => 'trabalheconosco'));

            return;
        }

        $responde = array('status' => false);

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Trabalhe', $this->modulo)) {
            $responde = array('status' => true, 'redir' => '/trabalhe-conosco', 'trabalheconosco' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
