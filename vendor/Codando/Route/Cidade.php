<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Cidade {

    private $app;

    public function listAll() {

        $responde = array('status' => false);

        $estado = (int) input()->_toGet('estado', '%d');

        if ($estado >= 1) {

            $cidadeList = (array) app()->listModulo('cidade', array(' id_estado = :id ', array('id' => $estado)));

            $responde["list"] = array();

            /* @var $cidade \Codando\Modulo\Cidade */
            foreach ($cidadeList as $cidade) {

                $responde["list"][] = array('id' => $cidade->getId(), 'nome' => $cidade->getNome());
            }
        }

        db()->_disconect();

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function cidadeGeocode() {

        $responde = array('status' => false);

        $estado = input()->_toPost('estado', '%s');
        $cidade = input()->_toPost('cidade', '%s');
        $cep = input()->_toPost('cep', '%s');

        $parsWhere = array();


        $cep = preg_replace("/[^0-9]/", '', $cep);
        $parsWhere['cep'] = "%" . substr($cep, 0, 5);
        $parsWhere['cepf'] = substr($cep, 0, 5);
        $parsWhere['estado'] =  $estado;
        $parsWhere['cidade'] =  $cidade;
        
        /* @var $cidadeload \Codando\Modulo\Cidade  */
        $cidadeload = app()->loadModulo('cidade', array(' '
            . ' LOWER(c.nome) LIKE LOWER(:cidade) AND ( '
            . ' c.id_estado IN ( SELECT f.id_estado FROM faixacep f WHERE f.inicio <= :cepf AND f.fim >= :cepf  ) OR '
            . ' c.id_estado IN ( SELECT e.id_estado FROM estado e WHERE e.sigla = :estado ) ) ', $parsWhere));

        if(is_instanceof('Codando\Modulo\Cidade', $cidadeload) === true){
            $responde['cidade'] = $cidadeload->getObjectVars();
            input()->setSession('cidade', $cidadeload->getId());
            input()->setSession('cidade.nome', $cidadeload->getNome() .'/' . $cidadeload->getEstado()->getSigla());
        }

        
        db()->_disconect();
        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
