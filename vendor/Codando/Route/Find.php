<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Find extends Controller\Find {

    private $app;

    public function index() {

        tpl()->display('meus_dados', array('menuCurrent' => 'find'));
    }

    public function load() {
        $parsWhere = array();

        $where = array();

        $email = $_POST['email'];

        if (is()->nul($email) == false) {
            $parsWhere['email'] = $email;

            $where[] = '( LOWER(email) LIKE LOWER(:email) )';
        }

        $cpf = $_POST['cpf'];

        if (is()->nul($cpf) == false) {
            $parsWhere['cpf'] = $cpf;

            $where[] = '( cpf = :cpf )';
        }

        $whereTemp = [implode(' AND ', $where), $parsWhere];

        $contatoList = app()->listModulo('contato', ['( LOWER(email) LIKE LOWER(:email) )', ['email' => $email]]);

        $cadastroList = app()->listModulo('cadastro', $whereTemp);

        $anfitriaoList = app()->listModulo('anfitriao', $whereTemp);

        $menuCurrent = 'find';

        tpl()->display('consulta_dados', compact('menuCurrent', 'contatoList', 'cadastroList', 'anfitriaoList'));
    }

    public function deleteContato() {
        $responde = array('status' => false);

        $result = $this->_deleteContato();

        if ($result) {
            $responde = array('status' => true);
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');

        echo json_encode($responde);
    }

    public function deleteCadastro() {
        $responde = array('status' => false);

        $result = $this->_deleteCadastro();

        if ($result) {
            $responde = array('status' => true);
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');

        echo json_encode($responde);
    }

    public function deleteAnfitriao() {
        $responde = array('status' => false);

        $result = $this->_deleteAnfitriao();

        if ($result) {
            $responde = array('status' => true);
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');

        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
