<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Reserva extends Controller\Reserva {

    private $anfitriaoController;
    private $cadastroController;
    private $app;

    public function payment($id){
        
        $this->_load($id);

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {
            
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
        
        $success = input()->_toGet('success');
        $token = input()->_toGet('token');
        
        if($success == 'true'){
            
            $this->paypalPayment();
            
        }else{
            
            db()->update('reserva', array('paypal' => NULL, 'paypalurl' => NULL, 'datapagamento' => NULL), ' id_reserva = :res ', array('res' => $this->modulo->getId()));
        }
        
        tpl()->display('auth/paypal_return', array('menuCurrent' => 'reserva_view'));
    }
    
    public function getPayment($id){
        
         $this->_load($id);

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
        
        $this->paypalPayment();
        
    }
    
    public function massPay($id){
        
         $this->_load($id);

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
        
        $this->sendMoney();
        
    }

    public function calendario(){

        $responde = array();

        $this->_listCalendario();

        $responde['bloqueado'] = array();

        /* @var $reserva \Codando\Modulo\Reserva */
        foreach ($this->moduloList as $reserva) {

            $datainicio = new \DateTime($reserva->getDatainicio('Y-m-d'));
            $responde['bloqueado'][] = $datainicio->format('d');
            if ($reserva->getDatafim() === NULL) {
                continue;
            }

            $datafim = new \DateTime($reserva->getDatafim('Y-m-d'));
            $loopDia = true;

            while ($loopDia) {
                $datainicio->modify('+1 day');
                $addDia = $datainicio->format('d');
                $responde['bloqueado'][] = (int)$addDia;

                if ($datainicio == $datafim || $addDia >= 31) {
                    $loopDia = false;
                }
            }
        }
        
        $compromissoController = new \Codando\Controller\Compromisso();
        $compromissoController->_listCalendario();
        
        $compromissoList = $compromissoController->getModuloList();
        
        /* @var $compromisso \Codando\Modulo\Compromisso */
        foreach ($compromissoList as $compromisso) {
            $data = new \DateTime($compromisso->getData('Y-m-d'));
            $responde['bloqueado'][] = (int)$data->format('d');
        }
        
        $compromissoList = $compromissoController = NULL;
        
        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
        return;
    }

    public function load($id) {

        $this->_load($id);

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        tpl()->display('reserva_view', array('menuCurrent' => 'reserva_view', 'reserva' => $this->modulo));

        return;
    }

    public function confirmar($id) {

        $this->_loadByAnfitriao($id);

        $responde = array('status' => false);


        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === true) {
            
            $this->_confirmar();
            
            $responde = array('status' => true, 'reserva' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function cancelar($id){
        
        header('Content-type: application/json');
        
        $this->_loadByCadastro($id);

        $responde = array('status' => false);

        $this->_cancelar();

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === true) {
            $responde = array('status' => true, 'reserva' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        
        echo json_encode($responde);
    }
    
    public function cancelarAnf($id){

        $this->_loadByAnfitriao($id);

        $responde = array('status' => false);

        $this->_cancelar();

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === true) {
            $responde = array('status' => true, 'reserva' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }
    
    public function pagar($id) {

        $this->_loadByCadastro($id);

        $responde = array('status' => false);

        $this->_pagar();

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === true) {
            $responde = array('status' => true, 'reserva' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }
    
    public function insert($id) {

        $this->anfitriaoController->_loadAtivo($id);
        $anfitriao = $this->anfitriaoController->getModulo();

        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === false) {
            $this->app->redirect('/erro404?anfitriao='.$id);
            $this->app->stop();
            return false;
        }

        $this->cadastroController->_loadLogado();
        $cadastro = $this->cadastroController->getModulo();

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === false) {
            $this->app->redirect('/identificacao?tipo=1&msg='. urlencode('Você não esta logado.'));
            $this->app->stop();
            return false;
        }

        if ($this->app->request->isGet()) {
            return;
        }

        $responde = array();

        $this->_insert($anfitriao);

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/minhas-reservas', 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function index($id) {

        $this->anfitriaoController->_loadAtivo($id);
        $anfitriao = $this->anfitriaoController->getModulo();

        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === false) {
            $this->app->redirect('/erro404?anfitriao='.$id);
            $this->app->stop();
            return false;
        }

        $this->cadastroController->_loadLogado();
        $cadastro = $this->cadastroController->getModulo();

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === false) {
            $this->app->redirect('/identificacao?tipo=1&msg='. urlencode('Você não esta logado.'));
            $this->app->stop();
            return false;
        }

        $petList = app()->listModulo('pet', array(' id_cadastro = :id AND excluido <> 1 ', array('id' => $cadastro->getId())));
        $anfitriaomodalidade = app()->listModulo('anfitriaomodalidade', array(' id_anfitriao = :anfitriao ', array('anfitriao' => $anfitriao->getId())));

        tpl()->display('auth/adicionar_reserva', array('menuCurrent' => 'adicionar_reserva', 'anfitriaomodalidade' => $anfitriaomodalidade, 'petList' => $petList, 'cadastro' => $cadastro, 'anfitriao' => $anfitriao));
    }

    public function update() {

        $this->_loadLogado();

        if ($this->app->request->isGet()) {

            tpl()->display('alterar_anfitriao', array('anfitriao' => $this->modulo, 'menuCurrent' => 'altera-anfitriao'));
            return;
        }

        $responde = array('status' => false);

        $this->_update();

        if (is_instanceof('Codando\Modulo\Reserva', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/altera-anfitriao', 'anfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function listAll() {

        $this->_list();

        tpl()->display('hospede/reserva_list', array('menuCurrent' => 'reserva_list', 'paginacao' => $this->getPaginacao('/minhas-reservas'), 'reservaList' => $this->moduloList));
    }
    
    public function email(){
        
       
    }
    
    public function listCadastro(){

        $this->_listByCadastro();

        tpl()->display('hospede/reserva_list', array('menuCurrent' => 'reserva_list', 'paginacao' => $this->getPaginacao('/minhas-reservas'), 'reservaList' => $this->moduloList));
    }

    public function __construct(){

        $this->cadastroController = new \Codando\Controller\Cadastro();
        $this->anfitriaoController = new \Codando\Controller\Anfitriao();
        
        $this->app = Slim::getInstance();
    }

    public function __destruct() { }

}