<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Faq extends Controller\Faq {

    private $app;

    public function listAll() {

        $this->_list();

        $this->setRegistrosPorPagina(12);
        meta()->addMeta('title', 'Perguntas Frequentes');
        tpl()->display('faq_list', array('menuCurrent' => 'faq',
            'paginacao' => $this->getPaginacao('/perguntas-frequentes'),
            'faqList' => $this->moduloList)
        );
    }


    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
