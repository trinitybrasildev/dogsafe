<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Dica extends Controller\Dica {

    private $app;

    public function listAll() {

        $this->_list();

        $this->setRegistrosPorPagina(12);
        
        meta()->addMeta('title', 'Dicas');
        
        tpl()->display('dica_list', array('menuCurrent' => 'dica',
            'paginacao' => $this->getPaginacao('/dicas'),
            'dicaList' => $this->moduloList)
        );
    }

    public function load($url, $id) {

        $this->_loadAtivo($id);

        if (is_instanceof('Codando\Modulo\Dica', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        meta()->addMeta('title', 'Dicas')
                ->addMeta('title', $this->modulo->getTitulo())
                ->addMeta('description', $this->modulo->getResumo());


        /* @var $imagemTemp \Codando\Modulo\Arquivo  */
        $imagemTemp = is_instanceof('Codando\Modulo\Arquivo', $this->modulo->getArquivo()) ? $this->modulo->getArquivo() : NULL;

        meta()->setMeta('og:url', $this->modulo->getUrl());

        If ($imagemTemp != NULL) {
            meta()->setMeta('og:image', $imagemTemp->getImage(600, 600));
        }

        tpl()->display('dica_view', array('menuCurrent' => 'dica', 'dica' => $this->modulo));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
