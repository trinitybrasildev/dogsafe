<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Comentarioanfitriao extends Controller\Comentarioanfitriao {
    
    private $anfitriaoController;
    private $cadastroController;
    private $app;

    public function listAll() {

        $this->_list();

        tpl()->display('auth/comentarioanfitriao_list', array('menuCurrent' => 'comentarioanfitriaos',
            'paginacao' => $this->getPaginacao('/meus-comentarios'),
            'petList' => $this->moduloList)
        );
    }

    public function insert($id) {
        
        $responde = array();
        
        $this->anfitriaoController->_loadAtivo($id);
        $anfitriao = $this->anfitriaoController->getModulo();

        if (is_instanceof('Codando\Modulo\Anfitriao', $anfitriao) === false) {
            $responde["msg"] = 'Anfitrião não existe.';
            return false;
        }

        $this->cadastroController->_loadLogado();
        $cadastro = $this->cadastroController->getModulo();

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === false) {
            $responde["msg"] = 'Você não esta logado como hospede.';
            return false;
        }

        $this->_insert($anfitriao);

        if (is_instanceof('Codando\Modulo\Comentarioanfitriao', $this->modulo) === true) {
            $responde = array('status' => true, 'comentarioanfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function update() {

        $this->_loadLogado();

        if ($this->app->request->isGet()) {

            tpl()->display('auth/alterar_comentarioanfitriao', array('comentarioanfitriao' => $this->modulo, 'menuCurrent' => 'altera-comentarioanfitriao'));
            return;
        }

        $responde = array('status' => false);

        $this->_update();

        if (is_instanceof('Codando\Modulo\Comentarioanfitriao', $this->modulo) === true) {
            $responde = array('status' => true, 'redir' => '/comentarios', 'comentarioanfitriao' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {
        $this->cadastroController = new \Codando\Controller\Cadastro();
        $this->anfitriaoController = new \Codando\Controller\Anfitriao();
        
        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
