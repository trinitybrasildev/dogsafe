<?php

 namespace Codando;

 use Symfony\Component\Yaml\Yaml;

 /**
  * Controller App
  * @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br> 
  * @copyright 2014 Luizz
  * @license http://www.luiz.com.br 
  * @version 3.0 
  */
 class App {

     public $erro = false;
     public $msg = array();

     /* @var $moduloList <Array>\Codando\Modulo\Modulo */
     private $moduloList;

     public function getModulo($modulo) {

         if (is_instanceof('Codando\Modulo\Modulo', $modulo) === true)
             return $modulo;
         
         $notarquivo = 0;
         $modulo = preg_replace('/\:notarquivo/', "", $modulo, 1, $notarquivo);
         
         $notforeign = 0;
         $modulo = preg_replace('/\:notforeign/', "", $modulo, 1, $notforeign);
         
         if (array_key_exists($modulo, (array)$this->moduloList) === false) {
             die(" MODULO NOT FOUND '". $modulo ."'");
             return false;
         }
         
         

         $yml_modulo = $this->moduloList[$modulo];

         $newModulo = new Modulo\Modulo;

         $newModulo->setId($yml_modulo['id'])
                 ->setUrl($modulo)
                 ->setPrimarykey($yml_modulo['primary_key'])
                 ->setTabela($yml_modulo['tabela'])
                 ->setView($yml_modulo['view'])
                 ->setForeign(($notforeign === 0 ? $yml_modulo['foreign']:NULL))
                 ->setArquivo( ( array_key_exists('quantidade', (array) $yml_modulo['arquivo']) === true && $notarquivo === 0) ? 1:0)
                 ->setClasseNome('\\Codando\\Modulo\\' . $yml_modulo['class']);

         if ($newModulo->getArquivo())
             $newModulo->setArquivoQuantidade($yml_modulo['arquivo']['quantidade']);

         return $newModulo;
     }

     public function loadModulo($moduloURL, $where = array()) {

         /* @var $modulo \Codando\Modulo\Modulo */
         $modulo = $this->getModulo($moduloURL);

         $loadModulo = db()->_load($modulo, (array) $where);

         if ($loadModulo != NULL) {

             if ($modulo->getArquivo() == 1)
                 $this->loadArquivo($modulo, $loadModulo);

             if (is_array($modulo->getForeign()) === true)
                 $this->loadForeign($modulo, $loadModulo);
         }

         return $loadModulo;
     }

     public function listModulo($moduloURL, $where = array(), $limit = NULL, $order = NULL) {

         $where = is_array($where) ? $where : array($where);

         /* @var $modulo \Codando\Modulo\Modulo */
         $modulo = $this->getModulo($moduloURL);

         $lists = db()->_list($modulo, array(), $where, $limit, $order);

         if (db()->isErro() !== false) {
             return NULL;
         }

         if (count($lists) > 0) {

             if ($modulo->getArquivo() == 1)
                 $this->loadArquivo($modulo, $lists);

             if (is_array($modulo->getForeign()) === true)
                 $this->loadForeign($modulo, $lists);
         }

         return (array) $lists;
     }

     public function countModulo($moduloURL, $where = array()) {

         $modulo = $this->getModulo($moduloURL);

         $count = db()->count($modulo->getTabela(true), (array) $where);

         return $count;
     }

     private function loadArquivo($modulo, $objeto) {

         if ($modulo->getArquivo() == 1) {
             if (is_array($objeto) === true) {

                 foreach ($objeto as $obj) {
                     $obj = $this->loadArquivo($modulo, $obj);
                 }
             } else {

                 $objeto = $this->setArquivos($modulo, $objeto);
             }
         }
     }

     private function setArquivos($modulo, $objet) {

         $listArquivoObject = array();

         $arquivoModulo = $this->getModulo('arquivo');

         $arquivoList = db()->_list($arquivoModulo, array(), array('id_est = :id_est AND id_modulo = :id_modulo', array('id_est' => $objet->getId(), 'id_modulo' => $modulo->getId())), $modulo->getArquivoQuantidade(), 'ordem DESC');

         unset($arquivoModulo);

         if (count($arquivoList) > 0) {

             $arquivoListTemp = array();

             $countOrdem = 0;

             /* @var $arquivo Arquivo */
             foreach ($arquivoList as $arquivo) {

                 $arquivo->setBase($modulo->getUrl());

                 $arquivoListTemp[] = $arquivo;

                 $countOrdem++;
             }

             if ($modulo->getArquivoQuantidade() > 1 && $arquivoListTemp !== NULL) {

                 $objet->setArquivoList($arquivoListTemp);
             } else if (count($arquivoListTemp) == 1 && is_instanceof('Codando\Modulo\Arquivo', current($arquivoListTemp)) === true) {

                 $objet->setArquivo(current($arquivoListTemp));
             }

             $listArquivoObject = $modulo->getArquivoQuantidade() > 1 ? $arquivoListTemp : (count($arquivoListTemp) == 1 ? current($arquivoListTemp) : new \Codando\Modulo\Arquivo());
         }

         return $objet;
     }

     private function loadForeign($modulo, $objects) {

         if (is_array($objects) === true) {

             foreach ($objects as $obj) {
                 $this->setForeign($modulo, $obj);
             }
             return;
         }

         $this->setForeign($modulo, $objects);
     }

     private function setForeign($modulo, $object) {

         foreach ($modulo->getForeign() as $foreign) {

             if (isset($this->moduloList[$foreign])) {

                 $modelYml = $this->moduloList[$foreign];

                 $get = "get" . $modelYml['class'];
                 $set = "set" . $modelYml['class'];

                 if (method_exists($object, $get) === true) {

                     $loadFore = $this->loadModulo($foreign, array(
                         $modelYml['primary_key'] . ' = :' . $modelYml['primary_key'],
                         array($modelYml['primary_key'] => $object->{$get}())
                             )
                     );

                     if (is_instanceof("Codando\Modulo\\" . $modelYml['class'], $loadFore)) {
                         $object->{$set}($loadFore);
                     }
                 } else if (method_exists($object, $set . 'List') === true) {

                     $listFore = $this->listModulo($foreign, array(
                         $modulo->getPrimarykey() . ' = :' . $modulo->getPrimarykey(), array($modulo->getPrimarykey() => $object->getId())
                             )
                     );

                     if (count($listFore) > 0) {
                         $object->{$set . 'List'}($listFore);
                     }
                 }
             }
         }
     }

     public function __construct() {
         $this->moduloList = \Codando\App::getModels();
     }

     public function __destruct() {
         
     }

    public static function __callStatic($method, $args) {
        
        $url = strtolower(str_replace(array('list','load','count'), '', $method));
        $w = isset($args[0])?$args[0]:NULL;
        
        if(strpos($method, 'list') !== false){
            $l = isset($args[1])?$args[1]:NULL;
            $o = isset($args[2])?$args[2]:NULL;
            return self::getInstance()->listModulo($url, $w, $l, $o);
        }
        
        if(strpos($method, 'load') !== false){
            $w = isset($args[0])?$args[0]:NULL;
            return self::getInstance()->loadModulo($url, $w);
        }
        
        if(strpos($method, 'count') !== false){
            $w = isset($args[0])?$args[0]:NULL;
            return self::getInstance()->countModulo($url, $w);
        }

        echo "unknown method " . $method;
        return false;
    }

     /**
      * @return Array
      */
     public static function getModels($name = NULL) {
         static $models = array();

         if ($name !== NULL && array_key_exists($name, $models) === true) {
             return $models[$name];
         }

         return $models ? : $models = Yaml::parse(file_get_contents((COD_DIR_APP . '/_config/modulo.yml')));
     }

     /**
      * @return Array
      */
     public static function getCache($name = NULL) {

         static $caches = array();

         if ($name !== NULL && array_key_exists($name, $caches) === true) {
             return $caches[$name];
         }

         return $caches[$name] = new \Codando\System\Cache(60, COD_DIR_APP . '/_arquivos/appcache/' . $name . '/', 'contr');
     }

     /**
      * @return Array
      */
     public static function getConfig($name = NULL) {
         static $config = array();

         if ($name !== NULL && array_key_exists($name, $config) === true) {
             return $config[$name];
         }

         return $config ? : $config = include(COD_DIR_APP . '/configuracao.php');
     }

     /**
      * @return Array
      */
    public static function getYml($file, $name = NULL) {

        static $yaml = array();
        
        if (array_key_exists($file, $yaml) === false) {
            $yaml[$file] = Yaml::parse(file_get_contents(COD_DIR_APP . '/_config/' . $file . '.yml'));
        }

        if ($name !== NULL && array_key_exists($name, $yaml[$file]) === true) {

            return $yaml[$file][$name];
        }

        return $yaml[$file];
    }

     /**
      * @return Codando
      */
     public static function getInstance() {
         static $instance = null;

         return $instance ? : $instance = new static;
     }

     private function __clone() {
         trigger_error('Clone is not allowed.', E_USER_ERROR);
     }

     final private function __wakeup() {
         trigger_error('Unserializing is not allowed.', E_USER_ERROR);
     }

 }
 